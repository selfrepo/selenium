package pages;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import junit.framework.Assert;
import utilities.CommonUtil;
import utilities.ConfigReader;
import utilities.LogUtil;

public class CreateAccountTrialFormPage extends CommonUtil {
	
	String optionStart = "//div[contains(@id,'react-select-2-option')][text()='";
	String optionEnd = "']";
	WebElement option;
	
	String stateOptionStart = "//div[contains(@id,'react-select')][text()='";
	String stateOptionEnd = "']";
	WebElement stateOption;
	
	@FindBy(xpath = "(//button[@aria-label='Close Drift Widget messenger preview overlay']")
	private WebElement helpChat ;

	@FindBy(xpath = "(//input[@class='uk-input'][@id='fname'])[1]")
	private WebElement firstNameInput ;
	
	@FindBy(xpath = "(//input[@id='lname'])[1]")
	private WebElement lastNameInput ;
	
	@FindBy(xpath = "(//input[@id='countryField'])[1]")
	private WebElement countryInput ;
	
	@FindBy(xpath = "(//input[@name='phone'])[1]")
	private WebElement phone ;
	
	@FindBy(xpath = "(//input[@name='companyName'])[1]")
	private WebElement companyName ;
	
	@FindBy(xpath = "(//input[@name='role'])[1]")
	private WebElement jobTitle ;
	
	@FindBy(xpath = "(//input[@id='zip'])[1]")
	private WebElement zip ;
	
	@FindBy(xpath = "(//input[@id='stateField'])[1]")
	private WebElement stateInput;
	
	@FindBy(xpath = "(//input[@id='email'])[1]")
	private WebElement email ;
	
	@FindBy(xpath = "(//input[@id='password'])[1]")
	private WebElement password ;
	
	@FindBy(xpath = "(//button[@id='primary-form-submit'])[1]")
	private WebElement startFreeTrail ;
	
	@FindBy(xpath = "//div[contains(text(),'We’ve emailed')]")
	private WebElement verificationMessage ;
	
	@FindBy(xpath = "//iframe[@class='drift-frame-controller']")
	private WebElement chatbotIFrame;
	
	@FindBy(xpath = "//*[@id='root']/main//div/button[contains(@class,'drift-widget-close-button')]")
	private WebElement chatBotCloseIcon;
	
	public void navigateToTrialForm() {
		CommonUtil.navigate(ConfigReader.getValue("trialFormUrl"));
	}
	
	public  void createAccountTrialForm (HashMap<String, String> dataMap) {
		int randNum = CommonUtil.generateRandomNumber(100, 200);
		String trialEmail = dataMap.get("FreetrialEmail");
		String [] arrEmail = trialEmail.split("\\@");
		String first = arrEmail[0] + "+" +randNum;
		String second = "@" + arrEmail[1];
		String newEmail = first.concat(second);
		CommonUtil.storedata("AccounttrailEmailid", newEmail);
		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(firstNameInput, 5, "FirstName");
		CommonUtil.jsinputText(firstNameInput, dataMap.get("FirstName"), "Entered First Name");
		CommonUtil.normalWait(2);
		CommonUtil.jsinputText(lastNameInput, dataMap.get("LastName")+randNum, "Entered LastName");
		CommonUtil.normalWait(2);
		CommonUtil.inputTextWithoutEnterKey(countryInput,dataMap.get("Country"),"Country : "+dataMap.get("Country"));
		option = CommonUtil.findElement(optionStart+dataMap.get("Country")+optionEnd, "xpath", "Checking option : "+dataMap.get("Country"));
		CommonUtil.clickUsingJavaScriptExecutor(option, "Selecting Country : "+dataMap.get("Country"));
		CommonUtil.jsinputText(phone, dataMap.get("Phone"), "Entered Phone Number");
		CommonUtil.inputTextWithoutEnterKey(stateInput, dataMap.get("State"), "Entered state : "+dataMap.get("State"));
		stateOption = CommonUtil.findElement(stateOptionStart+dataMap.get("State")+stateOptionEnd, "xpath", "Checking option : "+dataMap.get("State"));
		CommonUtil.clickUsingJavaScriptExecutor(stateOption, "Selecting state : "+dataMap.get("State"));
		try {
			CommonUtil.switchToFrame(chatbotIFrame, "chatbot");
			if(chatBotCloseIcon.isDisplayed())
				CommonUtil.clickUsingJavaScriptExecutor(chatBotCloseIcon, "close chat bot popup");
			
		} catch(Exception e) {
			LogUtil.infoLog(getClass(), "chatbot close event failed with exception: "+e);
			
		}
		CommonUtil.switchToDefaultFrame();
		CommonUtil.normalWait(2);
		CommonUtil.jsinputText(companyName, dataMap.get("CompanyName"), "Entered Company Name");
		CommonUtil.scrollIntoView(jobTitle, "Scrolling to text area");
		CommonUtil.normalWait(2);
		CommonUtil.jsinputText(jobTitle, dataMap.get("JobTitle"), "Enetered Job Title");		
		CommonUtil.normalWait(2);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.jsinputText(email, newEmail, "Enetered Email");
		CommonUtil.jsinputText(password, dataMap.get("FreetrialPassword"), "Enetered Password");
		CommonUtil.normalWait(2);
		CommonUtil.click(startFreeTrail, "Clicked Free Trail");
		CommonUtil.normalWait(10);
		CommonUtil.verifyText(verificationMessage, dataMap.get("VerificationMessage"), "Message Verification");
		Assert.assertTrue(CommonUtil.isElementDisplayed(verificationMessage, "Verification Message Confirmation"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

}