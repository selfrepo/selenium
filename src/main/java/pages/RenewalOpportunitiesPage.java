package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class RenewalOpportunitiesPage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================
	
	
	// ================================================================================
	// Objects
	// ================================================================================
			
	@FindBy(xpath = "//span[text()='Opportunity Record Type']//following::span[2]")
	private WebElement eleOpportunityRecordType;
	
	@FindBy(xpath = "//span[text()='Renewal Type']//following::lightning-formatted-text[1]")
	private WebElement eleRenewalType;
	
	@FindBy(xpath = "(//lightning-formatted-text[@data-output-element-id='output-field'])[1]")
	public static WebElement eleRenwlOppName;
	
	@FindBy(xpath = "(//div[@class='highlights slds-clearfix slds-page-header slds-page-header_record-home fixed-position']//div[@role='list']//slot[@name='secondaryFields']//records-highlights-details-item[@role='listitem']//div//p[@class='fieldComponent slds-text-body--regular slds-show_inline-block slds-truncate']//slot//lightning-formatted-text)[1]")
	private static WebElement closeDateTxtField;
	
	// ================================================================================
	// Methods
	// ================================================================================

	public void validationRenewalOpportunties(HashMap<String, String> dataMap) throws InterruptedException {
		CommonUtil.refresh("Pre Order Page");
		CommonUtil.normalWait(10);
			
		// PreOrder Status in PreOrder Page
		String ValOppRecordType = CommonUtil.getText(eleOpportunityRecordType,"Opportunity Record Type");
		Assert.assertTrue(ValOppRecordType.equals(dataMap.get("OpportunityRecordType")),"Opportunity Record Type in Renewal Opportunity page displayed as");
		
		// PreOrder Status in PreOrder Page
		String ValRenewalType = CommonUtil.getText(eleRenewalType,"Renewal Type");
		Assert.assertTrue(ValRenewalType.equals(dataMap.get("RenewalType")),"Renewal Type displayed as");
		
		String ValRenwlOppName = CommonUtil.getText(eleRenwlOppName,"Renewal Opportunity Name");
		LogUtil.infoLog(getClass(),"Renewal Opportunity Name is : " +ValRenwlOppName);
		
		String closeDateTxt = CommonUtil.getText(closeDateTxtField,"Close date text");
		
		String[] closingDateSplitter = closeDateTxt.split("/");
		String closingDateMonth = closingDateSplitter[1];
		String closingDateYear = closingDateSplitter[2];
		LogUtil.infoLog(getClass(),"Year is : "+closingDateYear);
		String convertedMonthTxt = CommonUtil.convertNumericMonthStringToWordMonth(closingDateMonth);
		 LogUtil.infoLog(getClass(),"Month is : "+convertedMonthTxt);
		if (ValRenwlOppName.contains("SC-") && ValRenwlOppName.contains("Renewals") && closingDateMonth.length()!=0 && closingDateYear.length()!=0)
		{
			LogUtil.infoLog(getClass(),"Text is Pass");
		}
		else
		{
			LogUtil.errorLog(getClass(),"Text is Fail",null);
		}
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}


			
			
	

		
