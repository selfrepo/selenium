package pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtil;

public class CaseAccountsPage extends CommonUtil {


	// ================================================================================
	// Variables
	// ================================================================================

	private String matchedAccountDataStart = "//mark[text()=' accounts']//parent::span[text()='{replacevalue}']//parent::*";

	// ================================================================================
	// Objects
	// ================================================================================

	@FindBy(xpath = "//flexipage-component2[@data-component-id='force_relatedListQuickLinksContainer']//a[contains(text(),'Show All')]")
	private WebElement showalllink;

	@FindBy(xpath = "//*[contains(text(),'Cases')]//parent::slot//parent::a")
	private WebElement caseslink;

	@FindBy(xpath = "//span[text()='MDM Task Id']//parent::div//parent::div/div[2]/span/slot/lightning-formatted-text")
	private WebElement MDMTaskID;

	@FindBy(xpath = "//span[text()='Master Data ID']//parent::div//parent::div/div[2]/span/slot/lightning-formatted-text")
	private WebElement MDMDataID;

	@FindBy(xpath = "(//button[@class='slds-button slds-button slds-button_icon slds-button_icon slds-button_icon-container slds-button_icon-small slds-global-actions__notifications slds-global-actions__item-action forceHeaderButton']//following::*//*[local-name()='svg' and @data-key='notification']//*[local-name()='g'])[2]//*[local-name()='path']")
	private WebElement notifications;

	@FindBy(css = "button[title='Close']")
	private WebElement AccountNameAliasClose;

	@FindBy(xpath = "(//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//a[@data-refid='recordId'])[1]")
	private WebElement casenumberlink;

	@FindBy(xpath = "(//*[local-name()='svg' and @class='slds-button__icon slds-button__icon_hint'])[1]")
	private WebElement changeownericon;

	@FindBy(xpath = "(//input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup'])[2]")
	private WebElement changeownerinputtext;

	@FindBy(xpath = "//button[@name='change owner']//parent::lightning-button")
	private WebElement changeownerbutton;

	@FindBy(xpath = "//button[@name='cancel']//parent::lightning-button")
	private WebElement changeownerCancelbutton;

	@FindBy(xpath = "//div[@class='notification-content']//following::span[@class='notification-text-title uiOutputText']")
	private WebElement notificationtext;

	@FindBy(xpath = "//span[@title='Assigned To']//parent::div//span[@class='uiOutputText']")
	private WebElement assigntotext;

	@FindBy(xpath = "(//runtime_platform_actions-action-renderer[@apiname='Edit']//parent::li[@data-target-selection-name='sfdc:StandardButton.Case.Edit']//button[@class='slds-button slds-button_neutral' and @name='Edit']//parent::*)[1]")
	private WebElement caseeditbutton;

	@FindBy(xpath = "//span[text()='Status']//parent::span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//parent::div //div[@class='uiMenu']")
	private WebElement casestatusdropdown;

	@FindBy(xpath = "//div[@class='inlineFooter']//span[text()='Save']//parent::button")
	private WebElement casestatussavebutton;

	@FindBy(xpath = "//span[text()='Closure Summary']//parent::label//parent::div//following::textarea")
	private WebElement casestatusclosuresummary;

	@FindBy(xpath = "//span[text()='Account Verification Status']//parent::div//parent::div/div[2]/span/slot/lightning-formatted-text")
	private WebElement AccountStatus;

	@FindBy(xpath = "//div[@class='slds-icon-waffle']")
	private WebElement sideMenuNavigationBar;

	@FindBy(xpath = "//a[@id='07p1t000000cZWHAA2']//div[@class='slds-size_small']")
	private WebElement salesLink;

	@FindBy(xpath = "//span[contains(text(),'My Approvals')]")
	private WebElement myApprovalsSpan;

	@FindBy(xpath = "//iframe[@title='dashboard']")
	private WebElement iframeSales;

	@FindBy(xpath = "//span[normalize-space()='Dashboard']")
	private WebElement dashboardText;

	@FindBy(xpath = "//button[@aria-label='Search']")
	private WebElement searchButton;

	@FindBy(xpath = "//div[@class='forceSearchAssistantDialog']/div/div[@data-aura-class='forceSearchInputEntitySelector']//following-sibling::lightning-input/div/input")
	private WebElement searchBarInput;

	@FindBy(css = "div[title='Status']")
	private WebElement statusDiv;

	@FindBy(xpath = "//span[contains(text(),'Show more results for')]")
	private WebElement showmoreresult;

	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//a[@data-refid='recordId']")
	private WebElement matchedDatalink;

	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//a[text()='Details']")
	private WebElement LeadDetail;

	@FindBy(xpath = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr[1]/td[6]/span[@class='slds-grid slds-grid--align-spread']/a[contains(@href,'mailto:')]")
	private WebElement matchedLeadData;
	
	@FindBy(xpath = "//div[@class='slds-form-element__control']//lightning-formatted-text[text()='Closed']")
	private WebElement caseaccountstatus;

	// ================================================================================
	// Methods
	// ================================================================================

	public void navigatetoaccount(HashMap<String, String> dataMap) {

		CommonUtil.waitForElementVisibilityWithDuration(sideMenuNavigationBar, 5, "Navigation bar");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(searchButton, 5, "global search bar");
		CommonUtil.click(searchButton, "global search bar");
		CommonUtil.waitForVisible(searchBarInput, "global search box");
		CommonUtil.inputText(searchBarInput, dataMap.get("AccountName"), "Entered Account Name");
		WebElement matchedAccountObj = CommonUtil.getWebElement("xpath", matchedAccountDataStart,
				dataMap.get("AccountName"));
		CommonUtil.waitForVisible(matchedAccountObj, "Searched Account " + dataMap.get("AccountName"));
		CommonUtil.click(matchedAccountObj, "matched data");
		WebElement matchaccountlink = CommonUtil.getWebElement("xpath",
				"//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//a[text()='{replacevalue}']",
				dataMap.get("AccountName"));
		CommonUtil.click(matchaccountlink, "matched account Link");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	//Verifying account details sync with MDM data
	public void accountverification(HashMap<String, String> dataMap) {
		navigatetoaccount(dataMap);
		CommonUtil.scrollViewUpto(MDMDataID, "MDM Data ID");
		CommonUtil.elementisDisplayed(MDMDataID, "MDM Data ID");
		CommonUtil.elementisDisplayed(MDMTaskID, "MDM Task ID");
		CommonUtil.scrollViewUpto(AccountStatus, "Account Status Verification");
		CommonUtil.verifyText(AccountStatus, dataMap.get("AccountStatus"), "Account Status Verification");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	//Modifying the case status to closed state with closure comments
	public void modifycasestatus(HashMap<String, String> dataMap) {
		navigatetoaccount(dataMap);
		CommonUtil.scrollViewUpto(showalllink, "Click on Show All Link");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(showalllink, "Click on Show All Link");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollViewUpto(caseslink, "Click on Cases Link");
		CommonUtil.isElementDisplayed(caseslink, "Click on Cases Link");
		CommonUtil.click(caseslink, "Click on Cases Link");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(casenumberlink, "Click on Case number");
		CommonUtil.normalWait(3);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(caseeditbutton, "Click on Case Edit button");
		CommonUtil.normalWait(3);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(casestatusdropdown, "Click on Case Status Dropdown");
		CommonUtil.normalWait(3);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		WebElement Statusopt = CommonUtil.getWebElement("xpath",
				" //li[@class='uiMenuItem uiRadioMenuItem']//a[text()='{replacevalue}']", dataMap.get("CaseStatus"));
		CommonUtil.click(Statusopt, "Click on Case Status");
		CommonUtil.normalWait(3);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.inputText(casestatusclosuresummary, dataMap.get("CaseClosureSummary"), "Enter the Closure Summary");
		CommonUtil.normalWait(3);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(casestatussavebutton, "Click on Case Save button");
        CommonUtil.normalWait(3);
        CommonUtil.attachScreenshotOfPassedTestsInReport();
        CommonUtil.scrollViewUpto(caseaccountstatus, "Case Account Status Verification");
        CommonUtil.verifyText(caseaccountstatus, dataMap.get("CaseStatus"),"Case Account Status Verification");
        CommonUtil.normalWait(3);
        CommonUtil.attachScreenshotOfPassedTestsInReport();


	}

}
