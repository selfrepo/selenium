package pages;

import java.util.HashMap;
import java.util.List;

import utilities.CommonUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class EditQuotePage extends CommonUtil {


	// ================================================================================
	// Variables
	// ================================================================================

	Boolean result;
	private WebElement amsOption;
	
	private String amsOptionStart = "(//span[text()='";
	
	private String amsOptionEnd = "'])[1]";
	
	private WebElement cifOption;
	
	private String cifOptionStart = "//*[@class='actionBody']//label[text()='Incoterm']//following-sibling::div//button/parent::div//following-sibling::div[contains(@id,'dropdown-element')]/lightning-base-combobox-item//span[text()='";
	
	private String cifOptionEnd = "']";

	//Bill to Contact suggestion list
	private WebElement billToContactSuggestionList;
	private String billToContactSuggestionListStart = "(//*[@class='actionBody']//label[text()='Bill To Contact']//following-sibling::div//input/parent::div/following-sibling::div/lightning-base-combobox-item/following-sibling::ul//*[@title='";
	private String billToContactSuggestionListEnd = "'])";

	//Ship to Contact suggestion list
	private WebElement shipToContactSuggestionList;
	private String shipToContactSuggestionListStart = "(//*[@class='actionBody']//label[text()='Ship To Contact']//following-sibling::div//input/parent::div/following-sibling::div/lightning-base-combobox-item/following-sibling::ul//*[@title='";
	private String shipToContactSuggestionListEnd = "'])";

	//End Customer Contact suggestion list
	private WebElement endCustomerContactSuggestionList;
	private String endCustomerContactSuggestionListStart = "(//*[@class='actionBody']//label[text()='End Customer Contact']//following-sibling::div//input/parent::div/following-sibling::div/lightning-base-combobox-item/following-sibling::ul//*[@title='";
	private String endCustomerContactSuggestionListEnd = "'])";

	// ================================================================================
		// Objects
		// ================================================================================
	@FindBy(xpath = "//li[@data-target-selection-name='sfdc:StandardButton.SBQQ__Quote__c.Edit']//button[@name='Edit'][normalize-space()='Edit']")
	private WebElement editButton;
		
	@FindBy(xpath = "//label[contains(text(),'Term End Date')]")
	private WebElement termEndDate;
		
	@FindBy(xpath = "//*[@class='actionBody']//span[text()='Quote Total']")
	private WebElement quoteTotal;
		
	@FindBy(xpath = "//lightning-combobox[label[contains(text(),'Incoterm')]]//lightning-base-combobox[@class='slds-combobox_container']/div/div/button")
	private WebElement incoTermLabel;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Incoterm']//following-sibling::div//button/parent::div//following-sibling::div[contains(@id,'dropdown-element')]/lightning-base-combobox-item//span[text()='CIF']")
	private WebElement incoTermCIFSpan;
		
	@FindBy(xpath = "//button[contains(@id,'combobox')][contains(@aria-label,'Incoterm')]]")
	private WebElement incoTermDropDown;
		
	@FindBy(xpath = "//button[contains(@aria-label,'CT Provisioning Region')]")
	private WebElement ctProvisioningRegionDropDown;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[span[text()='Show SA Product']]//following-sibling::div//input[@type='checkbox']")
	private WebElement showSAProductCheckbox;
		
	@FindBy(xpath = "//label[contains(text(),'Customer Specific Terms')]//following-sibling::div/textarea[@class='slds-textarea']")
	private WebElement customerSpecificTermTextArea;
		
	@FindBy(xpath = "//button[@name='SaveEdit']")
	private WebElement editSaveButton;
		
	@FindBy(xpath = "//button[contains(@aria-label,'CT Provisioning Region')]")
	private WebElement CTProvisioningRegion;
		
	@FindBy(xpath = "(//span[text()='AMS'])[1]")
	private WebElement AMS;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[span[text()='Primary']]//following-sibling::div/span/input[@type='checkbox'][@name=\"SBQQ__Primary__c\"]")
	private WebElement primaryCheckbox;

	@FindBy(xpath = "//a[text()='Name']/../../../../tbody/tr[1]/td[1]/a")
	private WebElement BillToContactDrp;
		
	@FindBy(xpath = "//sb-page-container[@id='sbPageContainer'][@class='--desktop']")
	private WebElement sdPageContainer;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Incoterm']//following-sibling::div//button")
	private WebElement incoTermButton;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Bill To Contact']//following-sibling::div//input")
	private WebElement billToContact;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Bill To']//following-sibling::div//input")
	private WebElement billToInput;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Bill To Contact']//following-sibling::div//input/parent::div/following-sibling::div/lightning-base-combobox-item/following-sibling::ul//*[@title='AN_Test Tx']")
	private WebElement billToContactTitle;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Ship To Contact']//following-sibling::div//input")
	private WebElement shipToContactInput;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Ship To']//following-sibling::div//input")
	private WebElement shipToInput;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Ship To Contact']//following-sibling::div//input/parent::div/following-sibling::div/lightning-base-combobox-item/following-sibling::ul//*[@title='AN_Test Tx']")
	private WebElement shipToContactTitle;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='End Customer Contact']//following-sibling::div//input")
	private WebElement endCustomerContactInput;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='End Customer']//following-sibling::div//input")
	private WebElement endCustomerInput;
		
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='End Customer Contact']//following-sibling::div//input/parent::div/following-sibling::div/lightning-base-combobox-item/following-sibling::ul//*[@title='AN_Test Tx']")
	private WebElement endCustomerTitle;
	
	@FindBy(xpath = "//button[text() ='Advanced Configurator']")
	private WebElement advancedConfigurationBtn;

	@FindBy(xpath = "//div[@title ='Not Configured']")
	private WebElement configurationStatus;

	//Not Configured
	@FindBy(xpath = "//tbody/tr/th/div/a")
	private WebElement salesProductForConfiguration;

	@FindBy(xpath = "//label[text() = 'Version/Platform']/following:: input")
	private WebElement versionPlatformInputBox;

	@FindBy(xpath = "//div[@id='listbox-id-1']/ul/li[1]/following::span[2]")
	private WebElement versionOption;

	@FindBy(xpath = "//div[@class='slds-modal__footer']/child::button[text()='Save']")
	private WebElement saveBtnForAdvancedConfigurationWin;

	@FindBy(xpath = "//button[@title='Close this window']")
	private WebElement closeBtnForAdvancedConfigurationWin;

	@FindBy(xpath = "//slot/lightning-button/button[text()='Submit for Approval']")
	private WebElement submitForApprovalBtn;
	
	@FindBy(xpath = "//span[text()='Status']/parent::div/following-sibling::div//slot//lightning-formatted-text[text()='Pending Approval']")
	private WebElement pendingApprovalQuoteStatus;
	
	@FindBy(xpath = "//span[text()='Quote Sub Status']/parent::div/following-sibling::div//slot//lightning-formatted-text[text()='Pending Approval']")
	private WebElement pendingApprovalQuoteSubStatus;
	
	@FindBy(xpath = "//span[text()='Status']/parent::div/following-sibling::div//button[@title='Edit Status']")
	private WebElement editStatusPencilIcon;
	
	@FindBy(xpath = "//label[text()='Status']//following-sibling::div//button[@role='combobox']/span")
	private WebElement statusButton;
	
	@FindBy(xpath = "//lightning-base-combobox-item[@data-value='Customer Accepted']")
	WebElement customerAcceptedStatus;
	
	@FindBy(xpath = "//span[text()='Status']/parent::div/following-sibling::div//slot//lightning-formatted-text")
	private WebElement quoteStatus;
	
	@FindBy(xpath = "//span[text()='Quote Sub Status']/parent::div/following-sibling::div//slot//lightning-formatted-text")
	private WebElement quoteSubStatus;

	@FindBy(xpath = "//a[contains(text(),'Show All')]")
	private WebElement showAllBtnForRelatedTab;
	
	@FindBy(xpath ="//a[@id='activityTab__item']")
	private WebElement activitiesSection;
	
	@FindBy(xpath = "//div//a[contains(text(),'Show All')]")
	private WebElement showAllRelatedQuickLinks;


	@FindBy(xpath = "//slot/span[contains(text(),'Approvals')]")
	private WebElement approvalsLinkInQuickLinkTab;

	@FindBy(xpath = "//table[@role='grid']/tbody/tr")
	private List<WebElement> rowsInApprovalTable;
	
	@FindBy(xpath = "//table[@role='grid']/tbody/tr/td[2]//span[text() !='Approved']")
	private List<WebElement> rowsNotApproved;
	
	@FindBy(xpath = "(//ul[@class='slds-button-group-list']//button[text()='Approve'])")
	private WebElement approveBtnAtApprovalPage;
	
	@FindBy(xpath = "(//ul[@class='slds-button-group-list']//button[text()='Approve'])[2]")
	private WebElement approveBtn1AtApprovalPage;
	
	@FindBy(xpath = "//div[@class='iframe-parent slds-template_iframe slds-card']/iframe[@title='accessibility title']")
	private WebElement approvalPopupIframe;
	

	@FindBy(xpath = "(//input[@type='submit'])[2]")
	private WebElement approveBtnAtApprovalPopUp;

	@FindBy(xpath = "//lst-breadcrumbs/nav/ol/li[2]/a")
	private WebElement quoteNumberBreadcrumb;


		

	// ================================================================================
	// Methods
	// ================================================================================
	public void editQuote(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(editButton, 10, "edit button");
		CommonUtil.click(editButton, "edit button");
		CommonUtil.normalWait(5);
		result = CommonUtil.isElementSelected(primaryCheckbox, "Checking primary checkbox");
		if (result == false) {
			CommonUtil.click(primaryCheckbox, "primary checkbox");
		}
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(incoTermButton, 7, "incoterm drop down");
		CommonUtil.clickUsingJavaScriptExecutor(incoTermButton, "incoterm drop down");
		cifOption = CommonUtil.findElement(cifOptionStart + dataMap.get("CIF") + cifOptionEnd, "xpath", "CIF option");
		CommonUtil.waitForElementVisibilityWithDuration(cifOption, 2, "CIF option");
		CommonUtil.clickUsingJavaScriptExecutor(cifOption, "CIF option");
		CommonUtil.clickUsingJavaScriptExecutor(CTProvisioningRegion, "CTProvisioningRegion");
		amsOption = CommonUtil.findElement(amsOptionStart + dataMap.get("AMS") + amsOptionEnd, "xpath", "AMS option");
		CommonUtil.clickUsingJavaScriptExecutor(amsOption, "AMS");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		result = CommonUtil.isElementSelected(showSAProductCheckbox, "Checking Show SA checkbox");
		if (result == false) {
			CommonUtil.click(showSAProductCheckbox, "Show SA checkbox");
			result = true;
		}
		Assert.assertTrue(result);
				CommonUtil.scrollIntoView(customerSpecificTermTextArea, "Scrolling to text area");
				CommonUtil.waitForElementVisibilityWithDuration(billToContact, 2, "bill to contact");
				CommonUtil.scrollIntoView(billToContact, "Scrolling to element");
				CommonUtil.waitForElementVisibilityWithDuration(billToContact,2, "bill to contact");
				CommonUtil.inputText(billToContact, dataMap.get("ContactName"), "Entered bill to contact");
				CommonUtil.click(billToContact, "Selecting suggestion bill to contact");
		        CommonUtil.waitForElementVisibilityWithDuration(shipToContactInput,2, "ship to contact");
				CommonUtil.scrollIntoView(shipToContactInput, "Scrolling to element");
				CommonUtil.inputText(shipToContactInput, dataMap.get("ContactName"), "Entered bill to contact");
				CommonUtil.click(shipToContactInput, "Selecting suggestion bill to contact");
				CommonUtil.waitForElementVisibilityWithDuration(endCustomerInput,2, "endCustomerInput");
				CommonUtil.scrollIntoView(endCustomerInput, "Scrolling to element");
				CommonUtil.inputText(endCustomerInput, dataMap.get("ContactName"), "Entered bill to contact");
				CommonUtil.click(endCustomerInput, "Selecting suggestion bill to contact");
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				CommonUtil.waitForElementVisibilityWithDuration(editSaveButton, 5, "save");
				CommonUtil.click(editSaveButton, "Clicking on save button of edit screen");
				CommonUtil.normalWait(10);
	}	

	public void enterContactDetails(HashMap<String, String> dataMap) {

		//Click on the Edit Button -> to edit the Quote
		CommonUtil.waitForElementVisibilityWithDuration(editButton, 10, "edit");
		CommonUtil.click(editButton,"edit button");
		CommonUtil.normalWait(5);

		//Enter Bill to contact
		CommonUtil.scrollIntoView(customerSpecificTermTextArea, "Scrolling to text area");
		CommonUtil.waitForElementVisibilityWithDuration(billToContact, 2, "bill to contact");
		CommonUtil.scrollIntoView(billToContact, "Scrolling to element");
		CommonUtil.waitForElementVisibilityWithDuration(billToContact, 10, "bill to contact");
		CommonUtil.inputText(billToContact, dataMap.get("ContactName"), "Entered bill to contact");
		billToContactSuggestionList = CommonUtil.findElement(billToContactSuggestionListStart+dataMap.get("ContactName")+billToContactSuggestionListEnd, "xpath","bill to contact suggestions");
		CommonUtil.click(billToContactSuggestionList, "Selecting suggestion bill to contact");

		//enter Ship to contact
		CommonUtil.waitForElementVisibilityWithDuration(shipToContactInput,10,"ship to contact");
		CommonUtil.scrollIntoView(shipToContactInput, "Scrolling to element");
		CommonUtil.inputText(shipToContactInput, dataMap.get("ContactName"), "Entered bill to contact");
		CommonUtil.normalWait(5);
		shipToContactSuggestionList = CommonUtil.findElement(shipToContactSuggestionListStart+dataMap.get("ContactName")+shipToContactSuggestionListEnd, "xpath","ship to contact suggestions");
		CommonUtil.click(shipToContactSuggestionList, "Selecting suggestion bill to contact");

		//Enter End customer Contact
		CommonUtil.waitForElementVisibilityWithDuration(endCustomerContactInput,10,"end customer");
		CommonUtil.scrollIntoView(endCustomerInput, "Scrolling to element");
		CommonUtil.inputText(endCustomerContactInput, dataMap.get("ContactName"), "Entered bill to contact");
		CommonUtil.normalWait(5);
		endCustomerContactSuggestionList = CommonUtil.findElement(endCustomerContactSuggestionListStart+dataMap.get("ContactName")+endCustomerContactSuggestionListEnd, "xpath","");
		CommonUtil.click(endCustomerContactSuggestionList, "Selecting suggestion bill to contact");

		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.waitForElementVisibilityWithDuration(editSaveButton, 5, "save button");
		CommonUtil.click(editSaveButton, "Clicking on save button of edit screen");
		CommonUtil.normalWait(15);
	}


	//this method will configure the Product at the Quote Page-> for the products which requires advanced configuration
	public void advancedConfigurationForProductsAtQuote(){
		//Click on the Advanced Configurator button
		CommonUtil.waitForElementVisibilityWithDuration(advancedConfigurationBtn,5, "advanced config button");
		CommonUtil.click(advancedConfigurationBtn, "on Advanced Configurator button at the Quote Page");
		CommonUtil.waitForElementVisibilityWithDuration(salesProductForConfiguration,5, "sales product config");

		//Select the Product
		CommonUtil.clickUsingJavaScriptExecutor(salesProductForConfiguration, "Sales Product for the Advanced configuration");
		CommonUtil.normalWait(4);

		//Enter Version/Platfrom -> First enter * to view all the options and then select the any
		CommonUtil.inputText(versionPlatformInputBox, "*", "Enter * to view all the options");
		CommonUtil.waitForElementVisibilityWithDuration(versionOption,30 ,"version option");
		CommonUtil.clickUsingJavaScriptExecutor(versionOption, "the option for Advanced configuration");
		CommonUtil.normalWait(2);

		//Click on the save button
		CommonUtil.clickUsingJavaScriptExecutor(saveBtnForAdvancedConfigurationWin, "Save button in Advanced configuration window");
		CommonUtil.normalWait(5);
		//Close the window
		CommonUtil.clickUsingJavaScriptExecutor(closeBtnForAdvancedConfigurationWin, "Close button for Advanced configuration window");	
		CommonUtil.normalWait(30);
	}

	public void submitQuoteForApproval() {
		CommonUtil.normalWait(5);
		//Click on Submit for approval button
		submitForApprovalBtn.click();
		CommonUtil.normalWait(10);

	}
	
	public void verifyQuoteStatusAndSubstatus(String matchValue) {
		CommonUtil.normalWait(5);
		Assert.assertTrue(quoteStatus.getText().equalsIgnoreCase(matchValue), "status is not displayed as "+matchValue);
		Assert.assertTrue(quoteSubStatus.getText().equalsIgnoreCase(matchValue), "sub status is not displayed as "+matchValue);
		
	}
	
	public void editQuoteStatus(String matchValue) {
		CommonUtil.clickUsingJavaScriptExecutor(editStatusPencilIcon, "edit Status");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(statusButton, "Status dropdown");
		CommonUtil.clickUsingJavaScriptExecutor(statusButton, "Status dropdown");
		CommonUtil.normalWait(5);
		CommonUtil.clickUsingJavaScriptExecutor(customerAcceptedStatus, "Customer Accepted status");
		CommonUtil.normalWait(2);
		CommonUtil.clickUsingJavaScriptExecutor(editSaveButton, "save button");
		
		
		CommonUtil.normalWait(5);
		Assert.assertTrue(quoteStatus.getText().equalsIgnoreCase(matchValue), "status is not displayed as "+matchValue);
		Assert.assertTrue(quoteSubStatus.getText().equalsIgnoreCase(matchValue), "sub status is not displayed as "+matchValue);
		
	}

	public void completeApprovalsForQuoteAndVerifyStatus(HashMap<String, String> dataMap) {
	
		//Click on show all button to expand the view
		CommonUtil.normalWait(5);
		CommonUtil.scrollToBottomOfPage();
		CommonUtil.normalWait(5);
		CommonUtil.click(showAllRelatedQuickLinks, "Show all quick links");
		CommonUtil.normalWait(5);

		//Click on the Approvals link
		CommonUtil.waitForElementVisibilityWithDuration(approvalsLinkInQuickLinkTab, 10, "approvals link");
		CommonUtil.clickUsingJavaScriptExecutor(approvalsLinkInQuickLinkTab, "on the approvals link");
		CommonUtil.normalWait(5);
		
		int approvals = rowsNotApproved.size();
		System.out.println(approvals);
		
		for (int i = 1; i <= approvals; i++) {
			if(approvals>0) {
			//Click on the approval#
			WebElement approval_ID = driver.findElement(By.xpath("(//table[@role='grid']/child::tbody/child::tr//td[2]//span[text()!='Approved']/ancestor::tr//descendant::a[@data-refid='recordId'][starts-with(@title,'A-')])["+i+"]"));
			CommonUtil.waitForElementVisibilityWithDuration(approval_ID, 20, "approve_ID");
			CommonUtil.clickUsingJavaScriptExecutor(approval_ID, "Clicked on the Approval ID -"+approval_ID.getText());
			
			try {
				if(approveBtnAtApprovalPage.isDisplayed())
				{
			//Click on the approve buttton at the top right on the approval page
			CommonUtil.waitForElementVisibilityWithDuration(approveBtnAtApprovalPage, 20, "approve button");
			CommonUtil.clickUsingJavaScriptExecutor(approveBtnAtApprovalPage, "Clicked on the approve button at the approval page");
				}else {

					//Click on the approve buttton at the top right on the approval page
					CommonUtil.waitForElementVisibilityWithDuration(approveBtn1AtApprovalPage, 20, "approve button");
					CommonUtil.clickUsingJavaScriptExecutor(approveBtn1AtApprovalPage, "Clicked on the approve button at the approval page");
					
				}
			}catch(Exception e) {
				
				//Click on the approve buttton at the top right on the approval page
				CommonUtil.waitForElementVisibilityWithDuration(approveBtn1AtApprovalPage, 20, "approve button");
				CommonUtil.clickUsingJavaScriptExecutor(approveBtn1AtApprovalPage, "Clicked on the approve button at the approval page");
				
			}
			//again click on the approve button
			CommonUtil.waitForElementVisibilityWithDuration(approvalPopupIframe, 35, "approval confirmation popup");
			CommonUtil.switchToFrame(approvalPopupIframe, "approval popup iframe");
			CommonUtil.clickUsingJavaScriptExecutor(approveBtnAtApprovalPopUp, "Confirm Approval");
			
			CommonUtil.normalWait(30);
			if(i+1<=approvals)
			{   i=0;
				CommonUtil.normalWait(5);
				CommonUtil.scrollToBottomOfPage();
				CommonUtil.normalWait(5);
				CommonUtil.click(showAllRelatedQuickLinks, "Show all quick links");
				CommonUtil.normalWait(5);

				//Click on the Approvals link
				CommonUtil.waitForElementVisibilityWithDuration(approvalsLinkInQuickLinkTab, 15, "approvals link");
				CommonUtil.clickUsingJavaScriptExecutor(approvalsLinkInQuickLinkTab, "on the approvals link");
				CommonUtil.normalWait(15);
				approvals = rowsNotApproved.size();
				continue;
			}
			else
				break;	
			}
		}
		//navigate back to quote page
		quoteNumberBreadcrumb.click();
		CommonUtil.normalWait(10);
		Assert.assertTrue(quoteStatus.getText().equalsIgnoreCase("Approved"), "status is displayed as 'Approved'");
		Assert.assertTrue(quoteSubStatus.getText().equalsIgnoreCase("Approved"), "sub status is displayed as 'Approved'");	


	}
	
	public void verifyContactFieldsOnEditQuote(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(editButton, 10, "edit button");
		CommonUtil.click(editButton, "edit button");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(incoTermButton, 7, "incoterm drop down");
		CommonUtil.scrollIntoView(customerSpecificTermTextArea, "Scrolling to text area");
		CommonUtil.scrollIntoView(billToInput, "Bill to contact");
		Assert.assertTrue(CommonUtil.isElementEnabled(billToInput, "Bill to contact"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.waitForElementVisibilityWithDuration(shipToInput, 2, "Ship to");
		CommonUtil.scrollIntoView(shipToInput, "Ship to");
		Assert.assertTrue(CommonUtil.isElementEnabled(shipToInput, "Ship to"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.waitForElementVisibilityWithDuration(endCustomerInput, 2, "End Customer");
		CommonUtil.scrollIntoView(endCustomerInput, "End Customer");
		Assert.assertTrue(CommonUtil.isElementEnabled(endCustomerInput, "End Customer"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.waitForElementVisibilityWithDuration(editSaveButton, 5, "save button of edit screen");
		CommonUtil.click(editSaveButton, "save button of edit screen");
		CommonUtil.normalWait(8);

	}
}

