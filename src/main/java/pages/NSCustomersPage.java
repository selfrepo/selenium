package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtil;

public class NSCustomersPage extends CommonUtil{
	//Locators
	@FindBy(css = "#ns-header-menu-main-item3> a > span")
	WebElement customersTab;
	
	@FindBy(css = "#ns-header-menu-main>li:nth-child(4)>ul>li:nth-child(4)>a>span")
	WebElement salesSubTab;
	
	@FindBy(css = "#ns-header-menu-main>li:nth-child(4)>ul>li:nth-child(4)>ul>li:nth-child(2)>a>span") 
	WebElement enterSalesOrdersSubTab;
	
	@FindBy(css = "#ns-header-menu-main>li:nth-child(4)>ul>li:nth-child(4)>ul>li:nth-child(2)>ul>li:nth-child(1)>a>span")
	WebElement salesOrdersListOption;
	
	@FindBy(xpath = "//a[span[normalize-space()='Enter Sales Orders']]")
	WebElement enterSalesOrderLnk;
	
	@FindBy(xpath = "//li[@data-title='Customers']//a/span[text()='Sales']")
	WebElement customerSalesLnk;
	
	@FindBy(xpath = "//span[normalize-space()='List']")
	WebElement customerSalesListLnk;
	//methods
	public void clickCustomerTab() { 
		CommonUtil.click(customersTab, "Customers");
	}	
	public void clickSalesTab() { 
		CommonUtil.click(salesSubTab, "Customers>Sales");
	}	

	public void clickEnterSalesOrdersTab() {
		CommonUtil.click(enterSalesOrdersSubTab, "Customers>Sales>Enter Sales Orders");
	}
	public void clickSalesOrdersListOption() {
		CommonUtil.click(salesOrdersListOption, "Customers>Sales>Enter Sales Orders>List");
	}
	
	public void clickOnCustomerSalesOrdersListOption() {
		CommonUtil.movetoElement(customersTab, "Customers");
		CommonUtil.normalWait(1);
		CommonUtil.movetoElement(customerSalesLnk, "Customers>Sales");
		CommonUtil.normalWait(1);
		CommonUtil.movetoElement(enterSalesOrderLnk, "Customers>Sales>Enter Sales Orders");
		CommonUtil.normalWait(1);	
		CommonUtil.click(customerSalesListLnk, "Customers>Sales>Enter Sales Orders>List");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
