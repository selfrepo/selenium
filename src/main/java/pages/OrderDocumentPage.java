package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;

public class OrderDocumentPage extends CommonUtil{
	

	// ================================================================================
	// Variables
	// ================================================================================	
	private WebElement customerAcceptedOption;
	
	private String customerAcceptedOptionStart = "//*[@class='actionBody']//label[text()='Status']//following-sibling::div//button/parent::div//following-sibling::div[contains(@id,'dropdown-element')]/lightning-base-combobox-item//span[text()='";
	
	private String customerAcceptedOptionEnd = "']";
	
	// ================================================================================
	// Objects
	// ================================================================================
	@FindBy(xpath = "//button[contains(text(),'Order Document')]") 
	private WebElement orderDocumentBtn;
	
	@FindBy(xpath = "//label[text()='PO Number']//following-sibling::div/input[@type='text']")
	private WebElement poNumberInputBox;
	
	@FindBy(xpath = "//label[text()='PO Date']//following-sibling::div//input[@type='text']")
	private WebElement poDateInputBox;
	
	@FindBy(xpath = "//label[text()='PO Amount']//following-sibling::div//input[@type='text']")
	private WebElement poAmountInputBox;
	
	@FindBy(xpath = "//label[span[text()='Fulfilment Download Email']]//following-sibling::div//select")
	private WebElement fulfillmentDownloadEmailSelect;
	
	@FindBy(xpath = "//label[span[text()='Technical Admin Email']]//following-sibling::div//select")
	private WebElement technicalEmailSelect;
	
	@FindBy(xpath = "//label[span[text()='SaaS Primary Admin Contact']]/following-sibling::div//select")
	private WebElement saasPrimaryAdminSelect;
	
	@FindBy(xpath = "//*[@class='DESKTOP uiModal forceModal open active']//label[contains(@id,'file-selector-label')]/span[text()='Upload Files']")
	private WebElement uploadFileInput;
	
	@FindBy(xpath = "//button[span[contains(text(),'Done')]]")
	private WebElement doneButton;
	
	@FindBy(xpath = "//*[@class='DESKTOP uiModal forceModal open active']//button[normalize-space()='Submit']")
	private WebElement submitButton;
	
	@FindBy(xpath = "//lightning-formatted-text[contains(text(),'Customer Accepted')]")
	private WebElement customerAcceptedSubStatusTxt;
	
	@FindBy(xpath = "//b[contains(text(),'NOTE: Uploaded File name must not have more than 8')]")
	private WebElement uploadFileNote;
	
	@FindBy(xpath = "//button[@title='Edit Primary']")
	private WebElement editPrimaryButton;
	
	@FindBy(xpath = "//*[@class='actionBody']//label[span[text()='Primary']]//following-sibling::div/span/input[@type='checkbox'][@name=\"SBQQ__Primary__c\"]")
	private WebElement primaryCheckbox;
	
	@FindBy(xpath = "(//button[@name='SaveEdit'][normalize-space()='Save'])[1]")
	private WebElement editSaveButton;
	
	@FindBy(xpath = "(//lightning-formatted-text[@data-output-element-id='output-field'][normalize-space()='Quote Issued'])[2]")
	private WebElement quoteIssuedTxt;
	
	@FindBy(xpath = "//button[contains(text(),'Edit')]") 
	private WebElement editButton;
	
	@FindBy(xpath = "//*[@class='actionBody']//label[text()='Status']//following-sibling::div//button[span[text()='Quote Issued']]")
	private WebElement statusDropDown; 
	
	@FindBy(xpath = "//div[span[text()='Quote Sub Status']]/following-sibling::div//lightning-formatted-text[normalize-space()='Not Released']")
	private WebElement notReleasedSubStatusTxt;
	
	@FindBy(xpath = "//div[span[text()='Status']]/following-sibling::div//lightning-formatted-text[contains(text(),'Customer Accepted')]")
	private WebElement customerAcceptedStatusTxt;
	
	// ================================================================================
	// Methods
	// ================================================================================
	public void orderDocument(HashMap<String, String> dataMap,String quoteNumber) throws InterruptedException {
		CommonUtil.waitForElementVisibilityWithDuration(orderDocumentBtn, 10,"Order Document");
		CommonUtil.click(orderDocumentBtn, "Order Document");
		CommonUtil.normalWait(8);
		CommonUtil.waitForElementVisibilityWithDuration(poNumberInputBox, 12,"PO Number input field");
		CommonUtil.inputText(poNumberInputBox, quoteNumber, quoteNumber+" in PO Number");
		CommonUtil.waitForVisible(poDateInputBox,"PO date field");
		CommonUtil.inputText(poDateInputBox,CommonUtil.getCurrentDate("dd-MMM-yyyy"),"Current Date");
		CommonUtil.waitForVisible(poAmountInputBox,"PO amount field");
		CommonUtil.inputText(poAmountInputBox, dataMap.get("PO_Amount"), dataMap.get("PO_Amount")+" in PO Amount");
		CommonUtil.selectByValue(fulfillmentDownloadEmailSelect,dataMap.get("FulfillMentEmail"));
		CommonUtil.selectByValue(technicalEmailSelect,dataMap.get("FulfillMentEmail"));
		CommonUtil.selectByVisibleText(saasPrimaryAdminSelect,dataMap.get("ContactName"));
		CommonUtil.scrollIntoView(uploadFileNote,"Upload file note");
		CommonUtil.normalWait(3);
		CommonUtil.waitForElementVisibilityWithDuration(uploadFileNote, 3, "Upload files note");
		CommonUtil.scrollIntoView(uploadFileInput,"Upload files input");
		CommonUtil.normalWait(2);
		CommonUtil.waitForElementVisibilityWithDuration(uploadFileInput, 2, "Upload files input");
		CommonUtil.uploadFilesUsingRobotClass(uploadFileInput, "Legal Document for "+quoteNumber+".pdf","Order document upload files");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(doneButton, 5,"Done button");
		CommonUtil.clickUsingJavaScriptExecutor(doneButton,"Done button");
		CommonUtil.normalWait(2);
		CommonUtil.waitForElementVisibilityWithDuration(submitButton, 5,"Submit button");
		CommonUtil.scrollIntoView(submitButton,"Submit button");
		CommonUtil.click(submitButton,"submit button");
		CommonUtil.normalWait(2);
		CommonUtil.waitForElementVisibilityWithDuration(customerAcceptedSubStatusTxt,5,"Customer Accepted sub status");
		Assert.assertTrue(CommonUtil.isElementDisplayed(customerAcceptedSubStatusTxt,"Customer Accepted sub status"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.delFileWithInDirectory("upload","Legal Document for "+quoteNumber+".pdf");
		CommonUtil.click(editButton,"edit button");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(statusDropDown,7,"quote status drop down");
		CommonUtil.clickUsingJavaScriptExecutor(statusDropDown, "quote status drop down");
		customerAcceptedOption= CommonUtil.findElement(customerAcceptedOptionStart+dataMap.get("Customer Accepted")+customerAcceptedOptionEnd,"xpath","Customer Accepted option");
		CommonUtil.waitForElementVisibilityWithDuration(customerAcceptedOption, 2,"Customer Accepted option");
		CommonUtil.clickUsingJavaScriptExecutor(customerAcceptedOption, "Customer Accepted option");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(editSaveButton, 5,"save button of edit screen");
		CommonUtil.click(editSaveButton, "save button of edit screen");
		CommonUtil.waitForElementVisibilityWithDuration(customerAcceptedStatusTxt, 20,"quote status as Customer Accepted");
		Assert.assertTrue(CommonUtil.isElementDisplayed(customerAcceptedStatusTxt,"quote status as Customer Accepted"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(notReleasedSubStatusTxt,"sub status as Not Released"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

}