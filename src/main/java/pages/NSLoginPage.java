package pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.ConfigReader;
import utilities.EncryptionDecryptionUtil;
import utilities.LogUtil;

public class NSLoginPage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================
	public String Username;

	// ================================================================================
	// Objects
	// ================================================================================
	@FindBy(css = "#spn_cRR_d1 > a > div.ns-role > span:nth-child(1)")
	WebElement userRoleDropdown;

	@FindBy(css = "#spn_cRR_d1 > a > div.ns-role > span:nth-child(2) >span:nth-child(2)")
	WebElement userCurrentRole;

	@FindBy(xpath = "//a[@href='/app/center/myroles.nl']/ancestor::div[contains(@class,'ns-menubar')]")
	WebElement roleDropdown;
	
	@FindBy(css = "#ns-header-menu-userrole-item1 >  a[href$='/app/center/myroles.nl']")
	WebElement viewMyRoles;

	@FindBy(css = "#ns-header-menu-userrole-item0 > a[href$='/pages/nllogoutnoback.jsp']")
	WebElement logOut;

	@FindBy(id = "username")
	WebElement usernameField;

	@FindBy(id = "password")
	WebElement passwordField;

	@FindBy(css = "p.form_submit > input[type=submit]")
	WebElement logInButton;

	@FindBy(css = "#searchTab > img")
	WebElement searchIcon;

	@FindBy(css = "#searchPanel > div > input")
	WebElement appSearchTextbox;

	@FindBy(css = "#ns-header-menu-main-item4 > a[href*='/app/center/card.nl?']")
	WebElement vendorsMenu;

	@FindBy(css = "#ns-header-menu-main-item4 >ul >li:nth-child(3)>a>span")
	WebElement vendorLists;

	@FindBy(id = "ns-header-menu-userrole")
	private WebElement viewProfileIcon;

	@FindBy(css = "a[href$='/pages/nllogoutnoback.jsp']") // .icon-signout
	private WebElement logOutLink;

	// change roles
	@FindBy(xpath = "//div[@role='button'][@data-type='link']/label")
	private List<WebElement> rolesList;

	@FindBy(id = "n-id-component-1291")
	private WebElement invoiceDistributionEMEARole;

	@FindBy(css = "#n-id-component-462 > div >span")
	private WebElement loggedinAsInvoiceDistEMEA;

	@FindBy(id = "ns-dashboard-heading-panel")
	private WebElement homePage;

	// global SO search
	@FindBy(css = "input#_searchstring")
	private WebElement globalSearchInputbox;

	@FindBy(css = "input#_searchSubmitter")
	private WebElement globalSearchButton;

	@FindBy(css = "div[data-widget='Popover'] a[href*='/app/accounting/transactions/transaction.nl?'][aria-label='Sales Order']")
	private WebElement globalSearchResultItem;

	@FindBy(css = "#div__bodytab tr:nth-child(2) td")
	private WebElement noSearchResults;

	@FindBy(css = ".uir-record-id")
	private WebElement soRecordId;

	@FindBy(css = "#edit")
	private WebElement editButton;

	@FindBy(css = "#btn_multibutton_submitter")
	private WebElement saveButton;

	@FindBy(id = "spn_secondarymultibutton_submitter")
	private WebElement saveSecondButton;

	@FindBy(css = "#div__alert > div > div.icon.confirmation")
	private WebElement confirmationAlert;

	@FindBy(css = "a#cmmnctntabtxt")
	private WebElement communicationsSubTab;

	@FindBy(css = "input#custbody_mf_invoice_emails")
	private WebElement invoiceEmailAddress;

	@FindBy(css = "textarea#custbody_mf_inv_dist_details")
	private WebElement manualInvoiceDistriTextbox;

	// ================================================================================
	// Methods
	// ================================================================================
	public void navigate() {

		String netSuiteEnvironment = ConfigReader.getValue("netSuiteEnvironment");
		String URL = ConfigReader.getValue(netSuiteEnvironment);
		CommonUtil.navigate(URL);
		LogUtil.infoLog(getClass(), "Navigated to URL: " + URL);
	}

	public void signIn(HashMap<String, String> dataMap) { // pass a parameter so we don't hardcode values in the object
															// class.
		CommonUtil.waitForVisible(usernameField, "Username");
		CommonUtil.inputText(usernameField, dataMap.get("Username"), "username");
		CommonUtil.waitForVisible(passwordField, "Password");

		// decryption of password
		CommonUtil.inputText(passwordField, EncryptionDecryptionUtil.decodeEncoded(dataMap.get("Password")),
				"password");
		CommonUtil.click(logInButton, "the Login button");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	public void selectARole(String roleName) {
		List<String> values = new ArrayList<String>();
		CommonUtil.click(roleDropdown, "role dropdown");
		CommonUtil.normalWait(2);
		System.out.println(rolesList.size());

		// Read list of roles to array
		for (WebElement we : rolesList) {
			values.add(we.getText());
		}
		int index = values.indexOf(roleName);
		System.out.println("Index " + index);

		// select the required role
		rolesList.get(index).click();
		CommonUtil.normalWait(2);
		CommonUtil.waitForVisible(userRoleDropdown, "user role drop down");
	}

	public void verifyCurrectRoleAndChangeRole(HashMap<String, String> dataMap) {
		CommonUtil.waitForVisible(userRoleDropdown, "user role drop down");
		String role = dataMap.get("Role");
		if (userCurrentRole.getText().equalsIgnoreCase(role)) {
			LogUtil.infoLog(NSLoginPage.class, "Role " + "Expected role is displayed - " + role);

		} else {
			selectARole(role);
		}
	}

	public void searchForSO(HashMap<String, String> dataMap) {
		String soNumber = dataMap.get("SO Number");

		try {
			for (int i = 1; i <= 3; i++) {
				CommonUtil.waitForVisible(globalSearchInputbox, "Global search input");
				CommonUtil.inputText(globalSearchInputbox, soNumber, "Entered SO#");
				CommonUtil.waitForVisible(globalSearchResultItem, "Global search result");
				Assert.assertTrue(globalSearchResultItem.isDisplayed(), "Globalsearch result item dispalyed");
				CommonUtil.movetoElement(globalSearchResultItem, "Global search result").click();

				if (noSearchResults.isDisplayed()) {
					CommonUtil.inputText(globalSearchInputbox, soNumber, "Entered SO#");
					CommonUtil.waitForVisible(globalSearchResultItem, "Global search result");
					CommonUtil.movetoElement(globalSearchResultItem, "Global search result").click();

				} else
					break;

			}
		} catch (Exception e) {
			LogUtil.errorLog(NSLoginPage.class, "SOSearch " + "SO search is unsuccessful with exception", e);
		}
		CommonUtil.waitForVisible(soRecordId, "SO record");
		Assert.assertTrue(soRecordId.isDisplayed(), "SO page dispalyed");

		if (soRecordId.getText().contentEquals(soNumber)) {
			LogUtil.infoLog(NSLoginPage.class, "SOserach " + "SO search is successful.");
		}

	}

	public void editSalesOrder() {
		CommonUtil.clickUsingJavaScriptExecutor(editButton, "Edit button");
		CommonUtil.waitForVisible(saveButton, "save button");
		Assert.assertTrue(saveButton.isDisplayed(), "Edit SO page dispalyed");
		CommonUtil.normalWait(10);

	}

	public void editCommunicationsTab(HashMap<String, String> dataMap) {
		CommonUtil.waitForVisible(communicationsSubTab, "Communications sub tab");
		CommonUtil.clickUsingJavaScriptExecutor(communicationsSubTab, "Communications sub tab");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(invoiceEmailAddress, "invoice email address");

		// edit invoice email address
		CommonUtil.inputText(invoiceEmailAddress, dataMap.get("Invoice Email Address"), "Invoice Email Address");
		CommonUtil.normalWait(2);

		// Enter Invoice Distribution Textbox
		String randomStr = RandomStringUtils.randomNumeric(5);
		CommonUtil.inputText(manualInvoiceDistriTextbox, "testing" + randomStr, "Invoice Distribution Textbox");

		// save SO
		CommonUtil.click(saveSecondButton, "click Save button");
		CommonUtil.normalWait(5);

		// verify confirmation message
		if (confirmationAlert.isDisplayed())
			LogUtil.infoLog(NSLoginPage.class, "Communications SubTab " + "Details saved successfully");
		CommonUtil.attachScreenshotOfPassedTestsInReport();

	}

	public void signOut() {
		Actions action = new Actions(driver);
		action.moveToElement(viewProfileIcon).perform();

		CommonUtil.clickUsingJavaScriptExecutor(logOutLink, "Logout Link");
		CommonUtil.waitForVisible(usernameField, "Username");
	}
}
