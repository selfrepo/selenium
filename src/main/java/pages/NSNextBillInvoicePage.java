package pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtil;

public class NSNextBillInvoicePage extends CommonUtil {
	
	// ================================================================================
	// Variables
	// ================================================================================
	
	GmailPage gmail;
	private static String parentWindow;
	
	// ================================================================================
	// Objects
	// ================================================================================

	
	@FindBy(id = "tdbody_nextbill")
	WebElement nextBillButton;

	@FindBy(css = "#memo")
	WebElement memoText;

	@FindBy(xpath = "//a[text()='Source']")
	WebElement source;

	@FindBy(id = "cmmnctntabtxt")
	WebElement communicationsTab;

	@FindBy(xpath = "//a[text()='Email Invoice']/../../..//img")
	WebElement emailInvoiceCheckBox;

	@FindBy(id = "custbody_mf_invoice_emails")
	WebElement emailInvoiceMail;

	@FindBy(xpath = "//a[text()='To Be E-mailed']/../../..//img")
	WebElement toBeEmailedCheckBox;

	@FindBy(xpath = "//a[text()='To Be E-mailed']/../../../..//input[@id='email']")
	WebElement toBeEmailedMail;

	@FindBy(id = "spn_secondarymultibutton_submitter")
	WebElement saveButton;

	@FindBy(xpath = "//div[contains(text(),'success')]")
	WebElement validateInvoice;

	@FindBy(xpath = "//td[text()='Email']")
	WebElement validateEmail;
	
	@FindBy(xpath = "//td[text()='Email']/..//a[text()='View']")
	WebElement viewEmail;
	
	@FindBy(xpath = "//th[contains(text(),'Invoice ')]")
	WebElement invoiceValidation;
	
	// ================================================================================
	// Methods
	// ================================================================================


	//Validation of invoice details
	public void invoiceDetails(HashMap<String, String> dataMap) {
		String memo = dataMap.get("Memo");
		CommonUtil.click(nextBillButton, "Click on NextBill Button");
		CommonUtil.waitForElementVisibilityWithDuration(memoText, 10, "memo");
		CommonUtil.inputText(memoText, memo, "Memo");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollIntoView(source, "Scroll to view Communications Tab");
		CommonUtil.normalWait(15);
		CommonUtil.clickUsingJavaScriptExecutor(communicationsTab, "Click on Communications Tab");
		CommonUtil.normalWait(15);
		CommonUtil.waitForVisible(emailInvoiceCheckBox, "email invoice checkbox");
		CommonUtil.clickUsingJavaScriptExecutor(emailInvoiceCheckBox, "Checked the Email Invoice");
		CommonUtil.waitForVisible(emailInvoiceMail, "email invoice address");
		// invoice email address
		CommonUtil.inputText(emailInvoiceMail, dataMap.get("Invoice Email Address"),
				dataMap.get("Invoice Email Address") + " input in emailInvoicefield ");
		CommonUtil.normalWait(2);
		CommonUtil.scrollIntoView(toBeEmailedCheckBox, "To be Emailed Checkbox");
		CommonUtil.clickUsingJavaScriptExecutor(toBeEmailedCheckBox, "toBe emailed checkbox");
		CommonUtil.normalWait(3);
		CommonUtil.clearInput(toBeEmailedMail, dataMap.get("Invoice Email Address"));
		CommonUtil.inputText(toBeEmailedMail, dataMap.get("Invoice Email Address"),
				dataMap.get("Invoice Email Address") + "in emailInvoiceMail ");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(saveButton, "Click on Save");
		CommonUtil.normalWait(5);
		CommonUtil.verifyText(validateInvoice, "Transaction successfully Saved", "Validate Invoice");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollIntoView(communicationsTab, "Communications Tab");
		CommonUtil.click(communicationsTab, "Communications Tab");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.verifyText(validateEmail, "Email", "Validate Email");
		CommonUtil.click(viewEmail, "view email option");
		parentWindow=CommonUtil.switchToNewWindow("Email message");
		CommonUtil.verifyText(invoiceValidation, "Invoice Details", "Validate Invoice Email Message");
		

	}

}
