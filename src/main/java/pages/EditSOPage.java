package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;

public class EditSOPage extends CommonUtil {

	@FindBy(css = "#secondaryedit")
	private WebElement secondaryEditBtn;
	
	@FindBy(css = ".uir-record-id")
	private WebElement soHeader;
	
	@FindBy(css = "#startdate")
	private WebElement startDate;
	
	@FindBy(css = "#btn_multibutton_submitter")
	private WebElement saveBtn;
	
	@FindBy(css = "input[value='Approve'][name='custpage_so_approve']")
	private WebElement approveBtn;
	
	@FindBy(xpath = "//h2[text()='Approving, please wait...']")
	private WebElement approvingPleaseWaitTxt;
	
	@FindBy(xpath = "//div[text()='Pending Fulfillment']")
	private WebElement pendingFulfillment;
	
	@FindBy(xpath = "//div[@class='title'][text()='This transaction requires approval by MF Order Management Manager.']")
	private List<WebElement> approvalByMgmtManagerTxt;
	
	@FindBy(xpath = "//div[text()='Pending Approval']")
	private WebElement pendingApproval;
	
	// ================================================================================
	// Methods
	// ================================================================================
	public void editSOAndApprovingFromOMSpecialist(HashMap<String, String> dataMap) {
		CommonUtil.click(secondaryEditBtn, "Edit Button");
		CommonUtil.waitForElementVisibilityWithDuration(soHeader, 40, "SO header");
		Assert.assertTrue(CommonUtil.isElementEnabled(startDate, "Start date field"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.inputText(startDate, CommonUtil.getCurrentDate("dd/MM/yyyy"),CommonUtil.getCurrentDate("dd/MM/yyyy")+ " Start Date");
		CommonUtil.click(saveBtn, "Save Button");
		CommonUtil.normalWait(25);
		CommonUtil.waitForElementToBeClickable(approveBtn, 25,"Approve button");
		CommonUtil.click(approveBtn, "Approve button");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.waitForElementInvisiblity(approvingPleaseWaitTxt, 30, "Approving please wait screen");
		if(approvalByMgmtManagerTxt.size()>0)
		{
			Assert.assertTrue(CommonUtil.isElementDisplayed(pendingApproval, "Pending Approval Status"));
		}
		else {
		Assert.assertTrue(CommonUtil.isElementDisplayed(pendingFulfillment, "Pending Fulfillment Status"));
		}
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
