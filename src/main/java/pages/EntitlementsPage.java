package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class EntitlementsPage extends CommonUtil{
	
	String rowEntitlements = "//table[@aria-label = 'Entitlements']//tbody[1]//tr";
	WebElement asset;
	WebElement entitlement;
	
	String entitlementLnkStart = "//table[@aria-label='Entitlements']/tbody/tr/th/span/a[contains(text(),'";
	String entitlementLnkEnd ="')]";
	WebElement entitlementLnk;
	
	String assetLnkStart = "//table[@aria-label='Entitlements']/tbody/tr/td/span/a[contains(text(),'";
	String assetLnkEnd ="')]";
	WebElement assetLnk;
	
	@FindBy(xpath = "//a[slot[span[contains(text(),'Entitlements')]]]")
	private WebElement relatedListEntitlementsLnk;
	
	@FindBy(xpath = "//table[@aria-label='Entitlements']//tr[count(//th[@aria-label='Asset']/preceding-sibling::td)+1]/td/span/a")
	private WebElement assetsColEleLnk;
	
	@FindBy(xpath = "//h1[normalize-space()='Entitlements']")
	private WebElement entitlementsHeader;
	
	@FindBy(xpath = "//div[span[text()='Entitlement Name']]/following-sibling::div/span/span")
	private WebElement entitlementNameTxt;
	
	@FindBy(xpath = "(//div[span[text()='Asset Name']]/following-sibling::div//lightning-formatted-text)[1]")
	private WebElement assetNameTxt;
	
	
	
    public void clickRelatedListEntitlements(HashMap<String, String> dataMap) throws InterruptedException {
		
		CommonUtil.refresh("Service Contract Page");
		CommonUtil.normalWait(10);
		CommonUtil.click(relatedListEntitlementsLnk, "Click entitlements link");
		CommonUtil.waitForElementVisibilityWithDuration(entitlementsHeader, 10, "Entitlements Header text");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
   }
    
	public void validateEntitlementsAndAssets(HashMap<String, String> dataMap) throws InterruptedException {
		
		CommonUtil.refresh("Entitlements Page");
		CommonUtil.normalWait(10);
	
		List<WebElement> rowCountEntitlements= CommonUtil.getAllElements(rowEntitlements, "Entitlement Page rows");
		int rowsizeEntitlements = rowCountEntitlements.size();

			for (int i = 1; i <= rowsizeEntitlements; i++) {
				entitlement = driver.findElement(By.xpath("//table[@aria-label='Entitlements']/tbody/tr["+i+"]/th//a"));
				String entitlementTxt = CommonUtil.getText(entitlement, "Getting Entitlement text for row "+i);
				LogUtil.infoLog(getClass(), "Row "+i+" "+ "Entitlement is : "+entitlementTxt);
				
				asset= driver.findElement(By.xpath("(//table[@aria-label='Entitlements']/tbody/tr["+i+"]/td/span/a)[1]"));
				String assetTxt = CommonUtil.getText(asset, "Getting Asset text for row "+i);
				LogUtil.infoLog(getClass(), "Row "+i+" "+ "Asset is : "+assetTxt);
				
				
			}
			CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void clickEntitlementInTable(HashMap<String, String> dataMap) throws InterruptedException {
		entitlementLnk = CommonUtil.findElement(entitlementLnkStart+dataMap.get("Entitlement")+entitlementLnkEnd, "xpath", "entitlement "+dataMap.get("Entitlement"));
		CommonUtil.click(entitlementLnk, "Entitlement link "+dataMap.get("Entitlement"));
		CommonUtil.waitForElementVisibilityWithDuration(entitlementNameTxt, 10, "Entitlement Name Text");
		String entitlementName= CommonUtil.getText(entitlementNameTxt,"Entitlement Name");
		Assert.assertTrue(entitlementName.equals(dataMap.get("Entitlement")),"Validating the entitlement name");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void navigateBackToEntitleMentsPage() throws InterruptedException {
		   
		   CommonUtil.back("EntitleMentsPage");
		   CommonUtil.waitForElementVisibilityWithDuration(entitlementsHeader, 10, "Entitlements Header text");
		   CommonUtil.attachScreenshotOfPassedTestsInReport();
	   }
	
	public void clickAssetInTable(HashMap<String, String> dataMap) throws InterruptedException {
		assetLnk = CommonUtil.findElement(assetLnkStart+dataMap.get("Asset")+assetLnkEnd, "xpath", "Asset "+dataMap.get("Asset"));
		CommonUtil.click(assetLnk, "Asset link "+dataMap.get("Asset"));
		CommonUtil.waitForElementVisibilityWithDuration(assetNameTxt, 10, "Asset Name Text");
		String assetName= CommonUtil.getText(assetNameTxt,"Asset Name");
		Assert.assertTrue(assetName.equals(dataMap.get("Asset")),"Validating the Asset name");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
