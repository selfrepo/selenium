package pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


import utilities.CommonUtil;
import utilities.ConfigReader;
import utilities.EncryptionDecryptionUtil;
import utilities.GlobalUtil;

public class LoginPage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================

	// ================================================================================
	// Objects
	// ================================================================================
	@FindBy(id = "username")
	private WebElement usernameField;
	
	@FindBy(id = "password")
	private WebElement passwordField;
	
	@FindBy(id = "Login")
	private WebElement logInButton;
	
	@FindBy(xpath = "//div[@class='profileTrigger branding-user-profile bgimg slds-avatar slds-avatar_profile-image-small circular forceEntityIcon']//span[@class='uiImage']")
	private WebElement viewProfileIcon;
	
	@FindBy(css = ".profile-link-label.logout.uiOutputURL")
	private WebElement logOutLink;
	


	// ================================================================================
	// Methods
	// ================================================================================
		
	public void navigate() {
		CommonUtil.navigate(ConfigReader.getValue(GlobalUtil.getCommonSettings().getAppEnviornment()));
	}
	
	public void signIn(HashMap<String, String> dataMap) { //pass a parameter so we don't hardcode values in the object class.
		CommonUtil.waitForVisible(usernameField);
		CommonUtil.inputText(usernameField,dataMap.get("Username"),"username");
		CommonUtil.waitForVisible(passwordField);
		//decrypted password
		CommonUtil.inputText(passwordField,EncryptionDecryptionUtil.decodeEncoded(dataMap.get("Password")),"password");
		
		CommonUtil.click(logInButton, "the Login button");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void signOut() { //pass a parameter so we don't hardcode values in the object class.
		CommonUtil.waitForVisible(viewProfileIcon);
		CommonUtil.click(viewProfileIcon, "View Profile Icon");
		CommonUtil.normalWait(8);
		CommonUtil.waitForVisible(logOutLink);
		CommonUtil.click(logOutLink, "Logout Link");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(usernameField);
		Assert.assertTrue(CommonUtil.isElementDisplayed(usernameField, "username"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.normalWait(5);
	}

}
