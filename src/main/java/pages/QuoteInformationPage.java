package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.CommonUtil;

public class QuoteInformationPage extends CommonUtil{

	//Objects
	@FindBy(xpath = "//iframe[contains(@name,'vfFrameId')][@height='100%']") 
	private WebElement editQuoteIFrame;
	@FindBy(xpath = "//div[span[normalize-space()='Quote Total']]/following-sibling::div//span//slot/lightning-formatted-text") 
	private WebElement quoteTotal;

	// ================================================================================
	// Methods
	// ================================================================================
	public void editingTheQuoteProductInformation(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(editQuoteIFrame, 3,"Edit Quote IFrame");
		CommonUtil.switchToFrame(editQuoteIFrame,"Edit Quote IFrame");
		CommonUtil.normalWait(15);	
		WebElement ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div:nth-child(6) > div\")","Quantity div");
		CommonUtil.movetoElementAndClick(ele,"Quantity div");	
		//Clicked on the pencil icon for quantity
		CommonUtil.normalWait(5);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.sf-has-focus > div > span\")","pencil icon of quantity");
		CommonUtil.normalWait(5);
		CommonUtil.clickUsingJavaScriptExecutor(ele,"pencil icon of quantity");
		CommonUtil.normalWait(2);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.sf-has-focus.editMode > div.w > div > sb-input\").shadowRoot.querySelector(\"#myinput\")","quantity inputbox");
		CommonUtil.inputTextWithEnterKey(ele,dataMap.get("Quantity"));
		CommonUtil.normalWait(2);
        CommonUtil.attachScreenshotOfPassedTestsInReport();
		//scrolling to nearest element
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div:nth-child(13) > div\")","pencil icon of additional discount div");
		CommonUtil.scrollIntoView(ele, "Scrolling to pencil icon of additional discount div element");
		CommonUtil.movetoElementAndClick(ele,"Scrolling to pencil icon of additional discount div element");
		
		//Click on the pencil icon
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div:nth-child(13) > div > span\")","pencil icon of additional discount");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"pencil icon of additional discount");
	
		//Find the additional discount field and enter 10
	    ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.editMode.sf-has-focus > div.w > div > sb-input\").shadowRoot.querySelector(\"#myinput\")","Additional discount inputbox");
		CommonUtil.inputTextWithEnterKey(ele,dataMap.get("Discount"));
		CommonUtil.normalWait(2);
        CommonUtil.attachScreenshotOfPassedTestsInReport();
		//search the dropdown
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div:nth-child(17)\")","Sales Event dropdown div");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"Sales Event dropdown div to bring the focus");
		CommonUtil.normalWait(2);
		//click on the pencil icon
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div:nth-child(17) > div > span\")","pencil icon for Sales Event dropdown");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"pencil icon for Sales Event dropdown");
		//open the dropdown
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#picklist\").shadowRoot.querySelector(\"#myselect\")","Sales Event dropdown");
		CommonUtil.movetoElementAndClick(ele,"Sales Event dropdown");
		CommonUtil.normalWait(2);
		CommonUtil.selectByIndex(ele,Integer.parseInt(dataMap.get("Index")),"Add On");
		CommonUtil.normalWait(2);
        CommonUtil.attachScreenshotOfPassedTestsInReport();
		//verifying the total value
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#subtotal > sb-field\").shadowRoot.querySelector(\"#f > sb-div\").shadowRoot.querySelector(\"div\")","Total amount");
		movetoElement(ele,"Total amount");
		CommonUtil.waitForVisible(ele,"Total amount");
		CommonUtil.normalWait(2);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#actions > sb-custom-action:nth-child(15)').shadowRoot.querySelector('#mainButton')","Save button");
		CommonUtil.click(ele,"Save button");
		CommonUtil.normalWait(10);
		CommonUtil.switchToDefaultFrame();
		CommonUtil.normalWait(2);
		CommonUtil.waitForElementVisibilityWithDuration(quoteTotal,3,"Quote total");
	}

	public void QuoteLineEditor_22_01(HashMap<String , String> dataMap) {
		CommonUtil.normalWait(10);
		//switch the Frame 
		CommonUtil.waitForElementVisibilityWithDuration(editQuoteIFrame, 3, "edit quote");
		CommonUtil.switchToFrame(editQuoteIFrame,"editquote iframe");
		CommonUtil.normalWait(10);	

		//Edit the Quantity for the Main product
		//Click on the Element -> click on the Pencil icon to edit-> enter the data
		WebElement ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#groupLayout').shadowRoot.querySelector('#Group_').shadowRoot.querySelector('#groupTabs').shadowRoot.querySelector('#pages > div > sb-group-tabs').shadowRoot.querySelector('sf-standard-table').shadowRoot.querySelector('#list > iron-list > div:nth-child(3) > sf-le-table-row').shadowRoot.querySelector('#content > div > div:nth-child(6) > div')","");
		CommonUtil.movetoElementAndClick(ele,"quantity");	
		//Clicked on the pencil icon for quantity
		CommonUtil.normalWait(5);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#groupLayout').shadowRoot.querySelector('#Group_').shadowRoot.querySelector('#groupTabs').shadowRoot.querySelector('#pages > div > sb-group-tabs').shadowRoot.querySelector('sf-standard-table').shadowRoot.querySelector('#list > iron-list > div:nth-child(3) > sf-le-table-row').shadowRoot.querySelector('#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.sf-has-focus > div > span')","");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"Pencil icon for quantity");
		CommonUtil.normalWait(2);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#groupLayout').shadowRoot.querySelector('#Group_').shadowRoot.querySelector('#groupTabs').shadowRoot.querySelector('#pages > div > sb-group-tabs').shadowRoot.querySelector('sf-standard-table').shadowRoot.querySelector('#list > iron-list > div:nth-child(3) > sf-le-table-row').shadowRoot.querySelector('#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.sf-has-focus.editMode > div.w > div > sb-input').shadowRoot.querySelector('#myinput')","");
		CommonUtil.inputTextWithEnterKey(ele,dataMap.get("Quantity"));
		CommonUtil.normalWait(2);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
		//scrolling to nearest element
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#groupLayout').shadowRoot.querySelector('#Group_').shadowRoot.querySelector('#groupTabs').shadowRoot.querySelector('#pages > div > sb-group-tabs').shadowRoot.querySelector('sf-standard-table').shadowRoot.querySelector('#list > iron-list > div:nth-child(3) > sf-le-table-row').shadowRoot.querySelector('#content > div > div:nth-child(13) > div')","");
		CommonUtil.scrollIntoView(ele, "Scrolling to desired div element");
		CommonUtil.movetoElementAndClick(ele,"additional discount");
		//Click on the pencil icon
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div:nth-child(13) > div > span\")","");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"Pencil icon for additional discount");
		//Find the additional discount field and enter 10
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(3) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.editMode.sf-has-focus > div.w > div > sb-input\").shadowRoot.querySelector(\"#myinput\")","");
		CommonUtil.inputTextWithEnterKey(ele,dataMap.get("Discount"));
		CommonUtil.normalWait(4);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
		//Enter the Manual Unit for the License extension product
		//Click on the Element -> click on the Pencil icon to edit-> enter the data
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(4) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div:nth-child(8)\")","Manula Unit Element");
		CommonUtil.scrollIntoView(ele, "Manual Unit element");
		CommonUtil.movetoElementAndClick(ele, "Manual Unit element");
		CommonUtil.normalWait(5);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#groupLayout').shadowRoot.querySelector('#Group_').shadowRoot.querySelector('#groupTabs').shadowRoot.querySelector('#pages > div > sb-group-tabs').shadowRoot.querySelector('sf-standard-table').shadowRoot.querySelector('#list > iron-list > div:nth-child(4) > sf-le-table-row').shadowRoot.querySelector('#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.sf-has-focus > div > span')", "pencil icon");
		CommonUtil.clickUsingJavaScriptExecutor(ele, "Pencil Icon for Manual Price");
		//enter the price 
		CommonUtil.normalWait(5);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#groupLayout\").shadowRoot.querySelector(\"#Group_\").shadowRoot.querySelector(\"#groupTabs\").shadowRoot.querySelector(\"#pages > div > sb-group-tabs\").shadowRoot.querySelector(\"sf-standard-table\").shadowRoot.querySelector(\"#list > iron-list > div:nth-child(4) > sf-le-table-row\").shadowRoot.querySelector(\"#content > div > div.container.td.sf-le-table-cell.sf-le-table-row.focusable.right.editable.editMode.sf-has-focus > div.w > div > sb-input\").shadowRoot.querySelector(\"#myinput\")","Manual unit price");
		CommonUtil.inputTextWithEnterKey(ele, dataMap.get("Manual Unit Pricing"));
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
		//Click on the Save button to save the line configurations and switch the frame
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#actions > sb-custom-action:nth-child(15)').shadowRoot.querySelector('#mainButton')","");
		CommonUtil.click(ele,"Save button");
		CommonUtil.normalWait(35);
		CommonUtil.switchToDefaultFrame();
		CommonUtil.normalWait(5);

	}
}
