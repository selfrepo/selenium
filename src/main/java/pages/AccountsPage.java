package pages;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;

public class AccountsPage extends CommonUtil{
	
	// ================================================================================
	// Variables
	// ================================================================================
	String currencyOptionStart = "//span[@title='";
    String currencyOptionEnd =	 "']";
    WebElement currencyOption;
    
    String accountNameStart = "//lightning-formatted-text[@class='custom-truncate'][text()='";
    String accountNameEnd =	 "']";
    WebElement accountName;
    CaseAccountsPage accPage = new CaseAccountsPage();
 
    // ================================================================================
 	// Objects
 	// ================================================================================	
	@FindBy(xpath = "//a[span[text()='Accounts']]") 
	private WebElement accountsTabLnk;
	
	@FindBy(xpath = "//a[span[text()='Contacts']]") 
	private WebElement contactsTabLnk;
	
	@FindBy(xpath = "//div[@class='slds-breadcrumb__item slds-line-height--reset']//span[text()='Accounts']") 
	private WebElement accountsHeader;
	
	@FindBy(xpath = "//div[@class='slds-breadcrumb__item slds-line-height--reset']//span[text()='Contacts']") 
	private WebElement contactsHeader;
	
	@FindBy(css = "a[title='New']")
	private WebElement newAccountBtn;
	
	@FindBy(xpath = "//label[text()='Account Name']/following-sibling::div/input[@type='text']")
	private WebElement accountNameInput;
	
	@FindBy(xpath = "//label[text()='Account Currency']/following-sibling::div//button[@type='button']")
    private WebElement accountCurrencyBtn;
	
	@FindBy(xpath = "//label[text()='Primary Country']/following-sibling::div//input[@type='text']")
	private WebElement primaryCountryInput;
	
	@FindBy(xpath = "//span[@class='test-id__field-label'][normalize-space()='Account Verification Status']")
	private WebElement accountVerificationStatusTxt;
	
	@FindBy(xpath = "//label[text()='Primary Street']/following-sibling::div//textarea")
	private WebElement primaryStreetTxtArea;
	
	@FindBy(xpath = "//label[text()='Primary City']/following-sibling::div//input")
	private WebElement primaryCityInput;
	
	@FindBy(xpath = "//label[text()='Primary State/Province']/following-sibling::div//input")
	private WebElement primaryStateInput;
	
	@FindBy(xpath = "//label[text()='Primary Zip/Postal Code']/following-sibling::div//input")
	private WebElement primaryZipCodeInput;
	
	@FindBy(xpath = "//button[@name='SaveEdit']")
	private WebElement saveEditBtn;
	
	@FindBy(xpath = "//span[@class='toastMessage slds-text-heading--small forceActionsText']")
	private WebElement successMsg;
	
	@FindBy(xpath = "//p[@title='Account Number']/following-sibling::p/slot/lightning-formatted-text")
	private WebElement accountNumberTxt;
	
	@FindBy(xpath = "//span[normalize-space()='Preferred Language']")
	private WebElement preferredLanguageTxt;
	
	@FindBy(xpath = "//div[span[text()='Account Verification Status']]/following-sibling::div//lightning-formatted-text[text()='Unverified']")
	private WebElement unverifiedStatusTxt;
	
	@FindBy(xpath = "//div[span[normalize-space()='MDM Integration Status']]")
	private WebElement mdmIntegrationStatusTxt;
	
	@FindBy(xpath = "//span[normalize-space()='Address Information']")
	private WebElement addressInfoSpan;
	
	@FindBy(xpath = "//div[@class='slds-page-header__name-switcher']//button[@title='Select a List View']")
	private WebElement listViewDropdownbutton;
	
	@FindBy(xpath = "//ul[@role='listbox']/li[2]/a[1]//span[@class=' virtualAutocompleteOptionText']")
	private WebElement allRecordsListLink;
	
	@FindBy(xpath = "//ul[@role='listbox']/li[3]/a[1]//span[@class=' virtualAutocompleteOptionText']")
	private WebElement myRecordsListLink;
	
	@FindBy(xpath = "//input[@name='Account-search-input']")
	private WebElement accountsSearchInputTextbox;
	
	@FindBy(xpath = "//input[@name='Contact-search-input']")
	private WebElement contactsSearchInputTextbox;
	
	@FindBy(xpath = "//h2[text()='Related List Quick Links']/parent::div/following::*//div//ul//a[contains(@href,'/related/Contacts/view')]//span[contains(text(), 'Contacts')]")
	private WebElement contactsQuickLink;
	
	@FindBy(xpath = "//div[@class='maincontent']//a/div[@title='New']")
	private WebElement newContactLinkFromQuickLinks;
	
	@FindBy(xpath = "//div[@class='isModal inlinePanel oneRecordActionWrapper']//h2[text()='New Contact']")
	private WebElement newContactWindow;
	
	@FindBy(xpath = "//label[text()='First Name']/parent::*//following-sibling::div/input[@name='firstName']")
	private WebElement firstNameInput;
	
	@FindBy(xpath = "//label[text()='Last Name']/parent::*//following-sibling::div/input[@name='lastName']")
	private WebElement lastNameInput;
	
	@FindBy(xpath = "//label[text()='Email']/parent::*//following-sibling::div/input[@name='Email']")
	private WebElement emailInput;
	
	@FindBy(xpath = "//h1/div[text()='Contact']")
	private WebElement contactDetailsPageHeader;
	
	@FindBy(xpath = "//flexipage-component2[@data-component-id='force_relatedListQuickLinksContainer']//a[contains(text(),'Show All')]")
	private WebElement showalllink;
	
	@FindBy(xpath = "//*[contains(text(),'Cases')]//parent::slot//parent::a")
	private WebElement caseslink;
	
	@FindBy(xpath = "(//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr[1]/th//span[@class='slds-grid slds-grid--align-spread']/a[@data-refid='recordId'])[2]")
	private WebElement casenumberlink;
	
	@FindBy(xpath = "(//input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup'])[2]")
	private WebElement changeownerinputtext;
	
	@FindBy(xpath = "//button[@name='ChangeOwnerOne']//parent::lightning-button")
	private WebElement accountchangeownerbutton;
	
	@FindBy(xpath = "//span[text()='Submit']//parent::button")
	private WebElement changeownerSubmitbutton;
	
	String newContact = "//span/a[@title='new contact']";
	
	
	// ================================================================================
	// Methods
	// ================================================================================	
 	public void navigateToAccountsPage() {
		CommonUtil.clickUsingJavaScriptExecutor(accountsTabLnk, "Accounts");
		CommonUtil.normalWait(10);
		CommonUtil.waitForVisible(accountsHeader,"Accounts Header");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
 	
 	public void navigateTocontactsPage() {
 		CommonUtil.clickUsingJavaScriptExecutor(contactsTabLnk, "Contacts");
		CommonUtil.normalWait(10);
		CommonUtil.waitForVisible(contactsHeader,"Contacts Header");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
 	}
	
	public void createAccount(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(newAccountBtn, 10, "New Button");
		CommonUtil.click(newAccountBtn, "New Button");
		CommonUtil.waitForElementVisibilityWithDuration(accountNameInput, 10, "Account Name input");
		int randomNumber= CommonUtil.generateRandomNumber(1, 10000);
		String dateAndTime = CommonUtil.getCurrentDate("dd/MMM/yyyy");
		String accountName = randomNumber+dataMap.get("AccountName")+dateAndTime;
		CommonUtil.inputText(accountNameInput, accountName, "account name : "+accountName);
		CommonUtil.normalWait(2);
		CommonUtil.click(accountCurrencyBtn, "Account Currency button");
		currencyOption = CommonUtil.findElement(currencyOptionStart+dataMap.get("Currency")+currencyOptionEnd, "xpath", "currency option");
		CommonUtil.click(currencyOption, "Currency option : "+dataMap.get("Currency"));
		CommonUtil.normalWait(2);
		CommonUtil.scrollIntoView(mdmIntegrationStatusTxt, "MDM Integration status");
		CommonUtil.click(primaryCountryInput, "Primary Country Input");
		currencyOption = CommonUtil.findElement(currencyOptionStart+dataMap.get("Country")+currencyOptionEnd, "xpath", "Country option");
		CommonUtil.scrollViewUpto(currencyOption, "Country option : "+dataMap.get("Country"));
		CommonUtil.click(currencyOption, "Country option : "+dataMap.get("Country"));
		CommonUtil.normalWait(2);
		CommonUtil.inputText(primaryStreetTxtArea, dataMap.get("Primary Street"),"Primary Street : "+dataMap.get("Primary Street"));
		CommonUtil.scrollIntoView(addressInfoSpan, "Address Information span");
		CommonUtil.inputText(primaryCityInput, dataMap.get("Primary City"),"Primary City : "+dataMap.get("Primary City"));
		CommonUtil.click(primaryStateInput, "Primary State Input");
		currencyOption = CommonUtil.findElement(currencyOptionStart+dataMap.get("State")+currencyOptionEnd, "xpath", "Primary State option");
		CommonUtil.scrollViewUpto(currencyOption, "Primary State option : "+dataMap.get("State"));
		CommonUtil.click(currencyOption, "Primary State option : "+dataMap.get("State"));
		CommonUtil.normalWait(2);
		CommonUtil.inputText(primaryZipCodeInput, dataMap.get("Zip Code"),"Primary Zip Code : "+dataMap.get("Zip Code"));
		CommonUtil.normalWait(2);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(saveEditBtn, "Save Button");
		CommonUtil.waitForElementVisibilityWithDuration(successMsg, 25, "Success message");
		Assert.assertTrue(CommonUtil.isElementDisplayed(successMsg, "Success message"));
		CommonUtil.storedata("Account Name", accountName);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String accountNumber = CommonUtil.getText(accountNumberTxt, "Account Number Text");
		CommonUtil.storedata("Account Number", accountNumber);
		dataMap.put("AccountName", CommonUtil.getStoredata("Account Name"));
		CommonUtil.scrollIntoView(preferredLanguageTxt, "Preferred Language Text");
		Assert.assertTrue(CommonUtil.isElementDisplayed(unverifiedStatusTxt,"Unverified Status Text"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		accountChangeOwner(dataMap);
		
		
	}
	
	//Validating the change owner
	public void accountChangeOwner(HashMap<String, String> dataMap) {
		accPage.navigatetoaccount(dataMap);
		CommonUtil.scrollViewUpto(showalllink, "Click on Show All Link");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(showalllink, "Click on Show All Link");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollViewUpto(caseslink, "Click on Cases Link");
		CommonUtil.isElementDisplayed(caseslink, "Click on Cases Link");
		CommonUtil.click(caseslink, "Click on Cases Link");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(casenumberlink, "Click on Case number");
		CommonUtil.normalWait(3);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(accountchangeownerbutton, "Click on changeownericon");
		CommonUtil.normalWait(3);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.inputStringText(changeownerinputtext, dataMap.get("ChangeOwnerName"), "Change Owner name Update");
		CommonUtil.normalWait(5);
		WebElement changeownername = CommonUtil.getWebElement("xpath","//div[@title='{replacevalue}']", dataMap.get("ChangeOwnerName"));
		CommonUtil.click(changeownername, "Select the Change Owner");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(changeownerSubmitbutton, "Click on changeowner Submit button");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	public void searchAccountandNavigateToAccountDetailsPage(String name) {
		navigateToAccountsPage();
		//navigate to AllAccounts View
		CommonUtil.click(listViewDropdownbutton, "accounts list view dropdown");
		CommonUtil.normalWait(5);
		CommonUtil.click(allRecordsListLink, "All accounts list view selection");
		CommonUtil.normalWait(20);
		CommonUtil.waitForElementVisibilityWithDuration(accountsSearchInputTextbox, 5, "search textbox");
		CommonUtil.inputTextWithEnterKey(accountsSearchInputTextbox, name);
		CommonUtil.normalWait(10);
		
		//verify account searched is displayed
		String accountLink  = "//div/table//span/a[@title='"+name+"']";
		WebElement ele = driver.findElement(By.xpath(accountLink));
		CommonUtil.waitForElementVisibilityWithDuration(ele, 25, "account search result: "+name);	
		
		CommonUtil.click(ele, "account name: "+name);
		CommonUtil.normalWait(20);	
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void navigateToAccountDetailsPageAndCreateNewContact(HashMap<String, String> dataMap) {
		searchAccountandNavigateToAccountDetailsPage(dataMap.get("AccountName"));
		//create new contact from related list quick links
		
		CommonUtil.click(contactsQuickLink, "contacts quick link");
		CommonUtil.waitForElementVisibilityWithDuration(newContactLinkFromQuickLinks, 10, "contact quick link window");
		CommonUtil.click(newContactLinkFromQuickLinks, "New button");
		CommonUtil.waitForElementVisibilityWithDuration(newContactWindow, 10, "New contact form");
		
		//Fill in contact details
		int randNum = CommonUtil.generateRandomNumber(100, 200);
		String fName = dataMap.get("FirstName")+randNum;
		
		//enter first name
		CommonUtil.inputText(firstNameInput, fName, "First Name");
		CommonUtil.scrollIntoView(lastNameInput, "Last Name");
		CommonUtil.waitForElementVisibilityWithDuration(lastNameInput, 5, "Last name");
		
		//enter last name
		String lName = dataMap.get("LastName")+randNum;
		CommonUtil.inputText(lastNameInput, lName, "Last Name");
		CommonUtil.scrollIntoView(emailInput, "Email");
		CommonUtil.waitForElementVisibilityWithDuration(emailInput, 5, "Email");
		
		String email = dataMap.get("Email");
		String [] arrEmail = email.split("@");
		String first = arrEmail[0] + "+" +randNum;
		String second = "@" + arrEmail[1];
		String newEmail = first.concat(second);
		
		CommonUtil.inputText(emailInput, newEmail, "email");
		CommonUtil.normalWait(5);
		CommonUtil.clickUsingJavaScriptExecutor(saveEditBtn, "save button");		
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
		//save contact
		String contact = fName+" "+lName;
		CommonUtil.setTestcaseName("navigateToAccountDetailsPageAndCreateNewContact");
		CommonUtil.storedata("Contact Name", contact);
		
	}

	public void navigateToContactDetailsPage(String contactName) {
		//search for contact under contacts tab
		navigateTocontactsPage();
		
		//navigate to All contacts View
		CommonUtil.click(listViewDropdownbutton, "records list view dropdown");
		CommonUtil.normalWait(5);
		CommonUtil.click(myRecordsListLink, "My contacts list view selection");
		CommonUtil.normalWait(20);
		CommonUtil.waitForElementVisibilityWithDuration(contactsSearchInputTextbox, 5, "search textbox");
		CommonUtil.inputTextWithEnterKey(contactsSearchInputTextbox, contactName);
		CommonUtil.normalWait(10);
		
		
		//navigate to contact details page
		String element = newContact.replace("new contact", contactName);
		WebElement ele = driver.findElement(By.xpath(element));
		CommonUtil.click(ele, "contact name: "+contactName);
		CommonUtil.waitForElementVisibilityWithDuration(contactDetailsPageHeader, 15, "Contact Details Page");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.normalWait(10);
		
	}
	
}
