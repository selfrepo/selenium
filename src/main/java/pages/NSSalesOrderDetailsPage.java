package pages;

import static org.testng.Assert.assertFalse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class NSSalesOrderDetailsPage extends CommonUtil {
	//Variables
	 private Map<String, String> mapRatableSupportItems = new HashMap<>();
	 private Map<String, HashMap<String, String>> mapRatableItems = new HashMap<String, HashMap<String, String>>();
	 private HashMap<String, String> mapRatableItemsDetails = new HashMap<>();
	 
	 //Objects
	 @FindBy(css = "#edit")
	 private WebElement editButton;
	 
	 @FindBy(css = "#btn_multibutton_submitter")
	 private WebElement saveButton;
	 
	 @FindBy(css = "a#cmmnctntabtxt")
	 private WebElement communicationsSubTab;
	 
	 @FindBy(css = "input#custbody_mf_invoice_emails")
	 private WebElement invoiceEmailAddress;
	 	 
	 @FindBy(css = "textarea#custbody_mf_inv_dist_details")
	 private WebElement manualInvoiceDistriTextbox;
	 
	 @FindBy(id = "spn_secondarymultibutton_submitter")
	 private WebElement saveSecondButton;
	 
	 @FindBy(css =  "#div__alert > div > div.icon.confirmation")
	 private WebElement confirmationAlert;
	 
	 @FindBy(css = ".uir-record-status")
	 private WebElement salesOrderStatus;
	 
	 @FindBy(css = "#itemstxt")
	 private WebElement itemsMenuHeader;
	 
	 @FindBy(xpath = "//*[@id='item_splits']/tbody/tr")
	 private List<WebElement> itemsMenuTableRows;
	 
	 //To get header row
	 @FindBy(xpath = "//*[@id='item_splits']/tbody/tr[1]/td")
	 private List<WebElement> itemsTableHeader;
	 
	 @FindBy(xpath = "//*[@id='custbody_mf_so_type_fs_lbl']/a")
	 private WebElement soTypeLabel;			//For scrolling purpose only
	 
	 @FindBy(xpath = "//*[@id='rlrcdstabtxt']")
	 private WebElement relatedRecordsTab;
	 
	 @FindBy(xpath = "//div[@class='uir-page-title-secondline']/div[@class='uir-record-id']")
	 private WebElement headerSONumber;
	 
	 @FindBy(xpath = "//div[@class='uir-page-title-secondline']/div[@class='uir-record-status']")
	 private WebElement statusValue;
	 
	 @FindBy(xpath = "//input[@value='Approve'][@id='custpage_so_approve']")
	 private WebElement approveButton;
	 
	 @FindBy(xpath = "//input[@value='Edit'][@id='edit']")
	 private WebElement editSOButton;
	 
	 @FindBy(xpath = "//input[@id='startdate']")
	 private WebElement startDate;
	 
	 @FindBy(xpath = "//input[@id='btn_multibutton_submitter']")
	 private WebElement saveDate;
	 
	//Methods
	public void editSalesOrder() { 
		CommonUtil.clickUsingJavaScriptExecutor(editButton, "Edit button");
		CommonUtil.waitForVisible(saveButton);	
		Assert.assertTrue(saveButton.isDisplayed(),"Edit SO page dispalyed");
		CommonUtil.normalWait(10);
		
	}
	
	public Map<String, String> getRatableSupportItemsMap(){
		return mapRatableSupportItems;
	}
	
	public Map<String, HashMap<String, String>> getRatableItemsHashMapWithKey(){
		return mapRatableItems;
	}
	
	public void editCommunicationsTab(HashMap<String, String> dataMap) { 
		CommonUtil.waitForVisible(communicationsSubTab);
		CommonUtil.clickUsingJavaScriptExecutor(communicationsSubTab, "Communications sub tab");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(invoiceEmailAddress);
		
		//edit invoice email address
		CommonUtil.inputText(invoiceEmailAddress,dataMap.get("Invoice Email Address"), "Invoice Email Address");
		CommonUtil.normalWait(2);
		
		//Enter Invoice Distribution Textbox
		String randomStr = RandomStringUtils.randomNumeric(5);
		CommonUtil.inputText(manualInvoiceDistriTextbox, "testing"+randomStr, "Invoice Distribution Textbox");
		
		//save SO
		CommonUtil.click(saveSecondButton, "click Save button");
		CommonUtil.normalWait(5);
		
		//verify confirmation message
		if(confirmationAlert.isDisplayed())
			LogUtil.infoLog(NSLoginPage.class,"Communications SubTab "+ "Details saved successfully");
		CommonUtil.attachScreenshotOfPassedTestsInReport();;
		
	}
	
	//Verify status of SO matches with expected value
	public void verifySalesOrderStatus(String expectedStatus) {
		String actualStatus = CommonUtil.verifyText(salesOrderStatus, expectedStatus, "SO Status");
		Assert.assertTrue(actualStatus.equals(expectedStatus));

	}
	
	//navigate to RelatedRecords sub tab
	public void navigateToRelatedRecords() {
		try {
			CommonUtil.scrollIntoView(soTypeLabel, "Related Records");
			CommonUtil.waitForVisible(relatedRecordsTab);
			relatedRecordsTab.click();
			CommonUtil.normalWait(5);
			
		}
		catch(Exception e) {
			LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"navigateToRelatedRecords -> Exception caught : "+e.getMessage(), e);
		}
	}

	
	//Verify if Asset Record are populated for all the SO lines
	public void verifyAssetRecordPopulated() {
		try {
			CommonUtil.scrollIntoView(itemsMenuHeader, "Items Menu Header");
			List<WebElement> tableHeaderRow = itemsTableHeader;
			
			LogUtil.infoLog(NSLoginPage.class, "\nItems Table column count : "+tableHeaderRow.size());
			
			List<String> allHeaderNames = new ArrayList<String>();
			for(WebElement header : tableHeaderRow) {
				LogUtil.infoLog(NSLoginPage.class, "\nHeader : "+header.getText());
				allHeaderNames.add(header.getText());
			}
			
			//Get Index and location of required columns
			int idxSrcTransactionKey = allHeaderNames.indexOf("SOURCE TRANSACTION LINE UNIQUE KEY");
			int idxAssetRecords = allHeaderNames.indexOf("ASSET RECORD");
			int idxItemName = allHeaderNames.indexOf("ITEM");
			int idxRate = allHeaderNames.indexOf("RATE");
			int idxRevenueStartDate = allHeaderNames.indexOf("REVENUE START DATE");
			int idxRevenueEndDate= allHeaderNames.indexOf("REVENUE END DATE");
			int idxTerm = allHeaderNames.indexOf("REVENUE TERM");
			int idxRevenueAllocationTerm = allHeaderNames.indexOf("REVENUE ALLOCATION TERM");
			
			//Get total Rows count
			List<WebElement> allRows = itemsMenuTableRows;
			LogUtil.infoLog(NSLoginPage.class,"\nTotal Rows : "+allRows.size());
			
			//Iterate through each row
			for( int i=1; i<allRows.size(); i++) {
				String locAssetRecord = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxAssetRecords+1)+"]";
				String locItemName = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxItemName+1)+"]";
				String locRate = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxRate+1)+"]";
				String locRevenueStartDate = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxRevenueStartDate+1)+"]";
				String locRevenueEndDate = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxRevenueEndDate+1)+"]";
				String locTerm = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxTerm+1)+"]";
				String locRevenueAllocationTerm = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxRevenueAllocationTerm+1)+"]";
				String locSrcTransactionKey = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxSrcTransactionKey+1)+"]";
			
				//Fetch a single row
				WebElement itemTableRow = allRows.get(i);
				
				String itemName = itemTableRow.findElement(By.xpath(locItemName)).getText();
				String itemRate = itemTableRow.findElement(By.xpath(locRate)).getText();
				String itemAssetRecord = itemTableRow.findElement(By.xpath(locAssetRecord)).getText();
				
				//Store item keys and item name into map, needed in verification of Item Fulfillments records
				if(!itemRate.equals("0.00")) {
					String itemSrcTransactionKey = itemTableRow.findElement(By.xpath(locSrcTransactionKey)).getText();
					String itemRevenueStartDate = itemTableRow.findElement(By.xpath(locRevenueStartDate)).getText();
					mapRatableItemsDetails.put("Revenue Start Date",itemRevenueStartDate);
					String itemRevenueEndDate = itemTableRow.findElement(By.xpath(locRevenueEndDate)).getText();
					mapRatableItemsDetails.put("Revenue End Date",itemRevenueEndDate);
					String itemTerm = itemTableRow.findElement(By.xpath(locTerm)).getText();
					mapRatableItemsDetails.put("Term",itemTerm);
					String itemRevenueAllocationTerm = itemTableRow.findElement(By.xpath(locRevenueAllocationTerm)).getText();
					mapRatableItemsDetails.put("Revenue Allocation Term",itemRevenueAllocationTerm);
					mapRatableSupportItems.put(itemSrcTransactionKey, itemName);
					mapRatableItems.put(itemSrcTransactionKey, mapRatableItemsDetails);
					
				}
				
				LogUtil.infoLog(NSLoginPage.class, "\n\nItem : "+itemName+" "+itemRate+" "+itemAssetRecord);				
				
				if(!itemRate.equals("0.00") && itemAssetRecord.trim().isEmpty()) {
					LogUtil.errorLog(NSLoginPage.class,"Asset Record in empty for non support item : "+itemName, null) ;
					assertFalse(itemRate.equals("0.00"),"Failed : "+itemName+" is non support item and has empty no Asset Record value");
				}
			}
		
			CommonUtil.normalWait(2);
		}
		catch(Exception e) {
			LogUtil.errorLog(NSSalesOrderDetailsPage.class, "Exception caught : "+e.getMessage(), e);
		}
	}


	public void approveSOAndVerifyStatus() {
		CommonUtil.waitForElementVisibilityWithDuration(headerSONumber, 5, "So Details page");
		try{
			if(statusValue.getText().equalsIgnoreCase("Pending Approval")) {
		       //verify approve button enabled
				if(approveButton.isEnabled()) {
					CommonUtil.clickUsingJavaScriptExecutor(approveButton, "approve button");
					CommonUtil.normalWait(30);			
				}
				else {//enter start date to enable approve button
					CommonUtil.click(editSOButton, "Edit button");
					CommonUtil.normalWait(2);
					CommonUtil.inputText(startDate, CommonUtil.getCurrentDate("dd/MM/yyyy"), "Current Date in Term Start Date");
					CommonUtil.normalWait(2);
					CommonUtil.click(saveButton, "Save button");
					CommonUtil.normalWait(20);
					if(approveButton.isEnabled()) {
						CommonUtil.clickUsingJavaScriptExecutor(approveButton, "approve button");
						CommonUtil.normalWait(20);
					}
				}
				CommonUtil.normalWait(10);
				Assert.assertTrue(statusValue.getText().equalsIgnoreCase("Pending Fulfillment"));
			}
			else if(statusValue.getText().equalsIgnoreCase("Pending Fulfillment")) {
				LogUtil.infoLog(getClass(), "SO status is already pending fulfillement");
			}
		} catch(Exception e) {
			LogUtil.errorLog(getClass(), "So required status not found", e);
		}
		
	}
}
