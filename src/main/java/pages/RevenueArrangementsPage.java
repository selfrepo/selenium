package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class RevenueArrangementsPage extends CommonUtil{
	
	@FindBy(xpath ="//a[normalize-space()='Accounting Book']")
	private WebElement accountingBook;
	
	@FindBy(xpath = "//h1[normalize-space()='Revenue Arrangement']")
	private WebElement revenueArrangementsHeader;
	
	@FindBy(css = "#viewrevenueplans")
	private WebElement viewRevenuePlansBtn;
	
	@FindBy(css = "#updaterevenuerecognitionplans_frame")
	private WebElement updateRevenuePlansIframe;
	
	@FindBy(css = "#editPlans")
	private WebElement editPlansBtn;
	
	@FindBy(xpath = "//table[@id='revenueplan_splits']/tbody/tr[contains(@id,'revenueplanrow')]")
	private List<WebElement> revenuePlanRows;
	
	@FindBy(xpath = "//table[@id='revenueplan_splits']/tbody/tr[contains(@id,'revenueplanrow')]/td[text()='Forecast' or text()='Actual']")
	private List<WebElement> revenuePlanTypeCells;
	
	@FindBy(xpath = "//span[span[a[text()='Number']]]/following-sibling::span")
	private WebElement numberTxtField;
	
	@FindBy(css = "#_back")
	private WebElement backBtn;
	
	@FindBy(css = "#secondary_cancel")
	private WebElement secondaryCancelBtn;
	
	@FindBy(xpath = "//a[normalize-space()='Rev Allocation ID']")
	private WebElement revAllocationIdFieldLnk;
	
	@FindBy(xpath = "//table[@id='plannedrevenue_splits']//tr[td[@data-label='Planned Period']]/following-sibling::tr/td[@data-ns-tooltip='PLANNED PERIOD']")
	private WebElement plannedPeriodCell;
	
	public void clickViewRevenuePlans(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(viewRevenuePlansBtn, 10, "View Revenue Plans Button");
		CommonUtil.click(viewRevenuePlansBtn, "View Revenue Plans Button");
		CommonUtil.waitForElementVisibilityWithDuration(updateRevenuePlansIframe, 10, "Update Revenue Plans Iframe");
		CommonUtil.attachScreenshotOfPassedTestsInReport();		
	}

	public void verifyRevenuePlansType(HashMap<String, String> dataMap) {
		CommonUtil.switchToFrame(updateRevenuePlansIframe, "Update Revenue Plans iframe");
		if(revenuePlanRows.size() == revenuePlanTypeCells.size()) {
		for(int i=1; i<revenuePlanRows.size();i++) {
			WebElement revenuePlanNumber = CommonUtil.findElement("//tbody/tr[@id='revenueplanrow"+(i-1)+"']/td[3]/a[1]", "xpath", "Revenue Plan Number in row "+i);
			CommonUtil.getText(revenuePlanNumber, "Revenue Plan Number text in row "+i);
			
			WebElement revenuePlanType = CommonUtil.findElement("//tbody/tr[@id='revenueplanrow"+(i-1)+"']/td[4]", "xpath", "Revenue Plan Type in row "+i);
			CommonUtil.getText(revenuePlanType, "Revenue Plan Type text in row "+i);
			
		  } 
	   }
		CommonUtil.switchToDefaultFrame();
	}
	
	public void clickOnForecastPlansTypeRevenueNumber(HashMap<String, String> dataMap) {
		CommonUtil.switchToFrame(updateRevenuePlansIframe, "Update Revenue Plans iframe");
		if(revenuePlanRows.size() == revenuePlanTypeCells.size()) {
		for(int i=1; i<revenuePlanRows.size();i++) {
			
			WebElement revenuePlanType = CommonUtil.findElement("//tbody/tr[@id='revenueplanrow"+(i-1)+"']/td[4]", "xpath", "Revenue Plan Type in row "+i);
			String revenuePlanTypeTxt = CommonUtil.getText(revenuePlanType, "Revenue Plan Type text in row "+i);
			if(revenuePlanTypeTxt.equals("Forecast"))
			{
			WebElement revenuePlanNumber = CommonUtil.findElement("//tbody/tr[@id='revenueplanrow"+(i-1)+"']/td[3]/a[1]", "xpath", "Revenue Plan Number in row "+i);
			String revenuePlanNumberTxt = CommonUtil.getText(revenuePlanNumber, "Revenue Plan number text in row "+i);
			CommonUtil.click(revenuePlanNumber, "Revenue Plan Number text in row "+i);
			CommonUtil.normalWait(5);
			CommonUtil.waitForElementVisibilityWithDuration(numberTxtField, 10, "Revenue Number text field");
			String numberTxt = CommonUtil.getText(numberTxtField, "Revenue Number text");
			Assert.assertTrue(revenuePlanNumberTxt.equals(numberTxt));
			CommonUtil.attachScreenshotOfPassedTestsInReport();
			break;
			}	
		  } 
	   }
		CommonUtil.switchToDefaultFrame();
	}
	
	public void clickOnActualPlansTypeRevenueNumber(HashMap<String, String> dataMap) {
		CommonUtil.switchToFrame(updateRevenuePlansIframe, "Update Revenue Plans iframe");
		if(revenuePlanRows.size() == revenuePlanTypeCells.size()) {
		for(int i=1; i<revenuePlanRows.size();i++) {
			
			WebElement revenuePlanType = CommonUtil.findElement("//tbody/tr[@id='revenueplanrow"+(i-1)+"']/td[4]", "xpath", "Revenue Plan Type in row "+i);
			String revenuePlanTypeTxt = CommonUtil.getText(revenuePlanType, "Revenue Plan Type text in row "+i);
			if(revenuePlanTypeTxt.equals("Actual"))
			{
			WebElement revenuePlanNumber = CommonUtil.findElement("//tbody/tr[@id='revenueplanrow"+(i-1)+"']/td[3]/a[1]", "xpath", "Revenue Plan Number in row "+i);
			String revenuePlanNumberTxt = CommonUtil.getText(revenuePlanNumber, "Revenue Plan number text in row "+i);
			CommonUtil.click(revenuePlanNumber, "Revenue Plan Number text in row "+i);
			CommonUtil.normalWait(5);
			CommonUtil.waitForElementVisibilityWithDuration(numberTxtField, 10, "Revenue Number text field");
			String numberTxt = CommonUtil.getText(numberTxtField, "Revenue Number text");
			Assert.assertTrue(revenuePlanNumberTxt.equals(numberTxt));
			CommonUtil.attachScreenshotOfPassedTestsInReport();
			break;
			}	
		  } 
	   }
		CommonUtil.switchToDefaultFrame();
	}
	
	public void clickOnBackBtnOnUpdateRevenuePlansPage() {
		CommonUtil.switchToFrame(updateRevenuePlansIframe, "Update Revenue Plans iframe");
		CommonUtil.waitForElementVisibilityWithDuration(backBtn, 10, "Back Button");
		CommonUtil.click(backBtn, "Back Button");
		CommonUtil.waitForElementVisibilityWithDuration(secondaryCancelBtn, 10, "Secondary Cancel Button");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.switchToDefaultFrame();
		
	}
	
	public void verifyPlannedPeriod() {
		CommonUtil.switchToFrame(updateRevenuePlansIframe, "Update Revenue Plans iframe");
		CommonUtil.scrollIntoView(revAllocationIdFieldLnk,"Revenue Allocation ID field");
		CommonUtil.waitForElementVisibilityWithDuration(plannedPeriodCell, 5, "Planned period cell");
		String plannedPeriodTxt = CommonUtil.getText(plannedPeriodCell, "Planned period cell text");
		if(plannedPeriodTxt.isEmpty()) {
			LogUtil.errorLog(getClass(), "Planned period is empty", null);
		}
		else {
			LogUtil.infoLog(getClass(), "Planned period is :" +plannedPeriodTxt);
		}
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.switchToDefaultFrame();
	}
}
