package pages;

import static org.testng.Assert.assertFalse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class NSSalesOrderRelatedRecordsPage extends CommonUtil {
	
	Map<String, HashMap<String, String>> itemsInfo;
	
	//There is a another tab 'Related Records' inside 'Related Records' tab
	@FindBy(xpath = "//*[@id='linkstxt']")
	private WebElement relatedRecordsSubTab;

	@FindBy(xpath = "//*[@id='links_splits']/tbody/tr[1]/td")
	private List<WebElement> relatedRecordsTableHeaderRow;
	
	@FindBy(xpath = "//*[@id='links_splits']/tbody/tr")
	private List<WebElement> relatedRecordsAllRows;
	
	@FindBy(xpath = "//*[@id='itemheader']/td/div")			
	private List<WebElement> itemFulfillmentItemsHeaderRow;	
	
	@FindBy(xpath = "//*[@id='item_splits']/tbody/tr")
	private List<WebElement> itemFulfillmentItemsAllRows;
	
	@FindBy(css = "#edit")
    private WebElement editBtn;
	
	@FindBy(xpath = "//*[@id='revenueelement_splits']/tbody/tr[1]/td")			
	private List<WebElement> revenueElementsHeaderRow;	
	
	@FindBy(xpath = "//*[@id='revenueelement_splits']/tbody/tr")										
	private List<WebElement> revenueElementsAllRows;
	
	@FindBy(xpath = "//*[@id='cke_1_contents']/iframe")
	private WebElement extendedMemoIFrame;
	
	@FindBy(xpath = "//html/body[@role='textbox']")																			
	private WebElement txtboxExtendedMemo;
	
	@FindBy(xpath = "//*[@id='cmmnctntabtxt']")
	private WebElement communicationSubTab;
	
	@FindBy(xpath = "//*[@id='mediaitemtxt']")
	private WebElement commFilesSubTab;
	
	@FindBy(xpath = "//*[@id='mediaitem_mediaitem_fs']/div/input")
	private WebElement attachExistingFileTextBox;
	
	@FindBy(css = "#secondaryedit")
	private WebElement secondaryEditBtn;
	
	@FindBy(xpath = "//span[@id='mediaitem_mediaitem_fs']//div[@class='uir-select-input-container']//a[2]")
	private WebElement attachExistingFileDropDownIcon;
	
	@FindBy(id = "mediaitem_popup_list")
	private WebElement popUPListLnk;
	
	@FindBy(id = "og")
    private WebElement selectTag;
	
	@FindBy(xpath = "//a[contains(text(),'QUOTE # Q-')]")
	private WebElement quoteFileLnk;
	
	@FindBy(id = "addmediaitem")
	private WebElement attachBtn;
	
	@FindBy(xpath = "//tbody[tr[td[div[contains(text(),'Attached Files')]]]]/tr[2]/td[1]/a")
	private List<WebElement> attachedFileInTableLnk;
	
	@FindBy(id = "btn_secondarymultibutton_submitter")
	private WebElement secondarySaveBtn;
	
	@FindBy(xpath = "//span[@id='custbody_mf_ra_extended_memo_fs_lbl_uir_label']/following-sibling::span[@class='uir-field inputreadonly uir-user-styled uir-resizable']")
	private WebElement extendedMemoValueSpan;
	
	@FindBy(xpath = "//div[normalize-space()='Classification']")
	private WebElement classificationDiv;
	
	//Verify Item Fulfillment Record is present in Related Records > Related Records and the status should be 'Shipped'
		public void verifyItemFulfillmentRecord() {
			try {
				LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"Is Related Record displayed : "+CommonUtil.isElementDisplayed(relatedRecordsSubTab, "Related Records Sub Tab"));
				CommonUtil.normalWait(5);
				CommonUtil.waitForVisible(relatedRecordsSubTab);
				relatedRecordsSubTab.click();
				//Fetch index of column TYPE,STATUS and DATE
				
				int idxType = -1;
				int idxStatus = -1;
				int idxDate = -1;				//Index of date is required to navigate to Item fulfillment details
				for(int i=0; i<relatedRecordsTableHeaderRow.size(); i++) {
					WebElement row = relatedRecordsTableHeaderRow.get(i);
					String valColumn = row.findElement(By.xpath("//*[@id='links_splits']/tbody/tr[1]/td["+(i+1)+"]/div")).getText().trim();
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nHeader Value : "+valColumn);
					if(valColumn.equals("TYPE"))
						idxType = i;
					else if(valColumn.equals("STATUS"))
						idxStatus = i;
					else if(valColumn.equals("DATE"))
						idxDate = i;
				}
				
				LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\n\nrelatedRecordsTableHeaderRow- size : "+relatedRecordsTableHeaderRow.size()+" value : "+relatedRecordsTableHeaderRow.toString());
				if(idxType == -1 || idxStatus == -1)
					throw new Exception("Couldn't find index of Column TYPE/STATUS");
				
				if( relatedRecordsAllRows.size() == 0)
					throw new Exception("Related Records table not found");
				
				//Iterate through each row
				for( int i=1; i<relatedRecordsAllRows.size(); i++) {
					
					//Fetch a single row
					WebElement relatedRecordRow = relatedRecordsAllRows.get(i);
					
					
					String xPathType = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxType+1)+"]";
					String valType = relatedRecordRow.findElement(By.xpath(xPathType)).getText();
					
					if( valType.trim().length() == 0)
						throw new Exception("Value under Type field is empty");
					
					if( valType.equals("Item Fulfillment")) {
					
						//Define Xpath for column 'Status'
						String xPathStatus = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxStatus+1)+"]";
						String valStatus = relatedRecordRow.findElement(By.xpath(xPathStatus)).getText();
						
						//Verify status
						if( valStatus.equals("Shipped")) {
							LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nPASSED : Expected Status : Shipped. Actual Status : "+valStatus);
						}
						else {
							LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"\nFAILED : Expected Status : Shipped. Actual Status : "+valStatus, null);
							throw new Exception("Item Fulfillment status should be shipped");
						}
						
						//Navigate to Item Fulfillment details
						String xPathDate = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxDate+1)+"]/a";
						WebElement date = relatedRecordRow.findElement(By.xpath(xPathDate));
						
						LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class, "WebElement Date is Displayed? : "+date.isDisplayed());
						date.click();
						break;		
						}
				}
				
				CommonUtil.normalWait(5);
			}
			catch(Exception e) {
				LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"verifyItemFulfillmentRecord -> Exception caught : "+e.getMessage(), e);
			}
		}
	
	//Verify 'recordType' of Record present in Related Records and the status should be 'recordStatus'
	public void verifyRelatedRecordTypeAndStatus(String recordType, String recordStatus) {
		try {
			LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"Is Related Record displayed : "+CommonUtil.isElementDisplayed(relatedRecordsSubTab, "Related Records Sub Tab"));
			CommonUtil.normalWait(5);
			CommonUtil.waitForVisible(relatedRecordsSubTab);
			relatedRecordsSubTab.click();
			//Fetch index of column TYPE,STATUS and DATE
			
			int idxType = -1;
			int idxStatus = -1;
			int idxDate = -1;				//Index of date is required to navigate to Item fulfillment details
			for(int i=0; i<relatedRecordsTableHeaderRow.size(); i++) {
				WebElement row = relatedRecordsTableHeaderRow.get(i);
				String valColumn = row.findElement(By.xpath("//*[@id='links_splits']/tbody/tr[1]/td["+(i+1)+"]/div")).getText().trim();
				LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nHeader Value : "+valColumn);
				if(valColumn.equals("TYPE"))
					idxType = i;
				else if(valColumn.equals("STATUS"))
					idxStatus = i;
				else if(valColumn.equals("DATE"))
					idxDate = i;
			}
			
			LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\n\nrelatedRecordsTableHeaderRow- size : "+relatedRecordsTableHeaderRow.size()+" value : "+relatedRecordsTableHeaderRow.toString());
			if(idxType == -1 || idxStatus == -1)
				throw new Exception("Couldn't find index of Column TYPE/STATUS");
			
			if( relatedRecordsAllRows.size() == 0)
				throw new Exception("Related Records table not found");
			
			//Iterate through each row
			for( int i=1; i<relatedRecordsAllRows.size(); i++) {
				
				//Fetch a single row
				WebElement relatedRecordRow = relatedRecordsAllRows.get(i);
				
				
				String xPathType = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxType+1)+"]";
				String valType = relatedRecordRow.findElement(By.xpath(xPathType)).getText();
				
				if( valType.trim().length() == 0)
					throw new Exception("Value under Type field is empty");
				
				if( valType.equals(recordType)) {
					
					//Define Xpath for column 'Status'
					String xPathStatus = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxStatus+1)+"]";
					String valStatus = relatedRecordRow.findElement(By.xpath(xPathStatus)).getText();
					
					//Verify status
					if( valStatus.equals(recordStatus)) {
						LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nPASSED : Expected Status : "+recordStatus+". Actual Status : "+valStatus);
					}
					else {
						LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"\nFAILED : Expected Status : "+recordStatus+". Actual Status : "+valStatus, null);
						throw new Exception("Item Fulfillment status should be shipped");
					}
					
					//Navigate to Item Fulfillment details
					String xPathDate = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxDate+1)+"]/a";
					WebElement date = relatedRecordRow.findElement(By.xpath(xPathDate));
					
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class, "WebElement Date is Displayed? : "+date.isDisplayed());
					date.click();
					break;		
				}
			}			
			CommonUtil.normalWait(5);
		}
		catch(Exception e) {
			LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"verifyItemFulfillmentRecord -> Exception caught : "+e.getMessage(), e);
		}
	}
	
	
	
	public void verifyItemFulfillmentDates(Map<String, String> mapRatableSupportItems) {
		try {
			
			CommonUtil.normalWait(5);
			
			int idxSrcTransactionKey = -1;
			int idxStartDate = -1;
			int idxEndDate = -1;
			for(int i=0; i<itemFulfillmentItemsHeaderRow.size(); i++) {
				WebElement row = itemFulfillmentItemsHeaderRow.get(i);
				String valColumn = row.findElement(By.xpath("//*[@id='itemheader']/td["+(i+1)+"]/div")).getText().trim();
				LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nHeader Value : "+valColumn);
				if(valColumn.equals("SOURCE TRANSACTION LINE UNIQUE KEY"))
					idxSrcTransactionKey = i;
				else if(valColumn.equals("REVENUE START DATE"))
					idxStartDate = i;
				else if(valColumn.equals("REVENUE END DATE"))
					idxEndDate = i;
			}
			
			//Verify if mapRatableSupportItems is empty or null
			if(mapRatableSupportItems.isEmpty())
				throw new Exception("No data found in mapRatableSupportItems");
			
			//Navigate and verify start and end dates for all Ratable records. Ratable records are stored in 'mapRatableSupportItems'
			for(int i=1; i<itemFulfillmentItemsAllRows.size(); i++) {
				String xPathSrcTransactionKey = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxSrcTransactionKey+1)+"]";
			
				//Fetch Each Row record
				WebElement row = itemFulfillmentItemsAllRows.get(i);
				String valSrcTransactionKey = row.findElement(By.xpath(xPathSrcTransactionKey)).getText();
				
				if(mapRatableSupportItems.containsKey(valSrcTransactionKey)) {
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class, "Record ID "+valSrcTransactionKey+" is Ratable record");
					String xPathStartDate = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxStartDate+1)+"]";
					String xPathEndDate = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxEndDate+1)+"]";
					
					String valStartDate = row.findElement(By.xpath(xPathStartDate)).getText();
					String valEndDate = row.findElement(By.xpath(xPathEndDate)).getText();
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class, "Record ID "+valSrcTransactionKey+" : Start Date-"+valStartDate+"; End Date-"+valEndDate);
					
					if( valStartDate.trim().length() == 0 || valEndDate.trim().length() == 0) {
						LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"Revenue start date or end date is empty", null);
						break;
					}
				}
			}
			
		}
		catch(Exception e) {
			LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"verifyItemFulfillmentRecord -> Exception caught : "+e.getMessage(), e);
		}
	}

	
	//To verify fields of RevenueArrangement
	public void verifyRevenueElementsDetails(Map<String, String> mapRatableSupportItems, Map<String, HashMap<String, String>> mapRatableItems) {
		try {
			
			CommonUtil.normalWait(5);
			
			int idxSrcTransactionKey = -1;
			int idxAssetRecord = -1;
			int idxStartDate = -1;
			int idxEndDate = -1;
			int idxForecastStartDate = -1;
			int idxForecastEndDate = -1;
			int idxRevAllocTerm = -1;
			int idxTerm = -1;
			for(int i=0; i<revenueElementsHeaderRow.size(); i++) {
				WebElement row = revenueElementsHeaderRow.get(i); 
				String valColumn = row.findElement(By.xpath("//*[@id='revenueelement_splits']/tbody/tr[1]/td["+(i+1)+"]/div")).getText().trim();
				LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nHeader Value : "+valColumn);
				if(valColumn.equals("SOURCE TRANSACTION LINE UNIQUE KEY"))
					idxSrcTransactionKey = i;
				else if(valColumn.equals("ASSET RECORD"))
					idxAssetRecord = i;
				else if(valColumn.equals("START DATE"))
					idxStartDate = i;
				else if(valColumn.equals("END DATE"))
					idxEndDate = i;
				else if(valColumn.equals("FORECAST START DATE"))
					idxForecastStartDate = i;
				else if(valColumn.equals("FORECAST END DATE"))
					idxForecastEndDate = i;
				else if(valColumn.equals("REVENUE ALLOCATION TERM"))
						idxRevAllocTerm = i;
				else if(valColumn.equals("TERM"))
					idxTerm = i;
			}
			
			//Verify if mapRatableSupportItems is empty or null
			if(mapRatableSupportItems.isEmpty())
				throw new Exception("No data found in mapRatableSupportItems");
			itemsInfo = mapRatableItems;
			//Navigate and verify details for all Ratable records. Ratable records are stored in 'mapRatableSupportItems'
			for(int i=1; i<revenueElementsAllRows.size(); i++) {
				String xPathSrcTransactionKey = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxSrcTransactionKey+1)+"]";
			
				//Fetch Each Row record
				WebElement row = revenueElementsAllRows.get(i);
				String valSrcTransactionKey = row.findElement(By.xpath(xPathSrcTransactionKey)).getText();
				if(mapRatableSupportItems.containsKey(valSrcTransactionKey)) {
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class, "Record ID "+valSrcTransactionKey+" is Ratable record");
					
					String xPathAssetRecord = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxAssetRecord+1)+"]";
					String xPathStartDate = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxStartDate+1)+"]";
					String xPathEndDate = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxEndDate+1)+"]";
					String xPathForecastStartDate = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxForecastStartDate+1)+"]";
					String xPathForecastEndDate = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxForecastEndDate+1)+"]";
					String xPathRevAllocTerm = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxRevAllocTerm+1)+"]";
					String xPathTerm = "//*[@id='revenueelement_splits']/tbody/tr["+(i+1)+"]/td["+(idxTerm+1)+"]";
					
					
					String valAssetRecord = row.findElement(By.xpath(xPathAssetRecord)).getText();
					String valStartDate = row.findElement(By.xpath(xPathStartDate)).getText();
					String valEndDate = row.findElement(By.xpath(xPathEndDate)).getText();
					String valForecastStartDate = row.findElement(By.xpath(xPathForecastStartDate)).getText();
					String valForecastEndDate = row.findElement(By.xpath(xPathForecastEndDate)).getText();
					String valRevAllocTerm = row.findElement(By.xpath(xPathRevAllocTerm)).getText();
					String valTerm = row.findElement(By.xpath(xPathTerm)).getText();
					
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class, "Record ID "+valSrcTransactionKey+" : Start Date-"+valStartDate
							+"; End Date-"+valEndDate+" : ForecastStartDate-"+valForecastStartDate+"; Foreast End Date-"+valForecastStartDate
							+" : Revenue Allocation Term-"+valRevAllocTerm+" : Term-"+valTerm+" : Asset Record-"+valAssetRecord);
					
					assertFalse(valAssetRecord.trim().length() == 0, "FAIL - Asset Record is Empty");
					assertFalse(valStartDate.trim().length() == 0, "FAIL - Start Date is Empty");

					assertFalse(valEndDate.trim().length() == 0, "FAIL - End Date is Empty");
					assertFalse(valForecastStartDate.trim().length() == 0, "FAIL - Forecast Start Date is Empty");
		
					assertFalse(valForecastEndDate.trim().length() == 0, "FAIL - Forecast End Date is Empty");

					assertFalse(valRevAllocTerm.trim().length() == 0, "FAIL - Revenue Allocation Term is Empty");
					assertFalse(valRevAllocTerm.trim().equals("0"), "FAIL - Revenue Allocation Term is Zero");
					
					assertFalse(valTerm.trim().length() == 0, "FAIL - Term is Empty");
					assertFalse(valTerm.trim().equals("0"), "FAIL - Term is Zero");
					for(int k=0; k< itemsInfo.size(); k++) {
						if(itemsInfo.containsKey(valSrcTransactionKey))
						{
						Map<String, String> items = itemsInfo.get(valSrcTransactionKey);
						String startDate = items.get("Revenue Start Date").trim();
						Assert.assertTrue(startDate.equals(valStartDate.trim()), "Verifying Start date of ratable item with SO for key : "+valSrcTransactionKey);
						String endDate = items.get("Revenue End Date").trim();
						Assert.assertTrue(endDate.equals(valEndDate), "Verifying End date of ratable item with SO for key : "+valSrcTransactionKey);
						String term = items.get("Term").trim();
						Assert.assertTrue(term.equals(valTerm), "Verifying Term of ratable item with SO for key : "+valSrcTransactionKey);
						String revenueAllocationTerm = items.get("Revenue Allocation Term").trim();
						Assert.assertTrue(revenueAllocationTerm.equals(valRevAllocTerm), "Verifying Revenue Allocation Term of ratable item with SO for key : "+valSrcTransactionKey);
					    break;
						}
					}

				}
			}
			
		
			
		}
		catch(Exception e) {
			LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"verifyItemFulfillmentRecord -> Exception caught : "+e.getMessage(), e);
		}
	}

	
	public void editAddMemoAndFileToRevenueArrangement(HashMap<String, String> dataMap) {
		
		CommonUtil.scrollViewUpto(secondaryEditBtn, "Edit button");
		CommonUtil.waitForVisible(secondaryEditBtn);
		CommonUtil.click(secondaryEditBtn, "Edit Button");
		
		String memoMessage = dataMap.get("Memo");
		CommonUtil.waitForElementVisibilityWithDuration(extendedMemoIFrame, 10, "Extended Memo IFrame");
		CommonUtil.switchToFrame(extendedMemoIFrame, "Extended Memo IFrame");
		CommonUtil.waitForElementVisibilityWithDuration(txtboxExtendedMemo, 10, "Extended Memo Textbox");
		CommonUtil.inputStringText(txtboxExtendedMemo, memoMessage, "Data Entered into Extended memo");
		CommonUtil.switchToDefaultFrame();
		
		CommonUtil.scrollViewUpto(communicationSubTab, "Communication Tab");
		CommonUtil.click(communicationSubTab, "Communication Tab");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(commFilesSubTab, "Files Tab under Communications Tab");
		CommonUtil.click(commFilesSubTab, "Files Tab");
		CommonUtil.waitForElementVisibilityWithDuration(attachExistingFileDropDownIcon, 10, "Attach file type drop down icon");
		CommonUtil.click(attachExistingFileDropDownIcon, "Attach file type drop down icon");
		CommonUtil.waitForElementVisibilityWithDuration(popUPListLnk, 5, "Pop up List Link");
		CommonUtil.click(popUPListLnk, "Pop up List Link");
		CommonUtil.waitForElementVisibilityWithDuration(selectTag, 5, "Select all tag");
		String quoteFileTxt = CommonUtil.getText(quoteFileLnk, "Quote File link text").trim();
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(quoteFileLnk,"Quote File link");
		CommonUtil.waitForElementVisibilityWithDuration(attachBtn, 5, "Attach Button");
		CommonUtil.click(attachBtn, "Attach Button");
		for(int i=1; i<= attachedFileInTableLnk.size(); i++ )
		{
		WebElement ele = CommonUtil.findElement("//tbody[tr[td[div[contains(text(),'Attached Files')]]]]/tr[2]/td[1]/a["+i+"]", "xpath", "Getting Element on row "+i);
		CommonUtil.waitForElementVisibilityWithDuration(ele, 5, "Attach File in table link");
		String quoteFileInTableTxt = CommonUtil.getText(ele, "Attached File in table link text").trim();
	    if(quoteFileTxt.equals(quoteFileInTableTxt))
	    {
	    	LogUtil.infoLog(getClass(), "File is being attached : "+quoteFileTxt);
	    	CommonUtil.attachScreenshotOfPassedTestsInReport();
	    	break;
	    }
	    else
	    {
	    	LogUtil.errorLog(getClass(), "Selected file is not attached successfully");
	    	CommonUtil.attachScreenshotOfPassedTestsInReport();
	    }
		}
		
	    CommonUtil.click(secondarySaveBtn, "Secondary Button");
	    CommonUtil.normalWait(2);
	    CommonUtil.scrollIntoView(classificationDiv,"Classification div");
	    CommonUtil.waitForElementVisibilityWithDuration(extendedMemoValueSpan, 10, "Extended Memo value span");
	    String extendedMemoSpanTxt = CommonUtil.getText(extendedMemoValueSpan,"Extended Memo value span");
	    Assert.assertTrue(extendedMemoSpanTxt.equals(memoMessage), "Checking extended memo value");
	    CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollViewUpto(communicationSubTab, "Communication Tab");
		CommonUtil.click(communicationSubTab, "Communication Tab");
		
		CommonUtil.waitForVisible(commFilesSubTab, "Files Tab under Communications Tab");
		CommonUtil.click(commFilesSubTab, "Files Tab");
		for(int j=1; j<= attachedFileInTableLnk.size(); j++ )
		{
		WebElement savedAttachedFileLnk = CommonUtil.findElement("//tbody[tr[td[div[contains(text(),'Attached Files')]]]]/tr[2]/td[1]/a["+j+"]", "xpath", "Getting Element after saving on row "+j);	
		CommonUtil.waitForElementVisibilityWithDuration(savedAttachedFileLnk, 5, "Attach File in table link");
		String quoteFileAfterSavingInTableTxt = CommonUtil.getText(savedAttachedFileLnk, "Attached File in table link text").trim();
	    if(quoteFileAfterSavingInTableTxt.equals(quoteFileTxt))
	    {
	    	LogUtil.infoLog(getClass(), "File is being attached after saving : "+quoteFileAfterSavingInTableTxt);
	    	CommonUtil.attachScreenshotOfPassedTestsInReport();
	    	break;
	    }
	    else
	    {
	    	LogUtil.errorLog(getClass(), "Selected file is not attached successfully after clicking save button");
	    	CommonUtil.attachScreenshotOfPassedTestsInReport();
	    }
	}
	}
	
	//Verify Item Fulfillment Record is present in Related Records > Related Records and the status should be 'Shipped'
			public void verifyItemFulfillmentRecordWithoutClickingOnIt() {
				try {
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"Is Related Record displayed : "+CommonUtil.isElementDisplayed(relatedRecordsSubTab, "Related Records Sub Tab"));
					CommonUtil.normalWait(5);
					CommonUtil.waitForVisible(relatedRecordsSubTab);
					relatedRecordsSubTab.click();
					//Fetch index of column TYPE,STATUS and DATE
					
					int idxType = -1;
					int idxStatus = -1;
					int idxDate = -1;				//Index of date is required to navigate to Item fulfillment details
					for(int i=0; i<relatedRecordsTableHeaderRow.size(); i++) {
						WebElement row = relatedRecordsTableHeaderRow.get(i);
						String valColumn = row.findElement(By.xpath("//*[@id='links_splits']/tbody/tr[1]/td["+(i+1)+"]/div")).getText().trim();
						LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nHeader Value : "+valColumn);
						if(valColumn.equals("TYPE"))
							idxType = i;
						else if(valColumn.equals("STATUS"))
							idxStatus = i;
						else if(valColumn.equals("DATE"))
							idxDate = i;
					}
					
					LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\n\nrelatedRecordsTableHeaderRow- size : "+relatedRecordsTableHeaderRow.size()+" value : "+relatedRecordsTableHeaderRow.toString());
					if(idxType == -1 || idxStatus == -1)
						throw new Exception("Couldn't find index of Column TYPE/STATUS");
					
					if( relatedRecordsAllRows.size() == 0)
						throw new Exception("Related Records table not found");
					
					//Iterate through each row
					for( int i=1; i<relatedRecordsAllRows.size(); i++) {
						
						//Fetch a single row
						WebElement relatedRecordRow = relatedRecordsAllRows.get(i);
						
						
						String xPathType = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxType+1)+"]";
						String valType = relatedRecordRow.findElement(By.xpath(xPathType)).getText();
						
						if( valType.trim().length() == 0)
							throw new Exception("Value under Type field is empty");
						
						if( valType.equals("Item Fulfillment")) {
						
							//Define Xpath for column 'Status'
							String xPathStatus = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxStatus+1)+"]";
							String valStatus = relatedRecordRow.findElement(By.xpath(xPathStatus)).getText();
							
							//Verify status
							if( valStatus.equals("Shipped")) {
								LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nPASSED : Expected Status : Shipped. Actual Status : "+valStatus);
							}
							else {
								LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"\nFAILED : Expected Status : Shipped. Actual Status : "+valStatus, null);
								throw new Exception("Item Fulfillment status should be shipped");
							}
							
							//Navigate to Item Fulfillment details
							String xPathDate = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxDate+1)+"]/a";
							WebElement date = relatedRecordRow.findElement(By.xpath(xPathDate));
							LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class, "WebElement Date is Displayed? : "+date.isDisplayed());
							break;		
							}
					}
					
					CommonUtil.normalWait(5);
				}
				catch(Exception e) {
					LogUtil.errorLog(NSSalesOrderRelatedRecordsPage.class,"verifyItemFulfillmentRecord -> Exception caught : "+e.getMessage(), e);
				}
			}
}
