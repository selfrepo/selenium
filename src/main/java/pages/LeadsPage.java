package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import junit.framework.Assert;
import utilities.CommonUtil;

public class LeadsPage extends CommonUtil {

	// Variables
	private String LeadData;

	// Objects
	@FindBy(xpath = "//a[text()='Details']")
	private WebElement DetailsClick;

	@FindBy(xpath = "//*[text()='Lead Owner']//following::lightning-formatted-name[1]")
	private WebElement CaptureCreatedLead;

	@FindBy(xpath = "(//span[@title='Product Group Interests'][normalize-space()='Product Group Interests'])[1]")
	private WebElement relatedProductGroupInterests;

	@FindBy(xpath = "//span[normalize-space()='Product Groups of Interest']")
	private WebElement pgiDropDown;
	
	@FindBy(xpath = "//a[@id='relatedListsTab__item']")
	private WebElement relatedTab;
	
	String campaignHistoryZerotext= "//span[@class='slds-truncate slds-m-right--xx-small'][@title='Campaign History']/following-sibling::span[text()='(0)']";

	// Methods
	public void leadPagePGIClick(HashMap<String, String> dataMap) {

		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(DetailsClick, "Details Tab");
		LeadData = CommonUtil.verifyText(CaptureCreatedLead, dataMap.get("CreatedLead"), "Lead Created");
		Assert.assertEquals(LeadData, dataMap.get("CreatedLead"));
		if(CommonUtil.getAllElements(campaignHistoryZerotext, "Campaign History").size()!=1)
			{
			CommonUtil.scrollIntoView(relatedTab, "Related tab");
			}
		CommonUtil.waitForVisible(relatedProductGroupInterests, "ProductGroupInterests");
		CommonUtil.click(relatedProductGroupInterests, "ProductGroupInterests");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
