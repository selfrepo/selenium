package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import junit.framework.Assert;
import utilities.CommonUtil;

public class ProductGroupInterestsPage extends CommonUtil {
	
	String pgiColumnNameStart = "//table[@aria-label = 'Product Group Interests']//thead//tr[1]//th[" ;
	String pgiRowNameStart = "//table[@aria-label = 'Product Group Interests']//tbody[1]//tr[";
	String pgiColumnNameEnd = "]" ; 
	String pgiRowElementLnkStart = "//table[@aria-label = 'Product Group Interests']//tbody[1]//tr[";
	String pgiRowElementLnkMiddle = "]//td[2]/span | //table[@aria-label = 'Product Group Interests']//tbody[1]//tr[";
	String pgiRowPrimaryCampaignLnkEnd = "]//td[2]/span[1]/a[1]";
	String pgiRowElementLnkEnd = "]//th[1]//span[1]//a[1]" ;
	String rowPGI = "//table[@aria-label = 'Product Group Interests']//tbody[1]//tr";
	String colPGI ="//table[@aria-label = 'Product Group Interests']//tbody[1]//tr[1]//th | //table[@aria-label = 'Product Group Interests']//tbody[1]//tr[1]//td";
	
	
	@FindBy(xpath = "//lightning-formatted-text[@slot = 'primaryField']")
	private WebElement eleexpPGI;

	@FindBy(xpath = "//div[span[text()='Owner']]/following-sibling::div//lightning-button-icon[button[@title='Change Owner']]/preceding-sibling::span//slot")
	private WebElement ChangeOwnerVal;
	
	@FindBy(xpath = "//div[span[text()='Owner']]/following-sibling::div//lightning-button-icon[button[contains(@title,'Preview')]]/preceding-sibling::a//span")
	private WebElement ChangeOwnerLnkVal;

	 @FindBy(xpath = "//div[span[text()='Owner']]/following-sibling::div//lightning-button-icon[button[@title='Change Owner']]")
	private WebElement iconChangeOwner;

	@FindBy(xpath = "//input[@title='Search Users']")
	private WebElement eleSearchUser;

	@FindBy(xpath = "//ul[@class='lookup__list  visible']//following::div[@title='Testuser LDR']")
	private WebElement clickTestUserLDR;
	
	@FindBy(xpath = "//ul[@class='lookup__list  visible']//following::div[@title='Testuser Sales_Rep']")
	private WebElement clickTestUserSalesRep;

	@FindBy(xpath = "//button[@name='change owner']")
	private WebElement btnChangeOwner;
	
	@FindBy(xpath ="//span[@title='Portfolio Interests']")
	private WebElement relatedPortfolioInterests;
	

	// Methods
	public void clickPGIInTable(HashMap<String, String> dataMap) {
		
		List<WebElement> rowCountPGI = CommonUtil.getAllElements(rowPGI, "Product Group Interests Rows");
		System.out.println(rowCountPGI.size());
		int rowsize = rowCountPGI.size();

		List<WebElement> colCountPGI =CommonUtil.getAllElements(colPGI, "Product Group Interests Columns");
		System.out.println(colCountPGI.size());
		int colsize = colCountPGI.size();

		for (int i = 1; i <= rowsize; i++) {
			for (int j = 1; j <= colsize; j++) {
				String PGIColNameExpected = CommonUtil.getText(CommonUtil.findElement(pgiColumnNameStart+j+ pgiColumnNameEnd, "xpath","PGI Column Element"), "PGI Column Element text");
				if (PGIColNameExpected.contains(dataMap.get("PGIColumnName"))) {
					String PGIRowNameExpected = CommonUtil.getText(CommonUtil.findElement(pgiRowNameStart+i+ pgiColumnNameEnd, "xpath","PGI Row Element"), "PGI Row Element text");
					WebElement PGIRowCampaignEle = CommonUtil.findElement(pgiRowElementLnkStart+i+pgiRowElementLnkMiddle+i+pgiRowPrimaryCampaignLnkEnd, "xpath", "PGI Row campaign Link");
					String campaignName = CommonUtil.getText(PGIRowCampaignEle, "PGI Row campaign Link text");
					if(!campaignName.isEmpty()) {
					if (PGIRowNameExpected.contains(dataMap.get("PGIVal"))) {
						CommonUtil.click(CommonUtil.findElement(pgiRowElementLnkStart+i+pgiRowElementLnkEnd,"xpath","PGI Row Element Link"), "PGI Row Element Link");
						CommonUtil.normalWait(10);
						CommonUtil.waitForVisible(eleexpPGI, dataMap.get("PGIVal"));
						CommonUtil.verifyText(eleexpPGI, dataMap.get("PGIVal"), "Product Group Interests matched");
						CommonUtil.normalWait(5);
						if (eleexpPGI.isDisplayed()) {
							i = rowsize + 1;
							j = colsize + 1;
						}
					}
					else
    				{
    					break;
    				}
				}
				}
			}
		}

	}

	public void changeOwnerPGI(HashMap<String, String> dataMap) {
		String changeOwnerVal = CommonUtil.getText(ChangeOwnerVal, "Change Owner Element");
		System.out.println(changeOwnerVal.hashCode());
		if(changeOwnerVal.hashCode()==0)
		{
			changeOwnerVal=	CommonUtil.getText(ChangeOwnerLnkVal, "Change Owner link element");
		}
		if(!(changeOwnerVal.equalsIgnoreCase(dataMap.get("ChangeOwner")))) {
			CommonUtil.normalWait(5);
			CommonUtil.click(iconChangeOwner, "Change Owner Icon");
			CommonUtil.normalWait(5);
			CommonUtil.waitForElementVisibilityWithDuration(eleSearchUser, 10, "Search User text box");
			Assert.assertTrue(CommonUtil.isElementDisplayed(eleSearchUser, "Change Owner Page"));
			CommonUtil.attachScreenshotOfPassedTestsInReport();
			CommonUtil.inputText(eleSearchUser, dataMap.get("ChangeOwner"), "Enter Owner");
			CommonUtil.click(clickTestUserLDR, "TestUser LDR");
			CommonUtil.normalWait(5);
			CommonUtil.attachScreenshotOfPassedTestsInReport();
			CommonUtil.click(btnChangeOwner, "Change Owner button");
		}
		else {
			Assert.assertTrue("Verifying the Owner values",changeOwnerVal.equalsIgnoreCase(dataMap.get("ChangeOwner")));
			CommonUtil.attachScreenshotOfPassedTestsInReport();
		}
		CommonUtil.waitForElementVisibilityWithDuration(eleexpPGI, 3, "Product Group Interets");
		Assert.assertTrue(CommonUtil.isElementDisplayed(eleexpPGI, "Product Group Interets Page"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.normalWait(5);
		CommonUtil.click(relatedPortfolioInterests, "Portfolio Interests in Related Section");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}