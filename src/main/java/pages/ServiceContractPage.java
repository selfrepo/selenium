package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;

public class ServiceContractPage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================

	String serviceContractColumnNameStart = "//table[@aria-label = 'Service Contracts']//thead//tr[1]//th[" ;
	String serviceContractRowNameStart = "//table[@aria-label = 'Service Contracts']//tbody[1]//tr[";
	String serviceContractColumnNameEnd = "]//span[2]" ; 
	String rowServiceContract = "//table[@aria-label = 'Service Contracts']//tbody[1]//tr";
	String colServiceContract ="//table[@aria-label = 'Service Contracts']//tbody[1]//tr[1]//th | //table[@aria-label = 'Service Contracts']//tbody[1]//tr[1]//td";
	// Renewal Opportunities
	String renewalOpportunitiesColumnNameStart = "//table[@aria-label = 'Renewal Opportunities']//thead//tr[1]//th[" ;
	String renewalOpportunitiesRowNameStart = "//table[@aria-label = 'Renewal Opportunities']//tbody[1]//tr[";
	String renewalOpportunitiesColumnNameEnd = "]" ; 
	String rowRenewalOpportunities = "//table[@aria-label = 'Renewal Opportunities']//tbody[1]//tr";
	String colRenewalOpportunities= "//table[@aria-label = 'Renewal Opportunities']//tbody[1]//tr[1]//th | //table[@aria-label = 'Renewal Opportunities']//tbody[1]//tr[1]//td";

	// ================================================================================
	// Objects
	// ================================================================================
	
	@FindBy(xpath = "//span[@title='Service Contracts']")
	private WebElement lnkServiceContracts;
	
	@FindBy(xpath = "//span[@class='test-id__field-label'][text()='Contract Name']//following::lightning-formatted-text[1]")
	private WebElement eleServiceContracts;
	
	@FindBy(xpath = "//h1[normalize-space()='Service Contracts']")
	private WebElement serviceContractsHeader;
	
	@FindBy(xpath = "//span[text()='Renewal Opportunities (1)']")
	private WebElement lnkRenewalOpportunities;
	
	@FindBy(xpath = "(//span[text()='Opportunity Name'])[2]//following::lightning-formatted-text[1]")
	public static WebElement eleRenwlOppName;


	// ================================================================================
	// Methods
	// ================================================================================
	
	
	public void clickRelatedListServiceContract(HashMap<String, String> dataMap) throws InterruptedException {
		
		//Click Service Contract link in Related section in PreOrder Page
		CommonUtil.click(lnkServiceContracts, "Click Pre-Order link in Quote details page under Related list");
		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(serviceContractsHeader, 10, "Service Contracts Header");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void validateAndClickOnContractName(HashMap<String, String> dataMap) throws InterruptedException {
				
		List<WebElement> rowCountServiceContract = CommonUtil.getAllElements(rowServiceContract, "Service Contract Page");
		int rowsizeServiceContract = rowCountServiceContract.size();

		List<WebElement> colCountServiceContract =CommonUtil.getAllElements(colServiceContract, "Service Contract Page");
		int colsizeServiceContract = colCountServiceContract.size();
		
		 outerloop :
			for (int j = 1; j <= colsizeServiceContract; j++) {
				String ContractColNameExpected = CommonUtil.getText(CommonUtil.findElement(serviceContractColumnNameStart+j+ serviceContractColumnNameEnd, "xpath","Contract Name Column Element"), "Contract Name Element text");
				if (ContractColNameExpected.contains(dataMap.get("ContractColumnName"))) {
					for (int i = 1; i <= rowsizeServiceContract; i++) {
					String ContractRowNameExpected = driver.findElement(By.xpath("//table[@aria-label = 'Service Contracts']//tbody[1]//tr//th["+i+"]//span[1]//a[1]")).getText();
					if (ContractRowNameExpected.trim().equals(dataMap.get("ContractRowItemName"))) {
					CommonUtil.attachScreenshotOfPassedTestsInReport();
					driver.findElement(By.xpath("//table[@aria-label = 'Service Contracts']//tbody[1]//tr//th["+i+"]//span[1]//a[1]")).click();
					CommonUtil.waitForElementVisibilityWithDuration(eleServiceContracts, 20, "Service Contract");
					CommonUtil.attachScreenshotOfPassedTestsInReport();
					String ValServiceContract = CommonUtil.getText(eleServiceContracts,"Service Contract");
					Assert.assertTrue(ValServiceContract.equals(ContractRowNameExpected),"Verifying the Service Contract values");
					break outerloop;
				    }
				}
			}
					
		}
	}	
	
  public void clickRenewalOpportunities(HashMap<String, String> dataMap) throws InterruptedException {
		
		CommonUtil.refresh("Service Contract Page");
		CommonUtil.normalWait(10);
	
		//Click Renewal Opportunities link in Related section in Service Contract Page
		CommonUtil.click(lnkRenewalOpportunities, "Opportunities in Service Contract page under Related list section");
		CommonUtil.normalWait(10);

		List<WebElement> colCountRenewalOpportunities =CommonUtil.getAllElements(colRenewalOpportunities, "Renewal Opportunties Page");
		int colsizeRenewalOpportunities = colCountRenewalOpportunities.size();
		
		for (int OppNameCol = 1; OppNameCol <= colsizeRenewalOpportunities; OppNameCol++) {
			String OpportunityNameColNameExpected = CommonUtil.getText(CommonUtil.findElement(renewalOpportunitiesColumnNameStart+OppNameCol+ renewalOpportunitiesColumnNameEnd, "xpath","Opportunity Name Column Element"), "Opportunity Name Element text");
			if (OpportunityNameColNameExpected.contains(dataMap.get("OppNameColumnName"))) {
				String OpportunityRowNameExpected = driver.findElement(By.xpath("//table[@aria-label = 'Renewal Opportunities']//tbody[1]//tr[1]//th[1]//span[1]//a[1]")).getText();
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				driver.findElement(By.xpath("//table[@aria-label = 'Renewal Opportunities']//tbody[1]//tr[1]//th[1]//span[1]//a[1]")).click();
				CommonUtil.waitForElementVisibilityWithDuration(eleRenwlOppName, 20, "Renewal Opportunity Name");
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				String ValRenwlOppName = CommonUtil.getText(eleRenwlOppName,"Renewal Opportunity Name");
				Assert.assertTrue(ValRenwlOppName.equals(OpportunityRowNameExpected),"Verifying the Renewal Opportunity Name");
				break;
				}
			}
		}
}

	
	
			
			
	

		
