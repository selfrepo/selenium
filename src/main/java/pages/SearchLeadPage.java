package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.CommonUtil;

public class SearchLeadPage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================

	// ================================================================================
	// Objects
	// ================================================================================
	@FindBy(xpath = "//div[@class='slds-icon-waffle']")
	private WebElement sideMenuNavigationBar;

	@FindBy(xpath = "//p[text()='Sales']")
	private WebElement salesLink;

	@FindBy(xpath = "//span[contains(text(),'My Approvals')]")
	private WebElement myApprovalsSpan;

	@FindBy(xpath = "//iframe[@title='dashboard']")
	private WebElement iframeSales;

	@FindBy(xpath = "//span[normalize-space()='Dashboard']")
	private WebElement dashboardText;

	@FindBy(xpath = "//button[@aria-label='Search']")
	private WebElement searchButton;

	@FindBy(xpath = "//div[@class='forceSearchAssistantDialog']/div/div[@data-aura-class='forceSearchInputEntitySelector']//following-sibling::lightning-input/div/input")
	private WebElement searchBarInput;

	@FindBy(css = "div[title='Status']")
	private WebElement statusDiv;

	@FindBy(xpath = "//lightning-formatted-rich-text//span[text()='Lead']")
	private WebElement LeadClick;
	
	@FindBy(xpath = "//one-app-launcher-search-bar//input[@type='search']")
	private WebElement searchInput;

	// Methods
	public void searchCreatedLead(HashMap<String, String> dataMap) {
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(searchButton, 5, "global search bar");
		CommonUtil.click(searchButton, "global search bar");
		CommonUtil.inputText(searchBarInput, dataMap.get("CreatedLead"), "Enter Lead");
		CommonUtil.normalWait(10);
		CommonUtil.click(LeadClick, "Click Created Lead");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
