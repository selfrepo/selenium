package pages;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class ContactsPage extends CommonUtil{
	
    // ================================================================================
 	// Objects
 	// ================================================================================	
	
    @FindBy(xpath = "(//h2[text()='Related List Quick Links']/parent::div/following-sibling::*//div//ul/li//a//span[contains(text(), 'Opportunities')])[2]")
    private WebElement opportunitiesQuickLink;
    
	@FindBy(xpath = "//li[@data-target-selection-name='sfdc:StandardButton.Opportunity.New']//a[@title='New']")
	private WebElement newButtonOnOpportunitiesWindow;
	
	@FindBy(xpath = "//span[text()='Next']/parent::button[@data-aura-class='uiButton']")
	private WebElement nextButtonOnNewOpportunityWindow;	
	
	@FindBy(xpath = "//h2[text()='New Opportunity: New Business']")
	private WebElement newBusinessOppoHeader;
	
	@FindBy(xpath = "//label/span[text()='Opportunity Name']/parent::*/following-sibling::input")
	private WebElement opportunityNameInput;
	
	@FindBy(xpath = "//span[text()='Close Date']/parent::*/following-sibling::div/input[@type='text']")
	private WebElement selCloseDate;
	
	@FindBy(xpath = "//label[text()='Forecast Category']//following-sibling::div//button")
	private WebElement forcastCategoryDropdown;
	
	@FindBy(xpath = "//label[text()='Forecast Category']//following-sibling::div//span[text()='Pipeline']")
	private WebElement forecasetPipelineValue;
	
	@FindBy(xpath = "//select[@name='SelectRT']")
	private WebElement selRecordType;
	
	@FindBy(xpath = "//label[text()='Account Name']/following-sibling::div//input")
	private WebElement enterAccountName;
	
	@FindBy(xpath = "//span[text()='Record Type']//following::span[text()='Save']")
	private WebElement btnSaveCreateOpportunity;   
	
	@FindBy(xpath = "//*[text()='Primary Portfolio']/parent::*/following-sibling::div//a[@role='button']")
	private WebElement primaryPortfolioDropdown;
	
	@FindBy(xpath = "//span[text()='Stage']/parent::span//following-sibling::div//div[@class='uiPopupTrigger']//a[@role='button'][normalize-space()='--None--']")
	private WebElement stageDropdown;
	
	@FindBy(xpath = "//a[@title='Stage 1: Discover']")
	private WebElement stage1Value;
	
	private String primaryPortfolioValue = "//div[@class='select-options']//a[@title='value']";
    
	@FindBy(xpath = "//button[@title='Save']")
	private WebElement saveEditBtn;
	
	@FindBy(xpath = "//h1/div[normalize-space()='Opportunity']")
	private WebElement opportunityDetailsHeader;
	
	// ================================================================================
	// Methods
	// ================================================================================	
	
	//navigates to opportunities quick link from contacts detail page
 	public void navigateToOpportunityQuickLink() {
 		CommonUtil.normalWait(10);
 		CommonUtil.scrollToBottomOfPage();
		CommonUtil.waitForElementVisibilityWithDuration(opportunitiesQuickLink, 10, "opportunities quick Link");
		CommonUtil.click(opportunitiesQuickLink, "Opportunties quick link");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
 	
	// creates new opportunity from contact>>opportunities quick link
	public void createNewOpportunity(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(newButtonOnOpportunitiesWindow, 5, "New button window");
		CommonUtil.click(newButtonOnOpportunitiesWindow, "New Opportunity");
		CommonUtil.waitForElementVisibilityWithDuration(nextButtonOnNewOpportunityWindow, 15, "Next button");
		CommonUtil.clickUsingJavaScriptExecutor(nextButtonOnNewOpportunityWindow, "Next button");
		CommonUtil.normalWait(10);
		
		if(newBusinessOppoHeader.isDisplayed()) {
			CommonUtil.waitForElementVisibilityWithDuration(opportunityNameInput, 5, "opportunity Name Input");	
			
			//enter opportunity name
			int randomNumber= CommonUtil.generateRandomNumber(1, 1000);
			String opportunityName = dataMap.get("OpportunityName")+randomNumber;
			CommonUtil.inputText(opportunityNameInput, opportunityName,"Opportunity Name as "+opportunityName);
			
			//select primary portfolio
			CommonUtil.click(primaryPortfolioDropdown, "primaryPortfolioDropdown");
			CommonUtil.normalWait(3);
			
			WebElement ele = driver.findElement(By.xpath(primaryPortfolioValue.replace("value", "Functional Testing")));
			CommonUtil.click(ele, "select primary portfolio");
			CommonUtil.normalWait(5);
					
			
			//select stage
			CommonUtil.scrollIntoView(stageDropdown, "stage");
			CommonUtil.click(stageDropdown, "stagedropdown");
			CommonUtil.normalWait(5);
			CommonUtil.click(stage1Value, "Stage 1:Discover");
			
			
			//enter close date
			CommonUtil.scrollIntoView(selCloseDate, "close date");
			CommonUtil.inputText(selCloseDate,CommonUtil.getFutureDate("dd/MM/yyyy", 15),"Future Date");
			CommonUtil.normalWait(2);
			CommonUtil.attachScreenshotOfPassedTestsInReport();
			//save
			CommonUtil.click(saveEditBtn, "Save button");
			CommonUtil.normalWait(20);
			
			//verify opprtunity saved
			String opportunityLink= "//a[@title='"+opportunityName+"']";
			WebElement opporLink = driver.findElement(By.xpath(opportunityLink));
			Assert.assertTrue(opporLink.isDisplayed());			
			CommonUtil.attachScreenshotOfPassedTestsInReport();
			
			//save opportunity in data store
			CommonUtil.setTestcaseName("createNewOpportunity");
			CommonUtil.storedata("Opportunity Name", opportunityName);
			
			//navigate to opportunity details page
			CommonUtil.click(opporLink, "opportunity link");
			CommonUtil.normalWait(15);
			CommonUtil.waitForElementVisibilityWithDuration(opportunityDetailsHeader, 5, "opportunity details page");
		}
		else {
		LogUtil.errorLog(getClass(), "New business opportunity not displayed");
		
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		}
	}
	
	
	
}
