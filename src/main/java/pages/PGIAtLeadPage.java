package pages;

import java.util.HashMap;
import utilities.CommonUtil;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class PGIAtLeadPage extends CommonUtil {
	
	//match PGI	
    private WebElement matchedPGI;
	
	private String matchedPGIStart = "//tbody//tr//th//span//a[@title='";
	
	private String matchedPGIEnd = "']";
	
	private String pgiScoreStart = "//lightning-formatted-number[normalize-space()='";
	private String pgiScoreEnd = "']";
	private WebElement pgiScore;
	
	// ================================================================================
	// Objects
	// ================================================================================
	
	@FindBy(xpath = "//span[@title='Product Group Interests']")
	private WebElement productGroupInterrest;   
	
	@FindBy(css = "a[title='Show all past activities in a new tab']")
	private WebElement viewAllLnk;
	
	@FindBy(xpath = "//span[normalize-space()='Lead']")
	private WebElement leadFieldTxt;
	
	// ================================================================================
	// Methods
	// ================================================================================
	
	public void verifyPGIScoreAtLead(HashMap<String, String> dataMap) {
		
		CommonUtil.waitForElementVisibilityWithDuration(viewAllLnk, 10, "View All link");
		CommonUtil.scrollIntoView(viewAllLnk, "View All link");
		//Click on PGI link on lead page
		CommonUtil.waitForElementVisibilityWithDuration(productGroupInterrest, 10, "Product Group Interest link");
		CommonUtil.clickUsingJavaScriptExecutor(productGroupInterrest,"Product Group Interest link");
		CommonUtil.normalWait(10);		
		
		//click on PGI Vertica
		matchedPGI = CommonUtil.findElement(matchedPGIStart+dataMap.get("PGIName")+matchedPGIEnd,"xpath","Searched PGI "+dataMap.get("PGIName"));
		CommonUtil.waitForVisible(matchedPGI,"Searched PGI "+dataMap.get("PGIName"));
		CommonUtil.click(matchedPGI,"matched PGI");
		
		CommonUtil.waitForElementVisibilityWithDuration(leadFieldTxt, 10, "Lead Field Text");
		CommonUtil.scrollIntoView(leadFieldTxt, "Lead Field Text");
		//Verify PGI score
		pgiScore = CommonUtil.findElement(pgiScoreStart+dataMap.get("PGIScore")+pgiScoreEnd,"xpath","Searched PGI "+dataMap.get("PGIScore"));
		CommonUtil.waitForElementVisibilityWithDuration(pgiScore, 10, "PGI score");
		CommonUtil.normalWait(3);
		Assert.assertTrue(CommonUtil.isElementDisplayed(pgiScore, "PGI score"));
	    CommonUtil.attachScreenshotOfPassedTestsInReport();			
	}
				
}
