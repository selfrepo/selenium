package pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtil;
import utilities.ExtentReports.ExtentTestManager;

public class QuotePage extends CommonUtil {
	
	// ================================================================================
	// Variables
	// ================================================================================
	
	SearchQuotePage searchquote = new SearchQuotePage();

	// ================================================================================
	// Objects
	// ================================================================================
	
	@FindBy(xpath = "//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11']//*[local-name()='slot']//lightning-formatted-text[text()='Completed']")
	private WebElement quotestatus;

	@FindBy(xpath = "//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only']//*[local-name()='slot']//lightning-formatted-text[text()='Invoiced']")
	private WebElement quotesubstatus;

	@FindBy(xpath = "(//a[@class='flex-wrap-ie11']//*[local-name()='slot']//*[local-name()='slot']/span)[1]")
	private WebElement quoteopportunitylink;

	@FindBy(xpath = "//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11']//*[local-name()='slot']//lightning-formatted-text[text()='Stage 6: Closed Won']")
	private WebElement quoteopportunitystage;

	@FindBy(xpath = "(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only']//*[local-name()='slot']//lightning-formatted-text[text()='Invoiced'])[2]")
	private WebElement quoteopportunitystatus;

	@FindBy(xpath = "//h2[@class='slds-card__header-title']//span[text()='Pre-Orders']//parent::a")
	private WebElement quotepreorderlink;

	@FindBy(xpath = "//span[@class='slds-grid slds-grid--align-spread']/a")
	private WebElement quotepreordernumberlink;

	@FindBy(xpath = "//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only']/span[text()='INVOICED']")
	private WebElement quotepreorderstatus;

	@FindBy(xpath = "//span[text()='NetSuite Status']//parent::div//parent::div//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only']/span[text()='Complete']")
	private WebElement quotenetsuitestatus;

	@FindBy(xpath = "//span[text()='SLD PDAPI URL']//parent::div//parent::div//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only']/span[1]")
	private WebElement quotesldpadpiurl;

	@FindBy(xpath = "//a[@class='flex-wrap-ie11']//*[local-name()='slot']/span[contains(text(),'Order')]")
	private WebElement quoteorderlink;

	@FindBy(xpath = "//span[text()='Original NS SO Number']")
	private WebElement quoteoriginalsonumberlabel;

	@FindBy(xpath = "//span[text()='Primary Campaign Name']")
	private WebElement quoteIncludeinforcastlabel;

	// ================================================================================
	// Methods
	// ================================================================================
	
    // Validating Quote status and subStatus
	public void verifyquoteinvoicestatus(HashMap<String, String> dataMap) {

		searchquote.globalSearchQuote(dataMap.get("QuoteNumber"));
		CommonUtil.normalWait(20);
		WebElement quotestatus = CommonUtil.getWebElement("xpath",
				"//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11']//*[local-name()='slot']//lightning-formatted-text[text()='{replacevalue}']",
				dataMap.get("QuoteStatus"));
		verifyText(quotestatus, dataMap.get("QuoteStatus"), "quote status");
		WebElement quotesubstatus = CommonUtil.getWebElement("xpath",
				"//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only']//*[local-name()='slot']//lightning-formatted-text[text()='{replacevalue}']",
				dataMap.get("QuoteSubStatus"));
		verifyText(quotesubstatus, dataMap.get("QuoteSubStatus"), "quote sub status");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	// Verifying PreOrder and netsuite status in mentioned quote details
	public void verifyquoteinvoiceorderstatus(HashMap<String, String> dataMap) {
		searchquote.searchQuote(dataMap);
		CommonUtil.normalWait(10);
		CommonUtil.scrollViewUpto(quoteorderlink, "Quote Order Link");
		CommonUtil.clickUsingJavaScriptExecutor(quoteorderlink, "Quote Order Link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(quotepreordernumberlink, "Quote Pre Order Number Link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollViewUpto(quoteoriginalsonumberlabel, "Orginal SO Number");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		WebElement quotenetsuitestatus = CommonUtil.getWebElement("xpath",
				"//span[text()='NetSuite Status']//parent::div//parent::div//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only']/span[text()='{replacevalue}']",
				dataMap.get("NetsuiteStatus"));
		verifyText(quotenetsuitestatus, dataMap.get("NetsuiteStatus"), "quote NetSuite Status");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		WebElement quotepreorderstatus = CommonUtil.getWebElement("xpath",
				"//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only']/span[text()='{replacevalue}']",
				dataMap.get("PreOrderStatus"));
		CommonUtil.scrollViewUpto(quotepreorderstatus, "quote pre order Status View");
		verifyText(quotepreorderstatus, dataMap.get("PreOrderStatus"), "quote pre order Status");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	// Validation for the QuoteOpportunity status after invoicing
	public void verifyquoteinvoiceopportunitystatus(HashMap<String, String> dataMap) {
		searchquote.searchQuote(dataMap);
		CommonUtil.normalWait(20);
		CommonUtil.scrollViewUpto(quoteopportunitylink, "Quote Opportunity Link");
		CommonUtil.clickUsingJavaScriptExecutor(quoteopportunitylink, "Quote Opportunity Link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		WebElement quoteopportunitystage = CommonUtil.getWebElement("xpath",
				"//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11']//*[local-name()='slot']//lightning-formatted-text[text()='{replacevalue}']",
				dataMap.get("QuoteOpportunityStage"));
		CommonUtil.scrollViewUpto(quoteopportunitystage, "quote Opportunity Stage");
		verifyText(quoteopportunitystage, dataMap.get("QuoteOpportunityStage"), "quote Opportunity Stage");
		CommonUtil.normalWait(20);
		WebElement quoteopportunitystatus = null;
		CommonUtil.scrollViewUpto(quoteIncludeinforcastlabel, "quote Include in Forecast");
		Boolean statusfileddisplay;
		try {
			quoteopportunitystatus = CommonUtil.getWebElement("xpath",
					"//div[@class='windowViewMode-normal oneContent active lafPageHost']//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only']//*[local-name()='slot']//lightning-formatted-text[text()='{replacevalue}']",
					dataMap.get("QuoteOpportunityStatus"));
			statusfileddisplay = quoteopportunitystatus.isDisplayed();
			CommonUtil.scrollViewUpto(quoteopportunitystatus, "quote Opportunity Status");
		} catch (Exception e) {
			statusfileddisplay = false;
		}
		System.out.println("statusfileddisplay  : " + statusfileddisplay);
		if (statusfileddisplay.equals(false)) {
			quoteopportunitystatus = CommonUtil.getWebElement("xpath",
					"(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11 is-read-only']//*[local-name()='slot']//lightning-formatted-text[text()='{replacevalue}'])[2]",
					dataMap.get("QuoteOpportunityStatus"));
			CommonUtil.scrollViewUpto(quoteopportunitystatus, "quote Opportunity Status");
		}
		CommonUtil.clickUsingJavaScriptExecutor(quoteopportunitystatus, "quote Opportunity Status click");
		CommonUtil.scrollViewUpto(quoteopportunitystatus, "quote Opportunity Status");
		verifyText(quoteopportunitystatus, dataMap.get("QuoteOpportunityStatus"), "quote Opportunity Status");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	public void verifyquoteinvoiceordersldstatus(HashMap<String, String> dataMap) {
		searchquote.searchQuote(dataMap);
		CommonUtil.normalWait(10);
		CommonUtil.scrollViewUpto(quoteorderlink, "Quote Order Link");
		CommonUtil.clickUsingJavaScriptExecutor(quoteorderlink, "Quote Order Link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(quotepreordernumberlink, "Quote Pre Order Number Link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		WebElement quotesldstatus = CommonUtil.getWebElement("xpath",
				"//span[text()='SLD Status']//parent::div//parent::div//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only']/span[text()='{replacevalue}']",
				dataMap.get("SLDStatus"));
		CommonUtil.scrollViewUpto(quotesldstatus, "quote SLD Status View");
		verifyText(quotesldstatus, dataMap.get("SLDStatus"), "quote SLD Status");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollViewUpto(quotesldpadpiurl, "quote SLD PDAPI URL View");
		verifycontainstext(quotesldpadpiurl, dataMap.get("SLDPDAPIURL"), "quote SLD PDAPI URL");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
