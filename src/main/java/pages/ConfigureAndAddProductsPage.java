package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.CommonUtil;

public class ConfigureAndAddProductsPage extends CommonUtil{


	// ================================================================================
	// Objects
	// ================================================================================
	@FindBy(xpath = "//button[normalize-space()='Configure Products']") 
	private WebElement configureButton;
	
	@FindBy(xpath = "//iframe[contains(@name,'vfFrameId')][@height='100%']") 
	private WebElement editQuoteIFrame;
	
	// ================================================================================
	// Methods
	// ================================================================================		
	public void configureAndAddProducts(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(configureButton,10,"Configure button");
		CommonUtil.click(configureButton,"Configure button");
		CommonUtil.normalWait(25);
		CommonUtil.waitForElementVisibilityWithDuration(editQuoteIFrame, 10,"Edit Quote IFrame");
		CommonUtil.switchToFrame(editQuoteIFrame,"Edit Quote IFrame");
		CommonUtil.normalWait(40);
		WebElement ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-line-editor\").shadowRoot.querySelector(\"#actions > sb-custom-action:nth-child(3)\").shadowRoot.querySelector(\"#mainButton\")","Add Products button");
		CommonUtil.clickUsingJavaScriptExecutor(ele, "Add Products button");	
		CommonUtil.normalWait(15);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#headersearch').shadowRoot.querySelector('#typeahead').shadowRoot.querySelector('#itemLabel')","Global Product search field");
		CommonUtil.inputText(ele,dataMap.get("ProductCode"), dataMap.get("ProductCode")+" product code");
			
		//Clicked on the search icon
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#headersearch').shadowRoot.querySelector('#search')","the search icon");
		CommonUtil.clickUsingJavaScriptExecutor(ele, "the search icon");
		CommonUtil.normalWait(4);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		//Searched the checkbox and click on it
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#lookupLayout').shadowRoot.querySelector('#tableRow').shadowRoot.querySelector('#selection').shadowRoot.querySelector('#g > div > sb-table-cell-select').shadowRoot.querySelector('#checkbox')",dataMap.get("ProductCode")+" product code checkbox");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"the checkbox for searched product");	
		CommonUtil.normalWait(2);
			
		//Click on select button
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#plSelect')","product select button");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"select button");	
		CommonUtil.normalWait(20);
		//Again searched the checkbox and click on it
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#bundles').shadowRoot.querySelector('#features').shadowRoot.querySelector('sb-tabs').shadowRoot.querySelector('#pages > div > sb-product-feature-list').shadowRoot.querySelector('#featureList > sb-product-feature').shadowRoot.querySelector('#ot').shadowRoot.querySelector('#row').shadowRoot.querySelector('#selection').shadowRoot.querySelector('#g > div > sb-table-cell-select').shadowRoot.querySelector('#checkbox').shadowRoot.querySelector('#checkboxContainer')","checkbox to Select One or more product");
		CommonUtil.click(ele, "checkbox to Select One or more product");
		normalWait(2);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		//Click on Save button
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"#sbPageContainer\").shadowRoot.querySelector(\"#content > sb-product-config\").shadowRoot.querySelector(\"#pcSave\")","Save button on Configure Products Page");	
		CommonUtil.click(ele,"Save button on Configure Products Page");
		normalWait(10);
		CommonUtil.switchToDefaultFrame();
	}

	public void configureAndAddProducts_22_01(HashMap<String, String> dataMap) {

		//Click on the Configure button
		CommonUtil.waitForElementVisibilityWithDuration(configureButton, 10, "confugure button");
		CommonUtil.click(configureButton,"Clicked on Configure button");
		CommonUtil.normalWait(20);

		//switch to the iframe
		CommonUtil.waitForElementVisibilityWithDuration(editQuoteIFrame, 10, "edit quote");
		CommonUtil.switchToFrame(editQuoteIFrame, "edit quote");
		CommonUtil.normalWait(20);

		//Click on the Add Products buttons
		WebElement ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-line-editor').shadowRoot.querySelector('#actions > sb-custom-action:nth-child(3)').shadowRoot.querySelector('#mainButton')", "");
		CommonUtil.clickUsingJavaScriptExecutor(ele, " on Add Products button");	
		CommonUtil.normalWait(40);
		
		//Enter Product Code in the Product Search box
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#headersearch').shadowRoot.querySelector('#typeahead').shadowRoot.querySelector('#itemLabel')", "");
		CommonUtil.inputText(ele,dataMap.get("ProductCode"), "Entering the product to configure/add");
		CommonUtil.normalWait(10);
		//Select the product code from the resulting suggestions
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#headersearch').shadowRoot.querySelector('#search')", "");
		CommonUtil.clickUsingJavaScriptExecutor(ele, "the search icon");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();

		//Select the Main Product by checking the Checkbox
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#lookupLayout').shadowRoot.querySelector('iron-list > sb-table-row:nth-child(3)').shadowRoot.querySelector('#selection').shadowRoot.querySelector('#g > div > sb-table-cell-select').shadowRoot.querySelector('#checkbox')", "");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"Checking off the checkbox");	
		CommonUtil.normalWait(10);
		//Click on select button
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-lookup').shadowRoot.querySelector('#plSelect')", "");
		CommonUtil.clickUsingJavaScriptExecutor(ele,"select");	
		CommonUtil.normalWait(30);

		//Select one of the products and Configure it by clicking on wrench icon
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#bundles').shadowRoot.querySelector('#features').shadowRoot.querySelector('sb-tabs').shadowRoot.querySelector('#pages > div > sb-product-feature-list').shadowRoot.querySelector('#featureList > sb-product-feature').shadowRoot.querySelector('#ot').shadowRoot.querySelector('#row').shadowRoot.querySelector('#selection').shadowRoot.querySelector('#g > div > sb-table-cell-select').shadowRoot.querySelector('#checkbox').shadowRoot.querySelector('#checkboxContainer')", "");
		CommonUtil.click(ele, "Checking the checkbox");
		normalWait(20);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#bundles').shadowRoot.querySelector('#features').shadowRoot.querySelector('sb-tabs').shadowRoot.querySelector('#pages > div > sb-product-feature-list').shadowRoot.querySelector('#featureList > sb-product-feature:nth-child(1)').shadowRoot.querySelector('#ot').shadowRoot.querySelector('#row').shadowRoot.querySelector('#swipe-header > div:nth-child(2) > sb-actions')", "");
		CommonUtil.click(ele, "Click on the Wrench icon");
		normalWait(20);

		//select the Environment usage
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#bundles').shadowRoot.querySelector('#features').shadowRoot.querySelector('#globalConfigAttributes').shadowRoot.querySelector('#firstColumn').shadowRoot.querySelector('#g > div > sb-attribute-item').shadowRoot.querySelector('#field').shadowRoot.querySelector('#picklist').shadowRoot.querySelector('#Testing')", "");
		CommonUtil.click(ele, "Environment usage selection");
		normalWait(20);

		//Select one of the Extension product with License type-> License extension
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#bundles').shadowRoot.querySelector('#features').shadowRoot.querySelector('sb-tabs').shadowRoot.querySelector('#pages > div.iron-selected > sb-product-feature-list').shadowRoot.querySelector('#featureList > sb-product-feature').shadowRoot.querySelector('#ot').shadowRoot.querySelector('#row:nth-child(5)').shadowRoot.querySelector('#selection').shadowRoot.querySelector('#g > div > sb-table-cell-select').shadowRoot.querySelector('#checkbox')","");
		normalWait(10);
		CommonUtil.scrollIntoView(ele, "Checking the license type product checkbox");
		CommonUtil.click(ele, "Checking the license type product checkbox");
		normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();

		//Again Configure the Extension product by clicking on the wrench button
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#bundles').shadowRoot.querySelector('#features').shadowRoot.querySelector('sb-tabs').shadowRoot.querySelector('#pages > div.iron-selected > sb-product-feature-list').shadowRoot.querySelector('#featureList > sb-product-feature').shadowRoot.querySelector('#ot').shadowRoot.querySelector('#row:nth-child(5)').shadowRoot.querySelector('#swipe-header > div:nth-child(2) > sb-actions\')", "");
		CommonUtil.click(ele, "Configure the Extension Product");
		normalWait(10);

		//Select the Geo type for the extension product as configuration
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#bundles').shadowRoot.querySelector('#features').shadowRoot.querySelector('#globalConfigAttributes').shadowRoot.querySelector('#firstColumn').shadowRoot.querySelector('#g > div > sb-attribute-item').shadowRoot.querySelector('#field').shadowRoot.querySelector('#picklist').shadowRoot.querySelector('#North\\\\ America')", "");
		CommonUtil.click(ele, "Configure the extensions product");
		normalWait(15);

		//Click on save button at the License extension product page
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#pcSave > sb-i18n')", "");
		CommonUtil.click(ele, "Save the Product extension page");
		normalWait(20);

		//Click on Save button
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#pcSave')","");	
		CommonUtil.click(ele,"Save button on the License extension Product page");
		normalWait(15);
		ele = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector('#sbPageContainer').shadowRoot.querySelector('#content > sb-product-config').shadowRoot.querySelector('#pcSave')","");	
		CommonUtil.click(ele,"Save button on the main Products page");
		normalWait(20);
		CommonUtil.switchToDefaultFrame();
	}



}