package pages;

import java.util.HashMap;
import utilities.CommonUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CreateNewQuotePage extends CommonUtil {

	// ================================================================================
	// Objects
	// ================================================================================

	@FindBy(xpath = "//button[@name='Opportunity.New_Quote']")
	private WebElement btnNewQuote;

	@FindBy(xpath = "//label[span[text()='Term Start Date']]//following-sibling::div//input[@type='text']")
	private WebElement edtTermStartDate;
	
	@FindBy(xpath = "//label[span[text()='Term End Date']]//following-sibling::div//input[@type='text']")
	private WebElement edtTermEndDate;

	@FindBy(xpath = "//label[span[text()='Term']]//following-sibling::input[@type='text']")
	private WebElement edtTerm;

	@FindBy(css = ".slds-button.slds-button_brand.cuf-publisherShareButton.undefined.uiButton")
	private WebElement btnSave;

	// ================================================================================
	// Methods
	// ================================================================================
	public void createNewQuote(HashMap<String, String> dataMap) {

		CommonUtil.waitForElementVisibilityWithDuration(btnNewQuote, 10, "New Quote button");
		CommonUtil.clickUsingJavaScriptExecutor(btnNewQuote, "New Quote button");
		CommonUtil.normalWait(10);

		// enter term start date
		CommonUtil.waitForElementVisibilityWithDuration(edtTermStartDate, 10, "Term start Date");
		CommonUtil.normalWait(2);
		CommonUtil.inputText(edtTermStartDate, CommonUtil.getCurrentDate("dd/MM/yyyy"), "Current Date in Term Start Date");

		// enter term
		CommonUtil.waitForElementVisibilityWithDuration(edtTerm, 10, "Term");
		CommonUtil.normalWait(2);
		CommonUtil.inputText(edtTerm, dataMap.get("Term"),dataMap.get("Term") + " in Term");
		CommonUtil.normalWait(5);
		CommonUtil.click(edtTermEndDate, "End Term Date field");
		
		// click on save
		CommonUtil.normalWait(5);		
		CommonUtil.waitForElementVisibilityWithDuration(btnSave, 10, "Save button");
		CommonUtil.clickUsingJavaScriptExecutor(btnSave, "Save button");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.normalWait(20);
	}
}
