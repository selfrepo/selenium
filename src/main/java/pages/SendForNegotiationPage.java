package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;

public class SendForNegotiationPage extends CommonUtil{
	
	
	// ================================================================================
	// Variables
	// ================================================================================
	Boolean result;
	
	private WebElement quoteNumberTxt;
	
	private String quoteNumberTxtStart = "//div[span[text()='Quote Number']]//following-sibling::div//lightning-formatted-text[text()='";
	
	private String quoteNumberTxtEnd = "']";
	
	private static String parentWindow;
	
	private WebElement sentForSignatureTxt;
	
	private String sentForSignatureTxtStart = "//h1[contains(text(),'[DEMO USE ONLY] Micro Focus Quote - ";
	
	private String sentForSignatureTxtEnd = "')]";
	
	private WebElement matchedData;
	
	private String matchedDataStart = "//span[not(contains(text(),'Legal Document for') or contains(text(),'Micro Focus Quote') )]/mark[text()='";
	
	private String matchedDataEnd = "']";
	
	// ================================================================================
	// Objects
	// ================================================================================
	@FindBy(xpath = "//button[@class=\"slds-button slds-button_icon-border-filled\"]")
	private WebElement dropDownIcon;
	
	@FindBy(xpath = "//a[@name='O2Q_Send_for_Negotiation']")
	private WebElement sendForNegotiation;
	
	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//iframe[@scrolling='yes']")
	private WebElement sendForNegotiationPageIframe;
	
	@FindBy(xpath = "//h1[@title='Send for Negotiation']")
	private WebElement sendForNegotiationPageHeader;
	
	@FindBy(id = "to-address-input")
	private WebElement toInputBox;
	
	@FindBy(xpath = "//div[@class=\"slds-page-header\"]//button[text()='Send']")
	private WebElement sendButton;
	
	@FindBy(xpath = "//div[@class=\"slds-page-header\"]//button[text()='Preview']")
	private WebElement previewButton;
	
	@FindBy(id="subject-preview")
	private WebElement subjectPreview;
	
	@FindBy(xpath = "//a[@name='O2Q_Send_For_Signature']")
	private WebElement sendForSignature;
	
	@FindBy(xpath = "//li[contains(text(),'AGREEMENT')]")
	private WebElement agreementText;
	
	@FindBy(xpath = "//button[@class='slds-button slds-button_icon-bare esign-recipient-delete-button slds-medium-show'][@data-order='1']")
	private WebElement removeSecondRecipient;
	
	@FindBy(xpath = "//div[@role='combobox'][@data-order='0']")
	private WebElement recipientSelect;
	
	@FindBy(xpath = "//div[@role='option'][@data-order='0']//span[text()='Email']")
	private WebElement emailOption;
	
	@FindBy(css = "#recipientEmailInput0")
	private WebElement emailInputbox;
	
	@FindBy(xpath = "//div[contains(@class,'windowViewMode-normal oneContent active lafPageHost')]//div[contains(@class,'slds-m-top_large slds-show_large')]//button[contains(@type,'button')][normalize-space()='Send']")
	private WebElement agreementSendBtn;
	
	@FindBy(xpath = "//span[text()='Out for Signature']")
	private WebElement outForSignatureTxt;
	
	@FindBy(xpath = "//button[@aria-label='Search']") 
	private WebElement searchButton;
	
	@FindBy(xpath = "//div[@class=\"forceSearchAssistantDialog\"]/div/div[@data-aura-class=\"forceSearchInputEntitySelector\"]//following-sibling::lightning-input/div/input") 
	private WebElement searchBarInput;
	
	@FindBy(xpath = "//button[contains(text(),'Edit')]") 
	private WebElement editButton;

	@FindBy(xpath = "//label[span[text()='Primary']]//following-sibling::div//span[@class='slds-checkbox_faux']")
	private WebElement primaryCheckbox;
	
	@FindBy(xpath = "//button[@name='SaveEdit']") 
	private WebElement editSaveButton;
	

	// ================================================================================
	// Methods
	// ================================================================================
	public void sendForNegotiation(HashMap<String, String> dataMap, String quoteNumber) throws InterruptedException {
			CommonUtil.waitForElementVisibilityWithDuration(dropDownIcon, 5, "dropdown icon");
			CommonUtil.click(dropDownIcon, "drop down icon");
			CommonUtil.waitForVisible(sendForNegotiation);
			CommonUtil.click(sendForNegotiation, "send for negotiation button");
			CommonUtil.normalWait(15);
			CommonUtil.switchToFrame(sendForNegotiationPageIframe, "send for negotiation");
			Assert.assertTrue(CommonUtil.isElementDisplayed(sendForNegotiationPageHeader, "page header"));
			CommonUtil.inputTextWithEnterKey(toInputBox, dataMap.get("Email"));
			WebElement sendButton = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"body > div.slds > div.slds-page-header > div.slds-grid > div.slds-col.slds-no-flex.slds-grid.slds-align-bottom > button.slds-button.slds-button--brand.edit-section-button.js-send\")", "");
			WebElement previewButton = CommonUtil.excuteJavaScriptExecutorScripts("return document.querySelector(\"body > div.slds > div.slds-page-header > div.slds-grid > div.slds-col.slds-no-flex.slds-grid.slds-align-bottom > button.slds-button.slds-button--brand.edit-section-button.js-preview\")", "");	
			CommonUtil.isElementEnabled(sendButton, "send button");
			CommonUtil.isElementEnabled(previewButton, "preview button");
			CommonUtil.normalWait(2);
			CommonUtil.clickUsingJavaScriptExecutor(sendButton,"Send button");
			CommonUtil.waitForVisible(subjectPreview);
			Assert.assertTrue(CommonUtil.isElementDisplayed(subjectPreview, "subject preview"));
			CommonUtil.normalWait(15);
			CommonUtil.switchToDefaultFrame();
			CommonUtil.normalWait(10);
			quoteNumberTxt = CommonUtil.findElement(quoteNumberTxtStart+quoteNumber+quoteNumberTxtEnd,"xpath", "");
			Assert.assertTrue(CommonUtil.isElementDisplayed(quoteNumberTxt, "quote number"));
			CommonUtil.click(dropDownIcon, "drop down icon");
			CommonUtil.waitForVisible(sendForSignature);
			CommonUtil.click(sendForSignature, "send for signature button");
			CommonUtil.normalWait(20);
			CommonUtil.waitForElementVisibilityWithDuration(agreementText, 35, "agreement text");
			Assert.assertTrue(CommonUtil.isElementDisplayed(agreementText, "agreement"));
			CommonUtil.waitForVisible(removeSecondRecipient);
			CommonUtil.click(removeSecondRecipient,"Remove icon for Second recipient");
			CommonUtil.click(recipientSelect, " recipient dropdown");
			CommonUtil.click(emailOption,"to choose email option");
			CommonUtil.inputTextWithEnterKey(emailInputbox, dataMap.get("Email"));
			CommonUtil.normalWait(2);
			CommonUtil.scrollIntoView(agreementSendBtn, "agreement send button");
			CommonUtil.click(agreementSendBtn,"send button");
			CommonUtil.normalWait(8);
			parentWindow = CommonUtil.switchToNewWindow("Confirm to Sign page");
			sentForSignatureTxt= CommonUtil.findElement(sentForSignatureTxtStart+quoteNumber+sentForSignatureTxtEnd, "xpath", "");
			Assert.assertTrue(CommonUtil.isElementDisplayed(sentForSignatureTxt, "send to signature"));
			CommonUtil.switchToParentWindow(parentWindow, "out for signature");
			CommonUtil.normalWait(10);
			CommonUtil.waitForElementVisibilityWithDuration(outForSignatureTxt, 5, "for signature");
			Assert.assertTrue(CommonUtil.isElementDisplayed(outForSignatureTxt, "for signature"));
			CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}