package pages;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;

import junit.framework.Assert;
import utilities.CommonUtil;


public class CampaignsPage  extends CommonUtil {
	
	 public static HashMap<String, String> dataMap = new HashMap<>();
	 private String parentCampaignResultStart = "(//a[@role='option']//following::div[@class='primaryLabel slds-truncate slds-lookup__result-text'])[";
	 private String parentCampaignResultEnd = "]";
	 private WebElement parentCampaignResult;

	@FindBy(xpath = "//span[text()='Leads']//parent::a") 
	private WebElement LeadLink;

	@FindBy(xpath = "//div[@class='slds-media slds-no-space slds-grow']//span[text()='Leads']") 
	private WebElement LeadHeader;
	
	@FindBy(xpath = "//span[contains(text(),'Show more results for')]") 
	private WebElement showmoreresult;
	
	@FindBy(xpath = "//button[@aria-label='Search']") 
	private WebElement searchButton;
	
	@FindBy(xpath = "//*[text()='Search Leads and more']/parent::lightning-input//input[@class='slds-input']")
	private WebElement searchBarInput;
	
	@FindBy(xpath = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr[1]/td[6]/span[@class='slds-grid slds-grid--align-spread']/a[contains(@href,'mailto:')]")
	private WebElement matchedData;

	@FindBy(xpath = "(//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr[1]/th[1]/span/a[@data-refid='recordId'])[2]")
	private WebElement matchedDatalink;
	
	@FindBy(xpath = "//a[text()='Details']")
	private WebElement LeadDetail;
	
	@FindBy(xpath = "//span[text()='Edit Name']//parent::button")
	private WebElement EditNamebutton;
	
	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement inputfirstname;
	
	@FindBy(xpath = "//input[@name='middleName']")
	private WebElement inputmiddlename;
	
	@FindBy(xpath = "//input[@name='lastName']")
	private WebElement inputlastname;
	
	@FindBy(xpath = "//button[@name='SaveEdit']")
	private WebElement editnamesave;
	
	@FindBy(xpath = "//span[text()='Campaigns']//parent::a") 
	private WebElement CampaignsLink;
	
	@FindBy(xpath = "//div[@class='slds-media slds-no-space slds-grow']//span[text()='Campaigns']") 
	private WebElement CampaignsHeader;
	
	
	@FindBy(xpath = "//div[text()='New']//parent::a") 
	private WebElement CampaignsNew;
	
	@FindBy(xpath = "//span[text()='Marketing Activity']//parent::div//parent::label/div/span") 
	private WebElement MarketingActivityOption;
	
	@FindBy(xpath = "//span[text()='IC']//parent::div//parent::label/div/span") 
	private WebElement ICOption;
	
	@FindBy(xpath = "//span[text()='Next']") 
	private WebElement CampaignsNext;
	
	@FindBy(xpath = "//span[text()='Campaign Name']//parent::label//following::input[1]") 
	private WebElement CampaignsName;
	
	
	@FindBy(xpath = "//span[text()='Brief Campaign Summary']//parent::label//following::input[1]") 
	private WebElement BriefCampaignsSummary;
	
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Activity Type']//following::a[1]") 
	private WebElement ActivityTypeField;
	

	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Activity Type']//following::a[text()='Creation/Mkt Intel Costs']") 
	private WebElement ActivityTypeOptions;
	
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Activity Sub Type']//following::a[1]") 
	private WebElement ActivitySubTypeField;
	

	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Activity Sub Type']//following::a[text()='Cust Ref/Succ Story']") 
	private WebElement ActivitySubTypeOptions;
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Status']//following::a[1]") 
	private WebElement StatusField;
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Status']//following::a[text()='In Progress']") 
	private WebElement StatusOptions;
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Region']//following::a[1]") 
	private WebElement RegionField;
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Region']//following::a[text()='AMS']") 
	private WebElement RegionOptions;
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='CMT']//following::a[1]") 
	private WebElement CmtField;
	
	@FindBy(xpath = "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='CMT']//following::a[text()='GovLLC']") 
	private WebElement CmtOptions;
		
	@FindBy(xpath = "//span[text()='Start Date']//parent::label//following::input[1]") 
	private WebElement Startdate;
	
	@FindBy(xpath = "//span[text()='End Date']//parent::label//following::input[1]") 
	private WebElement Enddate;
	
	@FindBy(xpath = "(//input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup'])[1]") 
	private WebElement ParentCanpaign;
	
	@FindBy(xpath = "(//span[text()='Budget Type']//preceding::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//following::div[@class='uiPopupTrigger'])[9]") 
	private WebElement BudgetType;
	
	@FindBy(xpath = "(//span[text()='Budget Type']//preceding::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//following::div[@class='uiPopupTrigger'])[9]//following::a[text()='Channel Partner MDF']") 
	private WebElement BudgetTypeOptions;

	@FindBy(xpath = "(//span[text()='Budget Type']//preceding::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//following::div[@class='uiPopupTrigger'])[10]") 
	private WebElement FundingAccount;
	
	@FindBy(xpath = "//span[text()='Funding Account']/ancestor::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']/span/following::div[@class='uiPopupTrigger']//following::a[text()='AMC Marketing']")
	private WebElement FundingAccountOptions;
	
	@FindBy(xpath = "//span[span[text()='Sales Team']]/following-sibling::div/div[@class='uiPopupTrigger']")
	private WebElement salesTeam;
	
	@FindBy(xpath = "//span[span[text()='Sales Team']]/following-sibling::div/div[@class='uiPopupTrigger']//following::a[text()='ADM INTL CEE']")
	private WebElement salesTeamOptions;
	
	@FindBy(xpath = "(//span[text()='Budget Type']//preceding::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//following::div[@class='uiPopupTrigger'])[11]") 
	private WebElement Quatar;
	
	@FindBy(xpath = "(//span[text()='Budget Type']//preceding::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//following::div[@class='uiPopupTrigger'])[11]//following::a[text()='Q1']") 
	private WebElement QuatarOptions;
	
	@FindBy(xpath = "(//span[text()='Budget Type']//preceding::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//following::div[@class='uiPopupTrigger'])[12]") 
	private WebElement FiscalYear;
	
	@FindBy(xpath = "(//span[text()='Budget Type']//preceding::div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//following::div[@class='uiPopupTrigger'])[12]//following::a[text()='FY19']") 
	private WebElement FiscalYearOptions;
	
	
	@FindBy(xpath = "//span[text()='Quarter 1 Budget']//following::input[1]") 
	private WebElement Quatar1Budget;
	
	@FindBy(xpath = "//span[text()='Quarter 2 Budget']//following::input[1]") 
	private WebElement Quatar2Budget;
	
	@FindBy(xpath = "//span[text()='Quarter 3 Budget']//following::input[1]") 
	private WebElement Quatar3Budget;
	
	@FindBy(xpath = "//span[text()='Quarter 4 Budget']//following::input[1]") 
	private WebElement Quatar4Budget;
	
	@FindBy(xpath = "//span[text()='Estimated Lead Generated Pipeline']//following::input[1]") 
	private WebElement EstimateLeadPipline;
	
	@FindBy(xpath = "//span[text()='Expected Revenue in Campaign']//following::input[1]") 
	private WebElement EstimateRevenueCampaign;
	
	@FindBy(xpath = "//span[text()='Product Group']//following::a[1]") 
	private WebElement ProductGroup;
	
	@FindBy(xpath = "//span[text()='Product Group']//following::a[text()='Vertica']") 
	private WebElement ProductGroupOptions;
		
	@FindBy(xpath = "//span[text()='Save']//parent::button[@title='Save']") 
	private WebElement CampaignSave;
	
	
    @FindBy(xpath = "(//span[text()='Details']//parent::a[1]/span[2])[2]") 
	private WebElement CampaignDetailsLink;
    
    @FindBy(xpath = "(//div[@class='slds-dueling-list__options'])[1]/ul")
   	private WebElement portfoliooption;
  
    @FindBy(xpath = "//div[@class='slds-listbox__option slds-listbox__option_plain slds-media slds-media_small slds-media_inline slds-is-selected']//following::span[1]") 
   	private WebElement portfoliooptionselect;
    
  
    @FindBy(xpath = "//lightning-formatted-text[@class='custom-truncate']") 
   	private WebElement campaignid;
    
  
    @FindBy(xpath = "//input[@aria-label='Search Recently Viewed list view.']") 
   	private WebElement campaignidsearch;
    
    @FindBy(xpath = "(//a[@data-refid='recordId'])[1]") 
   	private WebElement campaignidlink;
    
	@FindBy(css = "span[title='Product Group Interests']") 
    private WebElement productgroupInterestheaderlink;
	
	
	@FindBy(xpath = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody//span[@class='slds-checkbox--faux']") 
    private WebElement productgroupInterestnamecheckbox;
	
	@FindBy(css = "a[title='Change Owner']") 
    private WebElement ChangeOwnerLink;
	
	@FindBy(css = "input[role='combobox']") 
    private WebElement ChangeOwnerTextbox;
	
	@FindBy(xpath = "//div[@title='Testuser Sales_Rep']") 
    private WebElement ChangeOwnerToSalesRep;
		
	@FindBy(xpath = "//span[text()='Submit']//parent::button") 
    private WebElement ChangeOwnersubmit;
	
	
	@FindBy(css = "a[title='Add Leads']") 
    private WebElement CampaignAddLeadlink;
		
	@FindBy(css = "input[data-aura-class='uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup']") 
    private WebElement CampaignserachLeadtextbox;
	
	@FindBy(css = "div.searchButton.itemContainer") 
    private WebElement CampaignsearchLeadContainer;
	
	@FindBy(xpath = "(//span[@class='slds-grid slds-grid--align-spread slds-align_absolute-center checkbox-container uiInput forceVirtualCheckbox uiInput--default']//following::span[@class='slds-checkbox--faux'])[1]") 
    private WebElement LeadSelectCheckbox;
	
	
	@FindBy(xpath = "//button[text()='Next']") 
    private WebElement LeadNextButton;
	
	
	@FindBy(xpath = "//button[text()='Submit']") 
    private WebElement LeadSubmitButton;
	
	public void createcampaigns(HashMap<String, String> dataMap) {
		CommonUtil.clickUsingJavaScriptExecutor(CampaignsLink, "Campaigns");
		CommonUtil.normalWait(10);
		CommonUtil.waitForVisible(CampaignsHeader,"Campaigns Header");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(CampaignsNew, "Campaigns New");
		CommonUtil.click(MarketingActivityOption, "Marketing Activity Option");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(CampaignsNext, "Next Button");
		String CampaignName   = dataMap.get("CampaignsName");
		CommonUtil.inputText(CampaignsName,CampaignName,"Campaigns Name");
		String BriefCampaignSummary   = dataMap.get("BriefCampaignsSummary");
		CommonUtil.inputText(BriefCampaignsSummary,BriefCampaignSummary,"Campaigns Name");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(ActivityTypeField, "Activity Type Field");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String Activityfieldoption = dataMap.get("ActivityType");
		WebElement ActivityTypeOptions = CommonUtil.getWebElement("Xpath", "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Activity Type']//following::a[text()='{replacevalue}']", Activityfieldoption);
		CommonUtil.click(ActivityTypeOptions, "Activity Type Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(ActivitySubTypeField, "Activity Sub Type Field");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String ActivitySubTypeOption = dataMap.get("ActivitySubtype");
		WebElement ActivitySubTypeOptions = CommonUtil.getWebElement("Xpath", "//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Activity Sub Type']//following::a[text()='{replacevalue}']",ActivitySubTypeOption);
		CommonUtil.click(ActivitySubTypeOptions, "Activity Sub Type Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(StatusField, "Status Field");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(StatusOptions, "Status Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(RegionField, "Region Field");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(RegionOptions, "Region Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(CmtField, "CMT Field");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(CmtOptions, "CMT Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(ParentCanpaign, "Parent Canpaign");
		CommonUtil.normalWait(10);
		int index = CommonUtil.generateRandomNumber(1, 4);
		parentCampaignResult = CommonUtil.findElement(parentCampaignResultStart+index+parentCampaignResultEnd, "xpath", "Parent Campaign result at : "+index);
		CommonUtil.click(parentCampaignResult, "Parent Campaign result");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(salesTeam, "Sales Team");
		CommonUtil.normalWait(2);
		CommonUtil.click(salesTeamOptions, "Sales Team Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String CampaignStartDate   = CommonUtil.getCurrentDate("dd/MM/yyyy");
		CommonUtil.inputText(Startdate,CampaignStartDate,"Campaign Start Date");
		String CampaignEndDate   =CommonUtil.getFutureDate("dd/MM/yyyy", 90);
		CommonUtil.inputText(Enddate,CampaignEndDate,"Campaign End Date");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(BudgetType, "Budget Type");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(BudgetTypeOptions, "Budget Type Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(FundingAccount, "Funding Account");
		CommonUtil.normalWait(2);
		CommonUtil.click(FundingAccountOptions, "Funding Account Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(Quatar, "Quatar");
		CommonUtil.click(QuatarOptions, "Quatar Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(FiscalYear, "Fiscal Year");
		CommonUtil.click(FiscalYearOptions, "Fiscal Year Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String Budget1   = dataMap.get("Quater1");
		CommonUtil.inputText(Quatar1Budget,Budget1,"First Quarter Budget");
		String Budget2   = dataMap.get("Quater2");
		CommonUtil.inputText(Quatar2Budget,Budget2,"Second Quarter Budget");
		String Budget3   = dataMap.get("Quater3");
		CommonUtil.inputText(Quatar3Budget,Budget3,"Three Quarter Budget");
		String Budget4   = dataMap.get("Quater4");
		CommonUtil.inputText(Quatar4Budget,Budget4,"Four Quarter Budget");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String EstimatePipeline  = dataMap.get("EstimateLeadPipeline");
		CommonUtil.inputText(EstimateLeadPipline,EstimatePipeline,"Estimate Pipeline");
		String EstimateRevenueCampigns  = dataMap.get("EstimateRevenue");
		CommonUtil.inputText(EstimateRevenueCampaign,EstimateRevenueCampigns,"Estimate Revenue Campigns");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(ProductGroup, "Product Group");
		CommonUtil.click(ProductGroupOptions, "Product Group Options");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(portfoliooption, "Portfolio Option");
		CommonUtil.clickUsingJavaScriptExecutor(portfoliooptionselect, "Select Portfolio Option");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(CampaignSave, "Campaign Save");
		CommonUtil.normalWait(20);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String Campaignids = CommonUtil.getText(campaignid, "campaignid");
		dataMap.put("CampaignID",Campaignids);
		System.out.println("Campaignids  : " + Campaignids);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
	}
	
	public void campaignschangeowner(HashMap<String, String> dataMap) {
		CommonUtil.clickUsingJavaScriptExecutor(CampaignsLink, "Campaigns");
		CommonUtil.normalWait(10);
		CommonUtil.waitForVisible(CampaignsHeader,"Campaigns Header");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String searchcampaignid  = dataMap.get("CampaignID");
		CommonUtil.jsinputText(campaignidsearch,searchcampaignid,"Enter Campaign ID");
		CommonUtil.click(CampaignsHeader,"Campaigns Header");
		CommonUtil.normalWait(20);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(campaignidlink, "Campaigns ID link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
        CommonUtil.scrollIntoView(productgroupInterestheaderlink, "Product Group Interest Link");
		CommonUtil.clickUsingJavaScriptExecutor(productgroupInterestheaderlink, "Product Group Interest Link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(productgroupInterestnamecheckbox, "product group Interest name checkbox");
		CommonUtil.click(ChangeOwnerLink, "Change Owner Link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(ChangeOwnerTextbox, "Change Owner Id");
		String Changeownerid   = dataMap.get("ChangeOwnerName");
		CommonUtil.inputText(ChangeOwnerTextbox,Changeownerid,"Change Owner Id");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(ChangeOwnerToSalesRep, "Change Owner To SalesRep");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(ChangeOwnersubmit, "Change Owner submit");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}		
		

	
	public void campaignsaddlead(HashMap<String, String> dataMap) {
		CommonUtil.clickUsingJavaScriptExecutor(CampaignsLink, "Campaigns");
		CommonUtil.normalWait(10);
		CommonUtil.waitForVisible(CampaignsHeader,"Campaigns Header");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String searchcampaignid  = dataMap.get("CampaignID");
		CommonUtil.jsinputText(campaignidsearch,searchcampaignid,"Enter Campaign ID");
		CommonUtil.click(CampaignsHeader,"Campaigns Header");
		CommonUtil.normalWait(20);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(campaignidlink, "Campaigns ID link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(CampaignAddLeadlink, "Campaign Add Lead link");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(CampaignserachLeadtextbox, "Change Owner Id");
		String SearchLeadid   = dataMap.get("LeadIDs");
		CommonUtil.jsinputText(CampaignserachLeadtextbox,SearchLeadid,"Enter serach Lead ID");
		CommonUtil.clearInput(CampaignserachLeadtextbox, "Clear serach Lead ID");
		CommonUtil.inputText(CampaignserachLeadtextbox,SearchLeadid,"Enter serach Lead ID");
		CommonUtil.normalWait(10);
		CommonUtil.click(CampaignserachLeadtextbox, "Change Owner Id");
		CommonUtil.click(CampaignsearchLeadContainer, "Click Lead Search Container");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(LeadSelectCheckbox, "Click Lead Plus ICon");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(LeadNextButton, "Lead Next Button");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(LeadSubmitButton, "Lead Submit Button");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
}
