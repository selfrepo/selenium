package pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class SalesOrderPage extends CommonUtil {
	
	private String currencyStart= "//span[span[@id='currency_lbl'][a[text()='Currency']]]/following-sibling::span/span[text()='";
	private String currencyEnd= "']";
	private WebElement currency;
	
	private String billToAddressStart = "//span[span[a[text()='Bill To']]]/following-sibling::span[contains(.,'";
	private String billToAddressEnd = "')]";
	private WebElement billToAddress;
	
	private String amountStart = "//span[span[a[text()='USD Amount']]]/following-sibling::span[contains(.,'";
	private String amountEnd = "')]";
	private WebElement amount;
	
	private String termsStart = "//span[span[a[text()='Terms']]]/following-sibling::span/span[text()='";
	private String termsEnd= "']";
	private WebElement terms;
	
	private String shipToAddressStart = "//span[span[a[text()='Ship To']]]/following-sibling::span[contains(.,'";
	private String shipToAddressEnd = "')]";
	private WebElement shipToAddress;
	
	
	
	@FindBy(xpath = "//*[@id='item_splits']/tbody/tr")
	private List<WebElement> itemsMenuTableRows;

	@FindBy(css = "#_searchstring")
	private WebElement globalSearchInput;
	
	@FindBy(css = ".uir-record-id")
	private WebElement soHeader;
	
	@FindBy(css = "#edit")
	private WebElement editBtn;
	
	@FindBy(xpath = "//a[normalize-space()='Sales Order Type']")
	private WebElement salesOrderType; 
	
	@FindBy(xpath = "//a[normalize-space()='Memo']")
	private WebElement memoLnk;
	
	//To get header row
	@FindBy(xpath = "//*[@id='item_splits']/tbody/tr[1]/td")
	private List<WebElement> itemsTableHeader;
	
	@FindBy(xpath = "//a[@id='billingtabtxt']")
	private WebElement billingTabLnk;

	@FindBy(xpath = "//a[normalize-space()='USD Amount']")
	private WebElement usdAmountLnk;
	
	@FindBy(id = "rlrcdstabtxt")
	private WebElement relatedRecordsTab;
	
	@FindBy(id = "linkstxt")
	private WebElement internalRelatedRecordsTab;
	
	@FindBy(xpath = "//tr[contains(@id,'linksrow')]")
	private List<WebElement> internalRelatedRecordsTableRows;
	
	@FindBy(xpath = "//tr[contains(@id,'linksrow')]/td")
	private List<WebElement> internalRelatedRecordsTableCells;
	
	@FindBy(xpath = "//h1[normalize-space()='Revenue Arrangement']")
	private WebElement revenueArrangementsHeader;
	
	@FindBy(xpath = "//td[@data-label='Accounting Book']//div[@class='listheader']")
	private WebElement accountingBookHeader;
	
	@FindBy(xpath = "//span[span[a[text()='Bill To']]]/following-sibling::span")
	private WebElement billToAddressSpan;
	
	@FindBy(css = "#shippingtxt")
	private WebElement shippingTabLnk;
	
	@FindBy(xpath = "//span[span[a[text()='Ship To']]]/following-sibling::span")
	private WebElement shipToAddressSpan;
	
	@FindBy(css = ".uir-list-name")
	private WebElement salesOrderHeader;
	
	@FindBy(xpath = "//a[normalize-space()='Search']")
	private WebElement transactionSearchLnk;
	
	@FindBy(xpath = "//h1[normalize-space()='Transaction Search']")
	private WebElement transactionSearchHeader;
	
	@FindBy(xpath = "//a[normalize-space()='Period']")
	private WebElement periodLnk;
	
	@FindBy(xpath = "//input[@id='Transaction_NUMBERTEXT']")
	private WebElement transactionNumberTxtInput;
	
	@FindBy(xpath = "//input[@id='secondarysubmitter']")
	private WebElement secondarySubmitBtn;
	
	// ================================================================================
	// Methods
	// ================================================================================
	public void reviewFewDetailsForSO(HashMap<String, String> dataMap) {		
		CommonUtil.waitForElementVisibilityWithDuration(soHeader, 35, "SO header");
		currency = CommonUtil.findElement(currencyStart+dataMap.get("Currency")+ currencyEnd,"xpath", "Currency is : "+dataMap.get("Currency"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(currency, "Currency"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
		CommonUtil.scrollIntoView(memoLnk, "Memo Link");
		amount = CommonUtil.findElement(amountStart+dataMap.get("Amount")+ amountEnd,"xpath", "Amount is "+dataMap.get("Amount"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(amount, "Amount is : "+dataMap.get("Amount")));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollIntoView(amount, "Amount");
		
		List<WebElement> tableHeaderRow = itemsTableHeader;
		LogUtil.infoLog(NSLoginPage.class, "\nItems Table column count : "+tableHeaderRow.size());
		
		List<String> allHeaderNames = new ArrayList<String>();
		for(WebElement header : tableHeaderRow) {
			LogUtil.infoLog(NSLoginPage.class, "\nHeader : "+header.getText());
			allHeaderNames.add(header.getText());
		}
		
		//Get Index and location of required columns
		int idxSrcTransactionKey = allHeaderNames.indexOf("SOURCE TRANSACTION LINE UNIQUE KEY");
		int idxBillingSchedule = allHeaderNames.indexOf("BILLING SCHEDULE");
		int idxItemName = allHeaderNames.indexOf("ITEM");
		int idxQuantity = allHeaderNames.indexOf("QUANTITY");
		int idxTaxCode = allHeaderNames.indexOf("TAX CODE");
		int idxRevenueTerm = allHeaderNames.indexOf("REVENUE TERM");
		
		//Get total Rows count
		List<WebElement> allRows = itemsMenuTableRows;
		LogUtil.infoLog(NSLoginPage.class,"\nTotal Rows : "+allRows.size());	
		
		
		//Iterate through each row
		for( int i=1; i<allRows.size(); i++) {
			String locBillingSchedule = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxBillingSchedule+1)+"]/a";
			String locItemName = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxItemName+1)+"]/a";
			String locSrcTransactionKey = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxSrcTransactionKey+1)+"]";
			String locQuantity = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxQuantity+1)+"]";
			String locTaxCode = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxTaxCode+1)+"]";
			String locRevenueTerm  = "//*[@id='item_splits']/tbody/tr["+(i+1)+"]/td["+(idxRevenueTerm +1)+"]";
		
			
			//Fetch a single row
			WebElement itemTableRow = allRows.get(i);
			
			String itemName = itemTableRow.findElement(By.xpath(locItemName)).getText();
			String billingSchedule = itemTableRow.findElement(By.xpath(locBillingSchedule)).getText();
			String quantity = itemTableRow.findElement(By.xpath(locQuantity)).getText();
			String itemSrcTransactionKey = itemTableRow.findElement(By.xpath(locSrcTransactionKey)).getText();
			String taxCode = itemTableRow.findElement(By.xpath(locTaxCode)).getText();
			String revenueTerm = itemTableRow.findElement(By.xpath(locRevenueTerm)).getText();
			
			if(itemName.trim().isEmpty()) {
				LogUtil.errorLog(NSLoginPage.class,"Item is empty : "+itemName, null) ;
			}
			else {
				LogUtil.infoLog(NSLoginPage.class, "\n\nItem : "+itemName);
			}
			if(billingSchedule.trim().isEmpty()) {
				LogUtil.errorLog(NSLoginPage.class,"Billing Schedule is empty : "+billingSchedule, null) ;
			}
			else {
				LogUtil.infoLog(NSLoginPage.class, "\n\nBillingSchedule : "+billingSchedule);
			}
			if(itemSrcTransactionKey.trim().isEmpty()) {
				LogUtil.errorLog(NSLoginPage.class,"STLUK is empty : "+itemSrcTransactionKey, null) ;
			}
			else {
				LogUtil.infoLog(NSLoginPage.class, "\n\nSTLUK : "+itemSrcTransactionKey);
			}
			if(quantity.trim().isEmpty()) {
				LogUtil.errorLog(NSLoginPage.class,"Quantity is empty : "+quantity, null) ;
			}
			else {
				LogUtil.infoLog(NSLoginPage.class, "\n\nQuantity : "+quantity);
			}
			if(taxCode.trim().isEmpty()) {
				LogUtil.errorLog(NSLoginPage.class,"Tax Code is empty : "+taxCode, null) ;
			}
			else {
				LogUtil.infoLog(NSLoginPage.class, "\n\nTax Code : "+taxCode);
			}
			if(revenueTerm.trim().isEmpty()) {
				LogUtil.errorLog(NSLoginPage.class,"Revenue Term is empty : "+revenueTerm, null) ;
			}
			else {
				LogUtil.infoLog(NSLoginPage.class, "\n\nRevenue Term : "+revenueTerm);
			}
		}
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(billingTabLnk, "Billing Tab");
		CommonUtil.normalWait(2);
		String billToAddressText = CommonUtil.getText(billToAddressSpan, "Bill To Address").trim();
		
		String actualPrimaryAddress = dataMap.get("BillToAddress").trim();
		billToAddressText = billToAddressText.replaceAll("[^a-zA-Z0-9]"," ");
		billToAddressText = billToAddressText.replace("Map", "").trim();
		LogUtil.infoLog(getClass(), "Bill to Address is : "+billToAddressText);
		actualPrimaryAddress = actualPrimaryAddress.replaceAll("[^a-zA-Z0-9]"," ");
		if(!(dataMap.get("Customer").isEmpty())) {
		 actualPrimaryAddress = dataMap.get("Customer")+" "+actualPrimaryAddress;
		 LogUtil.infoLog(getClass(), "Primary Address is : "+actualPrimaryAddress);
		 Assert.assertTrue(actualPrimaryAddress.contains(billToAddressText), "Verifying Bill To address");
		 CommonUtil.attachScreenshotOfPassedTestsInReport();
		}
		else {
		 LogUtil.infoLog(getClass(), "Primary Address is : "+actualPrimaryAddress);
		 Assert.assertTrue(actualPrimaryAddress.contains(billToAddressText), "Verifying Bill To address");
		 CommonUtil.attachScreenshotOfPassedTestsInReport();	
		}
	}
	
	public void clickOnRelatedRecordsTab(HashMap<String, String> dataMap) {
		CommonUtil.scrollIntoView(usdAmountLnk, "USD amount Link");
		CommonUtil.waitForElementVisibilityWithDuration(relatedRecordsTab, 5, "Related Records Tab");
		CommonUtil.click(relatedRecordsTab, "Related Records Tab");
		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(internalRelatedRecordsTab, 5, "Internal Related Records Tab");
		CommonUtil.click(internalRelatedRecordsTab, "Internal Related Records Tab");
		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(accountingBookHeader, 5, "Accounting Book Header");
		Assert.assertTrue(CommonUtil.isElementDisplayed(accountingBookHeader, "Accounting Book Header"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
	}
	
	public void clickOnRevenueArrangementDate(HashMap<String, String> dataMap) {
		for(int i=1; i<internalRelatedRecordsTableRows.size();i++)
		{
			WebElement accountingBook = CommonUtil.findElement("//tr[contains(@id,'linksrow"+(i-1)+"')]/td[1]", "xpath", "Accounting Book element on row "+i);
			String accountingBookTxt = CommonUtil.getText(accountingBook, "Accounting Book text on row "+i);
			
			WebElement type = CommonUtil.findElement("//tr[contains(@id,'linksrow"+(i-1)+"')]/td[3]", "xpath", "Type element on row "+i);
			String typeTxt = CommonUtil.getText(type, "Type text on row "+i);
			
			if(accountingBookTxt.equals("Primary | IFRS") && typeTxt.equals("Revenue Arrangement")) {
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				WebElement date = CommonUtil.findElement("//tr[contains(@id,'linksrow"+(i-1)+"')]/td[2]/a[@id='links_displayval']", "xpath", "Date element on row "+i);
				CommonUtil.click(date, "Date link "+date.getText());
				CommonUtil.waitForElementVisibilityWithDuration(revenueArrangementsHeader, 10, "Revenue Arrangements Header");
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				break;
			}
		}
	}
	
	public void verifyTermsForSO(HashMap<String, String> dataMap) {
		terms = CommonUtil.findElement(termsStart+dataMap.get("Terms")+ termsEnd,"xpath", "Terms : "+dataMap.get("Terms"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(terms, "terms : "+dataMap.get("Terms")));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void verifyshipToAddressForSO(HashMap<String, String> dataMap) {
		CommonUtil.click(shippingTabLnk, "Shipping Tab");
		CommonUtil.normalWait(2);
		String shipToAddressText = CommonUtil.getText(shipToAddressSpan, "Ship To Address").trim();
		LogUtil.infoLog(getClass(), "Ship to Address is : "+shipToAddressText);
		shipToAddress = CommonUtil.findElement(shipToAddressStart+dataMap.get("ShipAddress1")+ shipToAddressEnd,"xpath", "First line of address : "+dataMap.get("ShipAddress1"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(shipToAddress, "Ship to Address line 1"));
		shipToAddress = CommonUtil.findElement(shipToAddressStart+dataMap.get("ShipAddress2")+ shipToAddressEnd,"xpath", "Second line of address : "+dataMap.get("ShipAddress2"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(shipToAddress, "Ship to Address line 2"));
		shipToAddress = CommonUtil.findElement(shipToAddressStart+dataMap.get("ShipAddress3")+ shipToAddressEnd,"xpath", "Third line of address : "+dataMap.get("ShipAddress3"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(shipToAddress, "Ship to Address line 3"));
		shipToAddress = CommonUtil.findElement(shipToAddressStart+dataMap.get("ShipAddress4")+ shipToAddressEnd,"xpath", "Fourth line of address : "+dataMap.get("ShipAddress4"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(shipToAddress, "Ship to Address line 4"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void searchSoOnSalesOrderPage(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(salesOrderHeader, 15, "Sales Order Heading");
		CommonUtil.click(transactionSearchLnk, "Search Link");
		CommonUtil.waitForElementVisibilityWithDuration(transactionSearchHeader, 15, "Transaction Search Header");
		CommonUtil.scrollIntoView(periodLnk, "Period Link");
		CommonUtil.waitForElementVisibilityWithDuration(transactionNumberTxtInput, 5, "Transaction Number Text Input");
		CommonUtil.inputText(transactionNumberTxtInput, dataMap.get("SO Number"), "Entered SO Number : "+dataMap.get("SO Number"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollIntoView(secondarySubmitBtn, "Secondary Submit Button");
		CommonUtil.waitForElementVisibilityWithDuration(secondarySubmitBtn, 5, "Secondary Submit Button");
		CommonUtil.click(secondarySubmitBtn, "Secondary Submit Button");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
