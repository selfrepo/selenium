package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtil;
import utilities.ConfigReader;

public class GmailPage extends CommonUtil{
	
	GmailPage gmailPage;
	
	@FindBy(css = "input[type='email']")
	WebElement emailTextBox;
	
	@FindBy(css = "#identifierNext > div > button > span")
	WebElement nextButton;
	
	@FindBy(css = "input[type='password']")
	WebElement passwordTextBox;
	
	@FindBy(xpath = "//span[@class='bog']")
	List<WebElement> emailThreads;
	
	@FindBy(xpath = "//img[@class='gb_Ba gbii']")
	WebElement profileLogo;
	
	@FindBy(xpath = "//a/div[text()='Sign out']")
	WebElement sigOut;
	
	@FindBy(css = "td[valign='middle'] > a[href*='https://email.email.software.microfocus.com']")
	WebElement confirmEmailAddress;
	
	@FindBy(xpath = "//h1[@id='headingText']/span[text()='Verify it’s you']")
	WebElement verificationHeader;
	
	@FindBy(css = "#view_container li:nth-child(4) div div:nth-child(2)")
	WebElement phoneNumberConfirm;
	
	@FindBy(css = "#phoneNumberId")
	WebElement phoneNumberTextbox;
	
    @FindBy(xpath = "//span[text()='More']")
    WebElement moreoption;
    
    @FindBy(xpath = "//a[text()='Spam']")
    WebElement spamfolder;
	
	
	public void loginToGmail() {
		CommonUtil.navigate(ConfigReader.getValue("gmailUrl"));
		CommonUtil.inputTextWithEnterKey(emailTextBox, ConfigReader.getValue("gmailId"));
		CommonUtil.normalWait(4);
		CommonUtil.waitForVisible(passwordTextBox, "password textbox");
		CommonUtil.inputTextWithEnterKey(passwordTextBox, ConfigReader.getValue("gPassword"));
	}
	
	public void clickEmailWithSubject(String subject) {
		try {
			if(verificationHeader.isDisplayed()) {
				CommonUtil.click(phoneNumberConfirm, "select phone number option");
				CommonUtil.inputTextWithEnterKey(phoneNumberTextbox, "9949044969");				
			}
		}catch(Exception e) {
			CommonUtil.waitForVisible(profileLogo, "profile logo");
		}		
		for(int i=0; i<emailThreads.size(); i++) {
			if(emailThreads.get(i).getText().contains(subject)) {
				emailThreads.get(i).click();
				System.out.println("email clicked");
				CommonUtil.normalWait(10);
				CommonUtil.waitForVisible(confirmEmailAddress, "Confirm email Address button");
				CommonUtil.click(confirmEmailAddress, "Confirm Email Address");
				break;				
			}
				
		}
		
	}

	
	public void verifyrecentemailid(String fromemailid) {
        WebElement frommailid = CommonUtil.getWebElement("xpath", "//div[contains(text(),'unread')]//parent::td//div[@class='yW']//span[@email='{replacevalue}']", fromemailid);
        Boolean elementdisplayed ;
        try {
            elementdisplayed = CommonUtil.elementisDisplayed(frommailid, "received email id");
        } catch (Exception e) {
            elementdisplayed = false;
        }
        if(elementdisplayed.equals(false)) {
            CommonUtil.clickUsingJavaScriptExecutor(moreoption, "Click on More Option");
            CommonUtil.normalWait(4);
            CommonUtil.clickUsingJavaScriptExecutor(spamfolder,"Click on Spam Folder");
            WebElement frommailids = CommonUtil.getWebElement("xpath", "//div[contains(text(),'unread')]//parent::td//div[@class='yW']//span[@email='{replacevalue}']", fromemailid);
            CommonUtil.isElementDisplayed(frommailids, "received email id in Spam");
        }
    }

}
