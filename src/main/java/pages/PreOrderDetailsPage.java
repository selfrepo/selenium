package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import utilities.CommonUtil;
import utilities.HTMLReportUtil;
import utilities.LogUtil;
import utilities.ExtentReports.ExtentTestManager;

public class PreOrderDetailsPage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================

	String ordersColumnNameStart = "//table[@aria-label = 'Orders']//thead//tr[1]//th[" ;
	String ordersRowNameStart = "//table[@aria-label = 'Orders']//tbody[1]//tr[";
	String ordersColumnNameEnd = "]//span[2]" ; 
	String rowOrders = "//table[@aria-label = 'Orders']//tbody[1]//tr";
	String colOrders ="//table[@aria-label = 'Orders']//tbody[1]//tr[1]//th | //table[@aria-label = 'Orders']//tbody[1]//tr[1]//td";

	// ================================================================================
	// Objects
	// ================================================================================	
			
	@FindBy(xpath = "//*[text()='Orders (1)']")
	private WebElement preOrderClick;
	
	@FindBy(xpath = "(//*[text()='Order Number'])[2]//following::span[1]")
	private WebElement eleOrderNumber;
	
	@FindBy(xpath = "//*[text()='Order Type']//following::span[1]")
	private WebElement eleOrderType;
	
	@FindBy(xpath = "(//*[text()='Status'])[2]//following::span[1]")
	private WebElement eleStatusInPreOrderPage;
	
	@FindBy(xpath = "(//*[text()='Pre Order Status'])[2]//following::span[1]")
	private WebElement elePreOrderStatus;
	
	@FindBy(xpath = "//*[text()='NetSuite Status']//following::span[1]")
	private WebElement eleNetsuiteStatus;
	
	@FindBy(xpath = "//*[text()='NetSuite Finance Approved/Reject Status']//following::span[1]")
	private WebElement eleNetSuiteFinanceStatus;
	
	@FindBy(xpath = "//*[text()='NetSuite SO Number']//following::span[1]")
	private WebElement eleNetSuiteSONumber;
	
	@FindBy(xpath = "//*[text()='OSGT Status']//following::span[1]")
	private WebElement eleOSGTStatus;
	
	@FindBy(xpath = "//*[text()='Export Compliance Status']//following::span[1]")
	private WebElement eleExportComplianceStatus;
	
	@FindBy(xpath = "//*[text()='SLD Status']//following::span[1]")
	private WebElement eleSLDStatus;
	
	@FindBy(xpath = "//span[text()='NetSuite RevRec Status']//following::span[2]")
	private WebElement eleNetSuiteRevRecStatus;
	
	@FindBy(xpath = "//span[text()='NS Invoice Status']//following::span[1]")
	private WebElement eleNSInvoiceStatus;
	
	@FindBy(xpath = "//span[text()='NS Invoice Internal Id']//following::span[1]")
	private WebElement eleNSInvoiceInternalId;
	
	@FindBy(xpath = "//span[text()='NS Invoice Number']//following::span[1]")
	private WebElement eleNSInvoiceNumber;
	

	// ================================================================================
	// Methods
	// ================================================================================
	public void clickPreOrderAndValidate(HashMap<String, String> dataMap) throws InterruptedException {
		
		CommonUtil.normalWait(10);
		CommonUtil.click(preOrderClick, "Click Pre-Order link in Quote details page under Related list");
		CommonUtil.normalWait(10);
		
		List<WebElement> rowCountOrders = CommonUtil.getAllElements(rowOrders, "Pre-Order Page");
		
		int rowsizeOrders = rowCountOrders.size();

		List<WebElement> colCountOrders =CommonUtil.getAllElements(colOrders, "Pre-Order Page");
		int colsizeOrders = colCountOrders.size();
		
		    outerloop:
			for (int j = 1; j <= colsizeOrders; j++) {
				String OrdersColNameExpected = CommonUtil.getText(CommonUtil.findElement(ordersColumnNameStart+j+ ordersColumnNameEnd, "xpath","Order Column Element"), "Order Column Element text");
				String OrdersColumnName = dataMap.get("OrderNumberColumn").trim();
				if (OrdersColNameExpected.trim().equals(OrdersColumnName)) {
					for (int i = 1; i <= rowsizeOrders; i++) {
					String OrderRowNameExpected = driver.findElement(By.xpath("//table[@aria-label = 'Orders']//tbody[1]//tr//th["+i+"]//span[1]//a[1]")).getText().trim();
					CommonUtil.attachScreenshotOfPassedTestsInReport();
					driver.findElement(By.xpath("//table[@aria-label = 'Orders']//tbody[1]//tr//th["+i+"]//span[1]//a[1]")).click();
					CommonUtil.waitForElementVisibilityWithDuration(eleOrderNumber, 20, "Order Number");
					Assert.assertTrue(CommonUtil.isElementDisplayed(eleOrderNumber, OrderRowNameExpected));
					CommonUtil.attachScreenshotOfPassedTestsInReport();
					String ValOrderNumber = CommonUtil.getText(eleOrderNumber,"Order Number");
					CommonUtil.storedata("OrderNumber", ValOrderNumber);
					LogUtil.infoLog(getClass(), "Order Number is : "+ValOrderNumber);
					if(!ValOrderNumber.isEmpty()) {
					CommonUtil.attachScreenshotOfPassedTestsInReport();
					break outerloop;
					 }
					}	
				}
				
		}
	}
	
	public void statusValidationInPreOrderPage(HashMap<String, String> dataMap) throws InterruptedException {
		CommonUtil.refresh("Pre Order Page");
		CommonUtil.normalWait(10);
		
		// Order Type in PreOrder Page
		String ValOrderType = CommonUtil.getText(eleOrderType,"Order Type");
		Assert.assertTrue(ValOrderType.equals(dataMap.get("OrderType")),"Order Type displayed as");
		
		// Status in PreOrder Page
		String ValStatusInPreOrderPage = CommonUtil.getText(eleStatusInPreOrderPage,"Status in Pre- Order page");
		Assert.assertTrue(ValStatusInPreOrderPage.equals(dataMap.get("StatusInPreOrderPage")),"Status in PreOrder page displayed as");
		
		// PreOrder Status in PreOrder Page
		String ValPreOrderStatus = CommonUtil.getText(elePreOrderStatus,"Pre- Order Status");
		Assert.assertTrue(ValPreOrderStatus.equals(dataMap.get("PreOrderStatus")),"Pre Order Status displayed as");
		
		// NetSuite Status in PreOrder Page
		String ValNetsuiteStatus = CommonUtil.getText(eleNetsuiteStatus,"Netsuite Status");
		Assert.assertTrue(ValNetsuiteStatus.equals(dataMap.get("NetsuiteStatus")),"Netsuite Status displayed as");
		
		// NetSuite Finance Approved/Reject Status in PreOrder Page
		String ValNetsuiteFinanceStatus = CommonUtil.getText(eleNetSuiteFinanceStatus,"NetSuite Finance Approved/Reject Status");
		Assert.assertTrue(ValNetsuiteFinanceStatus.equals(dataMap.get("NetSuiteFinanceStatus")),"NetSuite Finance Approved/Reject Status displayed as");
		
		// NetSuite SO Number in PreOrder Page
		String ValNetsuiteSONumber = CommonUtil.getText(eleNetSuiteSONumber,"NetSuite SO Number");
		Assert.assertTrue(ValNetsuiteSONumber.equals(dataMap.get("NetSuiteSONumber")),"NetSuite SO Number displayed as");
		
		// OSGT Status in PreOrder Page
		String ValOSGTStatus = CommonUtil.getText(eleOSGTStatus,"OSGT Status");
		Assert.assertTrue(ValOSGTStatus.equals(dataMap.get("OSGTStatus")),"OSGT Status displayed as");
				
		// Export Compliance Status in PreOrder Page
		String ValExportComplianceStatus = CommonUtil.getText(eleExportComplianceStatus,"Export Compliance Status");
		Assert.assertTrue(ValExportComplianceStatus.equals(dataMap.get("ExportComplianceStatus")),"Export Compliance Status displayed as");
		
		// SLD Status in PreOrder Page
		String ValSLDStatus = CommonUtil.getText(eleSLDStatus,"SLD Status");
		Assert.assertTrue(ValSLDStatus.equals(dataMap.get("SLDStatus")),"SLD Status displayed as");	
	}
	
	public void statusValidationForInvoice(HashMap<String, String> dataMap) throws InterruptedException {
		CommonUtil.refresh("Pre Order Page");
		CommonUtil.normalWait(10);
		
		// Status in PreOrder Page
		String ValStatusInPreOrderPage = CommonUtil.getText(eleStatusInPreOrderPage,"Status in Pre- Order page");
		Assert.assertTrue(ValStatusInPreOrderPage.equals(dataMap.get("StatusInPreOrderPage")),"Status in PreOrder page displayed as");
		
		// PreOrder Status in PreOrder Page
		String ValPreOrderStatus = CommonUtil.getText(elePreOrderStatus,"Pre- Order Status");
		Assert.assertTrue(ValPreOrderStatus.equals(dataMap.get("PreOrderStatus")),"Pre Order Status displayed as");
		
		// NetSuite Status in PreOrder Page
		String ValNetsuiteStatus = CommonUtil.getText(eleNetsuiteStatus,"Netsuite Status");
		Assert.assertTrue(ValNetsuiteStatus.equals(dataMap.get("NetsuiteStatus")),"Netsuite Status displayed as");
		
		// OSGT Status in PreOrder Page
		String ValOSGTStatus = CommonUtil.getText(eleOSGTStatus,"OSGT Status");
		Assert.assertTrue(ValOSGTStatus.equals(dataMap.get("OSGTStatus")),"OSGT Status displayed as");
				
		// Export Compliance Status in PreOrder Page
		String ValExportComplianceStatus = CommonUtil.getText(eleExportComplianceStatus,"Export Compliance Status");
		Assert.assertTrue(ValExportComplianceStatus.equals(dataMap.get("ExportComplianceStatus")),"Export Compliance Status displayed as");
		
		// SLD Status in PreOrder Page
		String ValSLDStatus = CommonUtil.getText(eleSLDStatus,"SLD Status");
		Assert.assertTrue(ValSLDStatus.equals(dataMap.get("SLDStatus")),"SLD Status displayed as");
		
		// NetSuite RevRec Status in PreOrder Page
		String ValNetSuiteRevRecStatus = CommonUtil.getText(eleNetSuiteRevRecStatus,"NetSuite RevRec Status");
		Assert.assertTrue(ValNetSuiteRevRecStatus.equals(dataMap.get("NetSuiteRevRecStatus")),"NetSuite RevRec Status displayed");
		
		// NS Invoice Status in PreOrder Page
		String ValNSInvoiceStatus = CommonUtil.getText(eleNSInvoiceStatus,"NS Invoice Status");
		Assert.assertTrue(ValNSInvoiceStatus.equals(dataMap.get("NSInvoiceStatus")),"NS Invoice Status displayed");
		
		// NS Invoice Internal Id in PreOrder Page
		String ValNSInvoiceInternalId = CommonUtil.getText(eleNSInvoiceInternalId,"NS Invoice Internal Id");
		ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor("NS Invoice Internal Id : "+ValNSInvoiceInternalId));
		
		// NS Invoice Number in PreOrder Page
		String ValNSInvoiceNumber = CommonUtil.getText(eleNSInvoiceNumber,"NS Invoice Number");
		ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor("NS Invoice Number : "+ValNSInvoiceNumber));

	}
}


			
			
	

		
