package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import junit.framework.Assert;
import utilities.CommonUtil;
import utilities.LogUtil;




public class PortfolioInterestsPage extends CommonUtil{
	// ================================================================================
	// Variables
	// ================================================================================
		
	String accountLnkStart = "//div[span[text()='Account Name']]/following-sibling::div//a[contains(.,'";
	String accountLnkEnd ="')]";
	
	String accountNameTxtStart = "//div[span[text()='Account Name']]/following-sibling::div//span//lightning-formatted-text[text()='";
	String accountNameTxtEnd ="']";
	
	String oppLnkStart = "//force-lookup[@data-navigation='enable']//div[@class='slds-grid']//a[slot[slot[span[text()='";
	String oppLnkEnd = "']]]]";
	WebElement oppLnk;
	

	@FindBy(xpath = "//button[@name='Portfolio__c.CreateOpportunity']")
	private WebElement btnCreateOpportunityPI;
	
	@FindBy(xpath = "//input[@name='accountname']")
	private WebElement enterAccountName;
	
	@FindBy(xpath = "//input[@name='oppname']")
	private WebElement enterOppName;
	
	@FindBy(xpath = "//input[@name='inputDate']")
	private WebElement selCloseDate;
	
	@FindBy(xpath = "//select[@name='SelectRT']")
	private WebElement selRecordType;
	
	@FindBy(xpath = "//span[text()='Record Type']//following::span[text()='Save']")
	private WebElement btnSaveCreateOpportunity;
	
	@FindBy(xpath = "//label[text()='Opportunity Name']")
	private WebElement oppNameLabel;
	
	@FindBy(xpath = "//a[span[normalize-space()='Home']]")
	private WebElement homeTxt;
	
	@FindBy(xpath = "//label[span[text()='MGO']]/preceding-sibling::input")
	private WebElement mgoCheckbox;
	
	@FindBy(xpath = "//div[span[text()='Stage']]/following-sibling::div//span//lightning-formatted-text[text()='Stage 0: Marketing Generated Opportunity']")
	private WebElement stageStatusTxt;
	
	@FindBy(xpath = "//span[normalize-space()='Preferred Language']")
	private WebElement preferredLanguageTxt;
	
	@FindBy(xpath = "//p[@title='Account Number']")
	private WebElement accountNumberTxt;
	
	@FindBy(xpath = "//span[normalize-space()='Invoice Preference']")
	private WebElement invoicePreferenceTxt;
	
	@FindBy(xpath = "//div[span[text()='Account Verification Status']]/following-sibling::div//span//lightning-formatted-text[text()='Unverified']")
	private WebElement accountUnverifiedTxt;
	
	@FindBy(xpath = "//div[span[text()='Case Owner']]/following-sibling::div//slot[contains(text(),'Account Management')]")
	private WebElement caseOwnerTxt;
	
	@FindBy(xpath = "//span[text()='Service Contract']")
	private WebElement addtnlInfoTxt;
	
	@FindBy(xpath = "//span[@class='title slds-path__title'][normalize-space()='Stage 0: Marketing Generated Opportunity']")
	private WebElement stageZeroTxt;
	
	@FindBy(xpath = "//button[span[text()='Goto Portfolio']]")
	private WebElement gotoPortfolioBtn;
	
	@FindBy(xpath = "//button[@aria-label='Search']")
	private WebElement searchButton;

	@FindBy(xpath = "//div[@class='forceSearchAssistantDialog']/div/div[@data-aura-class='forceSearchInputEntitySelector']//following-sibling::lightning-input/div/input")
	private WebElement searchBarInput;
	
	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//a[contains(text(),'Show All')]")
	private WebElement showAllLnk;
	
	@FindBy(xpath = "//p[@title='Account Number']/following-sibling::p")
	private WebElement accNumberPTxt;
	
	@FindBy(xpath = "//h2[text()='Related List Quick Links']")
	private WebElement relatedListTxt;
	
	@FindBy(xpath = "//a[slot[span[contains(text(),'Cases')]]]")
	private WebElement casesLnk;
	
	@FindBy(xpath = "//thead[tr[th[div[a[span[text()='Case']]]]]]/following-sibling::tbody//th//a")
    private WebElement casesColumnValueLnk;
	
	@FindBy(xpath = "//span[@title='Portfolio Interests (Primary Portfolio Interest)']")
	private WebElement portfolioInterestTxt;	
	
	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//p[@title='Primary Address']/following-sibling::p//lightning-formatted-address/a/div")
	private List<WebElement> accountPrimaryAddressDivs;
	
	//Methods
	public void clickPIInTable(HashMap<String, String> dataMap) {
		List<WebElement> rowCountPI = driver.findElements(By.xpath("//table[@aria-label = 'Portfolio Interests']//tbody[1]//tr"));
    	System.out.println(rowCountPI.size());
    	int rowsize = rowCountPI.size();
    	List<WebElement> colCountPI = driver.findElements(By.xpath("//table[@aria-label = 'Portfolio Interests']//thead//tr[1]//th"));
    	System.out.println(colCountPI.size());
    	int colsize = colCountPI.size();
    	
    	for (int i=1; i<=rowsize; i++)
    	{
    		for (int j=1; j<=colsize; j++)
    		{
    			String PIColNameExpected = driver.findElement(By.xpath("//table[@aria-label = 'Portfolio Interests']//thead//tr[1]//th[" + j + "]")).getText();
    			if (PIColNameExpected.contains(dataMap.get("PIColumnName")))
    			{
    				String PIRowNameExpected = driver.findElement(By.xpath("//table[@aria-label = 'Portfolio Interests']//tbody[1]//tr[" + i + "]")).getText();
    				if (PIRowNameExpected.contains(dataMap.get("PIVal")))
    				{
    					CommonUtil.attachScreenshotOfPassedTestsInReport();
    					driver.findElement(By.xpath("//table[@aria-label = 'Portfolio Interests']//tbody[1]//tr[" + i + "]//th[1]//span[1]//a[1]")).click();
    					CommonUtil.waitForElementVisibilityWithDuration(btnCreateOpportunityPI,15,"Create Opportunity button in Portfolio Interests Page");
    					CommonUtil.attachScreenshotOfPassedTestsInReport();
    					Assert.assertTrue(CommonUtil.isElementDisplayed(btnCreateOpportunityPI, "Portfolio Interets Page displayed"));
    					if (btnCreateOpportunityPI.isDisplayed())
    					{
    						i = rowsize + 1;
    						j = colsize + 1;
    					}
    				}
    				else
    				{
    					break;
    				}
    			}
    		}
    	}
	}


	public void createOpportunityPI(HashMap<String, String> dataMap) {
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(btnCreateOpportunityPI, 5,"Create Opportunity Button");
		CommonUtil.click(btnCreateOpportunityPI, "Create Opportunity in Portfolio Interests Page in Related Section");
		CommonUtil.waitForElementVisibilityWithDuration(enterAccountName, 5,"Enter Account Name");
		Assert.assertTrue(CommonUtil.isElementDisplayed(enterAccountName, "Create Opportunity Page"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	    int randomNumber= CommonUtil.generateRandomNumber(1, 1000);
		CommonUtil.inputText(enterAccountName,dataMap.get("AccountName")+randomNumber,"Account Name as "+dataMap.get("AccountName")+randomNumber);
		CommonUtil.scrollIntoView(oppNameLabel, "Opportunity Name Label");
		Assert.assertTrue(CommonUtil.isElementDisplayed(enterOppName, "Opportunity Name field"));
		CommonUtil.inputText(enterOppName,dataMap.get("PIOpportunityName")+randomNumber,"Opportunity Name as "+dataMap.get("PIOpportunityName")+randomNumber);
		CommonUtil.inputText(selCloseDate,CommonUtil.getFutureDate("dd-MMM-yyyy", 45),"Future Date");
		CommonUtil.selectByValue(selRecordType,dataMap.get("RecordType"));
		Assert.assertTrue(CommonUtil.isElementDisplayed(enterAccountName, "Create Opportunity Page"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(btnSaveCreateOpportunity, "Save button on Create Opportunity Page");
		CommonUtil.normalWait(20);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.waitForElementVisibilityWithDuration(gotoPortfolioBtn, 20,"Goto portfolio");
		CommonUtil.click(gotoPortfolioBtn, "Goto portfolio");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.refresh("Portfolio Page");
		CommonUtil.normalWait(15);
		CommonUtil.normalWait(10);
		oppLnk = CommonUtil.findElement(oppLnkStart + dataMap.get("PIOpportunityName")+randomNumber + oppLnkEnd, "xpath", "Opportunity "+dataMap.get("PIOpportunityName")+randomNumber);
		CommonUtil.waitForVisible(oppLnk, "Opportunity "+dataMap.get("PIOpportunityName")+randomNumber);
		//Storing the opportunity name
		CommonUtil.storedata("OpportunityName", dataMap.get("PIOpportunityName")+randomNumber);
		CommonUtil.clickUsingJavaScriptExecutor(oppLnk, "Opportunity "+dataMap.get("PIOpportunityName")+randomNumber);
		CommonUtil.normalWait(10);	
		CommonUtil.scrollIntoView(stageZeroTxt,"Stage 0 text");
		Assert.assertTrue(CommonUtil.isElementDisplayed(stageStatusTxt, "Stage 0 status"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollIntoView(addtnlInfoTxt, "Additional Information Text");
		Assert.assertTrue(CommonUtil.isElementSelected(mgoCheckbox, "MGO Checkbox"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollIntoView(stageZeroTxt,"Stage 0");
		WebElement accountLnk = CommonUtil.findElement(accountLnkStart+dataMap.get("AccountName")+randomNumber+accountLnkEnd, "xpath", "Account link "+dataMap.get("AccountName")+randomNumber);
		CommonUtil.waitForElementVisibilityWithDuration(accountLnk,2,"Account Link "+dataMap.get("AccountName")+randomNumber);	
		CommonUtil.clickUsingJavaScriptExecutor(accountLnk, "Account Link "+dataMap.get("AccountName")+randomNumber);
		CommonUtil.normalWait(5);
		int size = accountPrimaryAddressDivs.size();
		String primaryAddress = "";
		for(int i=1; i<3; i++) {
			String primaryAddressLine = driver.findElement(By.xpath("(//div[@class='windowViewMode-normal oneContent active lafPageHost']//p[@title='Primary Address']/following-sibling::p//lightning-formatted-address/a/div)["+i+"]")).getText().trim();
			primaryAddress = primaryAddress+" "+primaryAddressLine;
		}
		LogUtil.infoLog(getClass(), "Primary address is : "+primaryAddress);
		CommonUtil.storedata("PrimaryAddress", primaryAddress);
		WebElement accountTxt =CommonUtil.findElement(accountNameTxtStart+dataMap.get("AccountName")+randomNumber+accountNameTxtEnd, "xpath", "Account Name Text "+dataMap.get("AccountName")+randomNumber);
		CommonUtil.isElementDisplayed(accountTxt, "Account Name Text");
		CommonUtil.storedata("CustomerName", dataMap.get("AccountName")+randomNumber);
		CommonUtil.scrollIntoView(preferredLanguageTxt, "Preferred Language field");
		Assert.assertTrue(CommonUtil.isElementDisplayed(accountUnverifiedTxt, "Account Unverified"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.refresh("Account Page");
		CommonUtil.normalWait(15);
		CommonUtil.scrollIntoView(relatedListTxt, "Related Link");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(showAllLnk, 5, "Show All link");
		CommonUtil.click(showAllLnk, "Show All link");
		CommonUtil.normalWait(2);
		CommonUtil.scrollIntoView(relatedListTxt, "Related Link");
		CommonUtil.click(casesLnk, "Cases link");
		CommonUtil.normalWait(5);
		CommonUtil.click(casesColumnValueLnk, "Case cloumn value link");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(caseOwnerTxt, 5, "Case owner account management text");
		Assert.assertTrue(CommonUtil.isElementDisplayed(caseOwnerTxt, "Case owner account management text"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
