package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.LogUtil;

public class ContractLineItemsPage extends CommonUtil{
	
	String contractLineItemsColumnNameStart = "//table[@aria-label = 'Contract Line Items']//thead//tr[1]//th[" ;
	String contractLineItemsRowNameStart = "//table[@aria-label = 'Contract Line Items']//tbody[1]//tr[";
	String contractLineItemsColumnNameEnd = "]//span[2]" ; 
	String rowContractLineItems = "//table[@aria-label = 'Contract Line Items']//tbody[1]//tr";
	String colContractLineItems = "//table[@aria-label = 'Contract Line Items']//tbody[1]//tr[1]//th | //table[@aria-label = 'Service Contracts']//tbody[1]//tr[1]//td";
	String contractLineItemsLnkStart = "//table[@aria-label='Contract Line Items']/tbody//th//a[text()='";
	String contractLineItemsLnkEnd = "']";
	
	@FindBy(xpath = "//a[slot[span[contains(text(),'Contract Line Items')]]]")
	private WebElement lnkContractLineItems;
	
	@FindBy(xpath = "//a[contains(text(),'Show All')]")
	private WebElement lnkShowAll;
	
	@FindBy(xpath = "(//span[contains(text(),'Entitlements')])[1]")
	private WebElement lnkEntitlements;
	
	@FindBy(xpath = "(//div[span[text()='Line Item Number']]/following-sibling::div//lightning-formatted-text)[1]")
	private WebElement lineItemNumberTxt;
	
	@FindBy(xpath = "//h1[normalize-space()='Contract Line Items']")
	private WebElement contractLineItemsHeader;
	
	@FindBy(xpath = "//h1/div[text()='Service Contract']")
	private WebElement serviceContractHeader;
	
   public void clickRelatedListContractLineItems(HashMap<String, String> dataMap) throws InterruptedException {
		
		CommonUtil.refresh("Service Contract Page");
		CommonUtil.normalWait(10);
		CommonUtil.click(lnkContractLineItems, "Click Contract Line Items link");
		CommonUtil.waitForElementVisibilityWithDuration(contractLineItemsHeader, 10, "Contract Line Items Header text");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
   }
   
   public void validateLineItemsNoProductNoAndParentProducts(HashMap<String, String> dataMap) throws InterruptedException {
		List<WebElement> rowCountContractLineItems = CommonUtil.getAllElements(rowContractLineItems, "Contract Line Items Page");
		int rowsizeContractLineItems = rowCountContractLineItems.size();

			for (int i = 1; i <= rowsizeContractLineItems; i++) {
				WebElement lineItemNumber = driver.findElement(By.xpath("//table[@aria-label='Contract Line Items']/tbody/tr["+i+"]/th//a"));
				String lineItemNumberTxt = CommonUtil.getText(lineItemNumber, "Getting Line Item Number text for row "+i);
				LogUtil.infoLog(getClass(), "Row "+i+" "+ "line item number is : "+lineItemNumberTxt);
				
				WebElement productNumber= driver.findElement(By.xpath("(//table[@aria-label='Contract Line Items']/tbody/tr["+i+"]/td/span/a)[1]"));
				String productNumberTxt = CommonUtil.getText(productNumber, "Getting Product Number text for row "+i);
				LogUtil.infoLog(getClass(), "Row "+i+" "+ "line product number is : "+productNumberTxt);
				
				WebElement parentProduct = driver.findElement(By.xpath("//table[@aria-label='Contract Line Items']/tbody/tr["+i+"]/td[3]/span/a | //table[@aria-label='Contract Line Items']/tbody/tr["+i+"]/td[3]/span"));
				String parentProductTxt = CommonUtil.getText(parentProduct, "Getting Parent Product text for row "+i);
				LogUtil.infoLog(getClass(), "Row "+i+" "+ "line item number is : "+parentProductTxt);
			}
			CommonUtil.attachScreenshotOfPassedTestsInReport();
	
	}
	
   public void clickLineItemsNumber(HashMap<String, String> dataMap) throws InterruptedException {
		
		CommonUtil.waitForElementVisibilityWithDuration(contractLineItemsHeader, 10, "Contract Line Items Header text");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		WebElement lineItemNumber= CommonUtil.findElement(contractLineItemsLnkStart+dataMap.get("Line Item Number")+contractLineItemsLnkEnd, "xpath", "Line item number "+dataMap.get("Line Item Number"));
		CommonUtil.click(lineItemNumber, "Line item number "+dataMap.get("Line Item Number"));
		CommonUtil.waitForElementVisibilityWithDuration(lineItemNumberTxt, 10, "Line item number text");
		String valItemNumber= CommonUtil.getText(lineItemNumberTxt,"Line item number");
		Assert.assertTrue(valItemNumber.equals(dataMap.get("Line Item Number")),"Verifying the Item Number values");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
  }
   
   public void navigateBackToServiceContractPage() throws InterruptedException {
   
	   CommonUtil.back("Contract Line Items Page");
	   CommonUtil.back("Service Contract details Page");
	   CommonUtil.waitForElementVisibilityWithDuration(serviceContractHeader, 10, "Service Contract Header");
	   CommonUtil.attachScreenshotOfPassedTestsInReport();
   }
   
}



