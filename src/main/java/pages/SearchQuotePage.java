package pages;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;

public class SearchQuotePage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================
	private WebElement matchedData;

	private String matchedDataStart = "//span[not(contains(text(),'Legal Document for') or contains(text(),'Micro Focus Quote') )]/mark[text()='";

	private String matchedDataEnd = "']";

	// match lead
	private WebElement matchedLead;

	private String matchedLeadStart = "//span[@title='";

	private String matchedLeadEnd = "']";

	// ================================================================================
	// Objects
	// ================================================================================
	@FindBy(xpath = "//div[@class='slds-icon-waffle']")
	private WebElement sideMenuNavigationBar;

	@FindBy(xpath = "//a[@id='07p1t000000cZWHAA2']//div[@class='slds-size_small']")
	private WebElement salesLink;

	@FindBy(xpath = "//span[contains(text(),'My Approvals')]")
	private WebElement myApprovalsSpan;

	@FindBy(xpath = "//iframe[@title='dashboard']")
	private WebElement iframeSales;

	@FindBy(xpath = "//span[normalize-space()='Dashboard']")
	private WebElement dashboardText;

	@FindBy(xpath = "//button[@aria-label='Search']")
	private WebElement searchButton;

	@FindBy(xpath = "//div[@class='forceSearchAssistantDialog']/div/div[@data-aura-class='forceSearchInputEntitySelector']//following-sibling::lightning-input/div/input")
	private WebElement searchBarInput;

	@FindBy(css = "div[title='Status']")
	private WebElement statusDiv;

	private String quoteNumberLink = "//a[@title='quote#']";
	
	private String opportunityNumberLink = "//a[@title='Opportunity#']";
	
	@FindBy(xpath = "//span[contains(text(),'Show more results for')]") 
	private WebElement showmoreresult;
	
	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//a[@data-refid='recordId']")
	private WebElement matchedDatalink;
	
	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//a[text()='Details']")
    private WebElement LeadDetail;
	
	@FindBy(xpath = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr[1]/td[6]/span[@class='slds-grid slds-grid--align-spread']/a[contains(@href,'mailto:')]")
	private WebElement matchedLeadData;

	// ================================================================================
	// Methods
	// ================================================================================
	public void searchQuote(HashMap<String, String> dataMap) {

		CommonUtil.waitForElementVisibilityWithDuration(sideMenuNavigationBar, 5, "Navigation bar");
		CommonUtil.click(sideMenuNavigationBar, "Navigation bar");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(salesLink, "Sales Cloud");
		CommonUtil.clickUsingJavaScriptExecutor(salesLink, "Sales Cloud");
		CommonUtil.waitForElementVisibilityWithDuration(myApprovalsSpan, 15, "My approvals");
		Assert.assertTrue(CommonUtil.isElementDisplayed(myApprovalsSpan, "My approvals"));
		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(searchButton, 5, "global search bar");
		CommonUtil.click(searchButton, "global search bar");
		CommonUtil.waitForVisible(searchBarInput, "global search box");
		CommonUtil.inputText(searchBarInput, dataMap.get("QuoteNumber"), "Enter Quote Number : "+dataMap.get("QuoteNumber"));
		matchedData = CommonUtil.findElement(matchedDataStart + dataMap.get("QuoteNumber") + matchedDataEnd, "xpath", "Searched Quote " + dataMap.get("QuoteNumber"));
		CommonUtil.waitForVisible(matchedData, "Searched Quote " + dataMap.get("QuoteNumber"));
		CommonUtil.click(matchedData, "matched data");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	public void globalSearchQuote(String quoteNumber) {

		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(searchButton, 5, "global search bar");
		CommonUtil.click(searchButton, "global search bar");
		CommonUtil.waitForVisible(searchBarInput, "global search box");
		CommonUtil.inputTextWithEnterKey(searchBarInput, quoteNumber);
		CommonUtil.normalWait(15);
		String matchString = quoteNumberLink.toString().replace("quote#", quoteNumber);
		WebElement ele = driver.findElement(By.xpath(matchString));
		CommonUtil.click(ele, quoteNumber);
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	public void searchOpportunity(HashMap<String, String> dataMap) {

		CommonUtil.waitForElementVisibilityWithDuration(sideMenuNavigationBar, 5, "Navigation bar");
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(searchButton, 5, "global search bar");
		CommonUtil.click(searchButton, "global search bar");
		CommonUtil.waitForElementVisibilityWithDuration(searchBarInput, 5, "global search box");
		CommonUtil.inputText(searchBarInput, dataMap.get("OpportunityName"), "Entered Opportunity Name");
		matchedData = CommonUtil.findElement(matchedDataStart + dataMap.get("OpportunityName") + matchedDataEnd, "xpath", "Searched Opportunity " + dataMap.get("OpportunityName"));
		CommonUtil.waitForVisible(matchedData, "Searched Opportunity " + dataMap.get("OpportunityName"));
		CommonUtil.click(matchedData, "matched data");
		normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void globalSearchOpportunity(HashMap<String, String> dataMap) {

	CommonUtil.normalWait(5);
	CommonUtil.waitForElementVisibilityWithDuration(searchButton, 5, "global search bar");
	CommonUtil.click(searchButton, "global search bar");
	CommonUtil.waitForElementVisibilityWithDuration(searchBarInput, 5, "global search box");
	CommonUtil.inputTextWithEnterKey(searchBarInput, dataMap.get("OpportunityName"));

	CommonUtil.normalWait(15);
	String matchString = opportunityNumberLink.toString().replace("Opportunity#", dataMap.get("OpportunityName"));
	WebElement ele = driver.findElement(By.xpath(matchString));
	CommonUtil.click(ele, dataMap.get("OpportunityName"));
	CommonUtil.normalWait(5);
	CommonUtil.attachScreenshotOfPassedTestsInReport();
	}

	public void searchLead(HashMap<String, String> dataMap) {
		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(sideMenuNavigationBar, 5, "Navigation bar");
		CommonUtil.normalWait(10);
		CommonUtil.waitForElementVisibilityWithDuration(searchButton, 5, "global search bar");
		CommonUtil.click(searchButton, "global search bar");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(searchBarInput, "global search box");
		CommonUtil.inputText(searchBarInput, dataMap.get("LeadID"), "Entered lead Name");
		CommonUtil.normalWait(10);
		CommonUtil.click(showmoreresult,"Show More Result");
		CommonUtil.waitForVisible(matchedLeadData, "matched lead Data");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(matchedDatalink,"matched data link");	
		CommonUtil.normalWait(30);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}