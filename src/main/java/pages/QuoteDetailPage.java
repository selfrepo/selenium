package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;
import utilities.DriverUtil;

public class QuoteDetailPage extends CommonUtil {

	// ================================================================================
	// Variables
	// ================================================================================
	private static String parentWindow;
	
    String priceBookStart = "//div[span[text()='Price Book']]/following-sibling::div//a[slot[slot[span[text()='";

    String priceBookEnd = "']]]]";

    WebElement priceBook;

    private WebElement opportunityName;
    
    private String opportunityNameStart = "//p[@title='Opportunity']/following-sibling::p//a[//span[text()='";
    
    private String opportunityNameEnd = "']]";
	
    // ================================================================================
	// Objects
	// ================================================================================
	@FindBy(xpath = "//div[span[normalize-space()='Quote Total']]/following-sibling::div//span//slot/lightning-formatted-text")
	private WebElement quoteTotal;
	
	@FindBy(xpath = "//button[normalize-space()='Submit for Approval']")
	private WebElement submitForApprovalButton;
	
	@FindBy(xpath = "//button[normalize-space()='Generate Legal Document']")
	private WebElement generateLegalDocument;
	
	@FindBy(xpath = "//iframe[contains(@title,'Canvas IFrame for application Conga Composer')][@style=\"display: inline;\"]")
	private WebElement canvasIframe;
	
	@FindBy(xpath = "//iframe[@title='Conga Composer - EU']")
	private WebElement canvasInnerIframe;
	
	@FindBy(id = "apxt-c8-start-merge")
	private WebElement mergeAndDownloadButton;
	
	@FindBy(xpath = "//thead[tr[th[div[a[span[text()='Quote Number']]]]]]/following-sibling::tbody//tr//th[1]//a")
	private WebElement quoteLink;
	
	@FindBy(xpath = "//span[text()='Primary Quote']/parent::div/following-sibling::div/span//a//span")
	private WebElement primaryQuote;
	
	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//slot/span[contains(text(),'Quotes')]")
	private WebElement quotelinknew;

	@FindBy(xpath = "//lightning-formatted-text[normalize-space()='Net 30']")
    private WebElement paymentTerm;
	
	@FindBy(xpath = "//a[text()='Related'][@aria-selected=\"false\"]")
	private WebElement relatedTabLnk;

	@FindBy(xpath = "//span[text()='Status']//following::lightning-formatted-text[1]")
	private WebElement eleQuoteStatus;
	
	@FindBy(xpath = "//span[text()='Quote Sub Status']//following::lightning-formatted-text[1]")
	private WebElement eleQuoteSubStatus;
	

	// ================================================================================
	// Methods
	// ================================================================================
	public void generateLegalDocument(HashMap<String, String> dataMap,String quoteNumber) throws InterruptedException {

		Assert.assertTrue(CommonUtil.isElementDisplayed(generateLegalDocument, "Generate Legal Document"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(generateLegalDocument, "Generate Legal Document");
		CommonUtil.normalWait(10);
		parentWindow = CommonUtil.switchToNewWindow("Conga Composer page");
		CommonUtil.waitForVisible(canvasIframe, "Canvas IFrame for application Conga Composer");
		CommonUtil.switchToFrame(canvasIframe, "Canvas IFrame for application Conga Composer");
		CommonUtil.waitForVisible(canvasInnerIframe, "Conga Composer inner IFrame");
		CommonUtil.switchToFrame(canvasInnerIframe, "Conga Composer inner IFrame");
		CommonUtil.waitForVisible(mergeAndDownloadButton, "Merge & Download button");
		CommonUtil.scrollIntoView(mergeAndDownloadButton, "Merge & Download button");
		CommonUtil.click(mergeAndDownloadButton, "Merge & Download button");
		CommonUtil.normalWait(20);
		CommonUtil.waitUntilFileToDownload(DriverUtil.downloadFilePath);
		CommonUtil.normalWait(15);
		Assert.assertTrue(CommonUtil.isFileDownloadedWithoutFileDeletion("Legal Document for "+quoteNumber+".pdf"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.moveFileFromDownloadFilesToUploadFiles("Legal Document for "+quoteNumber+".pdf");
		CommonUtil.delFileWithInDirectory("download", "Legal Document for "+quoteNumber+".pdf");
		CommonUtil.switchToDefaultFrame();
		CommonUtil.switchToParentWindow(parentWindow, "Quote Details Page");
	}

	public void verifyQuoteTotalAndSubmitForApproval(HashMap<String, String> dataMap) {
		CommonUtil.waitForVisible(quoteTotal, dataMap.get("QuoteTotal"));
		CommonUtil.verifyText(quoteTotal, dataMap.get("QuoteTotal"), "Quote total " + dataMap.get("QuoteTotal"));
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(submitForApprovalButton, 8, "Submit for Approval button");
		CommonUtil.waitForVisible(submitForApprovalButton, "Submit for Approval button");
		CommonUtil.click(submitForApprovalButton, "Submit for Approval button");
		CommonUtil.normalWait(25);
		CommonUtil.refresh("Quote Details page");
		CommonUtil.normalWait(10);
	}

	
	public void openAndGetQuoteNumberAndVerifyPricebook(HashMap<String, String> dataMap) throws InterruptedException {
		
		//verify quotes link
		CommonUtil.waitForElementVisibilityWithDuration(quotelinknew, 10, "Quotes link");
		CommonUtil.clickUsingJavaScriptExecutor(quotelinknew,"Quotes link");
		CommonUtil.normalWait(10);
		
		//capture quote
		CommonUtil.waitForElementVisibilityWithDuration(quoteLink, 10, "Quote");	
		//Will use this Quote Number to write into the excel file and use it for next test cases


		String quoteNumber = CommonUtil.getText(quoteLink, "Quotes link");	
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");
		CommonUtil.storedata("QuoteNumber", quoteNumber);
		CommonUtil.clickUsingJavaScriptExecutor(quoteLink,"Quote");
		CommonUtil.normalWait(10);
		
		opportunityName = CommonUtil.findElement(opportunityNameStart+dataMap.get("OpportunityName")+opportunityNameEnd,"xpath","Opportunity name "+dataMap.get("OpportunityName"));
        CommonUtil.waitForVisible(opportunityName,"Opportunity name"+dataMap.get("OpportunityName"));
        
        CommonUtil.scrollIntoView(relatedTabLnk,"Related Tab");
		//Check visibility of Price book 
        priceBook= CommonUtil.findElement(priceBookStart +dataMap.get("SellToMarket")+priceBookEnd,"xpath","Price Book "+dataMap.get("SellToMarket"));
		CommonUtil.waitForElementVisibilityWithDuration(priceBook, 10, "Pricebook"+dataMap.get("SellToMarket"));
		CommonUtil.scrollViewUpto(priceBook, "price book");
		Assert.assertTrue(CommonUtil.isElementDisplayed(priceBook, "Pricebook"));
	    CommonUtil.attachScreenshotOfPassedTestsInReport();	
	    
	    CommonUtil.scrollIntoView(paymentTerm, "Payment Terms");
	    Assert.assertTrue(CommonUtil.isElementDisplayed(paymentTerm, "Payment Term Net 30"));
	    CommonUtil.attachScreenshotOfPassedTestsInReport();	
	    
	}
	
	public void statusValidationInQuoteDetailsPage(HashMap<String, String> dataMap) throws InterruptedException {
		CommonUtil.refresh("Quote Details Page");
		CommonUtil.normalWait(10);
		
		// Status in Quote Details Page
		String ValStatusInQuoteDetailsPage = CommonUtil.getText(eleQuoteStatus,"Status in Quote Details page");
		Assert.assertTrue(ValStatusInQuoteDetailsPage.equals(dataMap.get("QuoteStatus")),"Quote Status in Quote Details Page displayed");
		
		// Quote Sub Status in Quote Details Page
		String ValQuoteSubStatusQuoteDetailsPage = CommonUtil.getText(eleQuoteSubStatus,"Quote Sub Status in Quote Details");
		Assert.assertTrue(ValQuoteSubStatusQuoteDetailsPage.equals(dataMap.get("QuoteSubStatus")),"Quote Sub Status in Quote Details Page displayed");

	}
}