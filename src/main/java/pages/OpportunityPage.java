package pages;


import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.CommonUtil;
import org.testng.Assert;

import utilities.LogUtil;

public class OpportunityPage extends CommonUtil{
	
	//Variables
	
	//for ProfessionalServicesIncludedOptions
	private WebElement professionalServicesIncludedOption;
	private String professionalServicesIncludedOptionStart ="(//lightning-base-combobox-item//span[text()='";
	private String professionalServicesIncludedOptionEnd = "'])";
	
	//For CompletedStage
	private WebElement quoteNumInRelatedLink;
	private String quoteNumInRelatedLinkStart = "(//table[@role = 'grid']/tbody/tr/th/span/a[text()= '";
	private String quoteNumInRelatedLinkEnd = "'])";
	
	
	//Objects
	@FindBy(xpath = "//span[text()='Opportunity']/following::span[4]")
	WebElement opportunityLink;
	
	@FindBy(xpath = "//span [text()='Professional Service Included?']/parent::div/following-sibling::div/child::button")
	WebElement professionalServicesEditPencilBtn;
	
	@FindBy(xpath = "//label[text()='Professional Service Included?']/following::button[2]")
	WebElement professionalServicesEditboxBtn;
	
	@FindBy(xpath = "//button/descendant::span[text()='Mark Stage as Complete']")
	WebElement markStageAsCompBtn;
	
	@FindBy(xpath = "//lightning-formatted-text[contains(text(), 'Stage')]")
	private WebElement currentOpportunityStage;
	
	@FindBy(xpath = "//button[@name='SaveEdit']") 
	private WebElement editSaveButton;
	
	//@FindBy(xpath = "//ul[@class ='slds-grid slds-wrap list']/child:: li[2]/descendant:: a")
	@FindBy(xpath = "//ul[@class ='slds-grid slds-wrap list']/child:: li[2]/descendant:: a/slot/span[contains(text(),'Quotes')]")
	private WebElement quotesLinkInRelatedQuickLinks;
	
	@FindBy(xpath = "//one-app-nav-bar-item-root/a[@title='Opportunities']")
	WebElement opportunityTab;
	
	@FindBy(xpath = "//div[@class='slds-icon-waffle']")
	WebElement appLauncher;
	
	@FindBy(xpath = "//one-app-launcher-search-bar//input[@type='search']")
	WebElement appSearchTextBox;
	
	@FindBy(xpath = "//input[@class='slds-input'][@placeholder='Search this list...']")
	WebElement searchTextBox;
	
	@FindBy(xpath = "//*[@id='oneHeader']/div[2]/div[2]/div/button")
	WebElement globalSearchBox;
	
	@FindBy(css = "a[class='slds-truncate outputLookupLink slds-truncate outputLookupLink-0063M000006qH8DQAU-2728:0 forceOutputLookup']")
	WebElement randomOpportunity;
	
	@FindBy(xpath = "//div[@class='entityNameTitle slds-line-height--reset']//parent::h1/slot[@name='primaryField']")
	WebElement opportunitDetailPageHeader;
	
	@FindBy(xpath = "//h4[@class='slds-align-middle slds-text-heading--small']")
	WebElement prilimanaryPortfolioProductsHeader;
	
	@FindBy(xpath = "//h4[@class='slds-align-middle slds-text-heading--small']/parent::div//span[text()='Edit']/parent::button")
	WebElement EditProductsButton;
	
	@FindBy(xpath = "//div/a[text()='Add New']")
	WebElement addNewProduct;
	
	@FindBy(xpath = "//p[text()='New Portfolio']")
	WebElement newPortfolioLink;
	
	@FindBy(xpath = "//*[@class='slds-form-element slds-lookup']")
	WebElement newPortfolioLookup;
	
	@FindBy(xpath = "//div[@class='slds-popover__body']//input[@placeholder='Search Products']")
	WebElement searchProducts;
	
	@FindBy(css = ".slds-lookup__result-text")
	WebElement resultItem;
	
	@FindBy(xpath = "//label[text()='Quantity']/following-sibling::div/input")
	WebElement quantityInputBox;
	
	@FindBy(xpath = "//label[text()='Sales Price']/following-sibling::div/input")
	WebElement salesPriceInputBox;
	
	@FindBy(css = ".slds-button.slds-button_brand.slds-col--bump-left")
	WebElement saveButton;
	
	@FindBy(xpath = "//lightning-formatted-number[normalize-space()='USD 100.00']")
	WebElement PortfolioItem;
	
	@FindBy(xpath = "//span[@class='test-id__field-label'][text()='Amount']")
	WebElement amountLabel;
	
	@FindBy(xpath = "//sfa-output-opportunity-amount/lightning-formatted-text")
	WebElement amountValue;
	
	@FindBy(xpath ="//span[text()='Total LFR']/parent::div/following-sibling::div//lightning-formatted-text")
	WebElement totalLFRValue;
	
	@FindBy(xpath = "//a[@id='detailTab__item']")
	WebElement detailsPageView;
	
	@FindBy(xpath = "//span[text()='Stage']/parent::div/following-sibling::div/button[@title='Edit Stage']")
	WebElement stageEditIcon;
	
	@FindBy(xpath = "//label[text()='Stage']//following-sibling::div//button[@role='combobox']/span")
	WebElement stageCombobox;
	
	@FindBy(xpath = "//span[@class='slds-truncate'][normalize-space()='Stage 1: Discover']/parent::button")
	WebElement stage1DropdownIcon;
	
	@FindBy(xpath = "//lightning-base-combobox-item[@data-value='Stage 2: Validate']")
	WebElement stage2Select;
	
	@FindBy(xpath = "//lightning-base-combobox-item[@data-value='Stage 1: Discover']")
	WebElement stage1Select;
	
	@FindBy(xpath = "//button[@class='slds-button slds-button_brand'][text()='Save']")
	WebElement OpportunitySaveButton;
	
	@FindBy(xpath = "//ul/li[@role='presentation'][3]/a/span[text()='Stage 2: Validate']")
	WebElement statusBar;
	
	@FindBy(xpath = "//span[normalize-space()='Primary Portfolio']/parent::div//following-sibling::div/span//*[@data-output-element-id='output-field']")
	WebElement primaryportfolioValue;
	
	@FindBy(xpath = "//span[normalize-space()='Fulfillment Type']/parent::div//following-sibling::div/span//*[@data-output-element-id='output-field']")
	WebElement fulfillmentTypeValue;
	
	@FindBy(xpath = "(//slot/force-lookup/div/records-hoverable-link/div/a)[1]")
	WebElement accountNameLink;
	
	@FindBy(xpath = "//h1/div[text()='Account']")
	WebElement accountHeader;
	
	@FindBy(xpath = "//span[normalize-space()='Account Verification Status']/parent::div/following-sibling::div//*[@data-output-element-id='output-field']")
	WebElement accountVerifcationStatus;
    
	
	private WebElement matchedData;	
	private String matchedDataStart = "//a[@data-refid='recordId'][@title='";	
	private String matchedDataEnd = "']";	
	
	//Methods	
	//Method to Navigate to the Opportunity page from the Quote Page
	public void navigateToOpportunityPageFromQuotePage() {
		CommonUtil.waitForElementVisibilityWithDuration(opportunityLink,10, "Opportunity link");
		CommonUtil.clickUsingJavaScriptExecutor(opportunityLink, "the Opportunity Link");
		CommonUtil.waitForElementVisibilityWithDuration(markStageAsCompBtn,30, "markStageAsCompBtn");
	}
	
	//This method will edit Is Professional services included field
	public void editPsFieldInOpportunityPage(HashMap<String, String> dataMap) {
		
		//Select the Value for the Professional services included? from the dropdown
		CommonUtil.normalWait(5);
		CommonUtil.waitForElementVisibilityWithDuration(professionalServicesEditPencilBtn,10, "edit");
		CommonUtil.clickUsingJavaScriptExecutor(professionalServicesEditPencilBtn, "Professional Services Included Edit Pencil Button");
		
		CommonUtil.normalWait(5);
		CommonUtil.clickUsingJavaScriptExecutor(professionalServicesEditboxBtn, "Professional Services Included Dropdown");
		professionalServicesIncludedOption= CommonUtil.findElement(professionalServicesIncludedOptionStart+dataMap.get("PS Included Option")+professionalServicesIncludedOptionEnd,"xpath","");
		CommonUtil.waitForElementVisibilityWithDuration(professionalServicesIncludedOption,5, "prefessional service");
		CommonUtil.clickUsingJavaScriptExecutor(professionalServicesIncludedOption, "Professional Services Included Option Selected");
		
		//save the changes
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.waitForElementVisibilityWithDuration(editSaveButton, 5, "save button");
		CommonUtil.click(editSaveButton, "Clicking on save button of edit screen");
		CommonUtil.normalWait(15);
		
	}
	
	//Method to Mark Stage as Complete at the Opportunity Page
	public void markOpportunityStage(String stage) {
		CommonUtil.scrollIntoView(markStageAsCompBtn, "scroll to view");
		CommonUtil.normalWait(5);
		//mark the Stage for the opportunity
		String currOppStageName;
		currOppStageName = currentOpportunityStage.getText();
		
		//Base conditions -> if already at the desired stage then return from the function
		if(stage.equalsIgnoreCase(currOppStageName)) {
			return;
		}
		else {
			do{	
				CommonUtil.waitForElementVisibilityWithDuration(markStageAsCompBtn,2,"mark stage as complete");
				CommonUtil.clickUsingJavaScriptExecutor(markStageAsCompBtn, "Mark Stage as Complete");
				CommonUtil.normalWait(5);
				currOppStageName = currentOpportunityStage.getText();
			}while(!stage.equalsIgnoreCase(currOppStageName));
		}	
	}
	
	//Method to go back to the Quote's Page from the Opportunity Page
	public void goToQuotePageFromOpportunityPage(String quoteNumber) {
		//Click on Quotes link in the Related quick links
		CommonUtil.clickUsingJavaScriptExecutor(quotesLinkInRelatedQuickLinks, "Quotes link in the Related quick links tab");
		CommonUtil.normalWait(10);
		
		//Find the required Quote number from the table and click on it
		quoteNumInRelatedLink= CommonUtil.findElement(quoteNumInRelatedLinkStart+quoteNumber+quoteNumInRelatedLinkEnd,"xpath","");
		CommonUtil.clickUsingJavaScriptExecutor(quoteNumInRelatedLink, "Required Quote Number");
		CommonUtil.normalWait(10);		
	}

	
	
	public void navigateToOpportunities() {

		CommonUtil.normalWait(20);

		CommonUtil.waitForElementVisibilityWithDuration(appLauncher,5, "Navigation bar");
		CommonUtil.click(appLauncher,"Navigation bar");
		CommonUtil.waitForElementVisibilityWithDuration(appSearchTextBox, 2, "search textbox - app launcher");

		CommonUtil.inputTextWithEnterKey(appSearchTextBox, "Opportunities");

		CommonUtil.normalWait(8);
		CommonUtil.waitForElementVisibilityWithDuration(searchTextBox,5, "searchTextBox");	
		
	}

	public void searchOpportunity(HashMap<String, String> dataMap) {

		String oppor = dataMap.get("OpportunityName");
		CommonUtil.inputTextWithEnterKey(searchTextBox, oppor);

		CommonUtil.normalWait(2);
		matchedData = CommonUtil.findElement(matchedDataStart+dataMap.get("OpportunityName")+matchedDataEnd,"xpath","Searched Opportunity "+dataMap.get("OpportunityName"));
		CommonUtil.waitForElementVisibilityWithDuration(matchedData, 1, "searched Opportunity");
		CommonUtil.click(matchedData, "Searched Opportunity"+dataMap.get("OpportunityName"));
		CommonUtil.normalWait(2);
		CommonUtil.waitForVisible(opportunitDetailPageHeader, "Opportunity Detail Page");	
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
	
	public void editAndAddPreliminaryPortfolioProducts(HashMap<String, String> dataMap) {
		normalWait(10);
		CommonUtil.waitForVisible(prilimanaryPortfolioProductsHeader, "prilimanaryPortfolioProducts section");	
		//Add a preliminary product
		CommonUtil.scrollToBottomOfPage();
		CommonUtil.clickUsingJavaScriptExecutor(EditProductsButton, "Edit button");
		CommonUtil.waitForElementVisibilityWithDuration(addNewProduct, 2, "newPortfolioLink");
		CommonUtil.clickUsingJavaScriptExecutor(addNewProduct, "Add New link");
		CommonUtil.clickUsingJavaScriptExecutor(newPortfolioLink, "newPortfolioLink");
		CommonUtil.waitForElementVisibilityWithDuration(newPortfolioLookup, 2, "newPortfolioLookup");
		CommonUtil.inputTextWithEnterKey(searchProducts, dataMap.get("ProductCode"));
		CommonUtil.waitForElementVisibilityWithDuration(resultItem, 2, "resultItem");
		CommonUtil.clickUsingJavaScriptExecutor(resultItem, "Product");
		normalWait(2);
		CommonUtil.inputText(quantityInputBox, dataMap.get("Quantity"),"Quantity");
		CommonUtil.inputText(salesPriceInputBox, dataMap.get("Discount"),"Sales Price");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.clickUsingJavaScriptExecutor(saveButton, "save button");	
		CommonUtil.normalWait(20);
		//verify pp is added 
		Assert.assertTrue(PortfolioItem.isDisplayed());
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		
	}
	
	//verify amount field's value
	public void verifyOpportunityAmountisUpdatedBasedonPrilimanaryProductsAdded(HashMap<String, String> dataMap) {
		CommonUtil.scrollIntoView(amountLabel, "Opportunity Amount");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		String amount = amountValue.getText();
		System.out.println("Amount: "+amount);
		Assert.assertTrue(amount.contains(dataMap.get("QuoteTotal")));
		CommonUtil.storedata("Amount", amount);
		String revenueStreamValue = totalLFRValue.getText();
		System.out.println("revenueStreamValue: "+revenueStreamValue);
		Assert.assertTrue(revenueStreamValue.contains(dataMap.get("QuoteTotal")));	
			
		}
	
	public void changeOpportunityStatusToValidate() {
		CommonUtil.scrollIntoView(detailsPageView, "Stage Edit Icon");
		CommonUtil.clickUsingJavaScriptExecutor(stageEditIcon, "edit Stage");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(stageCombobox, "Stage dropdown");
		CommonUtil.clickUsingJavaScriptExecutor(stageCombobox, "Stage dropdown");
		CommonUtil.normalWait(5);
		CommonUtil.clickUsingJavaScriptExecutor(stage2Select, "validate status");
		CommonUtil.normalWait(2);
		CommonUtil.clickUsingJavaScriptExecutor(OpportunitySaveButton, "save button");
		CommonUtil.normalWait(10);
			
		}
	

	public void changeOpportunityStatusToDiscover() {
		CommonUtil.scrollIntoView(detailsPageView, "Stage Edit Icon");
		CommonUtil.clickUsingJavaScriptExecutor(stageEditIcon, "edit Stage");
		CommonUtil.normalWait(5);
		CommonUtil.waitForVisible(stageCombobox, "Stage dropdown");
		CommonUtil.clickUsingJavaScriptExecutor(stageCombobox, "Stage dropdown");
		CommonUtil.normalWait(5);
		CommonUtil.clickUsingJavaScriptExecutor(stage1Select, "discover status");
		CommonUtil.normalWait(2);
		CommonUtil.clickUsingJavaScriptExecutor(OpportunitySaveButton, "save button");
		CommonUtil.normalWait(5);
			
		}

	public void verifyFieldsOnOpportunity(HashMap<String, String> dataMap) {
	
		Assert.assertEquals(primaryportfolioValue.getText(), dataMap.get("PrimaryPortfolio"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		Assert.assertEquals(fulfillmentTypeValue.getText(), dataMap.get("FulfillmentType"));	
		
	}

	public void navigateToAccountPage(HashMap<String, String> dataMap) {
		CommonUtil.clickUsingJavaScriptExecutor(accountNameLink, "Account Name");
		CommonUtil.normalWait(10);					
	}	

	public void verifyAccountStatus() {
		try {
			if(accountHeader.isDisplayed()) {
				Assert.assertEquals(accountVerifcationStatus.getText(), "Verified");
			}
		} catch(Exception e) {
			LogUtil.errorLog(getClass(), "Account Page Navigation Failed", e);
		}
		
	}	


}
