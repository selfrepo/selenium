package pages;

import java.util.HashMap;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;

import utilities.CommonUtil;
import utilities.HTMLReportUtil;
import utilities.LogUtil;
import utilities.ExtentReports.ExtentTestManager;

public class PendingSalesOrderPage extends CommonUtil {

	@FindBy(xpath = "//a[normalize-space()='Sales Orders Pending OM Specialist Approval']")
	private WebElement pendingSalesOrderOMApprovalLnk;
	
	@FindBy(xpath = "//h1[contains(text(),'Sales Orders Pending OM Specialist Approval:')]")
	private WebElement pendingSalesOrderOMApprovalHeading;
	
	@FindBy(xpath = "//iframe[@id='list']")
	private WebElement listIframe;
	
	@FindBy(xpath = "//table[@id='div__labtab']/thead/tr[@class='uir-list-headerrow noprint']/td")
	private List<WebElement> salesOrderListHeaderRow;
	
	@FindBy(xpath = "//table[@id='div__bodytab']/tbody/tr[contains(@id,'row')]")
	private List<WebElement> salesOrderListAllRows;
	
	@FindBy(id="uir_totalcount")
	private WebElement totalCount;
	
	@FindBy(id="a[aria-label='Next']")
	private WebElement paginationNextLnk;
	
	@FindBy(xpath = "//a[normalize-space()='Search']")
	private WebElement transactionSearchLnk;
	
	@FindBy(xpath = "//h1[normalize-space()='Transaction Search']")
	private WebElement transactionSearchHeader;
	
	@FindBy(xpath = "//a[normalize-space()='Period']")
	private WebElement periodLnk;
	
	@FindBy(xpath = "//input[@id='Transaction_NUMBERTEXT']")
	private WebElement transactionNumberTxtInput;
	
	@FindBy(xpath = "//input[@id='secondarysubmitter']")
	private WebElement secondarySubmitBtn;
	
	@FindBy(xpath = "//h1[contains(text(),'Transaction Search:')]")
	private WebElement transactionSearchHeaderTxt;
	
	// ================================================================================
	// Methods
	// ================================================================================
	public void viewPendingSalesOrder(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(transactionSearchHeaderTxt, 15, "Transaction Search Header Text");
		int idxEditView = -1;
		int idxDocumentNumber = -1;
		int idxAccount = -1;				
		for(int i=0; i<salesOrderListHeaderRow.size()-1; i++) {
			WebElement row = salesOrderListHeaderRow.get(i);
			String valColumn = row.findElement(By.xpath("//table[@id='div__labtab']/thead/tr[@class='uir-list-headerrow noprint']/td["+(i+1)+"]/div")).getText().trim();
			LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\nHeader Value : "+valColumn);
			if(valColumn.equals("EDIT | VIEW"))
				idxEditView = i;
			else if(valColumn.equals("DOCUMENT NUMBER"))
				idxDocumentNumber = i;
			else if(valColumn.equals("ACCOUNT"))
				idxAccount = i;
		}
		
		LogUtil.infoLog(NSSalesOrderRelatedRecordsPage.class,"\n\nrelatedRecordsTableHeaderRow- size : "+salesOrderListHeaderRow.size()+" value : "+salesOrderListHeaderRow.toString());
		if(idxEditView == -1 || idxDocumentNumber == -1 || idxAccount == -1)
		{
			LogUtil.errorLog(getClass(), "Couldn't find index of Column EDIT | VIEW/DOCUMENT NUMBER/DATE");
		}
		 try {
		//Iterate through each row
		for( int i=0; i<salesOrderListAllRows.size(); i++) {
			
			//Fetch a single row
			WebElement salesOrderRecordRow = salesOrderListAllRows.get(i);
			
			
			String xDocumentNumber = "//*[@id='div__bodytab']/tbody/tr[contains(@id,'row"+i+"')]/td["+(idxDocumentNumber+1)+"]/a";
			String valDocumentNumber = salesOrderRecordRow.findElement(By.xpath(xDocumentNumber)).getText();
			
			if( valDocumentNumber.trim().length() == 0)
				throw new Exception("Value under Type field is empty");
			
			if( valDocumentNumber.equals(dataMap.get("SO Number"))) {
				
				//Define Xpath for column 'Status'
				String xPathAccount = "//*[@id='div__bodytab']/tbody/tr[contains(@id,'row"+i+"')]/td["+(idxAccount+1)+"]/a";
				String valAccount = salesOrderRecordRow.findElement(By.xpath(xPathAccount)).getText();
				
				//Verify status
				if( valAccount.equals(dataMap.get("Account"))) {
					LogUtil.infoLog(getClass(),"\nPASSED : Expected Status : "+dataMap.get("Account")+". Actual Status : "+valAccount);
				}
				else {
					LogUtil.errorLog(getClass(),"\nFAILED : Expected Status : "+dataMap.get("Account")+". Actual Status : "+valAccount, null);
					throw new Exception("Account should be : "+dataMap.get("Account"));
				}
				
				//Navigate to view link for pending SO
				String xView = "//*[@id='div__bodytab']/tbody/tr[contains(@id,'row"+i+"')]/td["+(idxEditView+1)+"]/a[2]";
				WebElement viewSO = salesOrderRecordRow.findElement(By.xpath(xView));
				
				LogUtil.infoLog(getClass(), "WebElement View is Displayed? : "+viewSO.isDisplayed());
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor("WebElement View is Displayed? : "+viewSO.isDisplayed()));
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				CommonUtil.click(viewSO, "View link for SO");
				CommonUtil.normalWait(10);
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				break;		
			}
			
		}			
	}
		 catch(Exception e) {
				LogUtil.errorLog(getClass(),"viewPendingSalesOrder -> Exception caught : "+e.getMessage(), e);
			}
	}
	
	public void handlePaginationForPendingSO(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(pendingSalesOrderOMApprovalLnk, 15, "Pending Sales Order Link");
		CommonUtil.click(pendingSalesOrderOMApprovalLnk, "Pending Sales Order Link");
		CommonUtil.waitForElementVisibilityWithDuration(pendingSalesOrderOMApprovalHeading, 15, "Pending Sales Order Heading");
	    String totalCountTxt = CommonUtil.getText(totalCount, "Total Count");
	    int totalCount = Integer.parseInt(totalCountTxt.substring(0, 6));
	    int i = totalCount/50;
	    for(int j=0; j<i;j++)
	    {
	    	CommonUtil.click(paginationNextLnk, "Pagination Next Link");
	    	CommonUtil.normalWait(2);
	    }
	}
	
	public void searchTransactionSalesOrder(HashMap<String, String> dataMap) {
		CommonUtil.waitForElementVisibilityWithDuration(pendingSalesOrderOMApprovalLnk, 15, "Pending Sales Order Link");
		CommonUtil.click(pendingSalesOrderOMApprovalLnk, "Pending Sales Order Link");
		CommonUtil.waitForElementVisibilityWithDuration(pendingSalesOrderOMApprovalHeading, 15, "Pending Sales Order Heading");
		CommonUtil.click(transactionSearchLnk, "Search Link");
		CommonUtil.waitForElementVisibilityWithDuration(transactionSearchHeader, 15, "Transaction Search Header");
		CommonUtil.scrollIntoView(periodLnk, "Period Link");
		CommonUtil.waitForElementVisibilityWithDuration(transactionNumberTxtInput, 5, "Transaction Number Text Input");
		CommonUtil.inputText(transactionNumberTxtInput, dataMap.get("SO Number"), "Entered SO Number : "+dataMap.get("SO Number"));
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.scrollIntoView(secondarySubmitBtn, "Secondary Submit Button");
		CommonUtil.waitForElementVisibilityWithDuration(secondarySubmitBtn, 5, "Secondary Submit Button");
		CommonUtil.click(secondarySubmitBtn, "Secondary Submit Button");
		CommonUtil.normalWait(5);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	}
}
