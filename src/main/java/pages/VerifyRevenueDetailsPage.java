package pages;

import java.util.HashMap;
import utilities.CommonUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtil;
import utilities.LogUtil;
public class VerifyRevenueDetailsPage extends CommonUtil{
	    // ================================================================================
		// Variables
		// ================================================================================
		  
	       Boolean result;

	    // ================================================================================
		// Objects
		// ================================================================================
		
		
		@FindBy(css = "span[id='compliant_fs'] img[alt='Checked']")
		private List<WebElement> chkCompliantElements;
		
		@FindBy(css = "span[id='compliant_fs'] img[alt='Checked']")
		private WebElement chkCompliant;
		
		
		@FindBy(xpath = "//a[@id='revenueelementstxt']")
		private WebElement Revenueelements;
        
		@FindBy(xpath = "//a[@id='allocationdetailtxt']")
		private WebElement allocationdetailssubtab;
		
		@FindBy(xpath = "//*[@id='allocationdetail_splits']/tbody/tr[1]/td")
		private List<WebElement> revenueelementstableHeaderrow;
		
		@FindBy(xpath = "//*[@id='allocationdetail_splits']/tbody/tr")
		private List<WebElement> revenueelementsAllRows;
		
		@FindBy(css = "a[id='arrangementmessagetxt'] span")
		private WebElement arrangementMessagetab;
		
		@FindBy(xpath = "//*[@id='arrangementmessage_splits']/tbody/tr[1]/td")
		private List<WebElement> arrangementMessagetableHeaderrow;
		
		@FindBy(xpath = "//*[@id='arrangementmessage_splits']/tbody/tr")
		private List<WebElement> arrangementmessageAllRows;
		//Added from TC24th 
		
		//There is a another tab 'Related Records' inside 'Related Records' tab
		@FindBy(xpath = "//*[@id=\"rlrcdstabtxt\"]")
		private WebElement relatedRecordsTab;
				
		@FindBy(xpath = "//*[@id='linkstxt']")
		private WebElement relatedRecordsSubTab;

		@FindBy(xpath = "//*[@id='links_splits']/tbody/tr[1]/td")
		private List<WebElement> relatedRecordsTableHeaderRow;
		
		@FindBy(xpath = "//*[@id='links_splits']/tbody/tr")
		private List<WebElement> relatedRecordsAllRows;
		
		@FindBy(xpath = "//*[@id='itemheader']/td/div")			
		private List<WebElement> itemFulfillmentItemsHeaderRow;	
		
		@FindBy(xpath = "//*[@id='item_splits']/tbody/tr")
		private List<WebElement> itemFulfillmentItemsAllRows;
		
		@FindBy(css = "#edit")
	    private WebElement editBtn;
		
		// ================================================================================
		// Methods
		// ================================================================================
		public void verifyRevenueDetails(HashMap<String, String> dataMap) {
			
			CommonUtil.normalWait(5);
			int size = chkCompliantElements.size();
			if (size>0) {
				CommonUtil.waitForElementVisibilityWithDuration(chkCompliant, 10, "Compliant checkbox");
				Assert.assertTrue(CommonUtil.isElementDisplayed(chkCompliant, "Compliant checkbox"));
			    CommonUtil.attachScreenshotOfPassedTestsInReport();	
			}	
			
		}
		
		public void verifyRevenueArrangementDetails(HashMap<String, String> dataMap) {
			try {
				
				LogUtil.infoLog(VerifyRevenueDetailsPage.class,"Is Related Record displayed : "+CommonUtil.isElementDisplayed(relatedRecordsTab, "Related Records Tab"));
				CommonUtil.normalWait(5);
				CommonUtil.waitForVisible(relatedRecordsTab,"Related Records Tab");
				relatedRecordsTab.click();
				
				LogUtil.infoLog(VerifyRevenueDetailsPage.class,"Is Related Record displayed : "+CommonUtil.isElementDisplayed(relatedRecordsSubTab, "Related Records Tab"));
				CommonUtil.normalWait(5);
				CommonUtil.waitForVisible(relatedRecordsSubTab,"Related Records SubTab");
				relatedRecordsSubTab.click();
				
				//Fetch index of column TYPE,STATUS and DATE				
				int idxType = -1;
				int idxStatus = -1;
				int idxDate = -1;				//Index of date is required to navigate to Item fulfillment details
				for(int i=0; i<relatedRecordsTableHeaderRow.size(); i++) {
					WebElement row = relatedRecordsTableHeaderRow.get(i);
					String valColumn = row.findElement(By.xpath("//*[@id='links_splits']/tbody/tr[1]/td["+(i+1)+"]/div")).getText().trim();
					LogUtil.infoLog(VerifyRevenueDetailsPage.class,"\nHeader Value : "+valColumn);
					if(valColumn.equals("TYPE"))
						idxType = i;
					else if(valColumn.equals("STATUS"))
						idxStatus = i;
					else if(valColumn.equals("DATE"))
						idxDate = i;
				}
				
				LogUtil.infoLog(VerifyRevenueDetailsPage.class,"\n\nrelatedRecordsTableHeaderRow- size : "+relatedRecordsTableHeaderRow.size()+" value : "+relatedRecordsTableHeaderRow.toString());
				if(idxType == -1 || idxStatus == -1 ||idxDate== -1)
					throw new Exception("Couldn't find index of Column TYPE/STATUS");
				
				if(relatedRecordsAllRows.size() == 0)
					throw new Exception("Related Records table not found");
				
				//Iterate through each row
				for(int i=1; i<relatedRecordsAllRows.size(); i++) {					
					//Fetch a single row
					WebElement relatedRecordRow = relatedRecordsAllRows.get(i);					
					
					String xPathType = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxType+1)+"]";
					String valType = relatedRecordRow.findElement(By.xpath(xPathType)).getText();
					
					if(valType.trim().length() == 0)
						throw new Exception("Value under Type field is empty");
					
					if(valType.equals("Revenue Arrangement")) {
					
						//Define Xpath for column 'Status'
						String xPathStatus = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxStatus+1)+"]";
						String valStatus = relatedRecordRow.findElement(By.xpath(xPathStatus)).getText();						
						//Verify status
						if( valStatus.equals("Approved")) {
							LogUtil.infoLog(VerifyRevenueDetailsPage.class,"\nPASSED : Expected Status : Approved. Actual Status : "+valStatus);
						}
						else {
							LogUtil.errorLog(VerifyRevenueDetailsPage.class,"\nFAILED : Expected Status : Approved. Actual Status : "+valStatus, null);
							throw new Exception("Revenue Arrangement status should be Approved");
						}
						
						//Navigate to Revenue Arrangement details
						String xPathDate = "//*[@id='links_splits']/tbody/tr["+(i+1)+"]/td["+(idxDate+1)+"]/a";
						WebElement date = relatedRecordRow.findElement(By.xpath(xPathDate));
						
						LogUtil.infoLog(VerifyRevenueDetailsPage.class, "WebElement Date is Displayed? : "+date.isDisplayed());
						date.click();
						break;		
						}
				}
				
				CommonUtil.normalWait(5);
			}
			catch(Exception e) {
				LogUtil.errorLog(VerifyRevenueDetailsPage.class,"verifyRevenueArrangementDetails -> Exception caught : "+e.getMessage(), e);
			}
		}
		
		
		public void verifyAllocationDetails(HashMap<String, String> dataMap) {
			try {
				
				CommonUtil.normalWait(15);
				LogUtil.infoLog(VerifyRevenueDetailsPage.class,"Is Revenue elements displayed : "+CommonUtil.isElementDisplayed(Revenueelements, " Revenue elements Sub Tab"));
				CommonUtil.normalWait(5);
				CommonUtil.waitForVisible(Revenueelements," Revenue elements Sub Tab");
				Revenueelements.click();
				CommonUtil.normalWait(5);
			
				
				LogUtil.infoLog(VerifyRevenueDetailsPage.class,"Is allocation details subtab displayed : "+CommonUtil.isElementDisplayed(allocationdetailssubtab, "allocation details Sub Tab"));
				CommonUtil.normalWait(5);
				CommonUtil.waitForVisible(allocationdetailssubtab,"allocation details Sub Tab");
				allocationdetailssubtab.click();
				CommonUtil.normalWait(5);
				
				//Fetch index of column BASE FAIR VALUE, CALCULATED FAIR VALUE AMOUNT and RANGE POLICY
					
				//int idxType = -1;
				int idxRngPolicy = -1;
				//int idxStatus = -1;
				int idxFairValue = -1;
				//int idxDate = -1;
				int idxCalFairValue = -1;
				//Index of col is required to fetch details 
				for(int i=0; i<revenueelementstableHeaderrow.size(); i++) {
					WebElement row = revenueelementstableHeaderrow.get(i);
					String valColumn = row.findElement(By.xpath("//*[@id='allocationdetail_splits']/tbody/tr[1]/td["+(i+1)+"]/div")).getText().trim();
					LogUtil.infoLog(VerifyRevenueDetailsPage.class,"\nHeader Value : "+valColumn);
					if(valColumn.equals("RANGE POLICY"))
						idxRngPolicy = i;
					else if(valColumn.equals("BASE FAIR VALUE"))
						idxFairValue = i;
					else if(valColumn.equals("CALCULATED FAIR VALUE AMOUNT"))
						idxCalFairValue = i;
				}
				
				LogUtil.infoLog(VerifyRevenueDetailsPage.class,"\n\nRevenueelementstableHeaderrow- size : "+revenueelementstableHeaderrow.size()+" value : "+revenueelementstableHeaderrow.toString());
				if(idxRngPolicy == -1 || idxFairValue == -1|| idxCalFairValue == -1)
					throw new Exception("Couldn't find index of Column RANGE POLICY/BASE FAIR VALUE/CALCULATED FAIR VALUE AMOUNT");
				
				if(revenueelementsAllRows.size() == 0)
					throw new Exception("Revenue details table not found");
				
				//Iterate through each row
				for( int i=1; i<revenueelementsAllRows.size(); i++) {
					
					//Fetch a single row
				WebElement relatedRecordRow = revenueelementsAllRows.get(i);
					
									
				//Define Xpath for column 'Fair Value'
				String xPathFairValue = "//*[@id='allocationdetail_splits']/tbody/tr["+(i+1)+"]/td["+(idxFairValue+1)+"]";
				String valFairVal = relatedRecordRow.findElement(By.xpath(xPathFairValue)).getText();
						
				//Define Xpath for column 'Calculated fair value'						
				String xPathCalFairValue = "//*[@id='allocationdetail_splits']/tbody/tr["+(i+1)+"]/td["+(idxCalFairValue+1)+"]";
				String valCalFV = relatedRecordRow.findElement(By.xpath(xPathCalFairValue)).getText();
						
				//Verify status ..........check not equal to blank
				if(valFairVal.trim().length()!=0) {
					LogUtil.infoLog(VerifyRevenueDetailsPage.class,"\nPASSED : Expected Fair value : is not empty. Actual Fair value : "+valFairVal);
				}
				else {
					LogUtil.errorLog(VerifyRevenueDetailsPage.class,"\nFAILED : Expected Fair value : is not empty. Actual Fair value : "+valFairVal, null);
					throw new Exception("Fair value should be updated");
				}
						
				if(valCalFV.trim().length()!=0) {
					LogUtil.infoLog(VerifyRevenueDetailsPage.class,"\nPASSED : Expected Calculated Fair value : is not empty. Actual Calculated Fair value : "+valCalFV);
				}
				else {
					LogUtil.errorLog(VerifyRevenueDetailsPage.class,"\nFAILED : Expected Fair value : is not empty. Actual Calculated Fair value : "+valCalFV, null);
					throw new Exception(" Calculated Fair value should be updated");
				}
								
				}				
				CommonUtil.normalWait(5);
			}
			catch(Exception e) {
			LogUtil.errorLog(VerifyRevenueDetailsPage.class,"verifyAllocationDetails -> Exception caught : "+e.getMessage(), e);
			}
		}
		
		
		public void verifyarrangementMessage(HashMap<String, String> dataMap) {
			try {
				CommonUtil.normalWait(5);		
				LogUtil.infoLog(VerifyRevenueDetailsPage.class,"Is allocation details subtab displayed : "+CommonUtil.isElementDisplayed(arrangementMessagetab, " arrangement Message Sub Tab"));
				CommonUtil.normalWait(5);
				CommonUtil.waitForVisible(arrangementMessagetab,"allocation details Sub Tab");
				arrangementMessagetab.click();
				CommonUtil.normalWait(5);				
				WebElement AllocationDescription = CommonUtil.getWebElement("xpath","//table[@id='arrangementmessage_splits']//td[contains(text(),'{replacevalue}')]//parent::tr/td[5]",dataMap.get("AllocationProcess"));
				CommonUtil.scrollViewUpto(AllocationDescription, "Scroll to Allocation msg");
				verifyText(AllocationDescription ,  dataMap.get("AllocationDescription"),"msg displayed for Allocation Description");
				CommonUtil.normalWait(5);
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				WebElement FairValueDescription = CommonUtil.getWebElement("xpath","//table[@id='arrangementmessage_splits']//td[contains(text(),'{replacevalue}')]//parent::tr/td[5]",dataMap.get("FairvalueProcess"));
				CommonUtil.scrollViewUpto(FairValueDescription, "Scroll to Fair Value msg");
				verifyText(FairValueDescription ,  dataMap.get("FairvalueDescription"),"msg displayed for Fairvalue Description");
				CommonUtil.attachScreenshotOfPassedTestsInReport();
				
			}
			catch(Exception e) {
			LogUtil.errorLog(VerifyRevenueDetailsPage.class,"verifyarrangementMessage -> Exception caught : "+e.getMessage(), e);
			}
		}
}
			

