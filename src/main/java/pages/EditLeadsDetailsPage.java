package pages;

import java.util.HashMap;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import utilities.CommonUtil;



public class EditLeadsDetailsPage extends CommonUtil {
	@FindBy(xpath = "//span[text()='Leads']//parent::a") 
	private WebElement LeadLink;

	@FindBy(xpath = "//div[@class='slds-media slds-no-space slds-grow']//span[text()='Leads']") 
	private WebElement LeadHeader;
	
	@FindBy(xpath = "//span[contains(text(),'Show more results for')]") 
	private WebElement showmoreresult;
	
	@FindBy(xpath = "//button[@aria-label='Search']") 
	private WebElement searchButton;
	
	@FindBy(xpath = "//*[text()='Search Leads and more']/parent::lightning-input//input[@class='slds-input']")
	private WebElement searchBarInput;
	
	@FindBy(xpath = "//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr[1]/td[6]/span[@class='slds-grid slds-grid--align-spread']/a[contains(@href,'mailto:')]")
	private WebElement matchedData;

	@FindBy(xpath = "(//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr[1]/th[1]/span/a[@data-refid='recordId'])[2]")
	private WebElement matchedDatalink;
	
	@FindBy(xpath = "//div[@class='windowViewMode-normal oneContent active lafPageHost']//a[text()='Details']")
    private WebElement LeadDetail;
	
	@FindBy(xpath = "//span[text()='Edit Name']//parent::button")
	private WebElement EditNamebutton;
	
	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement inputfirstname;
	
	@FindBy(xpath = "//input[@name='middleName']")
	private WebElement inputmiddlename;
	
	@FindBy(xpath = "//input[@name='lastName']")
	private WebElement inputlastname;
	
	@FindBy(xpath = "//button[@name='SaveEdit']")
	private WebElement editnamesave;
	
	
	
	
	public void editleads(HashMap<String, String> dataMap) {
		CommonUtil.normalWait(10);
		CommonUtil.clickUsingJavaScriptExecutor(LeadLink, "Leads");
		CommonUtil.normalWait(10);
		CommonUtil.waitForVisible(LeadHeader, "Lead Header");
		Assert.assertTrue(CommonUtil.isElementDisplayed(LeadHeader, "Lead Header"));
		CommonUtil.click(searchButton,"search bar");
		CommonUtil.waitForVisible(searchBarInput,"search Bar Input");
		CommonUtil.inputText(searchBarInput, dataMap.get("LeadIDs"),"Entered Lead data");
     	CommonUtil.storedata("LeadID",dataMap.get("LeadIDs"));
		CommonUtil.normalWait(10);
		CommonUtil.click(showmoreresult,"Show More Result");
		CommonUtil.waitForVisible(matchedData, "matched Data");
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(matchedDatalink,"matched data link");	
		CommonUtil.normalWait(30);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(LeadDetail,"Details");	
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.click(EditNamebutton,"Name Edit button");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
	    String Firstnameupdate   = dataMap.get("Firstname_update");
		System.out.println("Firstnameupdate  : " + Firstnameupdate);
		CommonUtil.scrollIntoView(LeadDetail, "Lead details tab");
		CommonUtil.click(inputfirstname,"firstName");	
		CommonUtil.jsinputText(inputfirstname,Firstnameupdate,"firstName Update");
		String middlenameupdate   = dataMap.get("Middlename_update"); 
		CommonUtil.jsinputText(inputmiddlename,middlenameupdate,"MiddleName Udpate");
		CommonUtil.normalWait(10);
	    String lastnameupdate   = dataMap.get("Lastname_update");
		System.out.println("lastnameupdate   :  " + lastnameupdate);
		CommonUtil.jsinputText(inputlastname,lastnameupdate,"LastName Update");
		CommonUtil.click(editnamesave,"Name Edit Save");
		CommonUtil.normalWait(10);
		CommonUtil.attachScreenshotOfPassedTestsInReport();
		CommonUtil.setTestcaseName("editleads");
		CommonUtil.storedata("LeadName", Firstnameupdate+" "+middlenameupdate+" "+lastnameupdate);
	}

}
