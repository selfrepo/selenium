package baseTest;


import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import utilities.DriverUtil;
import utilities.ExtentReports.ExtentManager;

@Listeners(utilities.Listeners.TestListener.class)
public class BaseTest {
	// Variables
	public static WebDriver driver;

	public static WebDriverWait wait;

	private static final int DEFAULT_WAIT_SECONDS = 30;

	static ExtentReports extent;

	public static ExtentTest logger;

	String pathForLogger;

	// Constructor
	public BaseTest() {
		PageFactory.initElements(driver, this);
	}

	@BeforeSuite
	public void directoryCleanUp() throws Exception {
		DriverUtil.folderCreation();
	}

	/**
	 * calling setupDriver of DriverUtil for driver instantiation
	 */
	@BeforeTest(alwaysRun = true)
	public void classLevelSetup() {
		extent = ExtentManager.getInstance();
		driver = new DriverUtil().setupDriver();
		wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_WAIT_SECONDS));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		driver.quit();
	}

	@AfterSuite(alwaysRun = true)
	public void closeSuite() {

	}

}