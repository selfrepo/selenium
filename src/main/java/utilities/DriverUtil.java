package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverUtil {

	String browser_name;
	public static Properties prop;
	public String arg_environment = System.getProperty("env");
	public static WebDriver driver;
	FileInputStream data, pwd;
	static File folder;
	public static String downloadFilePath;
	public static final int implicitlyWaitDurationSeconds = Integer
			.parseInt(ConfigReader.getValue("implicitlyWaitDurationSeconds"));
	String executionEnv = "";

	public WebDriver setupDriver() {
		folder = new File("Download_Files");
		folder.mkdir();
		// uncomment below statement whenever you change the password in key.properties
//		EncryptionDecryptionUtil.encodePassword("password");
		try {			
			data = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/ConfigFiles/config.properties");
			
			System.out.println("SCM read data from given file path");
			downloadFilePath = folder.getAbsolutePath();
			prop = new Properties();
			try {
				prop.load(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
			GlobalUtil.setCommonSettings(CommonSettings.getCommonSettings());

			String browser = "";
			browser = GlobalUtil.getCommonSettings().getBrowser();

			executionEnv = GlobalUtil.getCommonSettings().getAppEnviornment();

			String url = "";
			url = GlobalUtil.getCommonSettings().getUrl();

			if (browser == null)
				browser = ConfigReader.getValue("defaultBrowser");

			if (executionEnv == null)
				executionEnv = ConfigReader.getValue("defaultExecutionEnvironment");

			if (browser.equalsIgnoreCase("CHROME")) {
				System.setProperty("webdriver.chrome.silentOutput", "true");
				WebDriverManager.chromedriver().setup();
				ChromeOptions options = new ChromeOptions();
				//headless execution on jenkins code
				if(System.getProperty("os.name").equalsIgnoreCase("linux")) {
				options.addArguments("--window-size=1920,1080");	
				options.addArguments("headless");
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
				System.out.println("chrome initialized on linux");
				}
				else {
				Map<String, Object> prefs = new HashMap<String, Object>();
				Map<String, Object> profile = new HashMap<String, Object>();
				Map<String, Object> contentSettings = new HashMap<String, Object>();

				// SET CHROME OPTIONS
				// 0 - Default, 1 - Allow, 2 - Block
				prefs.put("download.default_directory", downloadFilePath);
				contentSettings.put("notifications", 2);
				profile.put("managed_default_content_settings", contentSettings);
				prefs.put("profile", profile);
				prefs.put("excludeSwitches", Arrays.asList("--disable-popup-blocking"));
				options.setExperimentalOption("prefs", prefs);
				options.setCapability(ChromeOptions.CAPABILITY, options);
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
				System.out.println("chrome initialized");
				}
			} else if (browser.equalsIgnoreCase("FIREFOX")) {
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				driver.manage().window().maximize();
				System.out.println("firefox initialized");
			} else if (browser.equalsIgnoreCase("EDGE")) {
				WebDriverManager.edgedriver().setup();
				;
				driver = new EdgeDriver();
				driver.manage().window().maximize();
				System.out.println("Edge browser initialized");
			}
			LogUtil.infoLog(getClass(),
					"\n\n+===========================================================================================================+");
			LogUtil.infoLog(getClass(), "Suite started" + " at " + new Date());
			LogUtil.infoLog(getClass(), "Suite Execution For Web application on environment : " + executionEnv);

			LogUtil.infoLog(getClass(),
					"\n\n+==========================================================================================================+");
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(implicitlyWaitDurationSeconds));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return driver;
	}

	public static void folderCreation() {
		String folderNameWithProject = "ExecutionReports";
		String filePathOriginal = System.getProperty("user.dir") + File.separator + folderNameWithProject;
		String filePathBackup1 = System.getProperty("user.dir") + File.separator + folderNameWithProject + "_Backup_1";
		String filePathBackup2 = System.getProperty("user.dir") + File.separator + folderNameWithProject + "_Backup_2";
		String filePathBackup3 = System.getProperty("user.dir") + File.separator + folderNameWithProject + "_Backup_3";
		boolean flag1 = false;
		boolean flag2 = false;
		boolean flag3 = false;
		if (new File(filePathOriginal).exists()) {
			if (new File(filePathBackup1).exists()) {
				flag1 = true;
			}

			if (new File(filePathBackup2).exists()) {
				flag2 = true;
			}

			if (new File(filePathBackup3).exists()) {
				flag3 = true;
			}
			try {
				if (flag1 && flag2 && flag3) {
					FileUtils.deleteDirectory(new File(filePathBackup1));
					FileUtils.moveDirectory(new File(filePathBackup2), new File(filePathBackup1));
					FileUtils.moveDirectory(new File(filePathBackup3), new File(filePathBackup2));
					FileUtils.moveDirectory(new File(filePathOriginal), new File(filePathBackup3));
				}

				if (flag1 && flag2 && !flag3) {
					FileUtils.moveDirectory(new File(filePathOriginal), new File(filePathBackup3));
				}

				if (flag1 && !flag2 && !flag3) {
					FileUtils.moveDirectory(new File(filePathOriginal), new File(filePathBackup2));
				}

				if (!flag1 && !flag2 && !flag3) {
					FileUtils.moveDirectory(new File(filePathOriginal), new File(filePathBackup1));
				} else {
					FileUtils.forceMkdir(new File(filePathOriginal));
					String filePath = filePathOriginal + File.separator + "%s";
					FileUtils.forceMkdir(new File(String.format(filePath, "HtmlReport")));
					FileUtils.forceMkdir(new File(String.format(filePath, "FailedScreenshots")));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
