package utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogUtil {

	private static ThreadLocal<Logger> logger = new ThreadLocal<Logger>();

	/**
	 * @return the logger
	 */
	private static ThreadLocal<Logger> getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	private static void setLogger(Class<?> c) {
		logger.set(LogManager.getLogger(c));
	}

	public static void infoLog(Class<?> c, String s) {
		setLogger(c);
		getLogger().get().info(s);
	}

	public static void errorLog(Class<?> c, String s, Throwable e) {
		setLogger(c);
		getLogger().get().error(s);
	}

	public static void warnLog(Class<?> c, String s) {
		setLogger(c);
		getLogger().get().warn(s);
	}

	public static void debugLog(Class<?> c, String s) {
		setLogger(c);
		getLogger().get().debug(s);
	}

	public static void fatalLog(Class<?> c, String s) {
		setLogger(c);
		getLogger().get().fatal(s);
	}

	public static void infoLog(Class<?> c, int n) {
		// TODO Auto-generated method stub
		setLogger(c);
		getLogger().get().info(n);
		
	}

	public static void errorLog(Class<?> c, String s) {
		setLogger(c);
		getLogger().get().error(s);		
	}

}
