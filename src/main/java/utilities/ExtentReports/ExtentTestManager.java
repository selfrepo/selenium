package utilities.ExtentReports;

import java.util.HashMap;
import java.util.Map;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

public class ExtentTestManager {
    static Map<Long, ExtentTest> extentTestMap = new HashMap<Long, ExtentTest>();
    static ExtentReports extent = ExtentManager.getInstance();
    protected static ExtentTest test;

    public static synchronized ExtentTest getTest() {
        return (ExtentTest) extentTestMap.get((long) (Thread.currentThread().getId()));
    }

    public static synchronized void endTest() {
        extent.flush();
    }

    public static synchronized ExtentTest startTest(String testName) {
        test = extent.createTest(testName);
        System.out.println("Running test method " + testName + " ....");
        extentTestMap.put((long) (Thread.currentThread().getId()), test);
        return test;

    }
}

