package utilities.ExtentReports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.*;
import com.aventstack.extentreports.reporter.configuration.Theme;

import utilities.ConfigReader;


public class ExtentManager {
    private static ExtentReports extent;
	private static final String htmlReportPath = ConfigReader.getValue("HtmlReportPath");

    public static ExtentReports getInstance() {
        if (extent == null)
            createInstance();
        return extent;
    }

    public static ExtentReports createInstance() {
    	String path=System.getProperty("user.dir") + htmlReportPath;
    	System.out.println("Extent Reports creatInstance Method, path :"+path);
    	ExtentSparkReporter Reporter = new ExtentSparkReporter(path);
        Reporter.config().setDocumentTitle("Automation Report");
        Reporter.config().setReportName("MicroFocus test report");
        Reporter.config().setTheme(Theme.DARK);
        Reporter.config().setTimeStampFormat("MM/dd/yyyy HH:mm:ss");
        Reporter.getRunDuration();
        
        extent = new ExtentReports();
        extent.attachReporter(Reporter);
        System.out.println("Exit Extent creatInstance Method");
        return extent;
    }
}
