package utilities;

/**
 * The type Html report util.
 */
public class HTMLReportUtil {

	/**
	 * The Html.
	 */
	static String html;

	/**
	 * The constant concatt.
	 */
	public static String concatt = ".";

	/**
	 * The constant DummyString.
	 */
	public static String DummyString;
	/**
	 * The constant DummyString1.
	 */
	public static String DummyString1;

	/**
	 * The constant ImagePathh.
	 */
	public static String ImagePathh;

	/**
	 * Fail string red color string.
	 *
	 * @param stepName the step name
	 * @return the string
	 */
	public static String failStringRedColor(String stepName) {
		html = "<span style='color:red'><b>" + stepName + "</b></span>";
		return html;
	}

	/**
	 * Pass string green color string.
	 *
	 * @param stepName the step name
	 * @return the string
	 */
	public static String passStringGreenColor(String stepName) {
		html = "<span style='color:#008000'><b>" + stepName + " - PASSED" + "</b></span>";
		return html;
	}

}
