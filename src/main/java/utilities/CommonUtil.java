package utilities;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.IOUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import baseTest.BaseTest;
import utilities.ExtentReports.ExtentTestManager;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CommonUtil extends BaseTest {

	private static final int DEFAULT_WAIT_SECONDS = 30;
	private static String parentWindow;
	public static String Testdatacasename;
	public static String Testdataenv;

	/**
	 * The constant lastAction.
	 */
	public static String lastAction = "";

	// ================================================================================
	// Common Methods
	// ================================================================================

	/**
	 * Wait for visible web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param seconds    the duration
	 * 
	 * @param logStep    the elementName
	 * 
	 * @return web element
	 */
	public static WebElement waitForElementVisibilityWithDuration(WebElement element, int seconds, String elementName) {
		try {
			WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(seconds));
			return wait1.until(ExpectedConditions.visibilityOf(element));

		} catch (Throwable e) {
			lastAction = elementName + " not visible ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * Wait for visible web element.
	 *
	 * @param locator the locator
	 * 
	 * @return web element
	 */
	public static WebElement waitForVisible(WebElement ele, String elementName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_WAIT_SECONDS));
			return wait.until(ExpectedConditions.visibilityOf(ele));
		} catch (Throwable e) {
			lastAction = elementName + " not visible " + ele;
			LogUtil.errorLog(CommonUtil.class, lastAction, e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * Wait for presence of web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName
	 * 
	 * @return web element
	 */
	public static WebElement waitForElementPresence(WebElement ele, String elementName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_WAIT_SECONDS));
			return wait.until(ExpectedConditions.presenceOfElementLocated((By) ele));
		} catch (Throwable e) {
			lastAction = elementName + " not visible " + ele;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * Click on web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName which needs to be click
	 * 
	 * @return NA
	 */
	public static void click(WebElement ele, String clickableElementName) {
		try {
			ele.click();
			lastAction = "Clicked on " + clickableElementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = clickableElementName + " Not Clickable";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * input text in web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param Text       the text which needs to be entered
	 * 
	 * @param logStep    the data which needs to be entered
	 * 
	 * @return NA
	 */
	public static void inputText(WebElement ele, String text, String data) {
		try {
			ele.click();
			ele.clear();
			int i;
			for (i = 0; i < text.length(); i++) {
				ele.sendKeys(Character.toString(text.charAt(i)));
				wait.until(ExpectedConditions.attributeContains(ele, "value", text.substring(0, i)));
				Thread.sleep(10);
			}
			lastAction = data + " successfully entered";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		}

		catch (Exception e) {
			lastAction = data + " not entered successfully ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " ", e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Wait for visible web element.
	 *
	 * @param locator the locator
	 * 
	 * @return web element
	 */
	public static WebElement waitForVisible(WebElement ele) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_WAIT_SECONDS));
			return wait.until(ExpectedConditions.visibilityOf(ele));

		} catch (Throwable e) {
			lastAction = "Element not visible " + ele;
			LogUtil.errorLog(CommonUtil.class, lastAction, e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * Switch to frame boolean.
	 *
	 * @param element the frame element in which you need to switch
	 *
	 * @param logStep the frame name
	 * 
	 * @return the boolean
	 */
	public static boolean switchToFrame(WebElement ele, String frameName) {

		try {
			waitForVisible(ele, frameName);
			driver.switchTo().frame(ele);
			lastAction = "Switched to " + frameName + " successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return true;

		} catch (Throwable e) {
			lastAction = "SWITCHING TO " + frameName + " FAILED" + ele;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * Switch to default frame boolean.
	 *
	 */
	public static void switchToDefaultFrame() {
		try {
			driver.switchTo().defaultContent();
			lastAction = "Switched back to default frame successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "SWITCHING TO DEFAULT FRAME FAILED";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * check whether web element is being displayed
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName
	 * 
	 * @return NA
	 */
	public static boolean isElementDisplayed(WebElement element, String elementName) {
		try {
			waitForVisible(element, elementName);
			Boolean result = element.isDisplayed();
			lastAction = elementName + " is being displayed";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return result;
		} catch (Throwable e) {
			lastAction = elementName + " is not being displayed";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * Select by visible text boolean.
	 *
	 * @param locator the locator
	 * 
	 * @param value   the value/option you want to select, same will be used for
	 *                logging purpose as well
	 * 
	 * @return boolean
	 */
	public boolean selectByVisibleText(By locator, String value) {
		try {
			Select sel = new Select(driver.findElement(locator));
			sel.selectByVisibleText(value);
			lastAction = value + " is selected";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

			return true;
		} catch (Throwable e) {
			lastAction = value + " is not selected";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * Check whether a webelement is selected or not
	 * 
	 * @param elementBy locator
	 * 
	 * @param logStep   the elementName
	 * 
	 * @return boolean
	 * 
	 *         to be checked not using assert stating in catch block as we have to
	 *         use this method with assertion in our pages
	 */
	public static boolean isElementSelected(WebElement element, String elementName) {
		try {
			waitForVisible(element, elementName);
			Boolean result = element.isSelected();
			lastAction = elementName + " is selected ";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return result;
		} catch (Throwable e) {
			lastAction = elementName + " is not selected ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			return false;
		}
	}

	/**
	 * Scrolling to particular web element
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName which needs to be entered
	 * 
	 * @return NA
	 */
	public static void scrollIntoView(WebElement ele, String elementName) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ele);
			lastAction = "Scroll into view of " + elementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

		} catch (Throwable e) {
			lastAction = elementName + " is not scrollable";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * In case click method is not working use this method, click using
	 * javaScriptExecutor
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName which needs to be clicked
	 * 
	 * @return NA
	 */
	public static void clickUsingJavaScriptExecutor(WebElement element, String elementName) {
		try {

			waitForVisible(element, elementName);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			lastAction = "Clicked on the " + elementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);

		} catch (Throwable e) {
			lastAction = elementName + " is Not clickable";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);

			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Wait until DOM is loaded completely
	 *
	 */
	public void waitForDOMLoadToComplete() {
		new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_WAIT_SECONDS)).until(new ExpectedCondition<Boolean>() {

			public Boolean apply(WebDriver dr) {
				dr = driver;
				JavascriptExecutor js = (JavascriptExecutor) dr;
				return (Boolean) js.executeScript("return document.readyState == complete");
			}
		});
	}

	/**
	 * Execute the java scripts using javaScriptExecutor
	 *
	 * @param Script  the element script or JS Path
	 * 
	 * @param logStep the elementName
	 * 
	 * @return WebElement
	 */
	public static WebElement excuteJavaScriptExecutorScripts(String script, String elementName) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			WebElement ele = (WebElement) js.executeScript(script);
			lastAction = "Executed the script for " + elementName + " successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return ele;
		}

		catch (Throwable e) {
			lastAction = "Unable to execute the script for " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * Move to particular Element using Action class
	 *
	 * @param element the element locator
	 * 
	 * @param logStep the elementName
	 * 
	 * @return WebElement
	 */
	public static WebElement movetoElement(WebElement ele, String elementName) {
		try {
			Actions actions = new Actions(driver);
			// Call moveToElement() method of actions class to move mouse cursor to a
			actions.moveToElement(ele).perform();
			lastAction = "Moved to " + elementName + " successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

		} catch (Throwable e) {
			lastAction = "Unable to move to " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return ele;
	}

	/**
	 * Move to particular Element using Action class and click upon it
	 *
	 * @param element the element locator
	 * 
	 * @param logStep the elementName
	 * 
	 * @return WebElement
	 */
	public static WebElement movetoElementAndClick(WebElement ele, String elementName) {
		try {
			Actions actions = new Actions(driver);
			// Call moveToElement() method of actions class to move mouse cursor to a
			actions.moveToElement(ele);
			actions.click().perform();
			lastAction = "Moved and clicked on the " + elementName + " successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

		} catch (Throwable e) {
			lastAction = "Unable to move and click on the " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return ele;
	}

	/**
	 * input text in web element and pressing Enter Key afterwards.
	 *
	 * @param WebElement the element
	 * 
	 * @param Text       the text which needs to be entered using the same for
	 *                   logging purpose
	 * 
	 * @return NA
	 */
	public static void inputTextWithEnterKey(WebElement ele, String text) {
		try {
			normalWait(2);
			ele.sendKeys(Keys.chord(Keys.CONTROL, "a") + Keys.DELETE);

			int i;
			for (i = 0; i < text.length(); i++) {
				ele.sendKeys(Character.toString(text.charAt(i)));
				wait.until(ExpectedConditions.attributeContains(ele, "value", text.substring(0, i)));
				Thread.sleep(10);
			}
			normalWait(2);
			ele.sendKeys(Keys.ENTER);
			lastAction = "Entered the " + text + " and pressed the enter key";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Unable to enter the " + text + " and not pressed the enter key";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Clear text in input field.
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName
	 * 
	 * @return NA
	 */
	public static void clearInput(WebElement ele, String elementName) {
		try {
			ele.clear();
			lastAction = "Successfully cleared the " + elementName + " textfield";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Unable to clear the " + elementName + " textfield";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Navigating to passed value.
	 *
	 * @param value   i.e. URL
	 * 
	 * @param logStep the value
	 * 
	 * @return NA
	 */
	public static void navigate(String value) {
		try {
			driver.navigate().to(value);
			lastAction = "Able to navigate to " + value;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Not able to navigate to " + value;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Stops the thread for mentioned time.
	 * 
	 * @param sec Seconds to wait for
	 */
	public static void normalWait(int sec) {
		try {
			Thread.sleep(sec * 1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Select by value using SelectByValue method of Select class
	 *
	 * @param WebElement the locator
	 * 
	 * @param value      the value/option for selection and same is being used for
	 *                   logging purpose as well
	 * 
	 * @return NA
	 */
	public static void selectByValue(WebElement ele, String value) {

		Select sel = new Select(ele);
		sel.selectByValue(value);
		try {
			// Check whether element is selected or not
			sel = new Select(ele);
			if (sel.getFirstSelectedOption().isDisplayed()) {
				lastAction = value + " is selected";
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			}
		} catch (Throwable e) {
			lastAction = "Not able to select " + value;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Select by value boolean using visible text method.
	 *
	 * @param locator the locator
	 * 
	 * @param value   the value/option for selection and same is being used for
	 *                logging purpose as well
	 * 
	 * @return boolean
	 */
	public static boolean selectByVisibleText(WebElement ele, String value) {
		try {
			Select sel = new Select(ele);
			sel.selectByVisibleText(value);

			// Check whether element is selected or not
			sel = new Select(ele);
			if (sel.getFirstSelectedOption().isDisplayed()) {
				lastAction = value + " is selected";
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
				return true;
			}
			return false;
		} catch (Throwable e) {
			lastAction = "Not able to select " + value;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * Select by index boolean.
	 *
	 * @param locator    the locator
	 * 
	 * @param index      the index at which option is present
	 * 
	 * @param optionName the name of the option
	 * 
	 * @return boolean
	 */
	public static boolean selectByIndex(WebElement ele, int index, String optionName) {
		try {
			Select sel = new Select(ele);
			sel.selectByIndex(index);

			// Check whether element is selected or not
			sel = new Select(ele);
			if (sel.getFirstSelectedOption().isDisplayed()) {
				lastAction = optionName + " is selected at index " + index;
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
				return true;
			}
			return false;
		} catch (Throwable e) {
			lastAction = "Not able to select the " + optionName + " at index " + index;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * Find element with respective locator type
	 *
	 * @param element the element
	 * 
	 * @param locator method the locator method like xpath, id, css so on
	 * 
	 * @param logStep the logStep i.e. elementName
	 * 
	 * @return WebElement
	 */
	public static WebElement findElement(String element, String locatorMethod, String elementName) {
		WebElement ele = null;
		try {
			if (locatorMethod == "xpath") {
				ele = driver.findElement(By.xpath(element));
				lastAction = "Found the " + elementName + " with locator " + locatorMethod;
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			}
		} catch (Throwable e) {
			lastAction = "Unable to find the " + elementName + " with locator " + locatorMethod;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return ele;
	}

	/**
	 * To verify the text of web element
	 *
	 * @param element the element
	 * 
	 * @param text    which needs to be verified
	 * 
	 * @param logStep the elementName
	 * 
	 * @return String
	 */
	public static String verifyText(WebElement ele, String text, String elementName) {
		String elementText = null;
		try {
			waitForVisible(ele, elementName);
			elementText = ele.getText();
			if (elementText.equalsIgnoreCase(text)) {
				lastAction = "Verified the '" + text + "' successfully for " + elementName;
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			}
			else {
				lastAction = "Verified the '" + text + "' not successfully for " + elementName;
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.FAIL, HTMLReportUtil.passStringGreenColor(lastAction));
			}
		} catch (Throwable e) {
			lastAction = "Not able to extract the " + text + " for " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return elementText;
	}

	/**
	 * To refresh the WebPage
	 *
	 * @param logStep i.e. pageName, the name of the WebPage
	 */
	public static void refresh(String pageName) {
		try {
			driver.navigate().refresh();
			lastAction = "Refreshed the " + pageName + " successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Not able to refresh the " + pageName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Switch to new window based on handles.
	 * 
	 * @param logStep i.e. windowName which is the name of the window
	 *
	 * @return String
	 */
	public static String switchToNewWindow(String windowName) {
		try {
			parentWindow = driver.getWindowHandle();

			// Loop through until we find a new window handle
			for (String windowHandle : driver.getWindowHandles()) {
				if (!parentWindow.contentEquals(windowHandle)) {
					driver.switchTo().window(windowHandle);
					lastAction = "Switched to the " + windowName + " successfully";
					LogUtil.infoLog(CommonUtil.class, lastAction);
					ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
					break;
				}
			}
		} catch (Throwable e) {
			lastAction = "Not able to switch to the " + windowName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return parentWindow;
	}

	/**
	 * Wait until file is downloaded
	 *
	 * @param folderLocation, the location at which file will be downloaded
	 * 
	 * @return NA
	 */
	public static void waitUntilFileToDownload(String folderLocation) throws InterruptedException {
		File directory = new File(folderLocation);
		boolean downloadinFilePresence = false;
		File[] filesList = null;
		LOOP: while (true) {
			filesList = directory.listFiles();
			for (File file : filesList) {
				downloadinFilePresence = file.getName().contains(".crdownload");
			}
			if (downloadinFilePresence) {
				for (; downloadinFilePresence;) {
					normalWait(1);
					continue LOOP;
				}
			} else {
				break;
			}
		}
	}

	/**
	 * Check whether file is downloaded or not, if yes then delete the file
	 *
	 * @param fileName, the name of the file which needs to be verified and same is
	 *                  used for logging purpose as well
	 * 
	 * @return boolean
	 * 
	 *         this method will be used in conjuction with assertion that is why not
	 *         using any assertion within method
	 */
	public static boolean isFileDownloaded(String fileName) {
		File dir = new File(DriverUtil.downloadFilePath);
		File[] dirContents = dir.listFiles();

		for (int i = 0; i < dirContents.length; i++) {
			if (dirContents[i].getName().contains(fileName)) {
				// File has been found, it can now be deleted:
				dirContents[i].delete();
				lastAction = fileName + " is downloaded and found successfully";
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
				return true;
			}
		}
		lastAction = fileName + " is not downloaded and found";
		LogUtil.errorLog(CommonUtil.class, lastAction);
		ExtentTestManager.getTest().log(Status.FAIL, HTMLReportUtil.failStringRedColor(CommonUtil.lastAction));
		return false;
	}

	/**
	 * Switch back to parent window based on handles.
	 * 
	 * @param parentWindow     i.e. parent window handle returned via
	 *                         switchToNewWindow method
	 *
	 * @param parentWindowName i.e. actual name of the parent window on which you
	 *                         want to switch
	 *
	 */
	public static void switchToParentWindow(String parentWindow, String parentWindowName) {
		try {
			// Loop through until we find a new window handle
			for (String windowHandle : driver.getWindowHandles()) {
				if (parentWindow.contentEquals(windowHandle)) {
					driver.close();
					driver.switchTo().window(parentWindow);
					lastAction = "Switched to the parent window " + parentWindowName + " successfully";
					LogUtil.infoLog(CommonUtil.class, lastAction);
					ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
					break;
				}
			}
		} catch (Throwable e) {
			lastAction = "Not able to switch to the parentwindow " + parentWindowName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * To verify the whether the web element is enabled or not
	 *
	 * @param element the element
	 * 
	 * @param logStep the elementName
	 * 
	 * @return boolean
	 */
	public static boolean isElementEnabled(WebElement ele, String elementName) {
		try {
			waitForVisible(ele, elementName);
			Boolean result = ele.isEnabled();
			lastAction = elementName + " is enabled ";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return result;
		} catch (Throwable e) {
			lastAction = elementName + " is not enabled";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * To Get the current date
	 *
	 * @param pattern in which you want the date for example dd-MMM-yyyy
	 * 
	 * @return String i.e. current Date
	 */
	public static String getCurrentDate(String pattern) {
		try {
			// Create object of SimpleDateFormat class and decide the format
			DateFormat dateFormat = new SimpleDateFormat(pattern);

			// get current date time with Date()
			Date date = new Date();

			// Now format the date
			String date1 = dateFormat.format(date);
			lastAction = "Returned the current date " + date1;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return date1;
		} catch (Throwable e) {
			lastAction = "Date is not returned ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * Upload files using send keys boolean.
	 *
	 * @param ele          the locator
	 * 
	 * @param fileName,    the file name which needs to be uploaded
	 * 
	 * @param elementName, the name of the element where file is being uploaded
	 * 
	 * @return boolean
	 * 
	 */
	public static boolean uploadFilesUsingSendKeys(WebElement ele, String fileName, String elementName) {
		try {
			String filePath = System.getProperty("user.dir") + "\\uploadFiles\\" + fileName;
			waitForVisible(ele, elementName);
			ele.clear();
			ele.sendKeys(filePath);
			lastAction = fileName + " is uploaded for " + elementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

			return true;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			lastAction = fileName + " is not uploaded for " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * Upload files using Robot Class
	 *
	 * @param ele          the locator
	 * 
	 * @param fileName,    the file name which needs to be uploaded
	 * 
	 * @param elementName, the name of the element where file is being uploaded
	 * 
	 * @return boolean
	 * 
	 */
	public static boolean uploadFilesUsingRobotClass(WebElement ele, String data, String elementName)
			throws InterruptedException {

		String fileName = System.getProperty("user.dir") + "\\uploadFiles\\" + data;
		waitForVisible(ele, elementName);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
		ele.click();
		StringSelection ss = new StringSelection(fileName);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		// native key strokes for CTRL, V and ENTER keys
		Robot robot;
		try {
			robot = new Robot();
			normalWait(1);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			normalWait(1);
			robot.keyRelease(KeyEvent.VK_ENTER);
			lastAction = fileName + " is uploaded for " + elementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return true;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			lastAction = fileName + " is not uploaded for " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
	}

	/**
	 * Del directory boolean.
	 * 
	 * @param folderPathAcronym, for example it could be either "upload" or
	 *                           "download"
	 * 
	 * @param fileName,          the name of the file which needs to be deleted
	 *
	 * @return boolean
	 */
	public static boolean delFileWithInDirectory(String folderPathAcronym, String fileName) {
		Boolean result = false;
		try {
			File delDestination = null;
			if (folderPathAcronym.equalsIgnoreCase("download")) {
				delDestination = new File(DriverUtil.downloadFilePath);
			} else if (folderPathAcronym.equalsIgnoreCase("upload")) {
				delDestination = new File(System.getProperty("user.dir") + "\\" + ConfigReader.getValue("UploadFiles"));
			}
			if (delDestination.exists()) {
				File[] files = delDestination.listFiles();
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						delFileWithInDirectory(folderPathAcronym, fileName);
					} else if (files[i].getName().matches(fileName)) {
						files[i].delete();
						result = true;
					} else {
						result = false;
					}
				}
				if (result == true) {
					lastAction = fileName + " is deleted ";
					LogUtil.infoLog(CommonUtil.class, lastAction);
					ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
				}
			}
		} catch (Throwable e) {
			lastAction = fileName + " is not deleted";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return false;
		}
		return result;
	}

	/**
	 * Check whether file is downloaded or not, but it will not delete the file
	 *
	 * @param fileName, the name of the file which needs to be verified and same is
	 *                  used for logging purpose as well
	 * 
	 * @return boolean
	 * 
	 *         this method will be used in conjuction with assertion that is why not
	 *         using any assertion within method
	 */
	public static boolean isFileDownloadedWithoutFileDeletion(String fileName) {
		try {
			File dir = new File(DriverUtil.downloadFilePath);
			File[] dirContents = dir.listFiles();

			for (int i = 0; i < dirContents.length; i++) {
				if (dirContents[i].getName().contains(fileName)) {
					lastAction = fileName + " is downloaded and found successfully ";
					LogUtil.infoLog(CommonUtil.class, lastAction);
					ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
					return true;
				}
			}
			return false;
		} catch (Throwable e) {
			lastAction = fileName + " is not downloaded and found";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			return false;
		}
	}

	/**
	 * Move file from Downloaded_Files to UploadFiles
	 *
	 * @param fileName, the name of the file which needs to be moved and same is
	 *                  used for logging purpose as well
	 * 
	 * @return NA
	 * 
	 */
	public static void moveFileFromDownloadFilesToUploadFiles(String fileName) {
		Path result = null;
		String src = DriverUtil.downloadFilePath + "\\" + fileName;
		String dest = System.getProperty("user.dir") + "\\" + ConfigReader.getValue("UploadFiles") + "\\" + fileName;
		try {
			result = Files.move(Paths.get(src), Paths.get(dest));

		} catch (IOException e) {
			lastAction = "Unable to move the file " + fileName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			ExtentTestManager.getTest().log(Status.FAIL, HTMLReportUtil.failStringRedColor(lastAction));
		}
		if (result != null) {
			lastAction = fileName + " moved successfully.";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		}
	}

	/**
	 * Takes the screenshot
	 * 
	 * @return String
	 * 
	 */
	public static String takeScreenShot() {
		String base64 = null;
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String scFileName = "ScreenShot_" + System.currentTimeMillis();
		String screenshotFilePath = System.getProperty("user.dir") + ConfigReader.getValue("screenshotPath") + "\\"
				+ scFileName + ".jpg";
		try {
			FileUtils.copyFile(source, new File(screenshotFilePath));
			byte[] imageBytes = null;
			try {
				InputStream is = new FileInputStream(screenshotFilePath);
				imageBytes = IOUtils.toByteArray(is);
				Thread.sleep(2000);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			base64 = Base64.getEncoder().encodeToString(imageBytes);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return base64;
	}

	/**
	 * For Attaching the screenshot for passed test cases only in HTML report
	 * 
	 * @return NA
	 * 
	 */
	public static void attachScreenshotOfPassedTestsInReport() {
		String screenshot = CommonUtil.takeScreenShot();

		// ExtentReports log and screenshot operations for passed step.
		try {
			ExtentTestManager.getTest().log(Status.PASS,
					HTMLReportUtil.passStringGreenColor("Attached screenshot for the step"),
					MediaEntityBuilder.createScreenCaptureFromPath("data:image/jpg;base64," + screenshot).build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * To verify the text of web element
	 *
	 * @param element the element
	 * 
	 * @param logStep the elementName
	 * 
	 * @return String
	 */
	public static String getText(WebElement ele, String elementName) {
		String elementText = null;
		try {
			elementText = ele.getText();
			lastAction = "Got text " + elementText + " for " + elementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Not able to extract the text for " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return elementText;
	}

	/**
	 * To get list of web elements for a particular locator
	 *
	 * @param locator the locator for the element
	 * 
	 * @param logStep the elementName
	 * 
	 * @return List<WebElement>
	 */
	public static List<WebElement> getAllElements(String locator, String elementName) {
		List<WebElement> elements = null;
		try {
			elements = driver.findElements(By.xpath(locator));
			lastAction = "Found " + elements.size() + " for " + elementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Not able to find any element for " + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return elements;
	}

	/**
	 * To Get the future date
	 *
	 * @param pattern      in which you want the date for example dd-MMM-yyyy
	 * 
	 * @param daysToExtend i.e.no of days added to the current date
	 * 
	 * @return String i.e. future Date
	 */
	public static String getFutureDate(String pattern, int daysToExtend) {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, daysToExtend);
			// Create object of SimpleDateFormat class and decide the format
			DateFormat dateFormat = new SimpleDateFormat(pattern);

			// get future date time with Date()
			Date date = calendar.getTime();

			// Now format the date
			String date1 = dateFormat.format(date);
			lastAction = "Returned the future date " + date1;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return date1;
		} catch (Throwable e) {
			lastAction = "Furtue Date is not returned ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * input text in web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param Text       the text which needs to be entered
	 * 
	 * @param logStep    the data which needs to be entered
	 * 
	 * @return NA
	 */
	public static void jsinputText(WebElement ele, String text, String data) {

		try {
			ele.click();
			ele.clear();

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].value='" + text + "';", ele);
			ele.sendKeys(Keys.TAB);
			lastAction = data + " successfully entered";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		}

		catch (Throwable e) {
			lastAction = data + " not entered successfully ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " ", e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Generates the random number.
	 *
	 * @param lowerRange the lower range for the number
	 * 
	 * @param upperRange the upper range for the number
	 * 
	 * @param logStep    the data which needs to be entered
	 * 
	 * @return NA
	 */
	public static int generateRandomNumber(int lowerRange, int upperRange) {
		int lr = lowerRange;
		int ur = upperRange;

		System.out.println("Random value between " + lr + " to " + ur + ":");
		int randomNumber = (int) (Math.random() * (ur - lr + 1) + lr);

		return randomNumber;
	}

	public static void scrollToBottomOfPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Scroll down till the bottom of the page
		js.executeScript("window.scrollBy(0,250)");
		LogUtil.infoLog(CommonUtil.class, "scrolled to bottom of the page");
	}

	/**
	 * Wait for clickable web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param seconds    the duration
	 * 
	 * @param logStep    the elementName
	 * 
	 * @return NA
	 */
	public static void waitForElementToBeClickable(WebElement element, int seconds, String elementName) {
		try {
			WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(seconds));
			wait1.until(ExpectedConditions.elementToBeClickable(element));
			lastAction = elementName + " is clickable";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

		} catch (Throwable e) {
			lastAction = elementName + " not clickable ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Wait for invisibility of web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param seconds    the duration
	 * 
	 * @param logStep    the elementName
	 * 
	 * @return NA
	 */
	public static void waitForElementInvisiblity(WebElement element, int seconds, String elementName) {
		try {
			WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(seconds));
			wait1.until(ExpectedConditions.invisibilityOf(element));
			lastAction = elementName + " is not visible now";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

		} catch (Throwable e) {
			lastAction = elementName + " is still visible ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * To navigate back to the WebPage
	 *
	 * @param logStep i.e. pageName, the name of the WebPage
	 */
	public static void back(String pageName) {
		try {
			driver.navigate().back();
			lastAction = "Went back to the " + pageName + " successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Not able to go back to the " + pageName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * To convert numeric month i.e. 02 to word month i.e. February
	 *
	 * @param numMonth i.e. numeric month string
	 */
	public static String convertNumericMonthStringToWordMonth(String numMonth) {
		try {
			int convertedMonth = Integer.parseInt(numMonth);
			String month;
			switch (convertedMonth) {

			// Case
			case 1:
				month = "January";
				break;
			case 2:
				month = "February";
				break;
			case 3:
				month = "March";
				break;
			case 4:
				month = "April";
				break;
			case 5:
				month = "May";
				break;
			case 6:
				month = "June";
				break;
			case 7:
				month = "July";
				break;
			case 8:
				month = "August";
				break;
			case 9:
				month = "September";
				break;
			case 10:
				month = "October";
				break;
			case 11:
				month = "November";
				break;
			case 12:
				month = "December";
				break;

			// Default case
			default:
				month = "Invalid Month";
			}
			lastAction = "Converted the numeric month " + numMonth + " to word month " + month + " successfully";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return month;
		} catch (Throwable e) {
			lastAction = "Not able to convert the numeric month " + numMonth + " to word month";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return null;
		}
	}

	/**
	 * Scrolling to specific web element
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName which needs to be entered
	 * 
	 * @return NA
	 */
	public static void scrollViewUpto(WebElement ele, String elementName) {
		try {
			waitForVisible(ele, elementName);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded();", ele);
			lastAction = "Scroll into view of " + elementName;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));

		} catch (Throwable e) {
			lastAction = elementName + " is not scrollable";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}

	/**
	 * Note : This method don't use assertion so even if element is not being
	 * displayed test will not fail check whether web element is being displayed
	 *
	 * @param WebElement the element
	 * 
	 * @param logStep    the elementName
	 * 
	 * @return NA
	 */
	public static boolean elementisDisplayed(WebElement element, String elementName) {
		try {
			waitForVisible(element, elementName);
			Boolean result = element.isDisplayed();
			lastAction = elementName + " is being displayed";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return result;
		} catch (Throwable e) {
			lastAction = elementName + " is not being displayed";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			return false;
		}
	}

	/**
	 * Verify the element text
	 *
	 * @param ele     the element
	 * 
	 * @param text    method the text to be verified
	 * 
	 * @param logStep the logStep i.e. elementName
	 * 
	 * @return String
	 */
	public static String verifycontainstext(WebElement ele, String text, String elementName) {
		String elementText = null;
		try {
			waitForVisible(ele, elementName);
			elementText = ele.getText();
			LogUtil.infoLog(CommonUtil.class, elementText);
			if (elementText.contains(text)) {
				lastAction = "Verified the " + text + " successfully for" + elementName;
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			} else {
				lastAction = "Verified the " + text + " not successfully for" + elementName;
				LogUtil.infoLog(CommonUtil.class, lastAction);
				ExtentTestManager.getTest().log(Status.FAIL, HTMLReportUtil.passStringGreenColor(lastAction));
			}
		} catch (Throwable e) {
			lastAction = "Not able to extract the " + text + " for" + elementName;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		return elementText;
	}

	/**
	 * Find element with respective locator type
	 *
	 * @param LocatorType the element locator type
	 * 
	 * @param locator     method the locator value like xpath value, id, css so on
	 * 
	 * @param logStep     the logStep i.e. data
	 * 
	 * @return WebElement
	 */
	public static WebElement getWebElement(String LocatorType, String LocatorValue, String data) {
		WebElement ele = null;
		By byLocator = null;

		try {
			switch (LocatorType.toLowerCase()) {
			case "id":
				ele = driver.findElement(By.id(LocatorValue));
				break;
			case "class":
				ele = driver.findElement(By.className(LocatorValue));
				break;
			case "name":
				ele = driver.findElement(By.name(LocatorValue));
				break;
			case "css":
				ele = driver.findElement(By.cssSelector(LocatorValue));
				break;
			case "xpath":
				System.out.println("LocatorValue Before  : " + LocatorValue);
				String LocatorValue1 = LocatorValue.replace("{replacevalue}", data);
				System.out.println("LocatorValue   After :   " + LocatorValue1);
				ele = driver.findElement(By.xpath(LocatorValue1));
				break;
			case "link":
				ele = driver.findElement(By.linkText(LocatorValue));
				break;
			}

		} catch (Exception e) {
			lastAction = "element is not visible";
			LogUtil.errorLog(CommonUtil.class, lastAction + " ", e);
		}
		return ele;
	}

	/**
	 * input text in web element without enter key.
	 *
	 * @param WebElement the element
	 * 
	 * @param Text       the text which needs to be entered
	 * 
	 * @param logStep    the data which needs to be entered
	 * 
	 * @return NA
	 */
	public static void inputTextWithoutEnterKey(WebElement ele, String text, String data) {
		try {
			normalWait(2);
			ele.sendKeys(Keys.chord(Keys.CONTROL, "a") + Keys.DELETE);

			int i;
			for (i = 0; i < text.length(); i++) {
				ele.sendKeys(Character.toString(text.charAt(i)));
				wait.until(ExpectedConditions.attributeContains(ele, "value", text.substring(0, i)));
				Thread.sleep(10);
			}
			normalWait(2);
			lastAction = "Entered the " + text;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		} catch (Throwable e) {
			lastAction = "Unable to enter the " + text;
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
	}
	
	/** input text in web element.
	 *
	 * @param WebElement the element
	 * 
	 * @param Text the text which needs to be entered
	 * 
	 * @param logStep the data which needs to be entered
	 * 
	 * @return NA
	 */
	public static void inputStringText(WebElement ele, String text, String data) {
		try {
			ele.click();
			ele.clear();
			ele.sendKeys(text.trim());
			lastAction = data + " successfully entered";
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
		}

		catch (Exception e) {
			lastAction = data + " not entered successfully ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " ", e);
			Assert.fail(lastAction, e);
		}
	}
	
	
	/** fetch search test data value from Datasource sheet.
	 *
	 * @param DataSource workbook
	 * 
	 * @param DataSource Sheet
	 * 
	 * @param Search Keyword
	 * 
	 * @return Collection of data will return in Map.
	 */
	
	public static Map<String,String> getsearchvalue(String excelsheet, String datasheet,String searchkey){
		Map<String,String> storedataMap = new TreeMap<String,String>();
		String StoreTestCaseName = CommonUtil.getTestcaseName();
		String query = null;
		String filepath = System.getProperty("user.dir")+excelsheet;
		query = String.format("SELECT * FROM %s WHERE StoreKey='%s' AND StoreTestCaseName='%s'", datasheet.trim(),searchkey.trim(),StoreTestCaseName.trim());
		Fillo fillo = new Fillo();
		Connection conn = null;
		Recordset recordset = null;
		
		try {
			conn = fillo.getConnection(filepath);
			recordset = conn.executeQuery(query);
			while(recordset.next()) {
				for(String field : recordset.getFieldNames()) {
					storedataMap.put(field, recordset.getField(field));
				}
			}
			conn.close();
		} catch (Exception e) {
			lastAction = "Search Key is not available";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
		}
		conn.close();
		return storedataMap;
		
	}
	
	/** Dynamic runtime value has been stored in DataSource sheet.
	 *
	 * @param DataSource workbook
	 * 
	 * @param DataSource Sheet
	 * 
	 * @param Search Keyword
	 * 
	 * @param Search Value
	 * 
	 */
	
	public static void setsearchvalue(String excelsheet, String datasheet,String searchkey,String searchvalue){
		Map<String,String> storedataMap = new TreeMap<String,String>();
		String StoreTestCaseName = CommonUtil.getTestcaseName();
		String query = null;
		if(searchvalue == null) {
			searchvalue = "";
		}
		String filepath = System.getProperty("user.dir")+excelsheet;
		query = String.format("INSERT Into %s (StoreTestCaseName,StoreKey,StoreValue) VALUES('%s','%s','%s')", datasheet.trim(),StoreTestCaseName.trim(),searchkey.trim(),searchvalue.trim());
		Fillo fillo = new Fillo();
		Connection conn = null;
		Recordset recordset = null;
		
		try {
			conn = fillo.getConnection(filepath);
			try {
				conn.executeUpdate(query);
				//			conn.close();
			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			lastAction = "Search Key is not available";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
		}
		conn.close();
		
	}
	
	/** Update the test data in Datasource sheet.
	 *
	 * @param DataSource workbook
	 * 
	 * @param DataSource Sheet
	 * 
	 * @param Search Keyword
	 * 
	 * @param Serach value
	 * 
	 * @return Collection of data will return in Map.
	 */
	public static Map<String,String> setupdatevalue(String excelsheet, String datasheet,String searchkey,String searchvalue){
		Map<String,String> storedataMap = new TreeMap<String,String>();
		String StoreTestCaseName = CommonUtil.getTestcaseName();
		String query = null;
		String filepath = System.getProperty("user.dir")+excelsheet;
		if(searchvalue == null) {
			searchvalue = "";
		}
		Fillo fillo = new Fillo();
		Connection conn = null;
		Recordset recordset = null;
		try {
			query = String.format("UPDATE %s SET StoreValue = '%s' WHERE  StoreKey='%s' AND StoreTestCaseName='%s'", datasheet.trim(),searchvalue.trim(),searchkey.trim(),StoreTestCaseName.trim());
			conn = fillo.getConnection(filepath);
				conn.executeUpdate(query);
		} catch (Exception e) {
			lastAction = "Search Key is not available";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
		}
		conn.close();
		return storedataMap;
		
	}
	
	/** fetch the test data in Datasource sheet using search key.
	 *
	 * @param Search Keyword
	 * 
	 * @return return the search value.
	 */
	public static String getStoredata(String searchkey) {
		String returndata = null;
		String StoreExcelName = ConfigReader.getValue("storeDataExcelPath");
		String StoredatasheetName = getenvironment();
		try {
			returndata = getstorevalue(StoreExcelName, StoredatasheetName, searchkey);
			lastAction = "Stored data fetched successfully " + searchkey + " is " + returndata;
			LogUtil.infoLog(CommonUtil.class, lastAction);
		} catch (Exception e) {
			lastAction = "Search Key is not available";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
		}
		return returndata;
	}
	
	
	/** fetch the test data in Datasource sheet using search key.
	 *
	 * @param DataSource workbook
	 * 
	 * @param DataSource Sheet
	 * 
	 * @param Search Keyword
	 * 
	 * @return return the search value.
	 */
	
	public static String getstorevalue(String excelsheet, String datasheet,String searchkey) {
		String returndata = null;
		
		try {
			Map<String, String> StoreDataInMap = getsearchvalue(excelsheet, datasheet, searchkey);
			returndata = StoreDataInMap.get("StoreValue");
			lastAction = "Stored data fetched successfully" + searchkey + " is" + returndata;
			LogUtil.infoLog(CommonUtil.class, lastAction);
		} catch (Exception e) {
			lastAction = "Stored data not fetched successfully" + searchkey + " is" + returndata;
			LogUtil.infoLog(CommonUtil.class, lastAction);
		}
		return returndata;
		
		
	}
	
	
	/** Store the test data in Datasource sheet using search key.
	 *
	 * @param Search Keyword
	 *
	 */
	
	public static void storedata(String Storekey, String Storevalue) {
		String StoreExcelName = ConfigReader.getValue("storeDataExcelPath");
		if(Storevalue==null) {
			Storevalue = "";
		}
		String StoredatasheetName = CommonUtil.getenvironment();
		String getexistingvalue = getstorevalue(StoreExcelName,StoredatasheetName,Storekey);
		if(getexistingvalue == null) {
			System.out.println("Search Key newly added");
			setsearchvalue(StoreExcelName,StoredatasheetName,Storekey,Storevalue);
			String getexistingvaluea = getstorevalue(StoreExcelName,StoredatasheetName,Storekey);
			System.out.println("getexistingvaluea  " + getexistingvaluea);
		}else {
			System.out.println("Search Key Already added");
			setupdatevalue(StoreExcelName,StoredatasheetName,Storekey,Storevalue);
			String getexistingvaluea = getstorevalue(StoreExcelName,StoredatasheetName,Storekey);
			System.out.println("getexistingvaluea OverWrite value " + getexistingvaluea);
		}
		
	}
	
	/** set the tetcase name tp store the test data.
	 *
	 * @param testcasename
	 *
	 * @return testcasename;
	 */
	
	public static String setTestcaseName(String testcasename) {
		Testdatacasename = testcasename;
		return Testdatacasename;
		
	}
	
	/** get the tetcase name tp store the test data.
	 *
	 * @return testcasename;
	 */
	public static String getTestcaseName() {
		return Testdatacasename;
		
	}
	
	/** set the environment name tp store the test data.
	 *
	 * @param environment
	 *
	 * @return environment;
	 */
	
	public static String setenvironment(String environment) {
		Testdataenv = environment;
		return Testdataenv;
		
	}
	
	/** get the environment name tp store the test data.
	 *
	 * @return environment;
	 */
	public static String getenvironment() {
		return Testdataenv;
		
	}
	
	/**
	 * To Get the current date
	 *
	 * @param pattern in which you want the date for example dd-MMM-yyyy HH:mm:ss
	 * 
	 * @return String i.e. current Date and time
	 */
	public static String getCurrentDateAndTime(String pattern) {
		try {
			// Create object of SimpleDateFormat class and decide the format
			DateFormat dateFormat = new SimpleDateFormat(pattern);

			// get current date time with Date()
			Calendar cal = Calendar.getInstance();

			// Now format the date
			String date1 = dateFormat.format(cal.getTime());
	
			lastAction = "Returned the current date and time " + date1;
			LogUtil.infoLog(CommonUtil.class, lastAction);
			ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor(lastAction));
			return date1;
		} catch (Throwable e) {
			lastAction = "Current date and time is not returned ";
			LogUtil.errorLog(CommonUtil.class, lastAction + " " + e);
			Assert.fail(lastAction, e);
			return null;
		}
	}
	
}