package utilities;

import org.openqa.selenium.WebDriver;


public class GlobalUtil {

	private static CommonSettings commonSettings = new CommonSettings();
	private static WebDriver driver = null;

	/**
	 * Gets common settings.
	 *
	 * @return common settings
	 */
	public static CommonSettings getCommonSettings() {
		return commonSettings;
	}

	/**
	 * Sets common settings.
	 *
	 * @param commonSettings the common settings
	 */
	public static void setCommonSettings(CommonSettings commonSettings) {
		GlobalUtil.commonSettings = commonSettings;
	}

	
	/**
	 * Gets driver.
	 *
	 * @return the driver
	 */
	public static WebDriver getDriver() {
		return driver;
	}	

	/**
	 * Sets driver.
	 *
	 * @param driver the driver to set
	 */
	public static void setDriver(WebDriver driver) {
		GlobalUtil.driver = driver;
	}

}
