package utilities;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;

/**
 * ExcelDateUtil class is refer to read and write in excel
 */
public class ExcelDataUtil {

	private static FileInputStream fs = null;
	private static Workbook workbook = null;
	private static Sheet sheet = null;
	private static final String testDatafilePath = ConfigReader.getValue("testDataExcelPath");
	private static final int columnToLookTestCaseID = Integer.parseInt(ConfigReader.getValue("columnToLookTestCaseID"));
	private static final String excelextensionxlsx = ".xlsx";
	/**
	 * The constant EXCEPTIONCAUGHT.
	 */
	public static final String EXCEPTIONCAUGHT = "Exception caught";
	private static final String excelextensionxls = ".xls";
	private static final String INVALID_SHEET_MESSAGE = "Error! No such sheet available in Excel file";

	/**
	 * <H1>Excel initialize</H1>
	 *
	 * @param filePath  the file path
	 * @param sheetName the sheet name
	 */
	public static void init(String filePath, String sheetName) {
		String fileExtensionName = filePath.substring(filePath.indexOf('.'));
		try {
			fs = new FileInputStream(System.getProperty("user.dir") + filePath);
			if (fileExtensionName.equals(excelextensionxlsx)) {
				// If it is xlsx file then create object of XSSFWorkbook class
				workbook = WorkbookFactory.create(fs);
			}
			// Check condition if the file is xls file
			else if (fileExtensionName.equals(excelextensionxls)) {
				// If it is xls file then create object of XSSFWorkbook class
				workbook = new HSSFWorkbook(fs);
			}
			if (workbook.getSheetIndex(workbook.getSheet(sheetName)) == -1) {
				LogUtil.infoLog(ExcelDataUtil.class, INVALID_SHEET_MESSAGE + sheetName);

			}
			sheet = workbook.getSheet(sheetName);
		} catch (Exception e) {
			LogUtil.errorLog(ExcelDataUtil.class, EXCEPTIONCAUGHT, e);
		}

	}

	/**
	 * <H1>Get test data with test case id</H1>
	 *
	 * @param sheetName  the sheet name
	 * @param testCaseID the test case id
	 * @return test data with test case id
	 */
	public static HashMap<String, String> getTestDataWithTestCaseName(String filePath, String sheetName, String testCaseName) {
		System.out.println("inside getTestDataWithTestCaseName method");
		boolean found = false;
		boolean isfirstRow = false;
		Row firstrow = null;
		DataFormatter formatter = new DataFormatter();
		final HashMap<String, String> currentRowData = new HashMap<String, String>();
		// Initialize class
		init(filePath, sheetName);
		Iterator<Row> rowIterator = sheet.iterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (!isfirstRow) {
					firstrow = row;
					isfirstRow = true;
				}
				if (row.getCell(columnToLookTestCaseID).getStringCellValue().equalsIgnoreCase(testCaseName)) {
					found = true;
					int k = row.getLastCellNum();
					for (int i = 0; i < k; i++) {
						String cellValue = formatter.formatCellValue(row.getCell(i));
						if (cellValue == null) {
							cellValue = "";
						}
						cellValue = getUniqueString(cellValue);
						currentRowData.put(firstrow.getCell(i).getStringCellValue(), cellValue);
					}
					break;
				}

			}

			fs.close();

		} catch (Exception e) {
			LogUtil.errorLog(ExcelDataUtil.class, EXCEPTIONCAUGHT, e);
		}
		if (!found)
			LogUtil.infoLog(ExcelDataUtil.class, "No data found with given key-> " + testCaseName);
		System.out.println("end of getTestDataWithTestCaseName method");
		return currentRowData;

	}

	/**
	 * Gets unique string.
	 *
	 * @param string the string
	 * @return the unique string
	 */
	public static String getUniqueString(String string) {
		return string.replaceAll("UNIQUE", "" + System.currentTimeMillis());
	}

	/**
	 * The type Invalid sheet exception.
	 */
	class InvalidSheetException extends Exception {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Instantiates a new Invalid sheet exception.
		 *
		 * @param s the s
		 */
		InvalidSheetException(String s) {
			super(s);
		}
	}
}
