package utilities;

/**
 * This CommonSetting class help in generate results
 */
public class CommonSettings {

	private String appType;
	private String appEnviornment;
	private String projectName;
	private String testLogs;
	private String executionEnv;
	private String Browser;
	private String Url;
	public static final String EXCEPTIONCAUGHT = "Exception caught";

	/**
	 * Instantiates a new Common settings.
	 *
	 * @param projectName    the project name
	 * @param appType        the app type
	 * @param appEnvironment the app environment
	 */
	public CommonSettings(String projectName, String appType, String appEnvironment, String testLogs) {
		super();
		this.projectName = projectName;
		this.appType = appType;
		this.appEnviornment = appEnvironment;
		this.testLogs = testLogs;
	}

	/**
	 * Instantiates a new Common settings.
	 */
	public CommonSettings() {
		super();
	}

	/**
	 * Gets project name.
	 *
	 * @return project name
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Sets project name.
	 *
	 * @param projectName the project name
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Gets app type.
	 *
	 * @return app type
	 */
	public String getAppType() {
		return appType;
	}

	/**
	 * Sets app type.
	 *
	 * @param appType the app type
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}

	/**
	 * Gets app enviornment.
	 *
	 * @return app enviornment
	 */
	public String getAppEnviornment() {
		return appEnviornment;
	}

	/**
	 * Sets app enviornment.
	 *
	 * @param appEnviornment the app enviornment
	 */
	public void setAppEnviornment(String appEnviornment) {
		this.appEnviornment = appEnviornment;
	}

	/**
	 * Gets test logs.
	 *
	 * @return test logs
	 */
	public String getTestLogs() {
		return testLogs;
	}

	/**
	 * Sets test logs.
	 *
	 * @param testLogs the test logs
	 */
	public void setTestLogs(String testLogs) {
		this.testLogs = testLogs;
	}

	/**
	 * Gets execution env.
	 *
	 * @return the execution env
	 */
	public String getExecutionEnv() {
		return executionEnv;
	}

	/**
	 * Sets execution env.
	 *
	 * @param executionEnv the execution env
	 */
	public void setExecutionEnv(String executionEnv) {
		this.executionEnv = executionEnv;
	}

	/**
	 * Gets browser.
	 *
	 * @return the browser
	 */
	public String getBrowser() {
		return Browser;
	}

	/**
	 * Sets browser.
	 *
	 * @param browser the browser
	 */
	public void setBrowser(String browser) {
		Browser = browser;
	}

	/**
	 * Gets url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return Url;
	}

	/**
	 * Sets url.
	 *
	 * @param url the url
	 */
	public void setUrl(String url) {
		Url = url;
	}

	/**
	 * <H1>Get common settings</H1>
	 *
	 * @return common settings
	 */
	public static CommonSettings getCommonSettings() {

		//
		CommonSettings commonSettings = new CommonSettings();
		try {

			// Set Fixed Common Settings

			// 1. Set Project name
			String projectName = ConfigReader.getValue("projectName");
			commonSettings.setProjectName(projectName);

			// 2. Set Application environment type
			String environment = ConfigReader.getValue("environment");
			commonSettings.setAppEnviornment(environment);

			// 3. Set URL
			String url = ConfigReader.getValue(environment);
			commonSettings.setUrl(url);

			// 3. Set Execution OS
			String executionOS = ConfigReader.getValue("executionOS");
			commonSettings.setExecutionEnv(executionOS);

			// 4. Set Browser
			String browser = ConfigReader.getValue("browser");
			commonSettings.setBrowser(browser);
			System.out.println("Browser is " + browser);

		} // End try
		catch (Exception e) {
			LogUtil.errorLog(ExcelDataUtil.class, EXCEPTIONCAUGHT, e);
		}
		return commonSettings;

	}

}
