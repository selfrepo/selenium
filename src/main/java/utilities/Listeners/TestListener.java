package utilities.Listeners;

import java.io.IOException;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

import baseTest.BaseTest;
import utilities.CommonUtil;
import utilities.HTMLReportUtil;
import utilities.ExtentReports.ExtentManager;
import utilities.ExtentReports.ExtentTestManager;

public class TestListener extends BaseTest implements ITestListener {

    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        iTestContext.setAttribute("WebDriver", driver);
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        ExtentTestManager.endTest();
        ExtentManager.getInstance().flush();
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }

	@Override
	public void onTestFailure(ITestResult iTestResult) {
		String screenshot = CommonUtil.takeScreenShot();
	
		// ExtentReports log and screenshot operations for failed tests.
		try {
			ExtentTestManager.getTest().log(Status.FAIL, HTMLReportUtil.failStringRedColor(CommonUtil.lastAction), MediaEntityBuilder.createScreenCaptureFromPath("data:image/jpg;base64," + screenshot).build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ExtentTestManager.getTest().fail(iTestResult.getThrowable());
        ExtentTestManager.getTest().log(Status.FAIL, HTMLReportUtil.failStringRedColor("Test Failed"));
		
	}

	@Override
	public void onTestSuccess(ITestResult iTestResult) {
        ExtentTestManager.getTest().log(Status.PASS, HTMLReportUtil.passStringGreenColor("Test Passed")) ;
		
	}

	@Override
	public void onTestSkipped(ITestResult iTestResult) {
		System.out.println("Test Skipped " + getTestMethodName(iTestResult) + " ....");
		ExtentTestManager.getTest().log(Status.SKIP, "Test Skipped");
		
	}

}
