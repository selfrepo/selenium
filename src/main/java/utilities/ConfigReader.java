
package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * This ConfigReader file will read the config file
 */

public class ConfigReader {


	/**
	 * will read the properties file with this function
	 *
	 * @param filePath
	 * @return
	 */
	private ConfigReader() {

	}

	/**
	 * Load property file properties.
	 *
	 * @param filePath the file path
	 * @return the properties
	 */
	public static Properties loadPropertyFile(String filePath, String key) {
		// Read from properties file
		File file = new File(System.getProperty("user.dir")+filePath);
		Properties prop = new Properties();

		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
			prop.load(fileInput);
			System.out.println("prop "+key+"  :"+prop.getProperty(key));
		} catch (Exception e) {
			LogUtil.errorLog(ConfigReader.class, "Caught the exception for prop:"+key+" with exception"+e, e);
		}
		return prop;

	}

	/**
	 * will get sting value from properties file
	 *
	 * @param key the key
	 * @return value
	 */
	public static String getValue(String key) {
		
		String osref = System.getProperty("os.name").toLowerCase();
		System.out.println("OS:"+osref);
		
		Properties prop;
		String filepath = "/src/main/resources/ConfigFiles/config.properties";
		
		System.out.println("filepath:"+filepath);	
		
		prop = loadPropertyFile(filepath, key);					
				
		return prop.getProperty(key);
	}

}
