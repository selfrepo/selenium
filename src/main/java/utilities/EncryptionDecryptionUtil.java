package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class EncryptionDecryptionUtil {
	private static Workbook workbook = null;
	private static Sheet sheet = null;
	private static final String testDatafilePath = ConfigReader.getValue("testDataExcelPath");
	private static final String userDatafilePath = ConfigReader.getValue("userLoginExcelPath");
	
	public static void encodePasswordExcel(String sheetName, String user) {
		FileInputStream fs = null;
		String password = null;
		int userRowIndex = 0;
		int passwordColumnIndex = 2;
		
		try {
			String filepath = System.getProperty("user.dir")+userDatafilePath;
			fs = new FileInputStream(filepath);
			workbook = new XSSFWorkbook(fs);
			sheet = workbook.getSheet(sheetName);
			int rowscnt = sheet.getPhysicalNumberOfRows();
			Row row = sheet.getRow(0);

			Cell cell;
			for(int i =1; i<rowscnt;i++){
				row = sheet.getRow(i);
				cell= row.getCell(0);
				String val = cell.getStringCellValue();
				if(val.equalsIgnoreCase(user))
					{
					cell.getStringCellValue();
					userRowIndex = i;
					LogUtil.infoLog(EncryptionDecryptionUtil.class, userRowIndex);					
					cell = row.getCell(passwordColumnIndex);
					password = cell.getStringCellValue();										
					break;
					}//close if match
				else
					continue;
			}//for loop close
  			fs.close();

		}catch(IOException IO) {
			IO.printStackTrace();
		}
		
		//encrypt the password
		Base64.Encoder encoder = Base64.getEncoder();
  		String eStr = encoder.encodeToString(password.getBytes());
		System.out.println(eStr);

		//write the encoded password to excel
		try {
			fs = new FileInputStream(System.getProperty("user.dir")+userDatafilePath);		
			XSSFWorkbook wb = new XSSFWorkbook(fs);
			//spreadsheet object
			Sheet sheet = wb.getSheet(sheetName);   
			//Get the first row from the sheet
			Row row = sheet.getRow(userRowIndex);
	        //change password in excel
			Cell cell = row.getCell(passwordColumnIndex); 
			cell.setCellValue(eStr);   
			fs.close();	

			FileOutputStream out = new FileOutputStream(System.getProperty("user.dir")
				+ userDatafilePath);
			wb.write(out);
			out.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

//	public static void encodePassword(String Password) {
//
//		FileInputStream fis = null;
//		try {
//		     fis = new FileInputStream(System.getProperty("user.dir")
//						+ "\\src\\main\\resources\\ConfigFiles\\key.properties");
//		}catch(FileNotFoundException e1){
//		     e1.printStackTrace();
//		}
//		
//		Properties prop = new Properties();
//		try {
//		    prop.load(fis);
//		}catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		Base64.Encoder encoder = Base64.getEncoder();
//		String S1 = prop.getProperty(Password);
//		String eStr = encoder.encodeToString(S1.getBytes());
//		System.out.println(eStr);
//		FileOutputStream out;
//		try {
//		    out = new FileOutputStream(System.getProperty("user.dir")
//					+ "\\src\\main\\resources\\ConfigFiles\\key.properties");
//		    prop.setProperty("password", eStr);
//		    prop.store(out, null);
//		    out.close();
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//
//   }
	
	public static String decodeEncoded(String eStr) {
        Base64.Decoder decoder = Base64.getDecoder();
        String dStr = new String(decoder.decode(eStr));
        return dStr;
    }	
	
   	
		

}

