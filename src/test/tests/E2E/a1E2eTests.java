package tests.E2E;

import org.testng.annotations.Test;
import java.io.IOException;
import java.util.HashMap;

import org.testng.annotations.Test;

import pages.LoginPage;
import pages.NSNextBillInvoicePage;
import testModules.CampaignPageModule;
import testModules.ContractLineItemsModule;
import testModules.EditQuoteModule;
import testModules.EditSOModule;
import testModules.EntitlementsModule;
import testModules.GmailModule;
import testModules.LeadsPageModule;
import testModules.LoginTestModule;
import testModules.MfTrailModule;
import testModules.NSNextBillInvoiceModule;
import testModules.NetSuiteLoginModule;
import testModules.NetSuiteSORelatedRecordsModule;
import testModules.NetSuiteSalesOrderModule;
import testModules.OpportunityTestModule;
import testModules.PGIAtLeadPageModule;
import testModules.PortfolioInterestsPageModule;
import testModules.PreOrderDetailsModule;
import testModules.ProductGroupInterestsPageModule;
import testModules.QuoteDetailsModule;
import testModules.QuoteModule;
import testModules.RenewalOpportunitiesModule;
import testModules.RevenueArrangementsModule;
import testModules.SFDCTestModule;
import testModules.SalesOrderModule;
import testModules.ServiceContractModule;
import utilities.CommonUtil;
import utilities.ConfigReader;
import utilities.ExcelDataUtil;
import utilities.GlobalUtil;
import utilities.ExtentReports.ExtentTestManager;

public class a1E2eTests {


	// Variables
	LoginTestModule loginTestModule;
	SFDCTestModule sfdcTestFlow;
	NetSuiteLoginModule netSuiteLoginModule;
	NetSuiteSalesOrderModule netSuiteSOModule;
	NetSuiteSORelatedRecordsModule netSuiteSORelatedRecordsModule;
	GmailModule gmailModule;
	EditQuoteModule editQuoteModule;
	QuoteDetailsModule quoteDetailsModule;
	OpportunityTestModule opportunityFlow;
	NSNextBillInvoicePage nsBill;
	MfTrailModule mfTrialModule;

	private static String userType, userType2;
	
	public static HashMap<String, String> SFDCDataMap = new HashMap<>();
	public static HashMap<String, String> NetsuiteDataMap = new HashMap<>();
	public static HashMap<String, String> NetSuiteLoginDataMap = new HashMap<>();
	private static String testDataFilePath = ConfigReader.getValue("testDataExcelPath");
	private static String userLoginFilePath = ConfigReader.getValue("userLoginExcelPath");
	LoginPage login;

	public void getTestCaseDetails(String dataSetTestCase) {
        String dataSheetName = "NetSuite_Tests_#".replace("#", ConfigReader.getValue("netSuiteEnvironment"));
        String loginDataSheet = "NetSuite";
    
        NetSuiteLoginDataMap = ExcelDataUtil.getTestDataWithTestCaseName(testDataFilePath,dataSheetName,dataSetTestCase);
        /* Netsuite Environment details added in NetSuiteLoginDataMap 
         * since using for test data fetch from Datasource sheet.
         * HashMap Name: NetSuiteLoginDataMap         
         * StoreKey : Testdataenv
         * Store Value :  ConfigReader.getValue("netSuiteEnvironment")
         */
        NetSuiteLoginDataMap.put("Testdataenv", ConfigReader.getValue("netSuiteEnvironment"));
        SFDCDataMap = ExcelDataUtil.getTestDataWithTestCaseName(testDataFilePath, "SFDC_Tests_#".replace("#", GlobalUtil.getCommonSettings().getAppEnviornment()), dataSetTestCase);
        /* SFDC Environment details added in SFDCDataMap 
         * since using for test data fetch from Datasource sheet.
         * HashMap Name: SFDCDataMap         
         * StoreKey : Testdataenv
         * Store Value :  ConfigReader.getValue("environment")
         */
        SFDCDataMap.put("Testdataenv", ConfigReader.getValue("environment"));

	}       
	
	@Test(priority = 0, testName = "gmail check", enabled=false)
    public void verifyGmailAndClickConfirmationLink() throws InterruptedException {
		
		ExtentTestManager.startTest("verifyGmailAndClickConfirmationLink");
        getTestCaseDetails("verifyGmailAndClickConfirmationLink");
        
        gmailModule = new GmailModule();        
        gmailModule.loginToGmail();
        gmailModule.clickEmailBySubjectAndConfirmEmailAddress("Welcome to Micro Focus Trial");


   }
	
	@Test(priority = 0, testName = "TC_02 : trial request", enabled=false)
    public void createAccountTrailForm() throws InterruptedException {
		
		ExtentTestManager.startTest("createAccountTrailForm");
        getTestCaseDetails("createAccountTrailForm");
       
         //Create Account on trial webform
          mfTrialModule = new MfTrailModule();        
          mfTrialModule.navigateToTrialForm();
          CommonUtil.setTestcaseName("createAccountTrailForm");
          CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
          mfTrialModule.trialRequest(SFDCDataMap);
          
          //Email verification and confirmation of new account created
          gmailModule = new GmailModule();        
          gmailModule.loginToGmail();
          gmailModule.clickEmailBySubjectAndConfirmEmailAddress("Welcome to Micro Focus Trial");

   }
	
	@Test(priority = 0, testName = "03.C2L_C2L.LMA.20_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void verifyLeadWithMarketingOperationRole() throws InterruptedException {
	
			ExtentTestManager.startTest("verifyLeadWithMarketingOperationRole");
			getTestCaseDetails("verifyLeadWithMarketingOperationRole");
			CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
			userType = SFDCDataMap.get("UserType");
			loginTestModule = new LoginTestModule();
			loginTestModule.navigateToLoginPage();
			loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType);
			LeadsPageModule editleads = new LeadsPageModule();
			CampaignPageModule createcampaignsPage =  new CampaignPageModule();
			
			CommonUtil.setTestcaseName("verifyLeadWithMarketingOperationRole");
			editleads.EditLeadsDetailsPage(SFDCDataMap);
			createcampaignsPage.createcampaigns(SFDCDataMap);
			createcampaignsPage.campaignsaddlead(SFDCDataMap);
		    createcampaignsPage.campaignschangeowner(SFDCDataMap);
		    loginTestModule.logout();
			
	}
	
	@Test(priority = 1, testName = "04.C2L_C2L.LMA.30 _E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void sfdcVerfiPGIAtLeadLevel() throws InterruptedException {
		ExtentTestManager.startTest("sfdcVerfiPGIAtLeadLevel");
		getTestCaseDetails("sfdcVerfiPGIAtLeadLevel");
		
		userType= SFDCDataMap.get("UserType");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		CommonUtil.setTestcaseName("verifyLeadWithMarketingOperationRole");
		SFDCDataMap.put("LeadID", CommonUtil.getStoredata("LeadID"));		
		// test case flow
		loginTestModule = new LoginTestModule();
		sfdcTestFlow = new SFDCTestModule(); 
		PGIAtLeadPageModule pgiAtLeadPageModule = new PGIAtLeadPageModule();
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType); 	
		
		sfdcTestFlow.searchLead(SFDCDataMap);  
		pgiAtLeadPageModule.verifyPGIScoreAtLead(SFDCDataMap);
		loginTestModule.logout();

	    }

	@Test(priority = 2, testName = "05.C2L_C2L.LMA.40 and 06.L2O_L2O.CRL.10_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void takeOwnerShip() throws InterruptedException {

			ExtentTestManager.startTest("takeOwnerShip");
			// Getting the test case details first
			getTestCaseDetails("takeOwnerShip");

			// test case flow
			loginTestModule = new LoginTestModule();
			sfdcTestFlow = new SFDCTestModule();
			userType = SFDCDataMap.get("UserType");
			CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
			CommonUtil.setTestcaseName("verifyLeadWithMarketingOperationRole");
			SFDCDataMap.put("LeadID", CommonUtil.getStoredata("LeadID"));
			SFDCDataMap.put("CreatedLead", CommonUtil.getStoredata("LeadName"));
			loginTestModule.navigateToLoginPage();
			loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);

			sfdcTestFlow.searchLead(SFDCDataMap); 

			CommonUtil.setTestcaseName("takeOwnerShip");
			LeadsPageModule leadsPageModule = new LeadsPageModule();
			leadsPageModule.leadPagePGIClick(SFDCDataMap);

			ProductGroupInterestsPageModule productGroupInterestsPageModule = new ProductGroupInterestsPageModule();
			productGroupInterestsPageModule.clickPGIInTable(SFDCDataMap);
			//productGroupInterestsPageModule.changeOwnerPGI(SFDCDataMap);

			PortfolioInterestsPageModule portfolioInterestsPageModule = new PortfolioInterestsPageModule();
			portfolioInterestsPageModule.clickPIInTable(SFDCDataMap);
			portfolioInterestsPageModule.createOpportunityPI(SFDCDataMap);
			
			loginTestModule.logout();
		}

	//We have to send the account number to change it to Verified from MDM
	//For the opportunity created in the previous test, please check the ownership is Sales_Rep,else change using deal_desk 
	//Also update opportunity stage from 0 to 1
	@Test(priority = 0, testName = "20.L2O_L2O.CMO.20_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void verifyOpportunityDetails() throws InterruptedException {
		ExtentTestManager.startTest("verifyOpportunityDetails");
		getTestCaseDetails("verifyOpportunityDetails");
		
		userType= SFDCDataMap.get("UserType");		
		sfdcTestFlow = new SFDCTestModule(); 
		loginTestModule = new LoginTestModule();
		opportunityFlow = new OpportunityTestModule();
        CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
			
		loginTestModule.navigateToLoginPage();
		//login as LDR
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		CommonUtil.setTestcaseName("takeOwnerShip");
		String opportunityName = CommonUtil.getStoredata("OpportunityName");
		SFDCDataMap.put("OpportunityName", opportunityName);
		
		// Search Opportunity
		sfdcTestFlow.globalSearchOpportunity(SFDCDataMap);		
		opportunityFlow.verifyFieldsOnOpportunity(SFDCDataMap);
		opportunityFlow.navigateToAccountPage(SFDCDataMap);
		opportunityFlow.verifyAccountStatus();
		
		loginTestModule.logout();

	}
	
	//Pre-requisite: find an opportunity(or create new under a valid contact) in stage1 status and preliminary products are empty.
	@Test(priority = 1, testName = "21.L2O_L2O.CMO.30_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void verifyOpportunityAddProducts() throws InterruptedException {
		ExtentTestManager.startTest("verifyOpportunityAddProducts");
		getTestCaseDetails("verifyOpportunityAddProducts");

		userType = SFDCDataMap.get("UserType");
		sfdcTestFlow = new SFDCTestModule();
		loginTestModule = new LoginTestModule();
		opportunityFlow = new OpportunityTestModule();
		
        CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
        CommonUtil.setTestcaseName("takeOwnerShip");
        String opportunityName = CommonUtil.getStoredata("OpportunityName");
		SFDCDataMap.put("OpportunityName", opportunityName);

		loginTestModule.navigateToLoginPage();
		//login as salesrep
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		
		CommonUtil.setTestcaseName("verifyOpportunityAddProducts");
		// Search Opportunity
		sfdcTestFlow.globalSearchOpportunity(SFDCDataMap);
		opportunityFlow.editAndAddPrilimanaryProducts(SFDCDataMap);
		opportunityFlow.verifyOpportunityAmountisUpdatedBasedonPrilimanaryProductsAdded(SFDCDataMap);
		opportunityFlow.changeOpportunityStatusToValidate();
		loginTestModule.logout();
	}



	@Test(priority = 2, testName = "22.00_O2Q_O2Q.PAQ.10 _E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void sfdcCreateNewQuoteFor1A() throws InterruptedException {
		ExtentTestManager.startTest("sfdcCreateNewQuoteFor1A");
		getTestCaseDetails("sfdcCreateNewQuoteFor1A");

		userType = SFDCDataMap.get("UserType");

		// test case flow
		loginTestModule = new LoginTestModule();
		sfdcTestFlow = new SFDCTestModule();
		editQuoteModule = new EditQuoteModule();
		quoteDetailsModule = new QuoteDetailsModule();
        CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
        CommonUtil.setTestcaseName("takeOwnerShip");
        String opportunityName = CommonUtil.getStoredata("OpportunityName");
		SFDCDataMap.put("OpportunityName", opportunityName);

		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);

		// Search Opportunity
		sfdcTestFlow.globalSearchOpportunity(SFDCDataMap);

		// CreateNewQuote
		sfdcTestFlow.createNewQuote(SFDCDataMap);
        CommonUtil.setTestcaseName("sfdcCreateNewQuoteFor1A");
		quoteDetailsModule.openAndGetQuoteNumberAndVerifyPricebook(SFDCDataMap);

		// Validating the edit quote fields
		editQuoteModule.verifyContactFieldsOnEditQuote(SFDCDataMap);
		loginTestModule.logout();

	}

	@Test(priority = 0, testName = "2201QuoteProductConfriguration")
	public void QuoteProductConfriguration_Step1() throws InterruptedException, IOException {
		ExtentTestManager.startTest("QuoteProductConfriguration_Step1");
		getTestCaseDetails("QuoteProductConfriguration_Step1");		
		// 1st User -> Sales_rep
		userType = SFDCDataMap.get("UserType");


		loginTestModule = new LoginTestModule();
		sfdcTestFlow = new SFDCTestModule(); 
		quoteDetailsModule = new QuoteDetailsModule();
		
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");	
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));	
		String quoteNumber = CommonUtil.getStoredata("QuoteNumber");	
		loginTestModule.navigateToLoginPage();	
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType);	
		//search for the Quote No in the global search	
		sfdcTestFlow.globalSearchQuote(quoteNumber);	
		sfdcTestFlow.configureAndAddProducts_22_01(SFDCDataMap);	
		sfdcTestFlow.QuoteLineEditor_22_01(SFDCDataMap);	
		sfdcTestFlow.navigateToOpportunityPageFromQuotePage(SFDCDataMap);	
		sfdcTestFlow.editPsFieldInOpportunityPage(SFDCDataMap);//professional service field edit	
		sfdcTestFlow.markOpportunityStage("Stage 3: Develop");	
		sfdcTestFlow.goToQuotePageFromOpportunityPage(quoteNumber);
		sfdcTestFlow.enterContactDetails(SFDCDataMap);		
		sfdcTestFlow.advancedConfigurationForProductsAtQuote();
		//submit the Quote for approval
		sfdcTestFlow.submitQuoteForApproval();		
		
		//logout as sales rep
		loginTestModule.logout();	


   }



	@Test(priority = 2, testName = "22.01QuoteProductConfriguration")
	public void QuoteProductConfriguration_Step2() throws InterruptedException, IOException {
		ExtentTestManager.startTest("QuoteProductConfriguration_Step2");
		getTestCaseDetails("QuoteProductConfriguration_Step2");		
	
		// 2nd User -> Deal_desk
		userType2 = SFDCDataMap.get("UserType2");

		loginTestModule = new LoginTestModule();
		sfdcTestFlow = new SFDCTestModule(); 
		quoteDetailsModule = new QuoteDetailsModule();
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		String quoteNumber = CommonUtil.getStoredata("QuoteNumber");

		loginTestModule.navigateToLoginPage();
		//login as Deal_Desk
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType2);		
		//search for the SO in global search
		sfdcTestFlow.globalSearchQuote(quoteNumber);
		sfdcTestFlow.verifyQuoteStatusAndSubstatus("Pending Approval");
		sfdcTestFlow.completeApprovalsForQuoteAndVerifyStatus(SFDCDataMap);
		//logout as deal desk
		loginTestModule.logout();	
	}

	@Test(priority = 3, testName = "22.01QuoteProductConfriguration")
	public void QuoteProductConfriguration_Step3() throws InterruptedException, IOException {
		ExtentTestManager.startTest("QuoteProductConfriguration_Step3");
		getTestCaseDetails("QuoteProductConfriguration_Step3");		
		// 1st User -> Sales_rep
		userType = SFDCDataMap.get("UserType");
		
		loginTestModule = new LoginTestModule();
		sfdcTestFlow = new SFDCTestModule(); 
		quoteDetailsModule = new QuoteDetailsModule();
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		String quoteNumber = CommonUtil.getStoredata("QuoteNumber");

		loginTestModule.navigateToLoginPage();	
		//login as sales rep
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType);
		//search for the Quote No in the global search
		sfdcTestFlow.globalSearchQuote(quoteNumber);
		sfdcTestFlow.verifyQuoteStatusAndSubstatus("Approved");
		sfdcTestFlow.navigateToOpportunityPageFromQuotePage(SFDCDataMap);
		sfdcTestFlow.markOpportunityStage("Stage 4: Propose");
		sfdcTestFlow.goToQuotePageFromOpportunityPage(quoteNumber);	
		//pre-requisite:testdata sheet should be updated with Legal document name 
		quoteDetailsModule.generateLegalDocument(SFDCDataMap, quoteNumber);
		sfdcTestFlow.navigateToOpportunityPageFromQuotePage(SFDCDataMap);
		sfdcTestFlow.markOpportunityStage("Stage 5: Negotiate");	
		sfdcTestFlow.goToQuotePageFromOpportunityPage(quoteNumber);
		sfdcTestFlow.sendForNegotiation(SFDCDataMap, quoteNumber);
		sfdcTestFlow.globalSearchQuote(quoteNumber);
		sfdcTestFlow.verifyQuoteStatusAndSubstatus("Quote Issued");		
    	sfdcTestFlow.orderDocument(SFDCDataMap, quoteNumber);   
    	//logutas sales rep
    	loginTestModule.logout();
	}
	
	//After processing above quote with OSGT team, a preorder will be created which will have the SO number	
	//Add the SO number in the further test in the test data sheet
	    @Test(priority = 0, testName = "22.02_Q2C_Q2C.Q2O.10 _E2E01(a)_New Sale from a Lead_Web_Product on Form")
	    public void searchAndReviewSOFor1A() throws InterruptedException {
	        ExtentTestManager.startTest("searchAndReviewSOFor1A");
	        getTestCaseDetails("searchAndReviewSOFor1A");


	       userType = NetSuiteLoginDataMap.get("UserType");
	       
	       CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
	       CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
	       NetSuiteLoginDataMap.put("SO Number", CommonUtil.getStoredata("SONumber"));
	       CommonUtil.setTestcaseName("takeOwnerShip");
	       NetSuiteLoginDataMap.put("BillToAddress", CommonUtil.getStoredata("PrimaryAddress"));
	       CommonUtil.setTestcaseName("verifyOpportunityAddProducts");
	       NetSuiteLoginDataMap.put("Amount", CommonUtil.getStoredata("Amount"));
	       CommonUtil.setTestcaseName("takeOwnerShip");
	       NetSuiteLoginDataMap.put("Customer", CommonUtil.getStoredata("CustomerName"));
	       CommonUtil.setTestcaseName("searchAndReviewSOFor1A");

	       // test case flow
	       netSuiteLoginModule = new NetSuiteLoginModule();

	       netSuiteLoginModule.navigateToLoginPage();
	       netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
	       netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
	       netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
	        
	       SalesOrderModule salesOrderModule = new SalesOrderModule();
	       salesOrderModule.reviewFewDetailsForSO(NetSuiteLoginDataMap);
	        
	       EditSOModule editSOModule = new EditSOModule();
	       editSOModule.editSOAndApprovingFromOMSpecialist(NetSuiteLoginDataMap);
	       netSuiteLoginModule.signOut();
	    }
	
	
	@Test(priority = 1, testName = "22.03_Q2C_Q2C.Q2O.10 _E2E01(a)_New Sale from a Lead_Web_Product on Form")
    public void netsuiteSOApprovalThroughOrderMgntMgrRole() throws InterruptedException {
        ExtentTestManager.startTest("netsuiteSOApprovalThroughOrderMgntMgrRole");
        getTestCaseDetails("netsuiteSOApprovalThroughOrderMgntMgrRole");
        
        userType= NetSuiteLoginDataMap.get("UserType");
        CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
        CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
	    NetSuiteLoginDataMap.put("SO Number", CommonUtil.getStoredata("SONumber"));
	    
        netSuiteLoginModule = new NetSuiteLoginModule();
        netSuiteSOModule = new NetSuiteSalesOrderModule();
        netSuiteSORelatedRecordsModule = new NetSuiteSORelatedRecordsModule();
        
        netSuiteLoginModule.navigateToLoginPage();
        netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
        netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
        netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
        netSuiteSOModule.approveSOAsOrderManagementManagerAndVerifyStatus();
        netSuiteLoginModule.signOut();
	}
	
	
	//Prerequisite : Three integration should happen which are OSGT, Export Compliance and SLD so that quote status should change
	@Test(priority = 2, testName = "23.00_O2Q_O2Q.OIB.10_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void sfdcPreOrderValidation() throws InterruptedException {
		ExtentTestManager.startTest("sfdcPreOrderValidation");	
		getTestCaseDetails("sfdcPreOrderValidation");
		userType = SFDCDataMap.get("UserType");
		
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");
		SFDCDataMap.put("QuoteNumber", CommonUtil.getStoredata("QuoteNumber"));
		CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
		SFDCDataMap.put("NetSuiteSONumber", CommonUtil.getStoredata("NetSuiteSONumber"));
		
		loginTestModule = new LoginTestModule();
    	sfdcTestFlow = new SFDCTestModule(); 
    	
    	PreOrderDetailsModule preOrderDetailsModule = new PreOrderDetailsModule();
    	ServiceContractModule serviceContractModule = new ServiceContractModule();
    	ContractLineItemsModule contractLineItemsModule = new ContractLineItemsModule();
    	EntitlementsModule entitlementsModule = new EntitlementsModule();
    	
    	loginTestModule.navigateToLoginPage();
    	loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType);
    	sfdcTestFlow.searchQuote(SFDCDataMap);
	CommonUtil.setTestcaseName("sfdcPreOrderValidation");
    	preOrderDetailsModule.clickPreOrderAndValidate(SFDCDataMap);	
    	preOrderDetailsModule.statusValidationInPreOrderPage(SFDCDataMap);
    	
    	serviceContractModule.clickRelatedListServiceContract(SFDCDataMap);
    	serviceContractModule.validateAndClickOnContractName(SFDCDataMap);
    	
    	contractLineItemsModule.clickContractLineItems(SFDCDataMap);
    	contractLineItemsModule.validateLineItemsNoProductNoAndParentProducts(SFDCDataMap);
    	contractLineItemsModule.clickLineItemsNumber(SFDCDataMap);
    	contractLineItemsModule.navigateBackToServiceContractPage();
    	
    	entitlementsModule.clickRelatedListEntitlements(SFDCDataMap);
    	entitlementsModule.validateEntitlementsAndAssets(SFDCDataMap);
    	entitlementsModule.clickEntitlementInTable(SFDCDataMap);
    	entitlementsModule.navigateBackToEntitleMentsPage();
    	entitlementsModule.clickAssetInTable(SFDCDataMap);
	}

	@Test(priority = 3, testName = "24.00_O2Q_O2Q.PAQ.30_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void verifyQuotePreOrderStatuses() throws InterruptedException {

		ExtentTestManager.startTest("verifyQuotePreOrderStatuses");
		getTestCaseDetails("verifyQuotePreOrderStatuses");
		userType = SFDCDataMap.get("UserType");
		
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");
		SFDCDataMap.put("QuoteNumber", CommonUtil.getStoredata("QuoteNumber"));
		loginTestModule = new LoginTestModule();
		QuoteModule quoteModule = new QuoteModule();
		gmailModule = new GmailModule();
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		quoteModule.verifyquoteinvoicestatus(SFDCDataMap);
		quoteModule.verifyquoteinvoiceopportunitystatus(SFDCDataMap);
		quoteModule.verifyquoteinvoiceorderstatus(SFDCDataMap);
		quoteModule.verifyquoteinvoiceordersldstatus(SFDCDataMap);
		gmailModule.loginToGmail();
		gmailModule.verifyEmailfromemailid(SFDCDataMap);
		loginTestModule.navigateToLoginPage();
		loginTestModule.logout();
	}
	
	 @Test(priority = 4, testName = "24.01_O2Q_O2Q.PAQ.30_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	    public void netsuiteAccountantRoleVerifyFields() throws InterruptedException {
	        ExtentTestManager.startTest("netsuiteAccountantRoleVerifyFields");
	        getTestCaseDetails("netsuiteAccountantRoleVerifyFields");
	        CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
	        userType= NetSuiteLoginDataMap.get("UserType");  
	        CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
	        NetSuiteLoginDataMap.put("SO Number", CommonUtil.getStoredata("SONumber"));
	        netSuiteLoginModule = new NetSuiteLoginModule();
	        netSuiteSOModule = new NetSuiteSalesOrderModule();
	        netSuiteSORelatedRecordsModule = new NetSuiteSORelatedRecordsModule();
	        
	        netSuiteLoginModule.navigateToLoginPage();
	        netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
	        netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
	        netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
	        
	        netSuiteSOModule.verifySalesOrderStatus(NetSuiteLoginDataMap.get("Status"));
	        netSuiteSOModule.verifyAssetRecordPopulated();
	        netSuiteSOModule.navigateToRelatedRecords();        
	        netSuiteSORelatedRecordsModule.verifyItemFulfillmentRecord();      
	        netSuiteLoginModule.signOut();
	   }   
		

	@Test(priority = 0, testName = "25.01_Q2C Invoice_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void verifyNSInvoiceRemediationAnalystRole() throws InterruptedException {

		ExtentTestManager.startTest("verifyNSInvoiceRemediationAnalystRole");
		getTestCaseDetails("verifyNSInvoiceRemediationAnalystRole");

		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		userType = NetSuiteLoginDataMap.get("UserType");
		CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
        NetSuiteLoginDataMap.put("SO Number", CommonUtil.getStoredata("SONumber"));
		netSuiteLoginModule = new NetSuiteLoginModule();
		NSNextBillInvoiceModule nsBill = new NSNextBillInvoiceModule();	
		netSuiteLoginModule.navigateToLoginPage();
		netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
		netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
		nsBill.invoiceDetails(NetSuiteLoginDataMap);
      netSuiteLoginModule.signOut();
	}

	
	//Wait for 30 minutes to status displayed as Invoiced in SFDC
    @Test(priority = 0, testName = "25.01_Q2C Invoice_E2E01(a)_New Sale from a Lead_Web_Product on FormV0.1")
	public void verifytest2invoiceconfirmation() throws InterruptedException {

		ExtentTestManager.startTest("verifytest2invoiceconfirmation");
		getTestCaseDetails("verifytest2invoiceconfirmation");
		userType = SFDCDataMap.get("UserType");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");
		SFDCDataMap.put("QuoteNumber", CommonUtil.getStoredata("QuoteNumber"));
		loginTestModule = new LoginTestModule();
		QuoteModule quoteModule = new QuoteModule();
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		quoteModule.verifyquoteinvoicestatus(SFDCDataMap);
		quoteModule.verifyquoteinvoiceopportunitystatus(SFDCDataMap);
		quoteModule.verifyquoteinvoiceorderstatus(SFDCDataMap);
	loginTestModule.logout();
	}

	
	@Test(priority = 0, testName = "26.O2Q_O2Q.PAQ.90 _E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void sfdcInvoiceRenewalOppValidation() throws InterruptedException {
		ExtentTestManager.startTest("sfdcInvoiceRenewalOppValidation");
		getTestCaseDetails("sfdcInvoiceRenewalOppValidation");
		userType = SFDCDataMap.get("UserType");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		CommonUtil.setTestcaseName("openAndGetQuoteNumberAndVerifyPricebook");
		SFDCDataMap.put("QuoteNumber", CommonUtil.getStoredata("QuoteNumber"));
		
		CommonUtil.setTestcaseName("sfdcPreOrderValidation");
		SFDCDataMap.put("OrderNumber", CommonUtil.getStoredata("OrderNumber"));
		
		CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
		SFDCDataMap.put("NetSuiteSONumber", CommonUtil.getStoredata("NetSuiteSONumber"));
		CommonUtil.setTestcaseName("takeOwnerShip");
		SFDCDataMap.put("ContractRowItemName","ServiceContract - "+CommonUtil.getStoredata("OpportunityName"));
		loginTestModule = new LoginTestModule();
    	sfdcTestFlow = new SFDCTestModule(); 
    	QuoteDetailsModule quoteDetailsModule = new QuoteDetailsModule();
    	PreOrderDetailsModule preOrderDetailsModule = new PreOrderDetailsModule();
    	ServiceContractModule serviceContractModule = new ServiceContractModule();
    	RenewalOpportunitiesModule renewalOpportunitiesModule = new RenewalOpportunitiesModule();
    	
    	loginTestModule.navigateToLoginPage();
    	loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType);
    	sfdcTestFlow.globalSearchQuote(SFDCDataMap.get("QuoteNumber"));
    	quoteDetailsModule.statusValidationInQuoteDetailsPage(SFDCDataMap);
    	preOrderDetailsModule.clickPreOrderAndValidate(SFDCDataMap);	
    	preOrderDetailsModule.statusValidationForInvoice(SFDCDataMap);
    	
    	serviceContractModule.clickRelatedListServiceContract(SFDCDataMap);
    	serviceContractModule.validateAndClickOnContractName(SFDCDataMap);
    	serviceContractModule.clickRenewalOpportunities(SFDCDataMap);
    	renewalOpportunitiesModule.validationRenewalOpportunties(SFDCDataMap);
  	loginTestModule.logout();	
    }
	

	@Test(priority = 0, testName = "27.Rev_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	 public void netsuiteVerifySORevenueArrangementFields() throws InterruptedException {
	   ExtentTestManager.startTest("netsuiteVerifySORevenueArrangementFields");
      getTestCaseDetails("netsuiteVerifySORevenueArrangementFields");    
      userType= NetSuiteLoginDataMap.get("UserType");   
      CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
      CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
      NetSuiteLoginDataMap.put("SO Number", CommonUtil.getStoredata("SONumber"));
      netSuiteLoginModule = new NetSuiteLoginModule();
      netSuiteSOModule = new NetSuiteSalesOrderModule();
      
      netSuiteLoginModule.navigateToLoginPage();
      netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
      netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
      netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
      
      netSuiteSOModule.verifyAssetRecordPopulated();
      netSuiteSOModule.navigateToRelatedRecords();
      String recordType = NetSuiteLoginDataMap.get("RecordType");
      String recordStatus = NetSuiteLoginDataMap.get("RecordStatus");
      netSuiteSORelatedRecordsModule = new NetSuiteSORelatedRecordsModule();
      netSuiteSORelatedRecordsModule.verifyRevenueArrangementRecord(recordType, recordStatus);      
      netSuiteSORelatedRecordsModule.addMemoAndFileToRevenueArrangement(NetSuiteLoginDataMap);
      netSuiteLoginModule.signOut();   
	   }
	
	  @Test(priority = 0, testName = "28.O2Q_O2Q.NCD.30_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	    public void NSSOVerifyRevenueDetails() throws InterruptedException {
		ExtentTestManager.startTest("NSSOVerifyRevenueDetails");
		getTestCaseDetails("NSSOVerifyRevenueDetails");		
			
		userType= NetSuiteLoginDataMap.get("UserType");	
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
        CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
        NetSuiteLoginDataMap.put("SO Number", CommonUtil.getStoredata("SONumber"));
		netSuiteLoginModule = new NetSuiteLoginModule(); 
			
		netSuiteLoginModule.navigateToLoginPage();
		netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
				
		netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);	
		netSuiteLoginModule.verifyRevenueArrangementDetails(NetSuiteLoginDataMap);
		netSuiteLoginModule.verifyRevenueDetails(NetSuiteLoginDataMap);
		netSuiteLoginModule.verifyAllocationDetails(NetSuiteLoginDataMap);				
		netSuiteLoginModule.verifyarrangementMessage(NetSuiteLoginDataMap);
				
	 }



	
	@Test(priority = 1, testName = "29.R2R_R2R.REV.40_E2E01(a)_New Sale from a Lead_Web_Product on Form")
	public void verifyRevenueRecognitionPlanAndPlannedPeriod() throws InterruptedException {
		ExtentTestManager.startTest("verifyRevenueRecognitionPlanAndPlannedPeriod");
		getTestCaseDetails("verifyRevenueRecognitionPlanAndPlannedPeriod");

		userType = NetSuiteLoginDataMap.get("UserType");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
        CommonUtil.setTestcaseName("QuoteProductConfriguration_Step3");
        NetSuiteLoginDataMap.put("SO Number", CommonUtil.getStoredata("SONumber"));
		// test case flow
		netSuiteLoginModule = new NetSuiteLoginModule();

		netSuiteLoginModule.navigateToLoginPage();
		netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
		netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
		
		SalesOrderModule salesOrderModule = new SalesOrderModule();
	    salesOrderModule.clickOnRelatedRecordsTab(NetSuiteLoginDataMap);
	    salesOrderModule.clickOnRevenueArrangementDate(NetSuiteLoginDataMap);
		
	    RevenueArrangementsModule revenueArrangementsModule = new RevenueArrangementsModule();
	    revenueArrangementsModule.clickViewRevenuePlans(NetSuiteLoginDataMap);
	    revenueArrangementsModule.verifyRevenuePlansType(NetSuiteLoginDataMap);
	    revenueArrangementsModule.clickOnForecastPlansTypeRevenueNumber(NetSuiteLoginDataMap);
	    revenueArrangementsModule.verifyPlannedPeriod();
	    revenueArrangementsModule.clickOnBackBtnOnUpdateRevenuePlansPage();
	    revenueArrangementsModule.clickOnActualPlansTypeRevenueNumber(NetSuiteLoginDataMap);
	    revenueArrangementsModule.verifyPlannedPeriod();
	    revenueArrangementsModule.clickOnBackBtnOnUpdateRevenuePlansPage();
	}
	

}

