package tests.E2E;

import org.testng.annotations.Test;
import java.util.HashMap;

import org.testng.annotations.Test;

import baseTest.BaseTest;
import pages.LoginPage;
import pages.NSNextBillInvoicePage;
import testModules.*;
import utilities.CommonUtil;
import utilities.ConfigReader;
import utilities.ExcelDataUtil;
import utilities.GlobalUtil;
import utilities.ExtentReports.ExtentTestManager;

public class c1E2eTests extends BaseTest{

	// Variables
	LoginTestModule loginTestModule;
	SFDCTestModule sfdcTestFlow;
	AccountsPageModule createAccount;
	ContactsPageModule contact;
	NetSuiteLoginModule netSuiteLoginModule;
	NetSuiteSalesOrderModule netSuiteSOModule;
	NetSuiteSORelatedRecordsModule netSuiteSORelatedRecordsModule;
	GmailModule gmailModule;
	EditQuoteModule editQuoteModule;
	QuoteDetailsModule quoteDetailsModule;
	OpportunityTestModule opportunityFlow;
	NSNextBillInvoicePage invoiceNS;
	MfTrailModule mfTrialModule;
	SalesOrderModule soModule;
	NetSuiteSalesModule netSuiteSalesModule;
	PendingSalesOrderPageModule pendingSalesOrderPageModule;
	private static String userType, userType2;

	public static HashMap<String, String> SFDCDataMap = new HashMap<>();
	public static HashMap<String, String> NetsuiteDataMap = new HashMap<>();
	public static HashMap<String, String> NetSuiteLoginDataMap = new HashMap<>();
	private static String testDataFilePath = ConfigReader.getValue("testDataExcelPath");
	private static String userLoginFilePath = ConfigReader.getValue("userLoginExcelPath");
	LoginPage login;

	public void getTestCaseDetails(String dataSetTestCase) {
		System.out.println("inside getTestDetails method");
		String dataSheetName = "NetSuite_Tests_#".replace("#", ConfigReader.getValue("netSuiteEnvironment"));
		String loginDataSheet = "NetSuite";

		NetSuiteLoginDataMap = ExcelDataUtil.getTestDataWithTestCaseName(testDataFilePath, dataSheetName,
				dataSetTestCase);
		/*
		 * Netsuite Environment details added in NetSuiteLoginDataMap since using for
		 * test data fetch from Datasource sheet. HashMap Name: NetSuiteLoginDataMap
		 * StoreKey : Testdataenv Store Value :
		 * ConfigReader.getValue("netSuiteEnvironment")
		 */
		NetSuiteLoginDataMap.put("Testdataenv", ConfigReader.getValue("netSuiteEnvironment"));
		SFDCDataMap = ExcelDataUtil.getTestDataWithTestCaseName(testDataFilePath,
				"SFDC_Tests_#".replace("#", GlobalUtil.getCommonSettings().getAppEnviornment()), dataSetTestCase);
		/*
		 * SFDC Environment details added in SFDCDataMap since using for test data fetch
		 * from Datasource sheet. HashMap Name: SFDCDataMap StoreKey : Testdataenv Store
		 * Value : ConfigReader.getValue("environment")
		 */
		SFDCDataMap.put("Testdataenv", ConfigReader.getValue("environment"));

	}

	@Test(priority = 0, testName = "01.L2O_L2O.MCA.10_E2E01(c)_New Sale from a New Account_Planning through to Comp_Transition")
	public void createUnverifiedAccount() throws InterruptedException {

		ExtentTestManager.startTest("createUnverifiedAccount");
		getTestCaseDetails("createUnverifiedAccount");

		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		userType = SFDCDataMap.get("UserType");

		loginTestModule = new LoginTestModule();
		createAccount = new AccountsPageModule();

		// login as Account Operations
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);

		CommonUtil.setTestcaseName("createUnverifiedAccount");
		createAccount.navigateToAccountsPage();
		createAccount.createAccount(SFDCDataMap);
		loginTestModule.logout();

	}
/*
	@Test(priority = 0, testName = "03.L2O_L2O.MCA.10_01(c )_V.02_Direct New Sale_New Sale from a New Account_Planning through to Comp_Transition")
	public void verifiedCaseAccountsStatus() throws InterruptedException {

		ExtentTestManager.startTest("verifiedCaseAccountStatus");
		getTestCaseDetails("verifiedCaseAccountStatus");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		userType = SFDCDataMap.get("UserType");

		loginTestModule = new LoginTestModule();

		// login as SalesRep
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		CaseAccountsPageModule caseaccounts = new CaseAccountsPageModule();

		// Validating the account MDM status
		caseaccounts.caseaccountverification(SFDCDataMap);

		// Modifying the account details regarding case state
		caseaccounts.caseaccountstatusmodify(SFDCDataMap);
	}

	@Test(priority = 0, testName = "06.L2O_L2O.MCA.40_01(c )_V.02_Direct New Sale_New Sale from a New Account_Planning through to Comp_Transition_ Sales Structure _ Tier 1A for america _ US")
	public void createContactAndOpportunityAddPreliminaryProducts() throws InterruptedException {

		ExtentTestManager.startTest("createContactAndOpportunityAddPreliminaryProducts");
		getTestCaseDetails("createContactAndOpportunityAddPreliminaryProducts");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));

		userType = SFDCDataMap.get("UserType");
		loginTestModule = new LoginTestModule();
		createAccount = new AccountsPageModule();
		contact = new ContactsPageModule();
		opportunityFlow = new OpportunityTestModule();

		// login as sales rep
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);

		CommonUtil.setTestcaseName("createUnverifiedAccount");
		SFDCDataMap.put("AccountName", CommonUtil.getStoredata("Account Name"));

		createAccount.navigateToAccountDetailsPageAndCreateNewContact(SFDCDataMap);
		String contactName = CommonUtil.getStoredata("Contact Name");

		createAccount.navigateToContactDetailsPage(contactName);
		contact.createOpportunityFromContactDetailsPage(SFDCDataMap);
		opportunityFlow.editAndAddPrilimanaryProducts(SFDCDataMap);
		opportunityFlow.verifyOpportunityAmountisUpdatedBasedonPrilimanaryProductsAdded(SFDCDataMap);
		opportunityFlow.changeOpportunityStatusToValidate();
		loginTestModule.logout();

	}

	@Test(priority = 1, testName = "08.Q2C_Q2C.Q2O.10_01(c )_V.02_Direct New Sale_New Sale from a New Account_Planning through to Comp_Transition_ Sales Structure _ Tier 1A for america _ US")
	public void verifyAssetsAndApproveSoFromOrderMgmtRole() throws InterruptedException {
		ExtentTestManager.startTest("verifyAssetsAndApproveSoFromOrderMgmtRole");
		getTestCaseDetails("verifyAssetsAndApproveSoFromOrderMgmtRole");

		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		userType = NetSuiteLoginDataMap.get("UserType");

		netSuiteLoginModule = new NetSuiteLoginModule();
		netSuiteSOModule = new NetSuiteSalesOrderModule();
		netSuiteSORelatedRecordsModule = new NetSuiteSORelatedRecordsModule();
		pendingSalesOrderPageModule = new PendingSalesOrderPageModule();

		netSuiteLoginModule.navigateToLoginPage();
		netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);

		pendingSalesOrderPageModule.searchTransactionSalesOrder(NetSuiteLoginDataMap);
		pendingSalesOrderPageModule.viewPendingSalesOrder(NetSuiteLoginDataMap);

		// verifying few details of SO like terms, shipping address, bill to address
		// etc.
		SalesOrderModule salesOrderModule = new SalesOrderModule();
		salesOrderModule.verifyTermsForSO(NetSuiteLoginDataMap);
		salesOrderModule.reviewFewDetailsForSO(NetSuiteLoginDataMap);
		salesOrderModule.verifyshipToAddressForSO(NetSuiteLoginDataMap);

		// Approving SO from OM specialist
		EditSOModule editSOModule = new EditSOModule();
		editSOModule.editSOAndApprovingFromOMSpecialist(NetSuiteLoginDataMap);
		netSuiteLoginModule.signOut();
	}

	@Test(priority = 1, testName = "09.Q2C_Q2C.Q2O.10_01(c )_V.02_Direct New Sale_New Sale from a New Account_Planning through to Comp_Transition_ Sales Structure _ Tier 1A for america _ US_Copy_1")
	public void invoiceDistributionTeamRoleVerifyItemFullfillmentRecord() throws InterruptedException {
		ExtentTestManager.startTest("invoiceDistributionTeamRoleVerifyItemFullfillmentRecord");
		getTestCaseDetails("invoiceDistributionTeamRoleVerifyItemFullfillmentRecord");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		userType = NetSuiteLoginDataMap.get("UserType");
		netSuiteLoginModule = new NetSuiteLoginModule();
		netSuiteSOModule = new NetSuiteSalesOrderModule();
		netSuiteSalesModule = new NetSuiteSalesModule();
		soModule = new SalesOrderModule();
		netSuiteSORelatedRecordsModule = new NetSuiteSORelatedRecordsModule();
		pendingSalesOrderPageModule = new PendingSalesOrderPageModule();

		netSuiteLoginModule.navigateToLoginPage();
		netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
		// Clicking on the Customer Sales Order List
		netSuiteSalesModule.clickOnCustomerSalesOrdersListOption();

		// search the SO and view it
		soModule.searchSoOnSalesOrderPage(NetSuiteLoginDataMap);
		pendingSalesOrderPageModule.viewPendingSalesOrder(NetSuiteLoginDataMap);

		// navigating to related records and verify item fulfillment record
		netSuiteSOModule.navigateToRelatedRecords();
		netSuiteSORelatedRecordsModule.verifyItemFulfillmentRecordWithInvoiceDistributionRole();
		netSuiteLoginModule.signOut();
	}

	@Test(priority = 0, testName = "10.Q2C_Q2C.Q2O.10_01(c )_V.02_Direct New Sale_New Sale from a New Account_Planning through to Comp_Transition")
	public void verifyNSInvoiceRemediationAnalystRole() throws InterruptedException {

		ExtentTestManager.startTest("verifyNSInvoiceRemediationAnalystRole");
		getTestCaseDetails("verifyNSInvoiceRemediationAnalystRole");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));

		userType = NetSuiteLoginDataMap.get("UserType");
		netSuiteLoginModule = new NetSuiteLoginModule();
		invoiceNS = new NSNextBillInvoicePage();

		// login as MFInvoiceRemediationAnalystRole
		netSuiteLoginModule.navigateToLoginPage();
		netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);
		// search for SO
		netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
		// validation of the invoice details
		invoiceNS.invoiceDetails(NetSuiteLoginDataMap);

	}

	@Test(priority = 0, testName = "11.Q2C_Q2C.Q2O.10_01(c )_V.02_Direct New Sale_New Sale from a New Account_Planning through to Comp_Transition")
	public void verifyQuoteStatusInvoiceValidation() throws InterruptedException {

		ExtentTestManager.startTest("verifyQuoteStatusInvoiceValidation");
		getTestCaseDetails("verifyQuoteStatusInvoiceValidation");
		userType = SFDCDataMap.get("UserType");
		CommonUtil.setenvironment(SFDCDataMap.get("Testdataenv"));
		loginTestModule = new LoginTestModule();
		QuoteModule quoteModule = new QuoteModule();
		// login as sales rep
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		// Validation of quote status and substatus
		quoteModule.verifyquoteinvoicestatus(SFDCDataMap);
		// Validation of quoteOpportunity status
		quoteModule.verifyquoteinvoiceopportunitystatus(SFDCDataMap);
		// Validation of quote PreOrder and NS status
		quoteModule.verifyquoteinvoiceorderstatus(SFDCDataMap);
		loginTestModule.logout();
	}
*/
}
