package tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import utilities.ConfigReader;
import utilities.ExcelDataUtil;
import utilities.GlobalUtil;
import utilities.ExtentReports.ExtentTestManager;
import testModules.EditQuoteModule;
import testModules.LoginTestModule;
import testModules.QuoteDetailsModule;
import testModules.SFDCTestModule;

public class SFDCTest{

	// Variables
	LoginTestModule loginTestModule;
	SFDCTestModule sfdcTestFlow;
	EditQuoteModule editQuoteModule;
	QuoteDetailsModule quoteDetailsModule;
	private static String userType;
	public static HashMap<String, String> SFDCDataMap = new HashMap<>();
	private static String testDataFilePath = ConfigReader.getValue("testDataExcelPath");
	private static String userLoginFilePath = ConfigReader.getValue("userLoginExcelPath");

	public void getTestCaseDetails(String dataSetTestCase) {
		SFDCDataMap = ExcelDataUtil.getTestDataWithTestCaseName(testDataFilePath, "SFDC_Tests_#".replace("#", GlobalUtil.getCommonSettings().getAppEnviornment()), dataSetTestCase);
		userType = SFDCDataMap.get("UserType");
	}

	@Test(priority = 0, testName = "SFDCTest")
	public void sfdcTestFlowWithSalesRepRole() throws InterruptedException {
		ExtentTestManager.startTest("sfdcTestFlowWithSalesRepRole");
		// Getting the test case details first
		getTestCaseDetails("sfdcTestFlowWithSalesRepRole");

		// test case flow
    	loginTestModule = new LoginTestModule();
    	sfdcTestFlow = new SFDCTestModule(); 
    	editQuoteModule = new EditQuoteModule();
    	quoteDetailsModule = new QuoteDetailsModule();
    	
    	loginTestModule.navigateToLoginPage();
    	loginTestModule.verifyValidLoginCredentials(userLoginFilePath,userType);
    	
    	sfdcTestFlow.searchQuote(SFDCDataMap);
    	editQuoteModule.editQuote(SFDCDataMap);
    	sfdcTestFlow.configureAndAddProducts(SFDCDataMap);
    	sfdcTestFlow.editingTheQuoteProductInformation(SFDCDataMap);
    	quoteDetailsModule.generateLegalDocument(SFDCDataMap,"");
    	sfdcTestFlow.sendForNegotiation(SFDCDataMap,"");
    	sfdcTestFlow.orderDocument(SFDCDataMap,"");
    	loginTestModule.logout();
	}

}