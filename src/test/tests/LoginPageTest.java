package tests;

import java.util.HashMap;
import org.testng.annotations.Test;

import baseTest.BaseTest;
import pages.LoginPage;
import testModules.LoginTestModule;
import utilities.ConfigReader;
import utilities.EncryptionDecryptionUtil;
import utilities.ExcelDataUtil;
import utilities.GlobalUtil;
import utilities.ExtentReports.ExtentTestManager;

public class LoginPageTest{

	// Variables
	LoginPage login;
	LoginTestModule loginTestModule;
	private static String userType;
	public static HashMap<String, String> dataMap = new HashMap<String, String>();
	private static String testDataFilePath = ConfigReader.getValue("testDataExcelPath");
	private static String userLoginFilePath = ConfigReader.getValue("userLoginExcelPath");

	public void getTestCaseDetails(String dataSetTestCase) {
		String dataSheetName = "SFDC_Tests_#".replace("#", GlobalUtil.getCommonSettings().getAppEnviornment());
		String loginDataSheet = "SFDC_#".replace("#", GlobalUtil.getCommonSettings().getAppEnviornment());

		dataMap = ExcelDataUtil.getTestDataWithTestCaseName(testDataFilePath, dataSheetName, dataSetTestCase);
		userType = dataMap.get("UserType");
		// Uncomment only when you want to encrypt your password for the first time
		// EncryptionDecryptionUtil.encodePasswordExcel(loginDataSheet, userType);
	}

	@Test(priority = 1, enabled = true)
	public void verifyValidLoginCredentials() throws InterruptedException {
		ExtentTestManager.startTest("verifyValidLoginCredentials");
		// Getting the test case details first
		getTestCaseDetails("verifyValidLoginCredentials");

		loginTestModule = new LoginTestModule();
		loginTestModule.navigateToLoginPage();
		loginTestModule.verifyValidLoginCredentials(userLoginFilePath, userType);
		loginTestModule.logout();
	}
}