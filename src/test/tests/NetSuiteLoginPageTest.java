package tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import testModules.NetSuiteLoginModule;
import utilities.ConfigReader;
import utilities.EncryptionDecryptionUtil;
import utilities.ExcelDataUtil;
import utilities.ExtentReports.ExtentTestManager;

public class NetSuiteLoginPageTest{
	
	
	//Variables
    public static HashMap<String, String> NetSuiteLoginDataMap = new HashMap<>();
    NetSuiteLoginModule netSuiteLoginModule;
    private static String userType;
    private static String testDataFilePath = ConfigReader.getValue("testDataExcelPath");
	private static String userLoginFilePath = ConfigReader.getValue("userLoginExcelPath");
    
    public void getTestCaseDetails(String dataSetTestCase)
    {	String dataSheetName = "NetSuite_Tests_#".replace("#", ConfigReader.getValue("netSuiteEnvironment"));
    	String loginDataSheet = "NetSuite";
    
    	NetSuiteLoginDataMap = ExcelDataUtil.getTestDataWithTestCaseName(testDataFilePath,dataSheetName,dataSetTestCase);
    	userType= NetSuiteLoginDataMap.get("UserType");
    	//EncryptionDecryptionUtil.encodePasswordExcel(loginDataSheet, userType);
    }
	
	
	@Test(priority=1, enabled=true)
	public void verifySearchSOEditCommunications() throws Exception {
		
		ExtentTestManager.startTest("verifySearchSOEditCommunications");
		getTestCaseDetails("verifySearchSOEditCommunications");
		
		netSuiteLoginModule = new NetSuiteLoginModule();    
    	netSuiteLoginModule.navigateToLoginPage();
    	netSuiteLoginModule.verifyValidLoginCredentials(userLoginFilePath,userType);	
    	netSuiteLoginModule.verifyCurrectRoleAndChangeRole(NetSuiteLoginDataMap);	
    	netSuiteLoginModule.searchForSO(NetSuiteLoginDataMap);
    	netSuiteLoginModule.editSalesOrder();
    	netSuiteLoginModule.editCommunicationsTab(NetSuiteLoginDataMap);		
    	netSuiteLoginModule.signOut();

	}
}