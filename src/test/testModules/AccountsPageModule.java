package testModules;

import java.util.HashMap;

import pages.AccountsPage;

public class AccountsPageModule {
	
	AccountsPage accPage = new AccountsPage();
	
	public void navigateToAccountsPage() throws InterruptedException {
		accPage.navigateToAccountsPage();
	}
	
	public void createAccount(HashMap<String, String> dataMap) throws InterruptedException {
		accPage.createAccount(dataMap);
	}
	
	public void searchAccountandNavigateToAccountDetailsPage(String name) throws InterruptedException  {
		accPage.searchAccountandNavigateToAccountDetailsPage(name);
	}
	
	public void navigateToAccountDetailsPageAndCreateNewContact(HashMap<String, String> dataMap) throws InterruptedException {
		accPage.navigateToAccountDetailsPageAndCreateNewContact(dataMap);
	}
	
	public void navigateToContactDetailsPage(String contactName) {
		accPage.navigateToContactDetailsPage(contactName);
	}
}
