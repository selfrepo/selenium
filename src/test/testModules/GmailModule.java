package testModules;

import java.util.HashMap;

import pages.GmailPage;

public class GmailModule {
	
	 public static HashMap<String, String> dataMap = new HashMap<>();
	    GmailPage gmail;
	    
	    
		public void loginToGmail() throws InterruptedException {
			gmail = new GmailPage();
			gmail.loginToGmail();
		}

		
		public void clickEmailBySubjectAndConfirmEmailAddress(String subject) {
			gmail.clickEmailWithSubject(subject);
		}

		
        public void verifyEmailfromemailid(HashMap<String, String> dataMap) {
            String subject = dataMap.get("FromMailID");
            gmail.verifyrecentemailid(subject);
        }


}
