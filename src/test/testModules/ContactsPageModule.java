package testModules;

import java.util.HashMap;

import pages.ContactsPage;

public class ContactsPageModule {
	
	ContactsPage contact = new ContactsPage();
	
	public void navigateToOpportunityQuickLink() throws InterruptedException {
		contact.navigateToOpportunityQuickLink();
	}
	
	public void createNewOpportunity(HashMap<String, String> dataMap) throws InterruptedException {
		contact.createNewOpportunity(dataMap);
	}
	
	public void createOpportunityFromContactDetailsPage(HashMap<String, String> dataMap) throws InterruptedException {
		navigateToOpportunityQuickLink();
		createNewOpportunity(dataMap);
		
	}
}
