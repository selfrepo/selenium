package testModules;

import java.util.HashMap;

import pages.NSNextBillInvoicePage;

public class NSNextBillInvoiceModule {

	NSNextBillInvoicePage nsPage = new NSNextBillInvoicePage();
	
	public void invoiceDetails(HashMap<String, String> dataMap) throws InterruptedException{
		nsPage.invoiceDetails(dataMap);
	}
	
}
