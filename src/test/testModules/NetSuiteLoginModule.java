package testModules;

import java.util.HashMap;

import pages.NSLoginPage;
import pages.NSSalesOrderDetailsPage;
import pages.VerifyRevenueDetailsPage;
import utilities.ExcelDataUtil;

public class NetSuiteLoginModule {
	
	
	//Variables
	NSLoginPage login;
	NSSalesOrderDetailsPage soPage;
	
	public static HashMap<String, String> dataMap = new HashMap<>();
		
				
		public void navigateToLoginPage() throws InterruptedException {
			login = new NSLoginPage();
			login.navigate();
		}
		public void verifyValidLoginCredentials(String filePath,String userType) throws InterruptedException {
			dataMap = ExcelDataUtil.getTestDataWithTestCaseName(filePath,"NetSuite",userType);
			login.signIn(dataMap);
		}	
		public void verifyCurrectRoleAndChangeRole(HashMap<String, String> dataMap) throws InterruptedException {
			login.verifyCurrectRoleAndChangeRole(dataMap);
		}	
		public void searchForSO(HashMap<String, String> dataMap) throws InterruptedException {
			login.searchForSO(dataMap);
		}	
		public void editSalesOrder() throws InterruptedException {
			soPage.editSalesOrder();
		}	
		public void editCommunicationsTab(HashMap<String, String> dataMap) throws InterruptedException {
			soPage.editCommunicationsTab(dataMap);
		}	
		public void signOut() throws InterruptedException {
			login.signOut();
		}	
		
		
		public void verifyRevenueDetails(HashMap<String, String> dataMap) throws InterruptedException {
			VerifyRevenueDetailsPage verifyRevenueDetailsPage =  new VerifyRevenueDetailsPage();
			verifyRevenueDetailsPage.verifyRevenueDetails(dataMap);
		}
				
		public void verifyAllocationDetails(HashMap<String, String> dataMap) throws InterruptedException {
			VerifyRevenueDetailsPage verifyRevenueDetailsPage =  new VerifyRevenueDetailsPage();
			verifyRevenueDetailsPage.verifyAllocationDetails(dataMap);
		}
				
		public void verifyRevenueArrangementDetails(HashMap<String, String> dataMap) throws InterruptedException {
			VerifyRevenueDetailsPage verifyRevenueDetailsPage =  new VerifyRevenueDetailsPage();
			verifyRevenueDetailsPage.verifyRevenueArrangementDetails(dataMap);
		}
				
		public void verifyarrangementMessage(HashMap<String, String> dataMap) throws InterruptedException {
			VerifyRevenueDetailsPage verifyRevenueDetailsPage =  new VerifyRevenueDetailsPage();
			verifyRevenueDetailsPage.verifyarrangementMessage(dataMap);
		}

	}
