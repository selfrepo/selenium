package testModules;

import java.util.HashMap;

import pages.QuoteDetailPage;

public class QuoteDetailsModule {

	QuoteDetailPage quoteDetailPage = new QuoteDetailPage();;
	
	public void generateLegalDocument(HashMap<String, String> dataMap, String quote) throws InterruptedException {
		quoteDetailPage.generateLegalDocument(dataMap, quote);
	}
	
	public void openAndGetQuoteNumberAndVerifyPricebook(HashMap<String, String> dataMap) throws InterruptedException {
		quoteDetailPage.openAndGetQuoteNumberAndVerifyPricebook(dataMap);
	}
	
	public void statusValidationInQuoteDetailsPage(HashMap<String, String> dataMap) throws InterruptedException {
		quoteDetailPage.statusValidationInQuoteDetailsPage(dataMap);
	}

}
