package testModules;

import java.util.HashMap;

import pages.PendingSalesOrderPage;

public class PendingSalesOrderPageModule {

	PendingSalesOrderPage pendingSOPage = new PendingSalesOrderPage();
	
	public void viewPendingSalesOrder(HashMap<String, String> dataMap) throws InterruptedException {
		pendingSOPage.viewPendingSalesOrder(dataMap);
	}
	
	public void searchTransactionSalesOrder(HashMap<String, String> dataMap) throws InterruptedException {
		pendingSOPage.searchTransactionSalesOrder(dataMap);
	}
}
