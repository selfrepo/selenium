package testModules;

import java.util.HashMap;

import pages.PreOrderDetailsPage;

public class PreOrderDetailsModule {
	PreOrderDetailsPage	preOrderDetailsPage =  new PreOrderDetailsPage();
	public void clickPreOrderAndValidate(HashMap<String, String> dataMap) throws InterruptedException {
		preOrderDetailsPage.clickPreOrderAndValidate(dataMap);
	}
	
	public void statusValidationInPreOrderPage(HashMap<String, String> dataMap) throws InterruptedException {
		preOrderDetailsPage.statusValidationInPreOrderPage(dataMap);
	}
	
	public void statusValidationForInvoice(HashMap<String, String> dataMap) throws InterruptedException {
		preOrderDetailsPage.statusValidationForInvoice(dataMap);
	}
}
