package testModules;

import java.util.HashMap;

import pages.ConfigureAndAddProductsPage;
import pages.CreateNewQuotePage;
import pages.EditQuotePage;
import pages.OpportunityPage;
import pages.OrderDocumentPage;
import pages.QuoteDetailPage;
import pages.QuoteInformationPage;
import pages.SearchQuotePage;
import pages.SendForNegotiationPage;
import utilities.CommonUtil;


public class SFDCTestModule{

	SearchQuotePage	searchQuotePage =  new SearchQuotePage();

	public void searchQuote(HashMap<String, String> dataMap) throws InterruptedException {
		SearchQuotePage	searchQuotePage =  new SearchQuotePage();
		CommonUtil.normalWait(30);
		searchQuotePage.searchQuote(dataMap);
		
	}

		
	public void verifyQuoteStatusAndSubstatus(String value) throws InterruptedException {
		EditQuotePage	editQuotePage =  new EditQuotePage();
		editQuotePage.verifyQuoteStatusAndSubstatus(value);
	}
	
	public void editQuoteStatus(String value) throws InterruptedException {
		EditQuotePage	editQuotePage =  new EditQuotePage();
		editQuotePage.editQuoteStatus(value);
	}

	public void globalSearchQuote(String quote) throws InterruptedException {
		SearchQuotePage	searchQuotePage =  new SearchQuotePage();
		CommonUtil.normalWait(30);
		searchQuotePage.globalSearchQuote(quote);
		
	}

	public void enterContactDetails(HashMap<String, String> dataMap) throws InterruptedException {
		EditQuotePage quotePage = new EditQuotePage();
		quotePage.enterContactDetails(dataMap);
	}
	

	public void editQuote(HashMap<String, String> dataMap) throws InterruptedException {
		EditQuotePage quotePage = new EditQuotePage();
		quotePage.editQuote(dataMap);
	}
	
	public void advancedConfigurationForProductsAtQuote() throws InterruptedException {
		EditQuotePage quotePage = new EditQuotePage();
		quotePage.advancedConfigurationForProductsAtQuote();
	}
	
	public void submitQuoteForApproval() throws InterruptedException {
		EditQuotePage quotePage = new EditQuotePage();
		quotePage.submitQuoteForApproval();
	}
	
	
	public void completeApprovalsForQuoteAndVerifyStatus(HashMap<String, String> dataMap) throws InterruptedException {
		EditQuotePage quotePage = new EditQuotePage();
		quotePage.completeApprovalsForQuoteAndVerifyStatus(dataMap);
	}


	public void configureAndAddProducts(HashMap<String, String> dataMap) throws InterruptedException {
		ConfigureAndAddProductsPage	configureAndAddProductsPage =  new ConfigureAndAddProductsPage();
		configureAndAddProductsPage.configureAndAddProducts(dataMap);
	}

	public void configureAndAddProducts_22_01(HashMap<String, String> dataMap) throws InterruptedException {
		ConfigureAndAddProductsPage	configureAndAddProductsPage =  new ConfigureAndAddProductsPage();
		configureAndAddProductsPage.configureAndAddProducts_22_01(dataMap);
	}

	public void QuoteLineEditor_22_01(HashMap<String, String> dataMap) throws InterruptedException {
		QuoteInformationPage quoteInformationPage = new QuoteInformationPage();
		quoteInformationPage.QuoteLineEditor_22_01(dataMap);
	}

	public void editingTheQuoteProductInformation(HashMap<String, String> dataMap) throws InterruptedException {
		QuoteInformationPage quoteInformationPage = new QuoteInformationPage();
		quoteInformationPage.editingTheQuoteProductInformation(dataMap);
	}


	public void generateLegalDocument(HashMap<String, String> dataMap, String quote) throws InterruptedException {
		QuoteDetailPage quoteDetailPage = new QuoteDetailPage();
		quoteDetailPage.generateLegalDocument(dataMap,quote);
	}


	public void sendForNegotiation(HashMap<String, String> dataMap, String quote) throws InterruptedException {
		SendForNegotiationPage sendForNegotiationPage = new SendForNegotiationPage();
		sendForNegotiationPage.sendForNegotiation(dataMap, quote);
	}

	public void orderDocument(HashMap<String, String> dataMap,String quote) throws InterruptedException {
		OrderDocumentPage orderDocumentPage = new OrderDocumentPage();
		orderDocumentPage.orderDocument(dataMap, quote);
	}
	

	public void navigateToOpportunityPageFromQuotePage(HashMap<String, String> dataMap) throws InterruptedException {
		OpportunityPage opportunityPage = new OpportunityPage();
		opportunityPage.navigateToOpportunityPageFromQuotePage();
	}
	
	public void editPsFieldInOpportunityPage(HashMap<String, String> dataMap) throws InterruptedException {
		OpportunityPage opportunityPage = new OpportunityPage();
		opportunityPage.editPsFieldInOpportunityPage(dataMap);
	}
	
	public void markOpportunityStage(String stage) throws InterruptedException {
		OpportunityPage opportunityPage = new OpportunityPage();
		opportunityPage.markOpportunityStage(stage);
	}
	
	public void goToQuotePageFromOpportunityPage(String quote) throws InterruptedException {
		OpportunityPage opportunityPage = new OpportunityPage();
		opportunityPage.goToQuotePageFromOpportunityPage(quote);
	}
	


	public void createNewQuote(HashMap<String, String> dataMap) throws InterruptedException {
		CreateNewQuotePage createNewQuotePage = new CreateNewQuotePage();
		createNewQuotePage.createNewQuote(dataMap);
	}
	
	public void searchOpportunity(HashMap<String, String> dataMap) throws InterruptedException {
		searchQuotePage.searchOpportunity(dataMap);
	}
	
	public void globalSearchOpportunity(HashMap<String, String> dataMap) throws InterruptedException {
		searchQuotePage.globalSearchOpportunity(dataMap);
	}
	
	public void searchLead(HashMap<String, String> dataMap) throws InterruptedException {
		searchQuotePage.searchLead(dataMap);
	}
	

}