package testModules;

import java.util.HashMap;

import pages.QuotePage;

public class QuoteModule {

	QuotePage quotePage = new QuotePage();
	
	public void verifyquoteinvoicestatus(HashMap<String, String> dataMap) throws InterruptedException{
		quotePage.verifyquoteinvoicestatus(dataMap);
	}
	
	public void verifyquoteinvoiceopportunitystatus(HashMap<String, String> dataMap) throws InterruptedException{
		quotePage.verifyquoteinvoiceopportunitystatus(dataMap);
	}
	
	public void verifyquoteinvoiceorderstatus(HashMap<String, String> dataMap) throws InterruptedException{
		quotePage.verifyquoteinvoiceorderstatus(dataMap);
	}
	
	public void verifyquoteinvoiceordersldstatus(HashMap<String, String> dataMap) throws InterruptedException{
		quotePage.verifyquoteinvoiceordersldstatus(dataMap);
	}
}
