package testModules;

import java.util.HashMap;
import pages.OpportunityPage;

public class OpportunityTestModule {
	OpportunityPage	opportunityPage =  new OpportunityPage();
	
	public void searchOpportunity(HashMap<String, String> dataMap) throws InterruptedException {		

		opportunityPage.navigateToOpportunities();
		opportunityPage.searchOpportunity(dataMap);
	}
		
	public void editAndAddPrilimanaryProducts(HashMap<String, String> dataMap) throws InterruptedException {
		opportunityPage.editAndAddPreliminaryPortfolioProducts(dataMap);
		
	}
	
	public void verifyOpportunityAmountisUpdatedBasedonPrilimanaryProductsAdded(HashMap<String, String> dataMap)throws InterruptedException {
		opportunityPage.verifyOpportunityAmountisUpdatedBasedonPrilimanaryProductsAdded(dataMap);
	}
	
	public void changeOpportunityStatusToValidate()throws InterruptedException {
		opportunityPage.changeOpportunityStatusToValidate();
	}
	
	public void changeOpportunityStatusToDiscover()throws InterruptedException {
		opportunityPage.changeOpportunityStatusToDiscover();
	}

	public void verifyFieldsOnOpportunity(HashMap<String, String> dataMap) {	
		opportunityPage.verifyFieldsOnOpportunity(dataMap);
	}

	public void navigateToAccountPage(HashMap<String, String> dataMap) {
		opportunityPage.navigateToAccountPage(dataMap);		
	}
	
	public void verifyAccountStatus() {
		opportunityPage.verifyAccountStatus();		
	}
		
}