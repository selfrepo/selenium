package testModules;

import java.util.HashMap;

import pages.EntitlementsPage;

public class EntitlementsModule {
	EntitlementsPage entitlementsPage = new EntitlementsPage();
	
	public void clickRelatedListEntitlements(HashMap<String, String> dataMap) throws InterruptedException {
		entitlementsPage.clickRelatedListEntitlements(dataMap);
	}
	
	public void validateEntitlementsAndAssets(HashMap<String, String> dataMap) throws InterruptedException {
		entitlementsPage.validateEntitlementsAndAssets(dataMap);
	}
	
	public void clickEntitlementInTable(HashMap<String, String> dataMap) throws InterruptedException {
		entitlementsPage.clickEntitlementInTable(dataMap);
	}
	
	public void navigateBackToEntitleMentsPage() throws InterruptedException {
		entitlementsPage.navigateBackToEntitleMentsPage();
	}
	
	public void clickAssetInTable(HashMap<String, String> dataMap) throws InterruptedException {
		entitlementsPage.clickAssetInTable(dataMap);
	}

}
