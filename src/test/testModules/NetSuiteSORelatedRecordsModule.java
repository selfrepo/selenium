package testModules;

import java.util.HashMap;
import java.util.Map;

import pages.NSSalesOrderRelatedRecordsPage;

public class NetSuiteSORelatedRecordsModule {
	NSSalesOrderRelatedRecordsPage soRelatedRecordsPage;
	public static Map<String, String> mapRatableSupportItems; 
	public static Map<String, HashMap<String, String>> mapRatableItems;
	
	
	public NetSuiteSORelatedRecordsModule() {
		soRelatedRecordsPage = new NSSalesOrderRelatedRecordsPage();
	}
	
	public void verifyItemFulfillmentRecord() {
		soRelatedRecordsPage.verifyItemFulfillmentRecord();
		soRelatedRecordsPage.verifyItemFulfillmentDates(NetSuiteSalesOrderModule.mapRatableSupportItems);
	}
	public void verifyItemFulfillmentRecord(String recordType, String recordStatus) {
		soRelatedRecordsPage.verifyRelatedRecordTypeAndStatus(recordType, recordStatus);
		soRelatedRecordsPage.verifyItemFulfillmentDates(NetSuiteSalesOrderModule.mapRatableSupportItems);
	}

	public void verifyRevenueArrangementRecord(String recordType, String recordStatus) {
		soRelatedRecordsPage.verifyRelatedRecordTypeAndStatus(recordType, recordStatus);
		soRelatedRecordsPage.verifyRevenueElementsDetails(NetSuiteSalesOrderModule.mapRatableSupportItems, NetSuiteSalesOrderModule.mapRatableItems);
	}
	
	public void addMemoAndFileToRevenueArrangement(HashMap<String, String> dataMap) {
		soRelatedRecordsPage.editAddMemoAndFileToRevenueArrangement(dataMap);
	}
	
	public void verifyItemFulfillmentRecordWithInvoiceDistributionRole() {
		soRelatedRecordsPage.verifyItemFulfillmentRecordWithoutClickingOnIt();;
	}
}
