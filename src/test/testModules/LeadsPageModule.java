package testModules;

import java.util.HashMap;

import pages.EditLeadsDetailsPage;
import pages.LeadsPage;

public class LeadsPageModule {
	
	public void leadPagePGIClick(HashMap<String, String> dataMap) throws InterruptedException {
		LeadsPage leadsPage = new LeadsPage();
		leadsPage.leadPagePGIClick(dataMap);
	}
	
	public void EditLeadsDetailsPage(HashMap<String, String> dataMap) throws InterruptedException {
		EditLeadsDetailsPage lsPage = new EditLeadsDetailsPage();
		lsPage.editleads(dataMap);
	}
	
	
	
}
