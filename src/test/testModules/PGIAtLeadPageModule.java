package testModules;

import java.util.HashMap;

import pages.PGIAtLeadPage;

public class PGIAtLeadPageModule {

	public void verifyPGIScoreAtLead(HashMap<String, String> dataMap) throws InterruptedException {
		PGIAtLeadPage verifyPGIScoreAtLeadPage =  new PGIAtLeadPage();
		verifyPGIScoreAtLeadPage.verifyPGIScoreAtLead(dataMap);
	}
}
