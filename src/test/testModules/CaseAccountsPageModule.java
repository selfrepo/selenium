package testModules;

import java.util.HashMap;

import pages.CaseAccountsPage;

public class CaseAccountsPageModule {
	
	CaseAccountsPage caseaccPage = new CaseAccountsPage();
	
	public void caseaccountverification(HashMap<String, String> dataMap) throws InterruptedException {
		caseaccPage.accountverification(dataMap);
	}

	
	public void caseaccountstatusmodify(HashMap<String, String> dataMap) throws InterruptedException {
		caseaccPage.modifycasestatus(dataMap);
	}

}
