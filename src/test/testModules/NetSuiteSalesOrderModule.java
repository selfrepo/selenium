//Added by Rohit
package testModules;

import java.util.HashMap;
import java.util.Map;
import pages.NSSalesOrderDetailsPage;

public class NetSuiteSalesOrderModule {
	NSSalesOrderDetailsPage soDetailsPage;
	
	public static Map<String, String> mapRatableSupportItems;
	public static Map<String, HashMap<String, String>> mapRatableItems;
	public NetSuiteSalesOrderModule() {
		soDetailsPage = new NSSalesOrderDetailsPage();
	}

	public void verifySalesOrderStatus(String expectedStatus) {
		soDetailsPage.verifySalesOrderStatus(expectedStatus);
	}
	
	public void verifyAssetRecordPopulated() {
		soDetailsPage.verifyAssetRecordPopulated();
		mapRatableSupportItems = soDetailsPage.getRatableSupportItemsMap();
		mapRatableItems = soDetailsPage.getRatableItemsHashMapWithKey();
	}
	
	public static Map<String, String> getRatableSupportItemsMap(){
		return mapRatableSupportItems;
	}
	public void navigateToRelatedRecords() {
		soDetailsPage.navigateToRelatedRecords();
	}
	
	public void approveSOAsOrderManagementManagerAndVerifyStatus() {
		soDetailsPage.approveSOAndVerifyStatus();
	
	}
	public static Map<String, HashMap<String, String>> getRatableItemsHashMapWithKey(){
		return mapRatableItems;
	}
}
