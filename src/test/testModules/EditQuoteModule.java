package testModules;

import java.util.HashMap;

import pages.EditQuotePage;

public class EditQuoteModule {
	EditQuotePage quotePage;
	
	public void editQuote(HashMap<String, String> dataMap) throws InterruptedException {
		quotePage = new EditQuotePage();
		quotePage.editQuote(dataMap);
	}
	
	public void verifyContactFieldsOnEditQuote(HashMap<String, String> dataMap) throws InterruptedException {
		quotePage = new EditQuotePage();
		quotePage.verifyContactFieldsOnEditQuote(dataMap);
	}

}
