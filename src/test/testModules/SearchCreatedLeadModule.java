package testModules;

import java.util.HashMap;

import pages.SearchLeadPage;

public class SearchCreatedLeadModule {

	public void searchCreatedLead(HashMap<String, String> dataMap) throws InterruptedException {
		SearchLeadPage searchLeadPage = new SearchLeadPage();
		searchLeadPage.searchCreatedLead(dataMap);
	}
}
