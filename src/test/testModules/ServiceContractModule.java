package testModules;

import java.util.HashMap;

import pages.ServiceContractPage;

public class ServiceContractModule {
	ServiceContractPage serviceContractPage = new ServiceContractPage();
	
	public void clickRelatedListServiceContract(HashMap<String, String> dataMap) throws InterruptedException {
		serviceContractPage.clickRelatedListServiceContract(dataMap);
	}
	
	public void validateAndClickOnContractName(HashMap<String, String> dataMap) throws InterruptedException {
		serviceContractPage.validateAndClickOnContractName(dataMap);
	}
	
	public void clickRenewalOpportunities(HashMap<String, String> dataMap) throws InterruptedException {
		serviceContractPage.clickRenewalOpportunities(dataMap);
	}

}
