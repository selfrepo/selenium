package testModules;

import java.util.HashMap;

import pages.CreateAccountTrialFormPage;

public class MfTrailModule {
	
	 public static HashMap<String, String> dataMap = new HashMap<>();
	 CreateAccountTrialFormPage trialForm;
	 
	    
	    
		public void navigateToTrialForm() throws InterruptedException {
			trialForm = new CreateAccountTrialFormPage();
			trialForm.navigateToTrialForm();
		}

		
		public void trialRequest(HashMap<String, String> dataMap) {
			trialForm.createAccountTrialForm(dataMap);
		}

}
