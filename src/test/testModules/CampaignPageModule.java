package testModules;

import java.util.HashMap;

import pages.CampaignsPage;

public class CampaignPageModule {
	
	CampaignsPage csPage = new CampaignsPage();
	
	public void createcampaigns(HashMap<String, String> dataMap) throws InterruptedException {
		csPage.createcampaigns(dataMap);
	}
	
	public void campaignsaddlead(HashMap<String, String> dataMap) throws InterruptedException {
		csPage.campaignsaddlead(dataMap);
	}
	
	public void campaignschangeowner(HashMap<String, String> dataMap) throws InterruptedException {
		csPage.campaignschangeowner(dataMap);
	}
	

}
