package testModules;

import java.util.HashMap;

import pages.PortfolioInterestsPage;


public class PortfolioInterestsPageModule {

	PortfolioInterestsPage portfolioInterestsPage;

	public void clickPIInTable(HashMap<String, String> dataMap) throws InterruptedException {
		portfolioInterestsPage = new PortfolioInterestsPage();
		portfolioInterestsPage.clickPIInTable(dataMap);
	}
	
	public void createOpportunityPI(HashMap<String, String> dataMap) throws InterruptedException {
		portfolioInterestsPage = new PortfolioInterestsPage();
		portfolioInterestsPage.createOpportunityPI(dataMap);
	}
}
