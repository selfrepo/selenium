package testModules;

import java.util.HashMap;


import pages.EditSOPage;

public class EditSOModule {
    EditSOPage soPage;
	
	public void editSOAndApprovingFromOMSpecialist(HashMap<String, String> dataMap) throws InterruptedException {
		soPage = new EditSOPage();
		soPage.editSOAndApprovingFromOMSpecialist(dataMap);
	}
}
