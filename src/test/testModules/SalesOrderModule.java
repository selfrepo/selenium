package testModules;

import java.util.HashMap;

import pages.SalesOrderPage;

public class SalesOrderModule {
	   SalesOrderPage soPage= new SalesOrderPage();;
		
		public void reviewFewDetailsForSO(HashMap<String, String> dataMap) throws InterruptedException {
			soPage.reviewFewDetailsForSO(dataMap);
		}
		
		public void clickOnRelatedRecordsTab(HashMap<String, String> dataMap) throws InterruptedException {
			soPage.clickOnRelatedRecordsTab(dataMap);
		}
		
		public void clickOnRevenueArrangementDate(HashMap<String, String> dataMap) throws InterruptedException {
			soPage.clickOnRevenueArrangementDate(dataMap);
		}
		
		public void verifyTermsForSO(HashMap<String, String> dataMap) throws InterruptedException {
			soPage.verifyTermsForSO(dataMap);
		}
		
		public void verifyshipToAddressForSO(HashMap<String, String> dataMap) throws InterruptedException {
			soPage.verifyshipToAddressForSO(dataMap);
		}
		
		public void searchSoOnSalesOrderPage(HashMap<String, String> dataMap) throws InterruptedException {
			soPage.searchSoOnSalesOrderPage(dataMap);
		}
}
