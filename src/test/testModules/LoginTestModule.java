package testModules;

import java.util.HashMap;
import pages.LoginPage;
import utilities.ExcelDataUtil;
import utilities.GlobalUtil;

public class LoginTestModule {
	
	 public static HashMap<String, String> dataMap = new HashMap<>();
	    LoginPage login;
	    
	    
		public void navigateToLoginPage() throws InterruptedException {
		    login = new LoginPage();
			login.navigate();
		}

		public void verifyValidLoginCredentials(String filePath,String userType) {
			dataMap = ExcelDataUtil.getTestDataWithTestCaseName(filePath,"SFDC_#".replace("#", GlobalUtil.getCommonSettings().getAppEnviornment()),userType);
			login.signIn(dataMap);
		}
		

		public void logout() {
			login.signOut();
		}


}
