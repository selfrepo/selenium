package testModules;

import java.util.HashMap;

import pages.RevenueArrangementsPage;

public class RevenueArrangementsModule {

	RevenueArrangementsPage revenueArrangementsPage = new RevenueArrangementsPage();
	
	public void clickViewRevenuePlans(HashMap<String, String> dataMap) throws InterruptedException {
		revenueArrangementsPage.clickViewRevenuePlans(dataMap);
	}
	
	public void verifyRevenuePlansType(HashMap<String, String> dataMap) throws InterruptedException {
		revenueArrangementsPage.verifyRevenuePlansType(dataMap);
	}
	
	public void clickOnForecastPlansTypeRevenueNumber(HashMap<String, String> dataMap) throws InterruptedException {
		revenueArrangementsPage.clickOnForecastPlansTypeRevenueNumber(dataMap);
	}
	
	public void clickOnActualPlansTypeRevenueNumber(HashMap<String, String> dataMap) throws InterruptedException {
		revenueArrangementsPage.clickOnActualPlansTypeRevenueNumber(dataMap);
	}
	
	public void clickOnBackBtnOnUpdateRevenuePlansPage() throws InterruptedException {
		revenueArrangementsPage.clickOnBackBtnOnUpdateRevenuePlansPage();
	}
	
	public void verifyPlannedPeriod() throws InterruptedException {
		revenueArrangementsPage.verifyPlannedPeriod();
	}
}
