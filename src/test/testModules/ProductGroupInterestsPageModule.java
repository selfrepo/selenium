package testModules;

import java.util.HashMap;

import pages.ProductGroupInterestsPage;

public class ProductGroupInterestsPageModule {
	
	ProductGroupInterestsPage productGroupInterestsPage;

	public void clickPGIInTable(HashMap<String, String> dataMap) throws InterruptedException {
		productGroupInterestsPage = new ProductGroupInterestsPage();
		productGroupInterestsPage.clickPGIInTable(dataMap);
	}
	
	public void changeOwnerPGI(HashMap<String, String> dataMap) throws InterruptedException {
		productGroupInterestsPage.changeOwnerPGI(dataMap);
	}
}
