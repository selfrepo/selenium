package testModules;

import java.util.HashMap;

import pages.NSCustomersPage;

public class NetSuiteSalesModule {
	
	
	//Variables
	NSCustomersPage cs = new NSCustomersPage();	
	
	
	public static HashMap<String, String> dataMap = new HashMap<>();		
				
		public void navigateToSalesPage() throws InterruptedException {
			cs.clickCustomerTab();
			cs.clickSalesTab();
		}

		public void navigateToSalesOrderListPage() throws InterruptedException {
			cs.clickSalesOrdersListOption();
		}
		
		public void clickOnCustomerSalesOrdersListOption() throws InterruptedException {
			cs.clickOnCustomerSalesOrdersListOption();
		}
	}
