package testModules;

import java.util.HashMap;

import pages.ContractLineItemsPage;

public class ContractLineItemsModule {

	ContractLineItemsPage contractLineItemsPage= new ContractLineItemsPage();
	
	public void clickContractLineItems(HashMap<String, String> dataMap) throws InterruptedException {
		contractLineItemsPage.clickRelatedListContractLineItems(dataMap);
	}
	
	public void validateLineItemsNoProductNoAndParentProducts(HashMap<String, String> dataMap) throws InterruptedException {
		contractLineItemsPage.validateLineItemsNoProductNoAndParentProducts(dataMap);
	}
	
	public void clickLineItemsNumber(HashMap<String, String> dataMap) throws InterruptedException {
		contractLineItemsPage.clickLineItemsNumber(dataMap);
	}
	
	public void navigateBackToServiceContractPage() throws InterruptedException{
		contractLineItemsPage.navigateBackToServiceContractPage();
	}
}
