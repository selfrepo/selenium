package testModules;

import java.util.HashMap;

import pages.RenewalOpportunitiesPage;


public class RenewalOpportunitiesModule {
	RenewalOpportunitiesPage renewalOpportunitiesPage = new RenewalOpportunitiesPage();
	
	public void validationRenewalOpportunties(HashMap<String, String> dataMap) throws InterruptedException {
		renewalOpportunitiesPage.validationRenewalOpportunties(dataMap);
	}
}
